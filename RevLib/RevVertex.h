/* 
 * File:   RevVertex.h
 * Author: Revers
 *
 * Created on 24 październik 2011, 16:54
 */

#ifndef REVVERTEX_H
#define	REVVERTEX_H


namespace rev {
    namespace graph {
        //Vertex, normal, tex
        //
        //SIZE : 4+4+4 +4+4+4 +4+4 = 4*8 = 32 bytes

        struct VertexPNT {
            float x, y, z;
            float nx, ny, nz;
            float s, t;

            VertexPNT(float x_, float y_, float z_,
                    float nx_, float ny_, float nz_,
                    float s_, float t_) :
            x(x_),
            y(y_),
            z(z_),
            nx(nx_),
            ny(ny_),
            nz(nz_),
            s(s_),
            t(t_) {
            }

            VertexPNT(float x_, float y_, float z_,
                    float nx_, float ny_, float nz_) :
            x(x_),
            y(y_),
            z(z_),
            nx(nx_),
            ny(ny_),
            nz(nz_),
            s(0.0f),
            t(0.0f) {
            }
        };

    } // end namespace graph
} // end namespace rev

#endif	/* REVVERTEX_H */

