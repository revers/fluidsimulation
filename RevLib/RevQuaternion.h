/*
 * File:   RevQuaternion.h
 * Author: Revers
 *
 * Created on 31 październik 2010, 21:30
 */

#ifndef REVQUATERNION_H
#define	REVQUATERNION_H

//#include <cassert>
#include <math.h>
#include "RevMatrix.h"

#define SQR(x) ((x)*(x))

namespace rev {
    namespace graph {

        template<typename T> struct TQuaternion {
        private:

            enum {
                kX = 0, kY = 1, kZ = 2, kW = 3
            };

        public:
            T x;
            T y;
            T z;
            T w;


            //pola
            //            T s;
            //            TVector3<T> v;

        public:
            //konstruktor

            TQuaternion(T s, TVector3<T> v)
            : x(v.x), y(v.y), z(v.z), w(s) {
            }

            TQuaternion(T x, T y, T z, T w)
            : x(x), y(y), z(z), w(w) {
            }

            TQuaternion() : x(0), y(0), z(0), w(1) {
            }

            explicit TQuaternion(const TMatrix44<T> &m);

            explicit TQuaternion(TMatrix33<T> m) {
                // Kwaternion z macierzy obrotu
                /*
                0 3 6  00 01 02 | 00 10 20
                1 4 7  10 11 12 | 01 11 21
                2 5 8  20 21 22 | 02 12 22
                 */

                T trace = 1 + m[0][0] + m[1][1] + m[2][2];
                if (trace >= 0) {
                    T s = sqrt(trace) / 2;
                    TVector3<T> v(m[2][1] - m[1][2], m[0][2] - m[2][0],
                            m[1][0] - m[0][1]);
                    v /= (4 * s);
                    w = s;
                    x = v.x;
                    y = v.y;
                    z = v.z;
                    return; // TQuaternion<T>(s, v);
                } else {
                    //wybor najwiekszego elementu
                    int i = 0; //m00
                    if (m[1][1] > m[0][0]) i = 1; //m11
                    if (m[2][2] > m[1][1]) i = 2; //m22

                    TVector3<T> v;
                    T s = 0, a;

                    switch (i) {
                        case 0:
                            a = sqrt((m[0][0]-(m[1][1] + m[2][2])) + 1) / 2;
                            v.x = a;
                            a = 1 / (4 * a);
                            v.y = (m[0][1] + m[1][0]) * a;
                            v.z = (m[2][0] + m[0][2]) * a;
                            s = (m[2][1] - m[1][2]) * a;
                            break;
                        case 1:
                            a = sqrt((m[1][1]-(m[2][2] + m[0][0])) + 1) / 2;
                            v.y = a;
                            a = 1 / (4 * a);
                            v.x = (m[0][1] + m[1][0]) * a;
                            v.z = (m[1][2] + m[2][1]) * a;
                            s = (m[0][2] - m[2][0]) * a;
                            break;
                        case 2:
                            a = sqrt((m[2][2]-(m[0][0] + m[1][1])) + 1) / 2;
                            v.z = a;
                            a = 1 / (4 * a);
                            v.x = (m[2][0] + m[0][2]) * a;
                            v.y = (m[1][2] + m[2][1]) * a;
                            s = (m[1][0] - m[0][1]) * a;
                            break;
                    }
                    w = s;
                    x = v.x;
                    y = v.y;
                    z = v.z;
                    //return TQuaternion<T>(s, v);
                }
            }

        public:
            //! Create quaternion from rotation angle and rotation axis.

            /** Axis must be unit length.
            The quaternion representing the rotation is
            q = cos(A/2)+sin(A/2)*(x*i+y*j+z*k).
            \param angle Rotation Angle in radians.
            \param axis Rotation axis. */
            static TQuaternion<T> createFromAngleAxis(T angle, const TVector3<T>& axis) {
                const T fHalfAngle = 0.5f * angle;
                const T fSin = sin(fHalfAngle);

                T x = fSin * axis.x;
                T y = fSin * axis.y;
                T z = fSin * axis.z;
                T w = cos(fHalfAngle);
                return TQuaternion<T>(x, y, z, w);
            }

            static TQuaternion<T> createFromAngleAxis(T angle, const T& x, const T& y,
                    const T & z) {
                const T fHalfAngle = 0.5f * angle;
                const T fSin = sin(fHalfAngle);

                T xq = fSin * x;
                T yq = fSin * y;
                T zq = fSin * z;
                T wq = cos(fHalfAngle);
                return TQuaternion<T>(xq, yq, zq, wq);
            }

            // To samo co powyzej
            //            static TQuaternion<T> createQuaternion(const T rotAngle, const TVector3<T> rotVect) {
            //                return TQuaternion<T>(cos(rotAngle / 2), rotVect * sin(rotAngle / 2));
            //            }

            static TQuaternion<T> createZeroQuaternion() {
                return TQuaternion<T>(0, TVector3<T > (0, 0, 0));
            }

            static TQuaternion<T> createOneQuaternion() {
                return TQuaternion<T>(1, TVector3<T > (0, 0, 0));
            }

            static TQuaternion<T> createIdentityQuaternion() {
                return TQuaternion<T>(0, 0, 0, 1);
            }

            static TQuaternion<T> createFromYawPitchRoll(float yaw, float pitch, float roll) {
                float num9 = roll * 0.5f;
                float num6 = (float) sin((double) num9);
                float num5 = (float) cos((double) num9);
                float num8 = pitch * 0.5f;
                float num4 = (float) sin((double) num8);
                float num3 = (float) cos((double) num8);
                float num7 = yaw * 0.5f;
                float num2 = (float) sin((double) num7);
                float num = (float) cos((double) num7);
                float X = ((num * num4) * num5) + ((num2 * num3) * num6);
                float Y = ((num2 * num3) * num5) - ((num * num4) * num6);
                float Z = ((num * num3) * num6) - ((num2 * num4) * num5);
                float W = ((num * num3) * num5) + ((num2 * num4) * num6);
                return TQuaternion<T>(X, Y, Z, W);
            }

            /**
             * WARNING: MODIFIES THE PARAMETERS!! (normalizes them).
             * @param src
             * @param dest
             * @return angle between two vectors
             */
            static TQuaternion<T> getAngleBetween(TVector3<T>& src, TVector3<T>& dest) {
                src.normalize();
                dest.normalize();

                float d = src.dotProduct(dest);

                if (d >= 1.0f) {
                    return TQuaternion<T>::createIdentityQuaternion();
                } else if (d < (1e-6f - 1.0f)) {
                    TVector3<T> axis = TVector3<T > (1, 0, 0).crossProduct(src);

                    if (axis.lengthSqr() == 0) {
                        axis = TVector3<T > (0, 1, 0).crossProduct(src);
                    }

                    axis.normalize();

                    return TQuaternion<T>::createFromAngleAxis(M_PI, axis);
                } else {
                    T s = (T) sqrt((1 + d) * 2);
                    T invS = 1 / s;

                    TVector3<T> c = src.crossProduct(dest);
                    TQuaternion<T> q(0.5 * s, invS * c);
                    q.normalize();

                    return q;
                }
            }

            /** q1 can be non-normalised quaternion */
            static TVector3<T> toEulers(TQuaternion<T>& q1) {
                double sqw = q1.w * q1.w;
                double sqx = q1.x * q1.x;
                double sqy = q1.y * q1.y;
                double sqz = q1.z * q1.z;
                double unit = sqx + sqy + sqz + sqw; // if normalised is one, otherwise is correction factor
                double test = q1.x * q1.y + q1.z * q1.w;

                T heading = 0;
                T attitude = 0;
                T bank = 0;
                if (test > 0.499 * unit) { // singularity at north pole
                    heading = 2 * (T) atan2(q1.x, q1.w);
                    attitude = (M_PI / 2.0);
                    bank = 0;
                    return TVector3<T > (heading, attitude, bank);
                }
                if (test < -0.499 * unit) { // singularity at south pole
                    heading = -2 * (T) atan2(q1.x, q1.w);
                    attitude = (-M_PI / 2.0);
                    bank = 0;
                    return TVector3<T > (heading, attitude, bank);
                }
                heading = (T) atan2(2 * q1.y * q1.w - 2 * q1.x * q1.z, sqx - sqy - sqz + sqw);
                attitude = (T) asin(2 * test / unit);
                bank = (T) atan2(2 * q1.x * q1.w - 2 * q1.y * q1.z, -sqx + sqy - sqz + sqw);
                return TVector3<T > (heading, attitude, bank);
            }

        public:

            T * dataPointer() {
                return &x;
            }

            //void toMatrix44(TMatrix44<T> &m);

            T & operator[](unsigned int i) {
                // assert(i < 4);
                return *(&x + i);
            }

            const T & operator[](unsigned int i) const {
                // assert(i < 4);
                return *(&x + i);
            }

            void set(T x, T y, T z, T w) {
                this->x = x;
                this->y = y;
                this->z = z;
                this->w = w;
            }

            void setAngleAndAxis(T angle, const TVector3<T>& axis) {
                const T fHalfAngle = 0.5f * angle;
                const T fSin = sin(fHalfAngle);

                x = fSin * axis.x;
                y = fSin * axis.y;
                z = fSin * axis.z;
                w = cos(fHalfAngle);
            }

            void setAngleAndAxis(T angle, T axisX, T axisY, T axisZ) {
                const T fHalfAngle = 0.5f * angle;
                const T fSin = sin(fHalfAngle);

                x = fSin * axisX;
                y = fSin * axisY;
                z = fSin * axisZ;
                w = cos(fHalfAngle);
            }

            void setAngle(T angle) {
                const T fHalfAngle = 0.5f * angle;
                const T fSin = sin(fHalfAngle);

                T rotAngle = getRotationAngle();

                if (rotAngle != 0) {
                    const T temp = sin(rotAngle / 2);
                    x /= temp;
                    y /= temp;
                    z /= temp;
                }

                x = fSin * x;
                y = fSin * y;
                z = fSin * z;
                w = cos(fHalfAngle);
            }

            std::string toString() const {
                std::ostringstream oss;
                oss << "[ x = " << x << ", y = " << y
                        << ", z = " << z
                        << ", w = " << w << " ]";
                return oss.str();
            }

            std::string toStringFull() const {
                std::ostringstream oss;
                oss << "Quaternion@" << this << "[ x = " << x
                        << ", y = " << y << ", z = " << z
                        << ", w = " << w << " ]";
                return oss.str();
            }

            //operatory relacji

            bool operator==(const TQuaternion<T> & q) const {
                return x == q.x && y == q.y && z == q.z && w == q.w;
            }

            bool operator!=(const TQuaternion<T> & q) const {
                return !(*this == q);
            }

            //operatory arytmetyczne

            TQuaternion<T> & operator+=(const TQuaternion<T> & q) {
                w += q.w;
                x += q.x;
                y += q.y;
                z += q.z;

                return *this;
            };

            TQuaternion<T> operator+(const TQuaternion<T> & q) const {
                return TQuaternion<T>(*this) += q;
            };

            TQuaternion<T> & operator-=(const TQuaternion<T> & q) {
                w -= q.w;
                x -= q.x;
                y -= q.y;
                z -= q.z;
                return *this;
            };

            TQuaternion<T> operator-(const TQuaternion<T> & q) const {
                return TQuaternion<T>(*this) -= q;
            };

            TQuaternion<T> & operator*=(const T s) {
                //jak mnozenie przez kwaternion z v=0
                w *= s;
                x *= s;
                y *= s;
                z *= s;
                return *this;
            };

            TQuaternion<T> operator*(const T s) const {
                return TQuaternion<T>(*this) *= s;
            };

            TQuaternion<T> & operator/=(const T s) {
                w /= s;
                x /= s;
                y /= s;
                z /= s;
                return *this;
            };

            TQuaternion<T> operator/(const T s) const {
                return TQuaternion<T>(*this) /= s;
            };

            TVector3<T> getVector() const {
                return TVector3<T > (x, y, z);
            }

            T getScalar() {
                return w;
            }

            TQuaternion<T> operator*(const TQuaternion<T> & other) const {
                TQuaternion<T> tmp;

                tmp.w = (other.w * w) - (other.x * x) - (other.y * y) - (other.z * z);
                tmp.x = (other.w * x) + (other.x * w) + (other.y * z) - (other.z * y);
                tmp.y = (other.w * y) + (other.y * w) + (other.z * x) - (other.x * z);
                tmp.z = (other.w * z) + (other.z * w) + (other.x * y) - (other.y * x);

                return tmp;
            }

            TQuaternion<T> & operator*=(const TQuaternion<T> & other) {
                return (*this = other * (*this));
            }

            TQuaternion<T> operator-() const {
                return TQuaternion<T>(-x, -y, -z, -w);
            }

            TQuaternion<T> & operator+() const {
                return *this;
            }

        public:

            T normSqr() const {
                return w * w + x * x + y * y + z*z;
            }

            T norm() const {
                return sqrt(normSqr());
            }

            void normalize() {
                T n = norm();
                if (n != 0)
                    * this /= n;
            }

            void conjugate() {
                x = -x;
                y = -y;
                z = -z;
            }

            void reverse() {
                conjugate();
                (*this) /= normSqr();
            }

            TQuaternion<T> createNormalized() const {
                TQuaternion<T> q(*this);
                q.normalize();
                return q;
            }

            TQuaternion<T> createConjugated() const {
                return TQuaternion<T>(-x, -y, -z, w);
            }

            TQuaternion<T> createReversed() const {
                return createConjugated() / normSqr();
            }

            TQuaternion<T> & operator/=(const TQuaternion<T> q) {
                return (*this) *= q.createReversed();
            };

            TQuaternion<T> operator/(const TQuaternion<T> q) const {
                return TQuaternion<T>(*this) /= q;
            };

            T getRotationAngle() const {
                //w=c=cos(theta/2)
                return 2 * acos(w);
            }

            TVector3<T> getRotationVector() const {
                T rotAngle = getRotationAngle();
                TVector3<T> v(x, y, z);
                if (rotAngle != 0)
                    return v / sin(rotAngle / 2);
                return v;
            }

            TMatrix33<T> toMatrix33() const {

                T n, s;
                T xs, ys, zs;
                T wx, wy, wz;
                T xx, xy, xz;
                T yy, yz, zz;


                n = (x * x) + (y * y) + (z * z) + (w * w);
                s = (n > 0.0f) ? (2.0f / n) : 0.0f;

                xs = x * s;
                ys = y * s;
                zs = z * s;
                wx = w * xs;
                wy = w * ys;
                wz = w * zs;
                xx = x * xs;
                xy = x * ys;
                xz = x * zs;
                yy = y * ys;
                yz = y * zs;
                zz = z * zs;

                TMatrix33<T> result; // = TMatrix33<T>::createIdentityMatrix();

                result[0][0] = 1.0f - (yy + zz);
                result[1][0] = xy - wz;
                result[2][0] = xz + wy;

                result[0][1] = xy + wz;
                result[1][1] = 1.0f - (xx + zz);
                result[2][1] = yz - wx;

                result[0][2] = xz - wy;
                result[1][2] = yz + wx;
                result[2][2] = 1.0f - (xx + yy);

                return result;
            }

            TMatrix44<T> toMatrix44() const {

                T n, s;
                T xs, ys, zs;
                T wx, wy, wz;
                T xx, xy, xz;
                T yy, yz, zz;


                n = (x * x) + (y * y) + (z * z) + (w * w);
                s = (n > 0.0f) ? (2.0f / n) : 0.0f;

                xs = x * s;
                ys = y * s;
                zs = z * s;
                wx = w * xs;
                wy = w * ys;
                wz = w * zs;
                xx = x * xs;
                xy = x * ys;
                xz = x * zs;
                yy = y * ys;
                yz = y * zs;
                zz = z * zs;

                TMatrix44<T> result = TMatrix44<T>::createIdentityMatrix();

                result[0][0] = 1.0f - (yy + zz);
                result[1][0] = xy - wz;
                result[2][0] = xz + wy;

                result[0][1] = xy + wz;
                result[1][1] = 1.0f - (xx + zz);
                result[2][1] = yz - wx;

                result[0][2] = xz - wy;
                result[1][2] = yz + wx;
                result[2][2] = 1.0f - (xx + yy);

                return result;
            }

            void toEulerAngles(TVector3<T>& euler) const {
                const double sqw = w*w;
                const double sqx = x*x;
                const double sqy = y*y;
                const double sqz = z*z;

                // heading = rotation about z-axis
                euler.z = (T) (atan2(2.0 * (x * y + z * w), (sqx - sqy - sqz + sqw)));

                // bank = rotation about x-axis
                euler.x = (T) (atan2(2.0 * (y * z + x * w), (-sqx - sqy + sqz + sqw)));

                // attitude = rotation about y-axis

                euler.y = asinf(clamp(-2.0f * (x * z - y * w), -1.0f, 1.0f));
            }

            TQuaternion<T> createFromEuler(T _x, T _y, T _z) {
                double angle;

                angle = _x * 0.5;
                const double sr = sin(angle);
                const double cr = cos(angle);

                angle = _y * 0.5;
                const double sp = sin(angle);
                const double cp = cos(angle);

                angle = _z * 0.5;
                const double sy = sin(angle);
                const double cy = cos(angle);

                const double cpcy = cp * cy;
                const double spcy = sp * cy;
                const double cpsy = cp * sy;
                const double spsy = sp * sy;

                T X = (T) (sr * cpcy - cr * spsy);
                T Y = (T) (cr * spcy + sr * cpsy);
                T Z = (T) (cr * cpsy - sr * spcy);
                T W = (T) (cr * cpcy + sr * spsy);

                TQuaternion<T> q(X, Y, Z, W);
                q.normalize();
                return q;
            }

            T & max_(const T& a, const T & b) {
                return a < b ? b : a;
            }

            T & min_(const T& a, const T & b) {
                return a < b ? a : b;
            }

            T & clamp(const T& value, const T& low, const T & high) {
                return min_(max_(value, low), high);
            }
        };

        template<class T>
        TQuaternion<T>::TQuaternion(const TMatrix44<T> &m) {
            /*
            This code can be optimized for m[kW][kW] = 1, which
            should always be true.  This optimization is excluded
            here for clarity.
             */

            double Tr = m[kX][kX] + m[kY][kY] + m[kZ][kZ] + m[kW][kW], fourD;
            int i, j, k;

            /*
            w >= 0.5 ?
             */
            if (Tr >= 1.0) {
                fourD = 2.0 * sqrt(Tr);
                (*this)[kW] = fourD / 4.0;
                (*this)[kX] = (m[kZ][kY] - m[kY][kZ]) / fourD;
                (*this)[kY] = (m[kX][kZ] - m[kZ][kX]) / fourD;
                (*this)[kZ] = (m[kY][kX] - m[kX][kY]) / fourD;
            } else {
                /*
                Find the largest component.
                 */
                if (m[kX][kX] > m[kY][kY]) {
                    i = kX;
                } else {
                    i = kY;
                }
                if (m[kZ][kZ] > m[i][i]) {
                    i = kZ;
                }

                /*
                Set j and k to point to the next two components
                 */
                j = (i + 1) % 3;
                k = (j + 1) % 3;

                /*
                fourD = 4 * largest component
                 */
                fourD = 2.0 * sqrt(m[i][i] - m[j][j] - m[k][k] + 1.0);

                /*
                Set the largest component
                 */
                (*this)[i] = fourD / 4.0;

                /*
                Calculate remaining components
                 */
                (*this)[j] = (m[j][i] + m[i][j]) / fourD;
                (*this)[k] = (m[k][i] + m[i][k]) / fourD;
                (*this)[kW] = (m[k][j] - m[j][k]) / fourD;
            }
        }

        template<typename T> TQuaternion<T> inline operator*(const T a, const TQuaternion<T> &q) {
            return q*a;
        }

        typedef TQuaternion<float> Quaternionf;
        typedef TQuaternion<double> Quaterniond;

    } // end namespace graph
} // end namespace rev


#endif	/* REVQUATERNION_H */

/*
 * DO TESTOWANIA KWATERNIONOW:
 *
 */
/*
      float x = 1.0f;
      float y = 1.0f;
      float z = 1.0f;
      float angleDegree = 45;
      float angle = toRadians(angleDegree);

      Vector3f vect(x, y, z);
  vect.normalize();

  Quaternionf qua = Quaternionf::createQuaternion(angle, vect);

  Quat4fT q;
  q.s.X = qua.x;
  q.s.Y = qua.y;
  q.s.Z = qua.z;
  q.s.W = qua.w;
  GLfloat m33[9] = {0};
  float *m44 = Matrix44f::createIdentityMatrix().dataPointer();

  Matrix3fSetRotationFromQuat4f((Matrix3fT*) m33, &q);
  Matrix33f m33f(m33);
  cout << "m33f = " << endl << m33f.toString() << endl;

  Matrix4fSetRotationFromMatrix3f((Matrix4fT*) m44, (Matrix3fT*) m33);
  Matrix44f m44f(m44);
  cout << "m44f = " << endl << m44f.toString() << endl;

  Matrix44f myM44f = Matrix44f::createRotationMatrix(vect, angle);
  cout << "myM44f = " << endl << myM44f.toString() << endl;

  Quaternionf qua2 = Quaternionf::createQuaternion(angle, vect);
  cout << "qua = " << qua.toString() << endl;
  cout << "qua2 = " << qua2.toString() << endl;

 // TUTAJ quaM33 i quaM44 ma inaczej odwrocenoe osie niz qua i qua2
 // DLACZEGO? Jak zmienie kont na przeciwny to zamieniaja sie te pary kwaternionow
 // znakami
  Quaternionf quaM33(m33f);
  cout << "quaM33 = " << quaM33.toString() << endl;

  Quaternionf quaM44(m44f);
  cout << "quaM44 = " << quaM44.toString() << endl;

  Matrix33f qm33f = quaM44.getRotationMatrix();
  cout << "qm33f = " << endl << qm33f.toString() << endl;

  Matrix44f qm44f = quaM44.toMatrix();
  cout << "qm44f = " << endl << qm44f.toString() << endl;
  cout << "ble = " << endl;
 */
