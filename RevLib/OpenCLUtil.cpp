/* 
 * File:   OpenCLUtil.cpp
 * Author: Revers
 * 
 * Created on 26 październik 2011, 22:39
 */

#include "OpenCLUtil.h"



#include <cstdlib>
#include <fstream>
#include <string>
#include <iostream>
#include <sstream>
#include <stdexcept>

using namespace std;

#define TAG_END_FAKE "//$END_FAKE$"
#define TAG_BEGIN "//$START$"
#define TAG_END "//$END$"
#define OPENCL_FILE_FOLDER "D:\\Revers\\NetBeansProjects\\MyOpenCL_FILES\\"

static
std::string readFileNormal(const char* filename) {
    cout << "Reading File: '" << filename << "'..." << endl;
    std::string buf;
    std::string line;
    std::ifstream in(filename);

    if (!in) {
        string msg("ERROR: FILE NOT FOUND: '");
        msg += filename;
        msg += "'!!";
        cout << msg << endl;
        throw std::runtime_error(msg.c_str());
        return string("FILE_NOT_FOUND");
    }

    while (std::getline(in, line)) {
        buf += line;
        buf += '\n';
    }


    return buf;
}

static
std::string getIncludeFile(const char* includeFile) {

    cout << "Reading File: '" << includeFile << "'..." << endl;

    std::string buf;
    std::string line;
    std::ifstream in(includeFile);

    if (!in) {
        string msg("ERROR: FILE NOT FOUND: '");
        msg += includeFile;
        msg += "'!!";
        cout << msg << endl;
        throw std::runtime_error(msg.c_str());
        return string("FILE_NOT_FOUND");
    }

    bool beginFound = false;
    bool endFound = false;

    while (std::getline(in, line)) {
        if (line == TAG_BEGIN) {
            beginFound = true;
            continue;
        }

        if (beginFound == false) {
            continue;
        }

        if (line == TAG_END) {
            endFound = true;
            break;
        }

        buf += line;
        buf += '\n';
    }

    if (beginFound == false) {
        string msg("ERROR: TAG BEGIN NOT FOUND IN FILE: '");
        msg += includeFile;
        msg += "'!!";
        cout << msg << endl;
        throw std::runtime_error(msg.c_str());

        return string("TAG_BEGIN_NOT_FOUND");
    }

    if (endFound == false) {
        string msg("ERROR: TAG END NOT FOUND IN FILE: '");
        msg += includeFile;
        msg += "'!!";
        cout << msg << endl;
        throw std::runtime_error(msg.c_str());

        return string("TAG_END_NOT_FOUND");
    }

    return buf;
}

std::string OpenCLUtil::readCLFile(const char* filename, bool useOpenCL_File_Folder, const char* includeFile) {
    if (useOpenCL_File_Folder == false) {
        return readFileNormal(filename);
    }

    string name(OPENCL_FILE_FOLDER);
    name += filename;

    int pos = name.find_last_of('.');

    if (pos != string::npos) {
        name = name.substr(0, pos);
    }

    name += ".cxx";

    cout << "Reading File: '" << name << "'..." << endl;

    std::string buf;
    std::string line;
    std::ifstream in(name.c_str());

    if (!in) {
        string msg("ERROR: FILE NOT FOUND: '");
        msg += filename;
        msg += "'!!";
        cout << msg << endl;
        throw std::runtime_error(msg.c_str());
        return string("FILE_NOT_FOUND");
    }

    if (includeFile != NULL) {
        buf += getIncludeFile(includeFile);
    }

    bool tagFound = false;
    while (std::getline(in, line)) {
        if (line == TAG_END_FAKE) {
            tagFound = true;
            continue;
        }

        if (tagFound == false) {
            continue;
        }

        buf += line;
        buf += '\n';
    }

    if (tagFound == false) {
        string msg("ERROR: TAG NOT FOUND IN FILE: '");
        msg += name;
        msg += "'!!";
        cout << msg << endl;
        throw std::runtime_error(msg.c_str());

        return string("TAG_NOT_FOUND");
    }

    return buf;
}

std::string OpenCLUtil::readCLFile(
        const char* filename,
        const char* directory/* = NULL */,
        const char* includeFile/* = NULL */) {

    string name;
    if (directory == NULL) {
        name = filename;
    } else {
        name = directory;
        name += filename;
    }

    int pos = name.find_last_of('.');

    if (pos != string::npos) {
        name = name.substr(0, pos);
    }

    name += ".cxx";

    cout << "Reading File: '" << name << "'..." << endl;

    std::string buf;
    std::string line;
    std::ifstream in(name.c_str());

    if (!in) {
        string msg("ERROR: FILE NOT FOUND: '");
        msg += filename;
        msg += "'!!";
        cout << msg << endl;
        throw std::runtime_error(msg.c_str());
        return string("FILE_NOT_FOUND");
    }

    if (includeFile != NULL) {
        buf += getIncludeFile(includeFile);
    }

    bool tagFound = false;
    while (std::getline(in, line)) {
        if (line == TAG_END_FAKE) {
            tagFound = true;
            continue;
        }

        if (tagFound == false) {
            continue;
        }

        buf += line;
        buf += '\n';
    }

    if (tagFound == false) {
        string msg("ERROR: TAG NOT FOUND IN FILE: '");
        msg += name;
        msg += "'!!";
        cout << msg << endl;
        throw std::runtime_error(msg.c_str());

        return string("TAG_NOT_FOUND");
    }

    return buf;
}
