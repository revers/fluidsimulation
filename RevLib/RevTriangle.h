/* 
 * File:   RevTriangle.h
 * Author: Revers
 *
 * Created on 29 październik 2010, 17:07
 */

#ifndef REVTRIANGLE_H
#define	REVTRIANGLE_H

#include <string>
#include <sstream>
#include <cmath>

#include "RevVector.h"

namespace rev {
    namespace graph {

        template<class T>
        class TTriangle {
        public:
            TVector3<T> a;
            TVector3<T> b;
            TVector3<T> c;

        public:

            TTriangle() {
            }

            TTriangle(TVector3<T> a, TVector3<T> b, TVector3<T> c) : a(a), b(b), c(c) {
            }

        public:

            TVector3<T>* dataPointer() {
                return &a;
            }

            std::string toString() {
                std::ostringstream oss;
                oss << "[ A(" << a.x << ", " << a.y << ", " << a.z << "), B("
                        << b.x << ", " << b.y << ", " << b.z << "), C("
                        << c.x << ", " << c.y << ", " << c.z << ") ]";
                return oss.str();
            }

            std::string toStringFull() {
                std::ostringstream oss;
                oss << "Triangle@" << this
                        << "[ A(" << a.x << ", " << a.y << ", " << a.z << "), B("
                        << b.x << ", " << b.y << ", " << b.z << "), C("
                        << c.x << ", " << c.y << ", " << c.z << ") ]";
                return oss.str();
            }

            TVector3<T> getNormal() {

                float a_x = b.x - a.x;
                float a_y = b.y - a.y;
                float a_z = b.z - a.z;

                float b_x = c.x - b.x;
                float b_y = c.y - b.y;
                float b_z = c.z - b.z;

                float resultX = a_y * b_z - a_z * b_y;
                float resultY = a_x * b_z - a_z * b_x;
                float resultZ = a_x * b_y - a_y * b_x;

                float len = sqrt(resultX * resultX + resultY * resultY
                        + resultZ * resultZ);

                if (len != 0.0f) {
                    resultX /= len;
                    resultY /= len;
                    resultZ /= len;
                }

                return TVector3< T > (resultX, resultY, resultZ);
            }
        };

        template<class T>
        class TTriangleRef {
        public:
            T refA;
            T refB;
            T refC;

        public:

            TTriangleRef() {
            }

            TTriangleRef(T refA, T refB, T refC) :
            refA(refA), refB(refB), refC(refC) {
            }

        public:

            T* dataPointer() {
                return &refA;
            }

            std::string toString() const {
                std::ostringstream oss;
                oss << "[ refA = " << refA << ", refB = " << refB
                        << ", refC = " << refC << " ]";
                return oss.str();
            }

            std::string toStringFull() const {
                std::ostringstream oss;
                oss << "TriangleRef@" << this << "[ refA = " << refA
                        << ", refB = " << refB << ", refC = " << refC <<
                        " ]";
                return oss.str();
            }
        };




        //--------------------------------------------------------------------

        template<class T>
        class TTexCoord {
        public:
            T u;
            T v;

        public:

            TTexCoord() {

            }

            TTexCoord(T u, T v) : u(u), v(v) {

            }

        public:

            T* dataPointer() {
                return &u;
            }

            std::string toString() const {
                std::ostringstream oss;
                oss << "[ u = " << u << ", v = " << v << " ]";
                return oss.str();
            }

            std::string toStringFull() const {
                std::ostringstream oss;
                oss << "TexCoord@" << this << "[ u = " << u << ", v = " << v << " ]";
                return oss.str();
            }
        };

        //----------------------------------------------------------------------


        typedef TTriangleRef<unsigned int> TriangleRef;
        typedef TTriangleRef<unsigned short> ShortTriangleRef;

        typedef TTriangle<float> Triangle;
        typedef TTriangle<double> Triangled;

        typedef TTexCoord<float> TexCoord;
        typedef TTexCoord<float> TexCoordd;
    } // end namespace graph
} // end namespace rev

#endif	/* REVTRIANGLE_H */

