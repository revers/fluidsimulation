/* 
 * File:   RevVBOGeometricPrimitive.h
 * Author: Revers
 *
 * Created on 24 październik 2011, 17:07
 */

#ifndef REVVBOGEOMETRICPRIMITIVE_H
#define	REVVBOGEOMETRICPRIMITIVE_H

#include <vector>
#include "RevColor.h"
#include "RevVector.h"
#include "RevVertex.h"
#include "RevOpenGL.h"

namespace rev {
    namespace ogl {

        using rev::graph::VertexPNT;
        using rev::ogl::Shader;

        class VBOGeometricPrimitive {
        private:
            GLint colorLoc;
            GLint translationLoc;
            GLint lightPosLoc;

        protected:

            std::vector<VertexPNT>* vertices;
            std::vector<GLushort>* indices;

            rev::graph::Color3f color;

            Shader* shader;

            GLuint vaoId;
            GLuint vboId;
            GLuint iboId;

            GLuint indicesCount;
            GLuint verticesCount;
            GLuint trianglesCount;

            rev::graph::Vector3f translation;
            rev::graph::Vector3f lightPosition;
        public:

            static const GLuint VERTEX_INDEX = 0;
            static const GLuint NORMAL_INDEX = 1;

        public:

            VBOGeometricPrimitive(Shader* shader_) :
            colorLoc(0),
            translationLoc(0),
            color(rev::graph::Color3f(0.0f, 1.0f, 1.0)),
            shader(shader_),
            vaoId(0),
            vboId(0),
            iboId(0),
            indicesCount(0),
            verticesCount(0),
            trianglesCount(0), 
            translation(0, 0, 0),
            lightPosition(100.0, 10.0, -100.0) {

                vertices = new std::vector<VertexPNT > ();
                indices = new std::vector<GLushort > ();

            }

            virtual ~VBOGeometricPrimitive() {
                glDeleteBuffers(1, &iboId);
                glDeleteBuffers(1, &vboId);
                glDeleteVertexArrays(1, &vaoId);
            }

            void addVertexNormal(rev::graph::Vector3f& position,
                    rev::graph::Vector3f& normal) {

                vertices->push_back(VertexPNT(
                        position.x,
                        position.y,
                        position.z,
                        normal.x,
                        normal.y,
                        normal.z
                        ));
            }

            static Shader* createDefaultShader();

            bool initializeVBO();

            void loadShaderSettings();

            void draw();

            void setTranslation(GLfloat tx, GLfloat ty, GLfloat tz);

            void setLightPosition(GLfloat tx, GLfloat ty, GLfloat tz);

            void setTranslation(const rev::graph::Vector3f& translation) {
                setTranslation(translation.x, translation.y, translation.z);
            }

            rev::graph::Vector3f& getTranslation() {
                return translation;
            }

            rev::graph::Vector3f getTranslation() const {
                return translation;
            }

            void setLightPosition(const rev::graph::Vector3f& lightPosition) {
                setLightPosition(lightPosition.x, lightPosition.y, lightPosition.z);
            }

            rev::graph::Vector3f& getLightPosition() {
                return lightPosition;
            }

            rev::graph::Vector3f getLightPosition() const {
                return lightPosition;
            }

            void addIndex(GLushort index) {
                indices->push_back(index);
            }

            rev::graph::Color3f& getColor() {
                return color;
            }

            rev::graph::Color3f getColor() const {
                return color;
            }

            void setColor(float r, float g, float b);
           
            void setColor(rev::graph::Color3f& color) {
                setColor(color.r, color.g, color.b);
            }

            GLuint getIndicesCount() const {
                return indicesCount;
            }

            GLuint getTrianglesCount() const {
                return trianglesCount;
            }

            GLuint getVerticesCount() const {
                return verticesCount;
            }

            virtual std::string toString() const {
                std::ostringstream oss;
                oss << "[ vertices = "
                        << verticesCount
                        << ", indices = "
                        << indicesCount
                        << ", triangles = "
                        << trianglesCount
                        << " ]";
                return oss.str();
            }

            virtual std::string toStringFull() const {
                std::ostringstream oss;
                oss << "GeometricPrimitive@"
                        << this
                        << "[ vertices = "
                        << verticesCount
                        << ", indices = "
                        << indicesCount
                        << ", triangles = "
                        << trianglesCount << " ]";

                return oss.str();
            }

        private:

            VBOGeometricPrimitive(const VBOGeometricPrimitive& orig);

        protected:

            int currentVertex() const {
                return vertices->size();
            }

        };
    } // namespace ogl
} // namespace rev

#endif	/* REVVBOGEOMETRICPRIMITIVE_H */

