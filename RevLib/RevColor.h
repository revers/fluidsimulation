/* 
 * File:   RevColor.h
 * Author: Revers
 *
 * Created on 1 listopad 2010, 21:04
 */

#ifndef REVCOLOR_H
#define	REVCOLOR_H

#include <string>
#include <sstream>
#include <cmath>



namespace rev {
    namespace graph {

        template<class T>
        class TColor3 {
        public:
            T r, g, b;

        public:

            TColor3() : r(0), g(0), b(0) {
            }

            TColor3(T r, T g, T b) : r(r), g(g), b(b) {
            }

        public:

            void set(T r, T g, T b) {
                this->r = r;
                this->g = g;
                this->b = b;
            }

            std::string toString() const {
                std::ostringstream oss;
                oss << "[ r = " << r << ", g = " << g
                        << ", b = " << b << " ]";
                return oss.str();
            }

            std::string toStringFull() const {
                std::ostringstream oss;
                oss << "Color3@" << this
                        << "[ r = " << r << ", g = " << g
                        << ", b = " << b << " ]";
                return oss.str();
            }

            T* dataPointer() {
                return &r;
            }
        };

        template<class T>
        class TColor4 {
        public:
            T r, g, b, a;


        public:

            TColor4() : r(0), g(0), b(0), a(0) {

            }

            TColor4(T r, T g, T b) : r(r), g(g), b(b), a(a) {
            }

        public:

            void set(T r, T g, T b, T a) {
                this->r = r;
                this->g = g;
                this->b = b;
                this->a = a;
            }

            std::string toString() const {
                std::ostringstream oss;
                oss << "[ r = " << r << ", g = " << g
                        << ", b = " << b << ", a = " << a << " ]";
                return oss.str();
            }

            std::string toStringFull() const {
                std::ostringstream oss;
                oss << "Color4@" << this
                        << "[ r = " << r << ", g = " << g
                        << ", b = " << b << ", a = " << a << " ]";
                return oss.str();
            }

            T* dataPointer() {
                return &r;
            }
        };

        typedef TColor3<unsigned int> Color3i;
        typedef TColor3<unsigned short> Color3s;
        typedef TColor3<unsigned char> Color3c;
        typedef TColor3<float> Color3f;

        typedef TColor4<unsigned int> Color4i;
        typedef TColor4<unsigned short> Color4s;
        typedef TColor4<unsigned char> Color4c;
        typedef TColor4<float> Color4f;

        static inline unsigned int colorFti(float f) {
            return (unsigned int) (f * 255.0f);
        }

        static inline float colorItf(unsigned int i) {
            return (float) i / 255.0f;
        }

        static inline unsigned short colorFts(float f) {
            return (unsigned short) (f * 255.0f);
        }

        static inline float colorStf(unsigned short i) {
            return (float) i / 255.0f;
        }

        static inline unsigned char colorFtc(float f) {
            return (unsigned char) (f * 255.0f);
        }

        static inline float colorCtf(unsigned char i) {
            return (float) i / 255.0f;
        }
    } // end namespace graph
} // end namespace rev



#endif	/* REVCOLOR_H */

