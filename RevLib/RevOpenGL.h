/* 
 * File:   RevOpenGL.h
 * Author: Revers
 *
 * Created on 24 październik 2011, 17:03
 */

#ifndef REVOPENGL_H
#define	REVOPENGL_H

#include <GL/glew.h>
#include <GL/glext.h>
#include <GL/gl.h>

namespace rev {
    namespace ogl {

        class Shader {
        public:
            GLuint vertexShader;
            GLuint fragmentShader;
            GLuint shaderProgram;

            Shader(GLuint vertexShader_,
                    GLuint fragmentShader_,
                    GLuint shaderProgram_) :
            vertexShader(vertexShader_),
            fragmentShader(fragmentShader_),
            shaderProgram(shaderProgram_) {
            }

            ~Shader() {
                glDetachShader(shaderProgram, vertexShader);
                glDetachShader(shaderProgram, fragmentShader);
                glDeleteShader(vertexShader);
                glDeleteShader(fragmentShader);
                glDeleteProgram(shaderProgram);
            }
        };

    } // end namespace ogl
} // end namespace rev

#endif	/* REVOPENGL_H */

