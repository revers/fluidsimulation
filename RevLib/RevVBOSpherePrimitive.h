/* 
 * File:   RevVBOSpherePrimitive.h
 * Author: Revers
 *
 * Created on 24 październik 2011, 17:10
 */

#ifndef REVVBOSPHEREPRIMITIVE_H
#define	REVVBOSPHEREPRIMITIVE_H

#include "RevVBOGeometricPrimitive.h"

namespace rev {
    namespace ogl {

        class VBOSpherePrimitive : public VBOGeometricPrimitive {
        private:
            rev::graph::Vector3f center;
            float radius;
            int tessellation;
            
            void init(rev::graph::Vector3f center, float radius, int tessellation);

        public:

            VBOSpherePrimitive(Shader* shader) :
            VBOGeometricPrimitive(shader) {
                init(rev::graph::Vector3f(0, 0, 0), 2, 16);
            }

            VBOSpherePrimitive(Shader* shader, float radius, int tessellation) :
            VBOGeometricPrimitive(shader) {
                init(rev::graph::Vector3f(0, 0, 0), radius, tessellation);
            }

            VBOSpherePrimitive(Shader* shader, rev::graph::Vector3f center,
                    float radius, int tessellation) :
            VBOGeometricPrimitive(shader) {
                init(center, radius, tessellation);
            }

            virtual ~VBOSpherePrimitive() {
            }

            virtual std::string toString() const {
                std::ostringstream oss;
                oss << "[ center = "
                        << center.toString()
                        << ", radius = "
                        << radius
                        << ", tesselation = "
                        << tessellation
                        << ", "
                        << VBOGeometricPrimitive::toString()
                        << " ]";
                return oss.str();
            }

            virtual std::string toStringFull() const {
                std::ostringstream oss;
                oss << "SpherePrimitive@"
                        << this
                        << "[ center = "
                        << center.toString()
                        << ", radius = "
                        << radius
                        << ", tesselation = "
                        << tessellation
                        << ", "
                        << VBOGeometricPrimitive::toString()
                        << " ]";

                return oss.str();
            }
        private:

            VBOSpherePrimitive(const VBOSpherePrimitive& orig);
        };
    } // namespace ogl
} // namespace rev
#endif	/* REVVBOSPHEREPRIMITIVE_H */
//
//struct Vertex {
//    float x;
//    float y;
//    float z;
//};
//
//typedef Vertex Normal;
//
//class Sphere {
//private:
//    const float pi;
//    const float pi_over_two;
//    const float two_pi;
//    const Vector3f up;
//    const Vector3f down;
//
//    rev::graph::Vector3f center;
//    float radius;
//    int tessellation;
//public:
//    vector<Vertex>* vertices;
//    vector<Normal>* normals;
//    vector<GLushort>* indices;
//public:
//
//    Sphere(Vector3f center, float radius, int tessellation) :
//    pi((float) M_PI),
//    pi_over_two((float) (M_PI / 2.0)),
//    two_pi((float) (M_PI * 2.0)),
//    up(0, 1, 0),
//    down(0, -1, 0) {
//        vertices = new vector<Vertex > ();
//        normals = new vector<Normal > ();
//        indices = new vector<GLushort > ();
//
//        init(center, radius, tessellation);
//
//        initArrays();
//    }
//
//    ~Sphere() {
//        delete vertices;
//        delete normals;
//        delete indices;
//
//        freeArrays();
//    }
//
//    void initArrays() {
//        glEnableClientState(GL_VERTEX_ARRAY);
//        glEnableClientState(GL_NORMAL_ARRAY);
//
//        glVertexPointer(3, GL_FLOAT, 0, vertices->data());
//        glNormalPointer(GL_FLOAT, 0, normals->data());
//    }
//
//    void freeArrays() {
//        glDisableClientState(GL_NORMAL_ARRAY);
//        glDisableClientState(GL_VERTEX_ARRAY);
//    }
//
//    void draw() {
//
//        glDrawElements(GL_TRIANGLES, indices->size(), GL_UNSIGNED_SHORT, indices->data());
//    }
//
//private:
//
//    int currentVertex() const {
//        return vertices->size();
//    }
//
//    void addVertexNormal(const Vector3f& position,
//            const Vector3f& normal) {
//
//        Vertex v = {position.x, position.y, position.z};
//
//        vertices->push_back(v);
//
//        Normal n = {normal.x, normal.y, normal.z};
//        normals->push_back(n);
//    }
//
//    void addIndex(GLushort index) {
//        indices->push_back(index);
//    }
//
//    void init(Vector3f center, float radius, int tessellation) {
//        this->center = center;
//        this->radius = radius;
//        this->tessellation = tessellation;
//
//        int verticalSegments = tessellation;
//        int horizontalSegments = tessellation * 2;
//
//        Vector3f vert = down * radius + center;
//        // Start with a single vertex at the bottom of the sphere.
//        addVertexNormal(vert, down);
//
//        // Create rings of vertices at progressively higher latitudes.
//        for (int i = 0; i < verticalSegments - 1; i++) {
//            float latitude = ((i + 1) * pi /
//                    verticalSegments) - pi_over_two;
//
//            float dy = (float) sin(latitude);
//            float dxz = (float) cos(latitude);
//
//            // Create a single ring of vertices at this latitude.
//            for (int j = 0; j < horizontalSegments; j++) {
//                float longitude = j * two_pi / horizontalSegments;
//
//                float dx = (float) cos(longitude) * dxz;
//                float dz = (float) sin(longitude) * dxz;
//
//                Vector3f normal(dx, dy, dz);
//
//                vert = normal * radius + center;
//                addVertexNormal(vert, normal);
//            }
//        }
//
//        // Finish with a single vertex at the top of the sphere.
//        vert = up * radius + center;
//        addVertexNormal(vert, up);
//
//        // Create a fan connecting the bottom vertex to the bottom latitude ring.
//        for (int i = 0; i < horizontalSegments; i++) {
//            addIndex(0);
//            addIndex(1 + (i + 1) % horizontalSegments);
//            addIndex(1 + i);
//        }
//
//        // Fill the sphere body with triangles joining each pair of latitude rings.
//        for (int i = 0; i < verticalSegments - 2; i++) {
//            for (int j = 0; j < horizontalSegments; j++) {
//                int nextI = i + 1;
//                int nextJ = (j + 1) % horizontalSegments;
//
//                addIndex(1 + i * horizontalSegments + j);
//                addIndex(1 + i * horizontalSegments + nextJ);
//                addIndex(1 + nextI * horizontalSegments + j);
//
//                addIndex(1 + i * horizontalSegments + nextJ);
//                addIndex(1 + nextI * horizontalSegments + nextJ);
//                addIndex(1 + nextI * horizontalSegments + j);
//            }
//        }
//
//        // Create a fan connecting the top vertex to the top latitude ring.
//        for (int i = 0; i < horizontalSegments; i++) {
//            addIndex(currentVertex() - 1);
//            addIndex(currentVertex() - 2 - (i + 1) % horizontalSegments);
//            addIndex(currentVertex() - 2 - i);
//        }
//    }
//};