/* 
 * File:   RevPoint.h
 * Author: Revers
 *
 * Created on 29 październik 2010, 16:50
 */

#ifndef REVPOINT_H
#define	REVPOINT_H


#include <string>
#include <sstream>
#include "RevVector.h"


namespace rev {
    namespace graph {
        typedef TVector2<int> Point2i;
        typedef TVector2<float> Point2f;
        typedef TVector2<double> Point2d;

        typedef TVector3<int> Point3i;
        typedef TVector3<float> Point3f;
        typedef TVector3<double> Point3d;

        typedef TVector4<int> Point4i;
        typedef TVector4<float> Point4f;
        typedef TVector4<double> Point4d;
    }// end namespace graph
} // end namespace rev

#endif	/* REVPOINT_H */

