/* 
 * File:   RevVBOSpherePrimitive.cpp
 * Author: Revers
 * 
 * Created on 24 październik 2011, 17:10
 */

#include <cassert>
#include <cmath>
#include "RevVBOSpherePrimitive.h"

using namespace rev::ogl;
using rev::graph::Vector3f;

const float pi = (float) M_PI;
const float pi_over_two = (float) (M_PI / 2.0);
const float two_pi = (float) (M_PI * 2.0);

Vector3f up(0, 1, 0);
Vector3f down(0, -1, 0);

void VBOSpherePrimitive::init(Vector3f center, float radius, int tessellation) {
    this->center = center;
    this->radius = radius;
    this->tessellation = tessellation;

    assert(tessellation >= 3);

    int verticalSegments = tessellation;
    int horizontalSegments = tessellation * 2;

    Vector3f vert = down * radius + center;
    // Start with a single vertex at the bottom of the sphere.
    addVertexNormal(vert, down);

    // Create rings of vertices at progressively higher latitudes.
    for (int i = 0; i < verticalSegments - 1; i++) {
        float latitude = ((i + 1) * pi /
                verticalSegments) - pi_over_two;

        float dy = (float) sin(latitude);
        float dxz = (float) cos(latitude);

        // Create a single ring of vertices at this latitude.
        for (int j = 0; j < horizontalSegments; j++) {
            float longitude = j * two_pi / horizontalSegments;

            float dx = (float) cos(longitude) * dxz;
            float dz = (float) sin(longitude) * dxz;

            Vector3f normal(dx, dy, dz);

            vert = normal * radius + center;
            addVertexNormal(vert, normal);
        }
    }

    // Finish with a single vertex at the top of the sphere.
    vert = up * radius + center;
    addVertexNormal(vert, up);

    // Create a fan connecting the bottom vertex to the bottom latitude ring.
    for (int i = 0; i < horizontalSegments; i++) {
        addIndex(0);
        addIndex(1 + (i + 1) % horizontalSegments);
        addIndex(1 + i);
    }

    // Fill the sphere body with triangles joining each pair of latitude rings.
    for (int i = 0; i < verticalSegments - 2; i++) {
        for (int j = 0; j < horizontalSegments; j++) {
            int nextI = i + 1;
            int nextJ = (j + 1) % horizontalSegments;

            addIndex(1 + i * horizontalSegments + j);
            addIndex(1 + i * horizontalSegments + nextJ);
            addIndex(1 + nextI * horizontalSegments + j);

            addIndex(1 + i * horizontalSegments + nextJ);
            addIndex(1 + nextI * horizontalSegments + nextJ);
            addIndex(1 + nextI * horizontalSegments + j);
        }
    }

    // Create a fan connecting the top vertex to the top latitude ring.
    for (int i = 0; i < horizontalSegments; i++) {
        addIndex(currentVertex() - 1);
        addIndex(currentVertex() - 2 - (i + 1) % horizontalSegments);
        addIndex(currentVertex() - 2 - i);
    }

    initializeVBO();
}


