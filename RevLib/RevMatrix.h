/*
 * File:   RevMatrix.h
 * Author: Revers
 *
 * Created on 29 październik 2010, 21:31
 */

#ifndef REVMATRIX_H
#define	REVMATRIX_H

//#include <cassert>
#include "RevVector.h"
#include "RevMath.h"

/*
 * TODO: Matrix decomposition to scale, rotation, translation
 * 
 * "If the transforms have been combined in the order Scale->Rotate->Translate, you can:

1. Compute the length of the first three basis vectors of the matrix - these are your scale factors.

2. Divide the first three basis vectors through by the scale factors to yield a pure rotation.

3. Extract the translation from the fourth basis vector, as you mentioned.

There are other methods that can be used as well, but the above should suffice for your typical SRT affine transform.
 * "
 * 
 * http://siddhantahuja.wordpress.com/2010/02/20/570/
 */

namespace rev {
    namespace graph {

        template<class T>
        class TMatrix33 {
        public:
            TVector3<T> cols[3];

        public:

            TMatrix33() {
            }

            TMatrix33(T v) {
                cols[0].set(v, v, v);
                cols[1].set(v, v, v);
                cols[2].set(v, v, v);
            }

            TMatrix33(const TVector3<T>& col1, const TVector3<T>& col2,
                    const TVector3<T>& col3) {
                cols[0] = col1;
                cols[1] = col2;
                cols[2] = col3;
            }

            TMatrix33(const TMatrix33<T>& mat) {
                cols[0] = mat[0];
                cols[1] = mat[1];
                cols[2] = mat[2];
            }

            // jak w OpenGL - kolumnowow

            TMatrix33(T* array9) {
                for (unsigned int row = 0; row < 3; row++) {
                    for (unsigned int col = 0; col < 3; col++) {
                        cols[col][row] = array9[col * 3 + row];
                    }
                }
            }

        public:

            static TMatrix33<T> createIdentityMatrix() {
                TMatrix33<T> result;
                result.identity();
                return result;
            }

            static TMatrix33<T> createRotationMatrix(T rad);

            static TMatrix33<T> createRotationMatrix(char axis, T rad);

            static TMatrix33<T> createScaleMatrix(T x, T y, T z) {
                TMatrix33<T> result = TMatrix33<T>::createIdentityMatrix();

                result[0][0] = x;
                result[1][1] = y;
                result[2][2] = z;

                return result;
            }

            static TMatrix33<T> createScaleMatrix(TVector2<T>& v) {
                return createScaleMatrix(v.x, v.y);
            }

            static TMatrix33<T> createTranslationMatrix(T x, T y) {
                TMatrix33<T> result = TMatrix33<T>::createIdentityMatrix();
                result[2][0] = x;
                result[2][1] = y;

                return result;
            }

            static TMatrix33<T> createTranslationMatrix(TVector2<T>& v) {
                return createTranslationMatrix(v.x, v.y);
            }

            static TMatrix33<T> createTransposed(const TMatrix33<T>& mat) {
                TMatrix33<T> result(mat);
                result.transpose();
                return result;
            }

        public:

            TVector3<T> transform(const TVector3<T>& vec) {
                return (*this) * vec;
            }

            void toArray(T* array9) {
                for (unsigned int row = 0; row < 3; row++) {
                    for (unsigned int col = 0; col < 3; col++) {
                        array9[col * 3 + row] = cols[col][row];
                    }
                }
            }

            T* dataPointer() {
                return reinterpret_cast<T*> (cols);
            }

            std::string toString();

            std::string toStringFull() const;

            void set(const TVector3<T>& col1, const TVector3<T>& col2,
                    const TVector3<T>& col3) {
                cols[0] = col1;
                cols[1] = col2;
                cols[2] = col3;
            }

            void identity() {
                cols[0].set(1.0f, 0.0f, 0.0f);
                cols[1].set(0.0f, 1.0f, 0.0f);
                cols[2].set(0.0f, 0.0f, 1.0f);
            }

            void transpose() {
                T temp;

                for (unsigned int col = 0; col < 3; col++) {
                    for (unsigned int row = col + 1; row < 3; row++) {
                        temp = cols[col][row];
                        cols[col][row] = cols[row][col];
                        cols[row][col] = temp;
                    }
                }
            }

            bool invert();

            void rotate(T rad) {
                TMatrix33<T> rotM = TMatrix33<T>::createRotationMatrix(rad);
                (*this) *= rotM;
            }

            void translate(T x, T y) {
                TMatrix33<T> transM = TMatrix33<T>::createTranslationMatrix(x, y);
                (*this) *= transM;
            }

            void scale(T x, T y, T z = 1.0f) {
                TMatrix33<T> scaleM = TMatrix33<T>::createScaleMatrix(x, y, z);
                (*this) *= scaleM;
            }

        public:

            TVector3<T> & operator[](unsigned int i) {
                //assert(i < 3);
                return cols[i];
            }

            const TVector3<T> & operator[](unsigned int i) const {
                //assert(i < 3);
                return cols[i];
            }

            TMatrix33<T> & operator=(const TMatrix33<T>& mat) {
                cols[0] = mat[0];
                cols[1] = mat[1];
                cols[2] = mat[2];
                return *this;
            }

            TMatrix33<T> & operator+=(const TMatrix33<T>& mat) {
                cols[0] += mat[0];
                cols[1] += mat[1];
                cols[2] += mat[2];
                return *this;
            }

            TMatrix33<T> & operator-=(const TMatrix33<T>& mat) {
                cols[0] -= mat[0];
                cols[1] -= mat[1];
                cols[2] -= mat[2];
                return *this;
            }

            TMatrix33<T> & operator*=(const TMatrix33<T>& mat);

            TMatrix33<T> & operator*=(T f) {
                cols[0] *= f;
                cols[1] *= f;
                cols[2] *= f;
                return *this;
            }

            TMatrix33<T> operator+(const TMatrix33<T>& mat) {
                TMatrix33<T> result(*this);
                result += mat;
                return result;
            }

            TMatrix33<T> operator-(const TMatrix33<T>& mat) {
                TMatrix33<T> result(*this);
                result -= mat;
                return result;
            }

            TMatrix33<T> operator*(const TMatrix33<T>& mat) {
                TMatrix33<T> result(*this);
                result *= mat;
                return result;
            }

            friend TVector3<T> operator*(const TMatrix33<T>& m, const TVector3<T>& v) {
                TVector3<T> result;
                result.x = v.x * m[0][0] + v.y * m[1][0] + v.z * m[2][0];
                result.y = v.x * m[0][1] + v.y * m[1][1] + v.z * m[2][1];
                result.z = v.x * m[0][2] + v.y * m[1][2] + v.z * m[2][2];
                return result;
            }

            friend TVector3<T> operator *(const TVector3<T> &v, const TMatrix33<T> &m) {
                TVector3<T> result;
                result.x = v.dotProduct(m[0]);
                result.y = v.dotProduct(m[1]);
                result.z = v.dotProduct(m[2]);
                return result;
            }

            friend TMatrix33<T> operator *(const TMatrix33<T> &m, T f) {
                TMatrix33<T> result(m);
                result *= f;
                return result;
            }

            friend TMatrix33<T> operator *(T f, const TMatrix33<T> &m) {
                TMatrix33<T> result(m);
                result *= f;
                return result;
            }

            bool operator==(const TMatrix33<T>& mat) {
                return ((cols[0] == mat[0]) && (cols[1] == mat[1])
                        && (cols[2] == mat[2]));
            }

            bool operator!=(const TMatrix33<T>& mat) {
                return ((cols[0] != mat[0]) || (cols[1] != mat[1])
                        || (cols[2] != mat[2]));
            }
        };

        //----------------------------------------------------------------------

        template<class T>
        class TMatrix44 {
        public:
            TVector4<T> cols[4];

        public:

            TMatrix44() {
            }

            TMatrix44(T v) {
                cols[0].set(v, v, v, v);
                cols[1].set(v, v, v, v);
                cols[2].set(v, v, v, v);
                cols[4].set(v, v, v, v);
            }

            // jak w OpenGL - kolumnowow

            TMatrix44(T* array16) {
                for (unsigned int row = 0; row < 4; row++) {
                    for (unsigned int col = 0; col < 4; col++) {
                        cols[col][row] = array16[col * 4 + row];
                    }
                }
            }

            TMatrix44(const TVector4<T>& col1, const TVector4<T>& col2,
                    const TVector4<T>& col3, const TVector4<T>& col4) {
                cols[0] = col1;
                cols[1] = col2;
                cols[2] = col3;
                cols[3] = col4;
            }

            TMatrix44(const TMatrix44<T>& mat) {
                cols[0] = mat[0];
                cols[1] = mat[1];
                cols[2] = mat[2];
                cols[3] = mat[3];
            }

            explicit TMatrix44(const TMatrix33<T>& m) {
                cols[0] = m[0];
                cols[1] = m[1];
                cols[2] = m[2];
                cols[3].set(0.0f, 0.0f, 0.0f, 1.0f);
            }

        public:

            void toArray(T* array16) {
                for (unsigned int row = 0; row < 4; row++) {
                    for (unsigned int col = 0; col < 4; col++) {
                        array16[col * 4 + row] = cols[col][row];
                    }
                }
            }

            T* dataPointer() {
                return reinterpret_cast<T*> (cols);
            }

            static TMatrix44<T> createIdentityMatrix() {
                TMatrix44<T> result;
                result.identity();
                return result;
            }

            // | 1    XY   XZ   0 |
            // | YX   1    YZ   0 |
            // | ZX   ZY   1    0 |
            // | 0    0    0    1 |

            static TMatrix44<T> createShearMatrix(T yx, T zx, T xy, T zy, T xz, T yz) {
                TMatrix44<T> result = TMatrix44<T>::createIdentityMatrix();
                result[0][1] = yx;
                result[0][2] = zx;
                result[1][0] = xy;
                result[1][2] = zy;
                result[2][0] = xz;
                result[2][1] = yz;
                return result;
            }

            static TMatrix44<T> createRotationMatrix(char axis, T rad);

            static TMatrix44<T> createRotationMatrix(const TVector3<T> &axis, T rad);

            static TMatrix44<T> createScaleMatrix(T x, T y, T z, T w = 1.0f) {
                TMatrix44<T> result = TMatrix44<T>::createIdentityMatrix();

                result[0][0] = x;
                result[1][1] = y;
                result[2][2] = z;
                result[3][3] = w;

                return result;
            }

            static TMatrix44<T> createScaleMatrix(TVector3<T>& v) {
                return createScaleMatrix(v.x, v.y, v.z);
            }

            static TMatrix44<T> createTranslationMatrix(T x, T y, T z) {
                TMatrix44<T> ret = TMatrix44<T>::createIdentityMatrix();

                ret[3][0] = x;
                ret[3][1] = y;
                ret[3][2] = z;

                return ret;
            }

            static TMatrix44<T> createTranslationMatrix(TVector3<T>& v) {
                return createTranslationMatrix(v.x, v.y, v.z);
            }

            static TMatrix44<T> createTransposed(const TMatrix44<T>& m) {
                TMatrix44<T> result(m);
                result.transpose();
                return result;
            }

        public:

            std::string toString();

            std::string toStringFull() const;

            TVector4<T> transform(const TVector4<T>& vec) {
                return (*this) * vec;
            }

            void set(const TVector4<T>& col1, const TVector4<T>& col2,
                    const TVector4<T>& col3, const TVector4<T>& col4) {
                cols[0] = col1;
                cols[1] = col2;
                cols[2] = col3;
                cols[3] = col4;
            }

            void identity() {
                cols[0].set(1.0f, 0.0f, 0.0f, 0.0f);
                cols[1].set(0.0f, 1.0f, 0.0f, 0.0f);
                cols[2].set(0.0f, 0.0f, 1.0f, 0.0f);
                cols[3].set(0.0f, 0.0f, 0.0f, 1.0f);
            }

            void transpose() {
                T temp;

                for (unsigned int col = 0; col < 4; col++) {
                    for (unsigned int row = col + 1; row < 4; row++) {
                        temp = cols[col][row];
                        cols[col][row] = cols[row][col];
                        cols[row][col] = temp;
                    }
                }
            }

            bool invert();

            void rotate(char axis, T rad) {
                TMatrix44<T> rotM = TMatrix44<T>::createRotationMatrix(axis, rad);
                (*this) *= rotM;
            }

            void rotate(const TVector3<T> &axis, T rad) {
                TMatrix44<T> rotM = TMatrix44<T>::createRotationMatrix(axis, rad);
                (*this) *= rotM;
            }

            void translate(T x, T y, T z) {
                TMatrix44<T> transM = TMatrix44<T>::createTranslationMatrix(x, y, z);
                (*this) *= transM;
            }

            void scale(T x, T y, T z, T w = 1.0f) {
                TMatrix44<T> scaleM = TMatrix44<T>::createScaleMatrix(x, y, z, w);
                (*this) *= scaleM;
            }

        public:

            TVector4<T> & operator[](unsigned int i) {
                //assert(i < 4);
                return cols[i];
            }

            const TVector4<T> & operator[](unsigned int i) const {
                // assert(i < 4);
                return cols[i];
            }

            TMatrix44<T> & operator=(const TMatrix44<T>& mat) {
                cols[0] = mat[0];
                cols[1] = mat[1];
                cols[2] = mat[2];
                cols[3] = mat[3];
                return *this;
            }

            TMatrix44<T> & operator+=(const TMatrix44<T>& mat) {
                cols[0] += mat[0];
                cols[1] += mat[1];
                cols[2] += mat[2];
                cols[3] += mat[3];
                return *this;
            }

            TMatrix44<T> & operator-=(const TMatrix44<T>& mat) {
                cols[0] -= mat[0];
                cols[1] -= mat[1];
                cols[2] -= mat[2];
                cols[3] -= mat[3];
                return *this;
            }

            TMatrix44<T> & operator*=(const TMatrix44<T>& mat);

            TMatrix44<T> & operator*=(T f) {
                cols[0] *= f;
                cols[1] *= f;
                cols[2] *= f;
                cols[3] *= f;
                return *this;
            }

            TMatrix44<T> operator+(const TMatrix44<T>& mat) {
                TMatrix44<T> result(*this);
                result += mat;
                return result;
            }

            TMatrix44<T> operator-(const TMatrix44<T>& mat) {
                TMatrix44<T> result(*this);
                result -= mat;
                return result;
            }

            TMatrix44<T> operator*(const TMatrix44<T>& mat) {
                TMatrix44<T> result(*this);
                result *= mat;
                return result;
            }

            friend TVector4<T> operator*(const TMatrix44<T>& m, const TVector4<T>& v) {
                TVector4<T> result;
                result.x = v.x * m[0][0] + v.y * m[1][0] + v.z * m[2][0] + v.w * m[3][0];
                result.y = v.x * m[0][1] + v.y * m[1][1] + v.z * m[2][1] + v.w * m[3][1];
                result.z = v.x * m[0][2] + v.y * m[1][2] + v.z * m[2][2] + v.w * m[3][2];
                result.w = v.x * m[0][3] + v.y * m[1][3] + v.z * m[2][3] + v.w * m[3][3];
                return result;
            }

            friend TVector4<T> operator *(const TVector4<T> &v, const TMatrix44<T> &m) {
                TVector4<T> result;
                result.x = v.dotProduct(m[0]);
                result.y = v.dotProduct(m[1]);
                result.z = v.dotProduct(m[2]);
                result.w = v.dotProduct(m[3]);

                return result;
            }

            friend TVector3<T> operator*(const TMatrix44<T>& m, const TVector3<T>& v3) {
                TVector4<T> v(v3);
                //TVector4<T> result;
                T x = v.x * m[0][0] + v.y * m[1][0] + v.z * m[2][0] + v.w * m[3][0];
                T y = v.x * m[0][1] + v.y * m[1][1] + v.z * m[2][1] + v.w * m[3][1];
                T z = v.x * m[0][2] + v.y * m[1][2] + v.z * m[2][2] + v.w * m[3][2];
               // result.w = v.x * m[0][3] + v.y * m[1][3] + v.z * m[2][3] + v.w * m[3][3];
                return TVector3<T > (x, y, z);

            }

            friend TVector3<T> operator *(const TVector3<T> &v3, const TMatrix44<T> &m) {
                TVector4<T> v(v3);
                //TVector4<T> result;
                T x = v.dotProduct(m[0]);
                T y = v.dotProduct(m[1]);
                T z = v.dotProduct(m[2]);
                //result.w = v.dotProduct(m[3]);

                return TVector3<T > (x, y, z);
            }

            friend TMatrix44<T> operator *(const TMatrix44<T> &m, T f) {
                TMatrix44<T> result(m);
                result *= f;
                return result;
            }

            friend TMatrix44<T> operator *(T f, const TMatrix44<T> &m) {
                TMatrix44<T> result(m);
                result *= f;
                return result;
            }

            bool operator==(const TMatrix44<T>& mat) {
                return ((cols[0] == mat[0]) && (cols[1] == mat[1])
                        && (cols[2] == mat[2]) && (cols[3] == mat[3]));
            }

            bool operator!=(const TMatrix44<T>& mat) {
                return ((cols[0] != mat[0]) || (cols[1] != mat[1])
                        || (cols[2] != mat[2]) || (cols[3] != mat[3]));
            }

            // Returns a "lookat" matrix44 given the current camera position (Vector3<T>),
            //   camera-up Vector3<T>, and camera-target Vector3<T>.

            static TMatrix44<T> createLookAtMatrix(const TVector3<T>& camPos, const TVector3<T>& target,
                    const TVector3<T>& camUp);

            // Returns a frustum matrix44 given the left, right, bottom, top,
            //   near, and far values for the frustum boundaries.

            static TMatrix44<T> createFrustumMatrix(T l, T r,
                    T b, T t, T n, T f);

            // Returns a perspective matrix44 given the field-of-view in the Y
            //   direction in degrees, the aspect ratio of Y/X, and near and
            //   far plane distances.

            static TMatrix44<T> createPerspectiveMatrix(T fovY, T aspect, T n, T f);

            // Return an orthographic matrix44 given the left, right, bottom, top,
            //   near, and far values for the frustum boundaries.

            static TMatrix44<T> createOrthoMatrix(T l, T r,
                    T b, T t, T n, T f);

            // Return an orientation matrix using 3 basis normalized vectors

            static TMatrix44<T> createOrthoNormalMatrix(const TVector3<T> &xdir,
                    const TVector3<T> &ydir, const TVector3<T> &zdir);

        };

        template<class T>
        TMatrix33<T> TMatrix33<T>::createRotationMatrix(T rad) {
            TMatrix33<T> result;

            T sinA, cosA;

            sinA = (T) sin(rad);
            cosA = (T) cos(rad);

            result[0][0] = cosA;
            result[1][0] = -sinA;
            result[2][0] = 0.0F;
            result[0][1] = sinA;
            result[1][1] = cosA;
            result[2][1] = 0.0F;
            result[0][2] = 0.0F;
            result[1][2] = 0.0F;
            result[2][2] = 1.0F;

            return result;

        }

        template<class T>
        TMatrix33<T> TMatrix33<T>::createRotationMatrix(char axis, T rad) {
            TMatrix33<T> rotM;
            T sinA, cosA;

            sinA = (T) sin(rad);
            cosA = (T) cos(rad);

            switch (axis) {
                case 'x':
                case 'X':
                    rotM[0][0] = 1.0F;
                    rotM[1][0] = 0.0F;
                    rotM[2][0] = 0.0F;
                    rotM[0][1] = 0.0F;
                    rotM[1][1] = cosA;
                    rotM[2][1] = -sinA;
                    rotM[0][2] = 0.0F;
                    rotM[1][2] = sinA;
                    rotM[2][2] = cosA;
                    break;

                case 'y':
                case 'Y':
                    rotM[0][0] = cosA;
                    rotM[1][0] = 0.0F;
                    rotM[2][0] = sinA;
                    rotM[0][1] = 0.0F;
                    rotM[1][1] = 1.0F;
                    rotM[2][1] = 0.0F;
                    rotM[0][2] = -sinA;
                    rotM[1][2] = 0.0F;
                    rotM[2][2] = cosA;
                    break;

                case 'z':
                case 'Z':
                    rotM[0][0] = cosA;
                    rotM[1][0] = -sinA;
                    rotM[2][0] = 0.0F;
                    rotM[0][1] = sinA;
                    rotM[1][1] = cosA;
                    rotM[2][1] = 0.0F;
                    rotM[0][2] = 0.0F;
                    rotM[1][2] = 0.0F;
                    rotM[2][2] = 1.0F;
                    break;
            }

            //            rotM[0][3] = 0.0F;
            //            rotM[1][3] = 0.0F;
            //            rotM[2][3] = 0.0F;
            //            rotM[3][0] = 0.0F;
            //            rotM[3][1] = 0.0F;
            //            rotM[3][2] = 0.0F;
            //            rotM[3][3] = 1.0F;
            return rotM;
        }

        template<class T>
        std::string TMatrix33<T>::toString() {
            std::ostringstream oss("\n");
            for (unsigned int row = 0; row < 3; row++) {
                oss << "\t[ ";
                for (unsigned int col = 0; col < 3; col++) {
                    oss << cols[col][row] << " ";
                }
                oss << "]\n";
            }

            return oss.str();
        }

        template<class T>
        std::string TMatrix33<T>::toStringFull() const {
            std::ostringstream oss;
            oss << "\nMatrix33@" << this << ":\n";
            for (unsigned int row = 0; row < 3; row++) {
                oss << "\t[ ";
                for (unsigned int col = 0; col < 3; col++) {
                    oss << cols[col][row] << " ";
                }
                oss << "]\n";
            }

            return oss.str();
        }

        template<class T>
        bool TMatrix33<T>::invert() {
            TMatrix33<T> b = TMatrix33<T>::createIdentityMatrix();

            unsigned int col, r;
            unsigned int cc;
            unsigned int rowMax;
            unsigned int row;
            T tmp;


            for (col = 0; col < 3; col++) {

                rowMax = col;
                for (r = col + 1; r < 3; r++) {
                    if (fabs(cols[col][r]) > fabs(cols[col][rowMax])) {
                        rowMax = r;
                    }
                }

                if (cols[rowMax][col] == 0.0F) {
                    return false;
                }

                for (cc = 0; cc < 3; cc++) {
                    tmp = cols[cc][col];
                    cols[cc][col] = cols[cc][rowMax];
                    cols[cc][rowMax] = tmp;
                    tmp = b[cc][col];
                    b[cc][col] = b[cc][rowMax];
                    b[cc][rowMax] = tmp;
                }

                tmp = cols[col][col];
                for (cc = 0; cc < 3; cc++) {
                    cols[cc][col] /= tmp;
                    b[cc][col] /= tmp;
                }

                for (row = 0; row < 3; row++) {
                    if (row != col) {
                        tmp = cols[col][row];
                        for (cc = 0; cc < 3; cc++) {
                            cols[cc][row] -= cols[cc][col] * tmp;
                            b[cc][row] -= b[cc][col] * tmp;
                        }
                    }
                }
            }

            *this = b;
            return true;
        }

        template<class T>
        TMatrix33<T> & TMatrix33<T>::operator*=(const TMatrix33<T>& mat) {
            for (unsigned int row = 0; row < 3; row++) {
                float colValues[3];
                for (unsigned int col = 0; col < 3; col++) {
                    T f = cols[0][row] * mat[col][0];
                    f += cols[1][row] * mat[col][1];
                    f += cols[2][row] * mat[col][2];

                    colValues[col] = f;
                }

                cols[0][row] = colValues[0];
                cols[1][row] = colValues[1];
                cols[2][row] = colValues[2];

            }

            return *this;
        }

        //-----------------------------------------------------------------------------

        template<class T>
        TMatrix44<T> TMatrix44<T>::createRotationMatrix(char axis, T rad) {
            TMatrix44<T> rotM;
            T sinA, cosA;

            sinA = (T) sin(rad);
            cosA = (T) cos(rad);

            switch (axis) {
                case 'x':
                case 'X':
                    rotM[0][0] = 1.0F;
                    rotM[1][0] = 0.0F;
                    rotM[2][0] = 0.0F;
                    rotM[0][1] = 0.0F;
                    rotM[1][1] = cosA;
                    rotM[2][1] = -sinA;
                    rotM[0][2] = 0.0F;
                    rotM[1][2] = sinA;
                    rotM[2][2] = cosA;
                    break;

                case 'y':
                case 'Y':
                    rotM[0][0] = cosA;
                    rotM[1][0] = 0.0F;
                    rotM[2][0] = sinA;
                    rotM[0][1] = 0.0F;
                    rotM[1][1] = 1.0F;
                    rotM[2][1] = 0.0F;
                    rotM[0][2] = -sinA;
                    rotM[1][2] = 0.0F;
                    rotM[2][2] = cosA;
                    break;

                case 'z':
                case 'Z':
                    rotM[0][0] = cosA;
                    rotM[1][0] = -sinA;
                    rotM[2][0] = 0.0F;
                    rotM[0][1] = sinA;
                    rotM[1][1] = cosA;
                    rotM[2][1] = 0.0F;
                    rotM[0][2] = 0.0F;
                    rotM[1][2] = 0.0F;
                    rotM[2][2] = 1.0F;
                    break;
            }

            rotM[0][3] = 0.0F;
            rotM[1][3] = 0.0F;
            rotM[2][3] = 0.0F;
            rotM[3][0] = 0.0F;
            rotM[3][1] = 0.0F;
            rotM[3][2] = 0.0F;
            rotM[3][3] = 1.0F;
            return rotM;
        }

        template<class T>
        TMatrix44<T> TMatrix44<T>::createRotationMatrix(const TVector3<T> &axis, T rad) {
            TMatrix44<T> result;
            T sinA, cosA;
            T invCosA;
            TVector3<T> nrm = axis;
            T x, y, z;
            T xSq, ySq, zSq;

            nrm.normalize();
            sinA = (T) sin(rad);
            cosA = (T) cos(rad);
            invCosA = 1.0F - cosA;

            x = nrm.x;
            y = nrm.y;
            z = nrm.z;

            xSq = x * x;
            ySq = y * y;
            zSq = z * z;

            result[0][0] = (invCosA * xSq) + (cosA);
            result[1][0] = (invCosA * x * y) - (sinA * z);
            result[2][0] = (invCosA * x * z) + (sinA * y);
            result[3][0] = 0.0F;

            result[0][1] = (invCosA * x * y) + (sinA * z);
            result[1][1] = (invCosA * ySq) + (cosA);
            result[2][1] = (invCosA * y * z) - (sinA * x);
            result[3][1] = 0.0F;

            result[0][2] = (invCosA * x * z) - (sinA * y);
            result[1][2] = (invCosA * y * z) + (sinA * x);
            result[2][2] = (invCosA * zSq) + (cosA);
            result[3][2] = 0.0F;

            result[0][3] = 0.0F;
            result[1][3] = 0.0F;
            result[2][3] = 0.0F;
            result[3][3] = 1.0F;

            return result;
        }

        template<class T>
        bool TMatrix44<T>::invert() {
            TMatrix44<T> b = TMatrix44<T>::createIdentityMatrix();

            unsigned int r, col;
            unsigned int cc;
            unsigned int rowMax;
            unsigned int row;
            T tmp;

            for (col = 0; col < 4; col++) {

                rowMax = col;
                for (r = col + 1; r < 4; r++) {
                    if (fabs(cols[col][r]) > fabs(cols[col][rowMax])) {
                        rowMax = r;
                    }
                }

                if (cols[rowMax][col] == 0.0F) {
                    return false;
                }

                for (cc = 0; cc < 4; cc++) {
                    tmp = cols[cc][col];
                    cols[cc][col] = cols[cc][rowMax];
                    cols[cc][rowMax] = tmp;
                    tmp = b[cc][col];
                    b[cc][col] = b[cc][rowMax];
                    b[cc][rowMax] = tmp;
                }

                tmp = cols[col][col];
                for (cc = 0; cc < 4; cc++) {
                    cols[cc][col] /= tmp;
                    b[cc][col] /= tmp;
                }

                for (row = 0; row < 4; row++) {
                    if (row != col) {
                        tmp = cols[col][row];
                        for (cc = 0; cc < 4; cc++) {
                            cols[cc][row] -= cols[cc][col] * tmp;
                            b[cc][row] -= b[cc][col] * tmp;
                        }
                    }
                }

            }

            *this = b;
            return true;
        }

        template<class T>
        TMatrix44<T> & TMatrix44<T>::operator*=(const TMatrix44<T>& mat) {
            for (unsigned int row = 0; row < 4; row++) {
                float colValues[4];
                for (unsigned int col = 0; col < 4; col++) {
                    T f = cols[0][row] * mat[col][0];
                    f += cols[1][row] * mat[col][1];
                    f += cols[2][row] * mat[col][2];
                    f += cols[3][row] * mat[col][3];

                    colValues[col] = f;
                }

                cols[0][row] = colValues[0];
                cols[1][row] = colValues[1];
                cols[2][row] = colValues[2];
                cols[3][row] = colValues[3];
            }

            return *this;
        }


        // Return a "lookat" matrix44 given the current camera position (Vector3<T>),
        //   camera-up Vector3<T>, and camera-target Vector3<T>.

        template<class T>
        TMatrix44<T> TMatrix44<T>::createLookAtMatrix(const TVector3<T>& camPos, const TVector3<T>& target,
        const TVector3<T>& camUp) {
            TMatrix44<T> result;

            TVector3<T> f = target - camPos;
            f.normalize();

            TVector3<T> camUpNormalized(camUp);
            camUpNormalized.normalize();
            TVector3<T> s = f.crossProduct(camUpNormalized);
            s.normalize();

            TVector3<T> u = s.crossProduct(f);
            u.normalize();

            result[0][0] = s.x;
            result[1][0] = s.y;
            result[2][0] = s.z;
            result[3][0] = 0.0;

            result[0][1] = u.x;
            result[1][1] = u.y;
            result[2][1] = u.z;
            result[3][1] = 0.0;

            result[0][2] = -f.x;
            result[1][2] = -f.y;
            result[2][2] = -f.z;
            result[3][2] = 0.0;

            result[0][3] = 0.0F;
            result[1][3] = 0.0F;
            result[2][3] = 0.0F;
            result[3][3] = 1.0F;

            result *= TMatrix44<T>::createTranslationMatrix(-camPos.x, -camPos.y, -camPos.z);

            return result;
        }

        // Return a frustum matrix44 given the left, right, bottom, top,
        //   near, and far values for the frustum boundaries.

        template<class T>
        TMatrix44<T> TMatrix44<T>::createFrustumMatrix(T l, T r,
        T b, T t, T n, T f) {
            TMatrix44<T> result;
            T width = r - l;
            T height = t - b;
            T depth = f - n;

            result[0][0] = (2 * n) / width;
            result[0][1] = 0.0F;
            result[0][2] = 0.0F;
            result[0][3] = 0.0F;

            result[1][0] = 0.0F;
            result[1][1] = (2 * n) / height;
            result[1][2] = 0.0F;
            result[1][3] = 0.0F;

            result[2][0] = (r + l) / width;
            result[2][1] = (t + b) / height;
            result[2][2] = -(f + n) / depth;
            result[2][3] = -1.0F;

            result[3][0] = 0.0F;
            result[3][1] = 0.0F;
            result[3][2] = -(2 * f * n) / depth;
            result[3][3] = 0.0F;

            return result;
        }

        // Return a perspective matrix44 given the field-of-view in the Y
        //   direction in degrees, the aspect ratio of Y/X, and near and
        //   far plane distances.

        template<class T>
        TMatrix44<T> TMatrix44<T>::createPerspectiveMatrix(T fovY, T aspect, T n, T f) {
            TMatrix44<T> result;
            T angle;
            T cot;

            angle = fovY / 2.0F;
            angle = rev::math::toRadians(angle);

            cot = (T) cos(angle) / (T) sin(angle);

            result[0][0] = cot / aspect;
            result[0][1] = 0.0F;
            result[0][2] = 0.0F;
            result[0][3] = 0.0F;

            result[1][0] = 0.0F;
            result[1][1] = cot;
            result[1][2] = 0.0F;
            result[1][3] = 0.0F;

            result[2][0] = 0.0F;
            result[2][1] = 0.0F;
            result[2][2] = -(f + n) / (f - n);
            result[2][3] = -1.0F;


            result[3][0] = 0.0F;
            result[3][1] = 0.0F;
            result[3][2] = -(2 * f * n) / (f - n);
            result[3][3] = 0.0F;

            return result;
        }

        // Return an orthographic matrix44 given the left, right, bottom, top,
        //   near, and far values for the frustum boundaries.

        template<class T>
        TMatrix44<T> TMatrix44<T>::createOrthoMatrix(T l, T r,
        T b, T t, T n, T f) {
            TMatrix44<T> result;
            T width = r - l;
            T height = t - b;
            T depth = f - n;

            result[0][0] = 2.0F / width;
            result[0][1] = 0.0F;
            result[0][2] = 0.0F;
            result[0][3] = 0.0F;

            result[1][0] = 0.0F;
            result[1][1] = 2.0F / height;
            result[1][2] = 0.0F;
            result[1][3] = 0.0F;

            result[2][0] = 0.0F;
            result[2][1] = 0.0F;
            result[2][2] = -(2.0F) / depth;
            result[2][3] = 0.0F;

            result[3][0] = -(r + l) / width;
            result[1][3] = -(t + b) / height;
            result[3][2] = -(f + n) / depth;
            result[3][3] = 1.0F;

            return result;
        }

        // Return an orientation matrix using 3 basis normalized vectors

        template<class T>
        TMatrix44<T> TMatrix44<T>::createOrthoNormalMatrix(const TVector3<T> &xdir,
        const TVector3<T> &ydir, const TVector3<T> &zdir) {
            TMatrix44<T> ret;

            ret[0] = (TVector4<T>) xdir;
            ret[1] = (TVector4<T>) ydir;
            ret[2] = (TVector4<T>) zdir;
            ret[3].set(0.0, 0.0, 0.0, 1.0);

            return ret;
        }

        template<class T>
        std::string TMatrix44<T>::toString() {
            std::ostringstream oss("\n");
            for (unsigned int row = 0; row < 4; row++) {
                oss << "\t[ ";
                for (unsigned int col = 0; col < 4; col++) {
                    oss << cols[col][row] << " ";
                }
                oss << "]\n";
            }

            return oss.str();
        }

        template<class T>
        std::string TMatrix44<T>::toStringFull() const {
            std::ostringstream oss;
            oss << "\nMatrix44@" << this << ":\n";
            for (unsigned int row = 0; row < 4; row++) {
                oss << "\t[ ";
                for (unsigned int col = 0; col < 4; col++) {
                    oss << cols[col][row] << " ";
                }
                oss << "]\n";
            }

            return oss.str();
        }

        typedef TMatrix33<float> Matrix33f;
        typedef TMatrix33<double> Matrix33d;

        typedef TMatrix44<float> Matrix44f;
        typedef TMatrix44<double> Matrix44d;
    } // end namespace graph
} // end namespace rev


#endif	/* REVMATRIX_H */

