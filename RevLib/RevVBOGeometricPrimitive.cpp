/* 
 * File:   RevVBOGeometricPrimitive.cpp
 * Author: Revers
 * 
 * Created on 24 październik 2011, 17:07
 */

#include <cassert>
#include <iostream>
#include "RevVBOGeometricPrimitive.h"

using namespace std;
using namespace rev::ogl;
using rev::graph::Vector3f;

#ifndef FALSE
#define FALSE 0
#endif

#define VERTEX_VAR_NAME "InVertex"
#define NORMAL_VAR_NAME "InNormal"
#define COLOR_VAR_NAME "DiffuseColor"
#define TRANSLATION_VAR_NAME "Translation"
#define LIGHT_POS_VAR_NAME "LightPosition"

#define STRINGIFY(A) #A

const char* vertexShaderSrc = STRINGIFY(
        in vec3 InVertex; // normal
        in vec3 InNormal;

        uniform vec3 Translation = vec3(0.0, 0.0, 0.0);
        uniform vec3 LightPosition = vec3(100.0, 10.0, -100.0); // glLightSource

        smooth out vec3 Normal;
        smooth out vec3 LightDir;
        //varying vec3 vertex_to_light_vector; 

        void main() {
    // Transforming The Vertex 
    vec4 vert = vec4(InVertex + Translation, 1.0);

            gl_Position = gl_ModelViewProjectionMatrix * vert;

            // Transforming The Normal To ModelView-Space 
            //normal = gl_NormalMatrix * gl_Normal;
            Normal = gl_NormalMatrix * InNormal;

            // Transforming The Vertex Position To ModelView-Space 
            vec4 vertexInModelView = gl_ModelViewMatrix * vert;
            //vec4 vertexInModelView = vert;

            // Calculating The Vector From The Vertex Position To The Light Position 
            LightDir = vec3(LightPosition - vertexInModelView.xyz);
}

);

const char* fragmentShaderSrc = STRINGIFY(
        smooth in vec3 Normal;
        smooth in vec3 LightDir;

        uniform vec4 AmbientColor = vec4(0.1, 0.1, 0.1, 1.0);
        uniform vec4 DiffuseColor = vec4(1.0, 0.0, 0.0, 1.0);

        out vec4 FragColor;

        void main() {
    vec3 eyeNormal = normalize(Normal);
            vec3 lightVector = normalize(LightDir);

            float DiffuseTerm = clamp(dot(eyeNormal, lightVector), 0.0, 1.0);

            FragColor = // DiffuseColor;
            AmbientColor + DiffuseColor * DiffuseTerm;
}
);

// printShaderInfoLog
// From OpenGL Shading Language 3rd Edition, p215-216
// Display (hopefully) useful error messages if shader fails to compile

void printShaderInfoLog(GLint shader) {
    int infoLogLen = 0;
    int charsWritten = 0;
    GLchar *infoLog;

    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLen);

    if (infoLogLen > 0) {
        infoLog = new GLchar[infoLogLen];
        // error check for fail to allocate memory omitted
        glGetShaderInfoLog(shader, infoLogLen, &charsWritten, infoLog);
        cout << "InfoLog : " << endl << infoLog << endl;
        delete [] infoLog;
    }
}

Shader* VBOGeometricPrimitive::createDefaultShader() {
    GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
    GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

    glShaderSource(vertexShader, 1, (const GLchar **) &vertexShaderSrc, 0);
    glShaderSource(fragmentShader, 1, (const GLchar **) &fragmentShaderSrc, 0);

    GLint compiled;

    glCompileShader(vertexShader);
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &compiled);
    if (compiled == FALSE) {
        cout << "Vertex shader not compiled." << endl;
        printShaderInfoLog(vertexShader);

        glDeleteShader(vertexShader);
        vertexShader = 0;
        glDeleteShader(fragmentShader);
        fragmentShader = 0;

        return NULL;
    }

    glCompileShader(fragmentShader);
    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &compiled);
    if (compiled == FALSE) {
        cout << "Fragment shader not compiled." << endl;
        printShaderInfoLog(fragmentShader);

        glDeleteShader(vertexShader);
        vertexShader = 0;
        glDeleteShader(fragmentShader);
        fragmentShader = 0;

        return NULL;
    }

    GLuint shaderProgram = glCreateProgram();

    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);

    glBindAttribLocation(shaderProgram, VERTEX_INDEX, VERTEX_VAR_NAME);
    glBindAttribLocation(shaderProgram, NORMAL_INDEX, NORMAL_VAR_NAME);

    glLinkProgram(shaderProgram);

    GLint IsLinked;
    glGetProgramiv(shaderProgram, GL_LINK_STATUS, (GLint *) & IsLinked);
    if (IsLinked == FALSE) {
        cout << "Failed to link shader." << endl;

        GLint maxLength;
        glGetProgramiv(shaderProgram, GL_INFO_LOG_LENGTH, &maxLength);
        if (maxLength > 0) {
            char *pLinkInfoLog = new char[maxLength];
            glGetProgramInfoLog(shaderProgram, maxLength, &maxLength, pLinkInfoLog);
            cout << pLinkInfoLog << endl;
            delete [] pLinkInfoLog;
        }

        glDetachShader(shaderProgram, vertexShader);
        glDetachShader(shaderProgram, fragmentShader);
        glDeleteShader(vertexShader);
        vertexShader = 0;
        glDeleteShader(fragmentShader);
        fragmentShader = 0;
        glDeleteProgram(shaderProgram);
        shaderProgram = 0;

        return NULL;
    }

    return new Shader(vertexShader, fragmentShader, shaderProgram);
}

void VBOGeometricPrimitive::loadShaderSettings() {
    glUseProgram(shader->shaderProgram);
    //Bind the VAO
    glBindVertexArray(vaoId);

    glUniform4f(colorLoc, color.r, color.g, color.b, 1.0f);
    glUniform3f(translationLoc, translation.x, translation.y, translation.z);
    glUniform3f(lightPosLoc, lightPosition.x, lightPosition.y, lightPosition.z);
}

void VBOGeometricPrimitive::draw() {
    // glUniform4f(colorLoc, color.r, color.g, color.b, 1.0f);
    //Draw command
    //The first to last vertex is 0 to 3
    //3 indices will be used to render 1 triangle.
    //The last parameter is the start address in the IBO => zero
    glDrawRangeElements(GL_TRIANGLES, 0, verticesCount, indicesCount, GL_UNSIGNED_SHORT, NULL);
}

void VBOGeometricPrimitive::setTranslation(GLfloat tx, GLfloat ty, GLfloat tz) {
    glUniform3f(translationLoc, tx, ty, tz);
    translation.set(tx, ty, tz);
}

void VBOGeometricPrimitive::setLightPosition(GLfloat tx, GLfloat ty, GLfloat tz) {
    glUniform3f(lightPosLoc, tx, ty, tz);
    lightPosition.set(tx, ty, tz);
}

void VBOGeometricPrimitive::setColor(float r, float g, float b) {
    color.set(r, g, b);
    glUniform4f(colorLoc, color.r, color.g, color.b, 1.0f);
}

void printGlError(GLenum error, const char* msg) {
    switch (error) {

        case GL_INVALID_ENUM:
            cout << "[GL_INVALID_ENUM] " << msg << endl;
            break;
        case GL_INVALID_VALUE:
            cout << "[GL_INVALID_VALUE] " << msg << endl;
            break;
        case GL_INVALID_OPERATION:
            cout << "[GL_INVALID_OPERATION] " << msg << endl;
            break;
        case GL_INVALID_FRAMEBUFFER_OPERATION:
            cout << "[GL_INVALID_FRAMEBUFFER_OPERATION] " << msg << endl;
            break;
        case GL_OUT_OF_MEMORY:
            cout << "[GL_OUT_OF_MEMORY] " << msg << endl;
            break;
    }
}

#define HANDLE_ERROR(MSG, BUFF_TYPE) error = glGetError(); \
        if(error != GL_NO_ERROR) { \
        printGlError(error, (MSG)); \
        glBindBuffer(BUFF_TYPE, 0); \
        return false; } 

#define BUFFER_OFFSET(i) ((char *)NULL + (i))

bool VBOGeometricPrimitive::initializeVBO() {

    //    for(int i = 0; i < vertices->size(); i++) {
    //        VertexPNT& vert = (*vertices)[i];
    //        cout << "[" << i << "] x = " << vert.x << "; y = " << vert.y
    //                << "; z = " << vert.z << endl;
    //    }

    GLenum error = GL_NO_ERROR;

    //Create the IBO for the quad
    //16 bit indices
    glGenBuffers(1, &iboId);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, iboId);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices->size() * sizeof (GLushort),
            indices->data(), GL_STATIC_DRAW);

    HANDLE_ERROR("Cannot create Index Buffer Object!", GL_ELEMENT_ARRAY_BUFFER);

    //Create VBO for the quad
    glGenBuffers(1, &vboId);
    glBindBuffer(GL_ARRAY_BUFFER, vboId);
    glBufferData(GL_ARRAY_BUFFER, vertices->size() * sizeof (VertexPNT), vertices->data(),
            GL_STATIC_DRAW);

    HANDLE_ERROR("Cannot create Vertex Buffer Object!", GL_ARRAY_BUFFER);

    //VAO for the quad *********************
    glGenVertexArrays(1, &vaoId);
    glBindVertexArray(vaoId);

    //Bind the VBO and setup pointers for the VAO
    glBindBuffer(GL_ARRAY_BUFFER, vboId);
    glVertexAttribPointer(VERTEX_INDEX, 3, GL_FLOAT, GL_FALSE, sizeof (VertexPNT), BUFFER_OFFSET(0));
    glVertexAttribPointer(NORMAL_INDEX, 3, GL_FLOAT, GL_FALSE, sizeof (VertexPNT), BUFFER_OFFSET(sizeof (float) *3));

    glEnableVertexAttribArray(VERTEX_INDEX);
    glEnableVertexAttribArray(NORMAL_INDEX);

    //Bind the IBO for the VAO
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, iboId);

    //Just testing
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);


    assert(indices->size() % 3 == 0);
    trianglesCount = indices->size() / 3;
    verticesCount = vertices->size();
    indicesCount = indices->size();

    delete indices;
    delete vertices;
    indices = NULL;
    vertices = NULL;

    colorLoc = glGetUniformLocation(shader->shaderProgram, COLOR_VAR_NAME);
    translationLoc = glGetUniformLocation(shader->shaderProgram, TRANSLATION_VAR_NAME);
    lightPosLoc = glGetUniformLocation(shader->shaderProgram, LIGHT_POS_VAR_NAME);

    return true;
}