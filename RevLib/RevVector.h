/*
 * File:   RevVector.h
 * Author: Revers
 *
 * Created on 29 październik 2010, 16:51
 */

#ifndef REVVECTOR_H
#define	REVVECTOR_H

#include <string>
#include <sstream>
#include <cmath>
//#include <cassert>



namespace rev {
    namespace graph {

        template<class T>
        class TVector2 {
        public:
            T x;
            T y;

        public:

            TVector2() : x(0), y(0) {
            }

            TVector2(T x, T y) : x(x), y(y) {
            }

            TVector2(const TVector2<T>& vect) : x(vect.x), y(vect.y) {
            }

        public:

            T* dataPointer() {
                return &x;
            }

            static TVector2<T> createZeroVector() {
                return TVector2<T > (0, 0);
            }

            static TVector2<T> createOneVector() {
                return TVector2<T > (1, 1);
            }

            static T dotProduct(const TVector2<T>& a, const TVector2<T>& b) {
                return a.dotProduct(b);
            }

            T & operator[](unsigned int i) {
                // assert(i < 2);
                return *(&x + i);
            }

            const T & operator[](unsigned int i) const {
                // assert(i < 2);
                return *(&x + i);
            }

            std::string toString() const {
                std::ostringstream oss;
                oss << "[ x = " << x << ", y = " << y
                        << " ]";
                return oss.str();
            }

            std::string toStringFull() const {
                std::ostringstream oss;
                oss << "Vector2@" << this << "[ x = " << x
                        << ", y = " << y << " ]";

                return oss.str();
            }

            void set(T x, T y) {
                this->x = x;
                this->y = y;
            }

            T length() const {
                return sqrt(x * x + y * y);
            }

            T lengthSqr() const {
                return (x * x + y * y);
            }

            void add(const TVector2<T>& vect) {
                this->x += vect.x;
                this->y += vect.y;
            }

            void add(const TVector2<T>* vect) {
                this->x += vect->x;
                this->y += vect->y;
            }

            void add(T x, T y) {
                this->x += x;
                this->y += y;
            }

            void subtract(const TVector2<T>& vect) {
                this->x -= vect.x;
                this->y -= vect.y;
            }

            void subtract(const TVector2<T>* vect) {
                this->x -= vect->x;
                this->y -= vect->y;
            }

            void subtract(T x, T y) {
                this->x -= x;
                this->y -= y;
            }

            void add(T val) {
                this->x += val;
                this->y += val;
            }

            void subtract(T val) {
                this->x -= val;
                this->y -= val;
            }

            void multiply(T val) {
                this->x *= val;
                this->y *= val;
            }

            void divide(T val) {
                this->x /= val;
                this->y /= val;
            }

            void normalize() {
                T len = length();
                if (len != 0.0f)
                    divide(len);
            }

            bool isZero() {
                return ((x == 0.0f) && (y == 0.0f));
            }

            void negate() {
                x = -x;
                y = -y;
            }

            T dotProduct(const TVector2<T>* vect) const {
                return (this->x * vect->x + this->y * vect->y);
            }

            T dotProduct(const TVector2<T>& vect) const {
                return (this->x * vect.x + this->y * vect.y);
            }

            bool operator==(const TVector2<T>& vect) {
                return ((this->x == vect.x) && (this->y == vect.y));
            }

            bool operator!=(const TVector2<T>& vect) {
                return ((this->x != vect.x) || (this->y != vect.y));
            }

            friend TVector2<T> operator-(const TVector2<T>& vect) {
                return TVector2<T > (-vect.x, -vect.y);
            }

            TVector2<T> operator+(T f) const {
                return TVector2<T > (this->x + f, this->y + f);
            }

            TVector2<T> operator-(T f) const {
                return TVector2<T > (this->x - f, this->y - f);
            }

            TVector2<T> operator+(const TVector2<T>& vect) const {
                return TVector2<T > (this->x + vect.x, this->y + vect.y);
            }

            TVector2<T> operator-(const TVector2<T>& vect) const {
                return TVector2<T > (this->x - vect.x, this->y - vect.y);
            }

            friend TVector2<T> operator*(const TVector2<T>& vect, T val) {
                return TVector2<T > (vect.x * val, vect.y * val);
            }

            friend TVector2<T> operator*(T val, const TVector2<T>& vect) {
                return TVector2<T > (vect.x * val, vect.y * val);
            }

            TVector2<T> operator/(T val) const {
                return TVector2<T > (this->x / val, this->y / val);
            }

            TVector2<T> & operator+=(const TVector2<T>& vect) {
                add(vect);
                return *this;
            }

            TVector2<T> & operator-=(const TVector2<T>& vect) {
                subtract(vect);
                return *this;
            }

            TVector2<T> & operator+=(T val) {
                add(val);
                return *this;
            }

            TVector2<T> & operator-=(T val) {
                subtract(val);
                return *this;
            }

            TVector2<T> & operator*=(T val) {
                multiply(val);
                return *this;
            }

            TVector2<T> & operator/=(T val) {
                divide(val);
                return *this;
            }

            TVector2<T> & operator=(const TVector2<T>& v) {
                x = v.x;
                y = v.y;
                return *this;
            }

        };
        //---------------------------------------------------------------------

        template<class T>
        class TVector3 {
        public:
            T x;
            T y;
            T z;

        public:

            TVector3() : x(0), y(0), z(0) {
            }

            TVector3(T x, T y, T z) : x(x), y(y), z(z) {
            }

            TVector3(const TVector3<T>& vect) {
                this->x = vect.x;
                this->y = vect.y;
                this->z = vect.z;
            }

            explicit TVector3(const TVector2<T>& vect) : x(vect.x), y(vect.y),
            z(0.0f) {
            }

        public:

            T* dataPointer() {
                return &x;
            }

            static TVector3<T> createZeroVector() {
                return TVector3<T > (0, 0, 0);
            }

            static TVector3<T> createOneVector() {
                return TVector3<T > (1, 1, 1);
            }

            static T dotProduct(const TVector3<T>& a, const TVector3<T>& b) {
                return a.dotProduct(b);
            }

            static TVector3<T> crossProduct(const TVector3<T>& a, const TVector3<T>& b) {
                return a.crossProduct(b);
            }

            T & operator[](unsigned int i) {
                // assert(i < 3);
                return *(&x + i);
            }

            const T & operator[](unsigned int i) const {
                // assert(i < 3);
                return *(&x + i);
            }

            void set(T x, T y, T z) {
                this->x = x;
                this->y = y;
                this->z = z;
            }

            std::string toString() const {
                std::ostringstream oss;
                oss << "[ x = " << x << ", y = " << y
                        << ", z = " << z << " ]";
                return oss.str();
            }

            std::string toStringFull() const {
                std::ostringstream oss;
                oss << "Vector3@" << this << "[ x = " << x
                        << ", y = " << y << ", z = " << z << " ]";
                return oss.str();
            }

            T length() const {
                return sqrt(x * x + y * y + z * z);
            }

            T lengthSqr() const {
                return (x * x + y * y + z * z);
            }

            void add(const TVector3<T>& vect) {
                this->x += vect.x;
                this->y += vect.y;
                this->z += vect.z;
            }

            void add(const TVector3<T>* vect) {
                this->x += vect->x;
                this->y += vect->y;
                this->z += vect->z;
            }

            void add(T x, T y, T z) {
                this->x += x;
                this->y += y;
                this->z += z;
            }

            void subtract(const TVector3<T>& vect) {
                subtract(&vect);
            }

            void subtract(const TVector3<T>* vect) {
                this->x -= vect->x;
                this->y -= vect->y;
                this->z -= vect->z;
            }

            void subtract(T x, T y, T z) {
                this->x -= x;
                this->y -= y;
                this->z -= z;
            }

            void add(T val) {
                this->x += val;
                this->y += val;
                this->z += val;
            }

            void subtract(T val) {
                this->x -= val;
                this->y -= val;
                this->z -= val;
            }

            void multiply(T val) {
                this->x *= val;
                this->y *= val;
                this->z *= val;
            }

            void divide(T val) {
                this->x /= val;
                this->y /= val;
                this->z /= val;
            }

            void normalize() {
                T len = length();
                if (len != 0.0f)
                    divide(len);
            }

            T dotProduct(const TVector3<T>* vect) const {
                return (this->x * vect->x + this->y * vect->y + this->z * vect->z);
            }

            T dotProduct(const TVector3<T>& vect) const {
                return (this->x * vect.x + this->y * vect.y + this->z * vect.z);
            }

            TVector3<T> crossProduct(const TVector3<T>* vect) const {
                // A = this;  B = vect
                //            | A_y  A_z |    | A_x  A_z |  | A_x  A_y |
                // RESULT = [ |          |, - |          |, |          | ]
                //            | B_y  B_z |    | B_x  B_z |  | B_x  B_y |

                T resX = this->y * vect->z - this->z * vect->y;
                T resY = this->z * vect->x - this->x * vect->z;
                T resZ = this->x * vect->y - this->y * vect->x;
                return TVector3<T > (resX, resY, resZ);
            }

            TVector3<T> crossProduct(const TVector3<T>& vect) const {
                // A = this;  B = vect
                //            | A_y  A_z |    | A_x  A_z |  | A_x  A_y |
                // RESULT = [ |          |, - |          |, |          | ]
                //            | B_y  B_z |    | B_x  B_z |  | B_x  B_y |

                T resX = this->y * vect.z - this->z * vect.y;
                T resY = this->z * vect.x - this->x * vect.z;
                T resZ = this->x * vect.y - this->y * vect.x;
                return TVector3<T > (resX, resY, resZ);
            }

            bool operator==(const TVector3<T>& vect) {
                return ((this->x == vect.x) && (this->y == vect.y) && (this->z == vect.z));
            }

            bool operator!=(const TVector3<T>& vect) {
                return ((this->x != vect.x) || (this->y != vect.y) || (this->y != vect.y));
            }

            friend TVector3<T> operator-(const TVector3<T>& vect) {
                return TVector3<T > (-vect.x, -vect.y, -vect.z);
            }

            TVector3<T> operator+(T f) const {
                return TVector3<T > (this->x + f, this->y + f,
                        this->z + f);
            }

            TVector3<T> operator-(T f) const {
                return TVector3<T > (this->x - f, this->y - f,
                        this->z - f);
            }

            TVector3<T> operator+(const TVector3<T>& vect) const {
                return TVector3<T > (this->x + vect.x, this->y + vect.y,
                        this->z + vect.z);
            }

            TVector3<T> operator-(const TVector3<T>& vect) const {
                return TVector3<T > (this->x - vect.x, this->y - vect.y,
                        this->z - vect.z);
            }

            //            TVector3<T> operator*(const TVector3<T>& vect) const {
            //                return crossProduct(&vect);
            //            }

            friend TVector3<T> operator*(const TVector3<T>& vect, T val) {
                return TVector3<T > (vect.x * val, vect.y * val,
                        vect.z * val);
            }

            friend TVector3<T> operator*(T val, const TVector3<T>& vect) {
                return TVector3<T > (vect.x * val, vect.y * val,
                        vect.z * val);
            }

            TVector3<T> operator/(T val) const {
                return TVector3<T > (this->x / val, this->y / val,
                        this->z / val);
            }

            TVector3<T> & operator+=(const TVector3<T>& vect) {
                add(vect);
                return *this;
            }

            TVector3<T> & operator-=(const TVector3<T>& vect) {
                subtract(vect);
                return *this;
            }

            TVector3<T> & operator+=(T val) {
                add(val);
                return *this;
            }

            TVector3<T> & operator-=(T val) {
                subtract(val);
                return *this;
            }

            TVector3<T> & operator*=(T val) {
                multiply(val);
                return *this;
            }

            TVector3<T> & operator/=(T val) {
                divide(val);
                return *this;
            }

            TVector3<T> & operator=(const TVector2<T>& v) {
                x = v.x;
                y = v.y;
                z = 0.0F;
                return *this;
            }

            TVector3<T> & operator=(const TVector3<T>& v) {
                x = v.x;
                y = v.y;
                z = v.z;
                return *this;
            }

            bool isZero() {
                return ((x == 0.0f) && (y == 0.0f) && (z == 0.0f));
            }

            void negate() {
                x = -x;
                y = -y;
                z = -z;
            }
        };

        //---------------------------------------------------------------------

        template<class T>
        class TVector4 {
        public:
            T x;
            T y;
            T z;
            T w;

        public:

            TVector4() : x(0), y(0), z(0) {
            }

            TVector4(T x, T y, T z, T w) : x(x), y(y),
            z(z), w(w) {
            }

            TVector4(const TVector4<T>& vect) {
                this->x = vect.x;
                this->y = vect.y;
                this->z = vect.z;
                this->w = vect.w;
            }

            explicit TVector4(const TVector2<T>& vect) : x(vect.x), y(vect.y),
            z(0.0f), w(1.0f) {
            }

            explicit TVector4(const TVector3<T>& vect) : x(vect.x), y(vect.y),
            z(vect.z), w(1.0f) {
            }

        public:

            T* dataPointer() {
                return &x;
            }

            static TVector4<T> createZeroVector() {
                return TVector4<T > (0, 0, 0, 0);
            }

            static TVector4<T> createOneVector() {
                return TVector4<T > (1, 1, 1, 1);
            }

            static T dotProduct(const TVector4<T>& a, const TVector4<T>& b) {
                return a.dotProduct(b);
            }

            T & operator[](unsigned int i) {
                // assert(i < 4);
                return *(&x + i);
            }

            const T & operator[](unsigned int i) const {
                // assert(i < 4);
                return *(&x + i);
            }

            void set(T x, T y, T z, T w) {
                this->x = x;
                this->y = y;
                this->z = z;
                this->w = w;
            }

            std::string toString() const {
                std::ostringstream oss;
                oss << "[ x = " << x << ", y = " << y
                        << ", z = " << z
                        << ", w = " << w << " ]";
                return oss.str();
            }

            std::string toStringFull() const {
                std::ostringstream oss;
                oss << "Vector4@" << this << "[ x = " << x
                        << ", y = " << y << ", z = " << z
                        << ", w = " << w << " ]";
                return oss.str();
            }

            T length() const {
                return sqrt(x * x + y * y + z * z + w * w);
            }

            T lengthSqr() const {
                return (x * x + y * y + z * z + w * w);
            }

            void add(const TVector4<T>& vect) {
                this->x += vect.x;
                this->y += vect.y;
                this->z += vect.z;
                this->w += vect.w;
            }

            void add(const TVector4<T>* vect) {
                this->x += vect->x;
                this->y += vect->y;
                this->z += vect->z;
                this->w += vect->w;
            }

            void add(T x, T y, T z, T w) {
                this->x += x;
                this->y += y;
                this->z += z;
                this->w += w;
            }

            void subtract(const TVector4<T>& vect) {
                this->x -= vect.x;
                this->y -= vect.y;
                this->z -= vect.z;
                this->w -= vect.w;
            }

            void subtract(const TVector4<T>* vect) {
                this->x -= vect->x;
                this->y -= vect->y;
                this->z -= vect->z;
                this->w -= vect->w;
            }

            void subtract(T x, T y, T z, T w) {
                this->x -= x;
                this->y -= y;
                this->z -= z;
                this->w -= w;
            }

            void add(T val) {
                this->x += val;
                this->y += val;
                this->z += val;
                this->w += val;
            }

            void subtract(T val) {
                this->x -= val;
                this->y -= val;
                this->z -= val;
                this->w -= val;
            }

            void multiply(T val) {
                this->x *= val;
                this->y *= val;
                this->z *= val;
                this->w *= val;
            }

            void divide(T val) {
                this->x /= val;
                this->y /= val;
                this->z /= val;
                this->w /= val;
            }

            void normalize() {
                T len = length();
                if (len != 0.0f)
                    divide(len);
            }

            bool isZero() {
                return ((x == 0.0f) && (y == 0.0f) && (z == 0.0f) && (w == 0.0f));
            }

            void negate() {
                x = -x;
                y = -y;
                z = -z;
                w = -z;
            }

            T dotProduct(const TVector4<T>* vect) const {
                return (this->x * vect->x + this->y * vect->y
                        + this->z * vect->z + this->w * vect->w);
            }

            T dotProduct(const TVector4<T>& vect) const {
                return (this->x * vect.x + this->y * vect.y
                        + this->z * vect.z + this->w * vect.w);
            }

            bool operator==(const TVector4<T>& vect) {
                return ((this->x == vect.x) && (this->y == vect.y)
                        && (this->z == vect.z) && (this->w == vect.w));
            }

            bool operator!=(const TVector4<T>& vect) {
                return ((this->x != vect.x) || (this->y != vect.y)
                        || (this->y != vect.y) || (this->w != vect.w));
            }

            friend TVector4<T> operator-(const TVector4<T>& vect) {
                return TVector4<T > (-vect.x, -vect.y, -vect.z, -vect.w);
            }

            TVector4<T> operator+(T f) const {
                return TVector4<T > (this->x + f, this->y + f,
                        this->z + f, this->w + f);
            }

            TVector4<T> operator-(T f) const {
                return TVector4<T > (this->x - f, this->y - f,
                        this->z - f, this->w - f);
            }

            TVector4<T> operator+(const TVector4<T>& vect) const {
                return TVector4<T > (this->x + vect.x, this->y + vect.y,
                        this->z + vect.z, this->w + vect.w);
            }

            TVector4<T> operator-(const TVector4<T>& vect) const {
                return TVector4<T > (this->x - vect.x, this->y - vect.y,
                        this->z - vect.z, this->w - vect.w);
            }

            friend TVector4<T> operator*(const TVector4<T>& vect, T val) {
                return TVector4<T > (vect.x * val, vect.y * val,
                        vect.z * val, vect.w * val);
            }

            friend TVector4<T> operator*(T val, const TVector4<T>& vect) {
                return TVector4<T > (vect.x * val, vect.y * val,
                        vect.z * val, vect.w * val);
            }

            TVector4<T> operator/(T val) const {
                return TVector4<T > (this->x / val, this->y / val,
                        this->z / val, this->w / val);
            }

            TVector4<T> & operator+=(const TVector4<T>& vect) {
                add(vect);
                return *this;
            }

            TVector4<T> & operator-=(const TVector4<T>& vect) {
                subtract(vect);
                return *this;
            }

            TVector4<T> & operator+=(T val) {
                add(val);
                return *this;
            }

            TVector4<T> & operator-=(T val) {
                subtract(val);
                return *this;
            }

            TVector4<T> & operator*=(T val) {
                multiply(val);
                return *this;
            }

            TVector4<T> & operator/=(T val) {
                divide(val);
                return *this;
            }

            // Assign from a vector3

            TVector4<T> & operator=(const TVector3<T>& v) {
                x = v.x;
                y = v.y;
                z = v.z;
                w = 0.0F;
                return *this;
            }

            TVector4<T> & operator=(const TVector2<T>& v) {
                x = v.x;
                y = v.y;
                z = 0.0F;
                w = 0.0F;
                return *this;
            }
        };

        typedef TVector2<float> Vector2f;
        typedef TVector2<double> Vector2d;

        typedef TVector3<float> Vector3f;
        typedef TVector3<double> Vector3d;

        typedef TVector4<float> Vector4f;
        typedef TVector4<double> Vector4d;

    } // end namespace graph
} // end namespace rev


#endif	/* REVVECTOR_H */

