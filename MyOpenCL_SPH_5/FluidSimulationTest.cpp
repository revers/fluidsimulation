/* 
 * File:   FluidSimulationTestTest.cpp
 * Author: Revers
 * 
 * Created on 13 listopad 2011, 19:35
 */

#include <iostream>
#include <cassert>

#include "FluidSimulation.h"
#include "FluidSimulationTest.h"

#include "CLBufferImage2D.h"

#include "Parameters.h"

#define SORT_Z_INDEX(A) (((A) * SORT_CELL_SIZE) + 1)

using namespace std;

FluidSimulationTest::FluidSimulationTest(FluidSimulation* fluidSimulation_) :
fluidSimulation(fluidSimulation_) {
    bool succ = tempBuffer.init(fluidSimulation->clContext,
            fluidSimulation->clCommandQueue,
            sizeof (cl_uint) * 2 * fluidSimulation->fluidParameters->getParticleCount(),
            "TEMP_BUFFER");
    assert(succ);

    cl_int err;
    kernelTestNeighbourBlockSearch = cl::Kernel(fluidSimulation->clProgram, "testNeighbourBlockSearch", &err);
    assert(err == CL_SUCCESS);

    kernelTestImage2D = cl::Kernel(fluidSimulation->clProgram, "testImage2D", &err);
    assert(err == CL_SUCCESS);

    neighborLookupTest = cl::Kernel(fluidSimulation->clProgram, "testNeighborLookup", &err);
    assert(err == CL_SUCCESS);
}

void FluidSimulationTest::testPrintInterleveLookup() {
    cout << "starting testPrintInterleveLookup()" << endl;
    const unsigned int size = fluidSimulation->interleaveLookupBuffer.getSize() / (sizeof (cl_uint));
    cl_uint* buffer = new cl_uint[size];

    fluidSimulation->interleaveLookupBuffer.read((void*) buffer);

    fluidSimulation->getClCommandQueue().finish();

    for (unsigned i = 0; i < size / INTERLEAVE_CELL_SIZE; i++) {

        cl_uint lookupX = buffer[i * INTERLEAVE_CELL_SIZE];
        cl_uint lookupY = buffer[i * INTERLEAVE_CELL_SIZE + 1];
        cl_uint lookupZ = buffer[i * INTERLEAVE_CELL_SIZE + 2];

        cout << "#" << i << ") LOOKUP: x = "
                << lookupX << "; y = "
                << lookupY << "; z = "
                << lookupZ << endl;
    }

    delete[] buffer;
}

bool FluidSimulationTest::testNeighbourBlockSearch() {

    cl_int err;
    // interleaveLookupBuffer:
    err = kernelTestNeighbourBlockSearch.setArg(0, fluidSimulation->interleaveLookupBuffer.buffer);
    assert(err == CL_SUCCESS);

    // blockCoordsLookup:
    err = kernelTestNeighbourBlockSearch.setArg(1, fluidSimulation->blockCoordsLookupBuffer.buffer);
    assert(err == CL_SUCCESS);

    // positionBuffer:
    err = kernelTestNeighbourBlockSearch.setArg(2, fluidSimulation->positionPPBuffer.getFirst().buffer);
    assert(err == CL_SUCCESS);

    // blockBuffer:
    err = kernelTestNeighbourBlockSearch.setArg(3, fluidSimulation->radixSortPPBuffer.getSecond().buffer);
    assert(err == CL_SUCCESS);

    // compactBlockBuffer:
    err = kernelTestNeighbourBlockSearch.setArg(4, fluidSimulation->radixSortPPBuffer.getFirst().buffer);
    assert(err == CL_SUCCESS);

    // densityBuffer:
    err = kernelTestNeighbourBlockSearch.setArg(5, fluidSimulation->densityBuffer.buffer);
    assert(err == CL_SUCCESS);

    // velocityBuffer:
    err = kernelTestNeighbourBlockSearch.setArg(6, fluidSimulation->velocityPPBuffer.getFirst().buffer);
    assert(err == CL_SUCCESS);

    // localNeighborPositionBuffer:
    err = kernelTestNeighbourBlockSearch.setArg(7, sizeof (cl_float4) * 32, NULL);
    assert(err == CL_SUCCESS);

    // localNeighborBlockBuffer:
    err = kernelTestNeighbourBlockSearch.setArg(8, sizeof (cl_uint2) * 32, NULL);
    assert(err == CL_SUCCESS);

    // computationData:
    err = kernelTestNeighbourBlockSearch.setArg(9, sizeof (DensityComputationData), (void*) (&fluidSimulation->densityComputationData));
    assert(err == CL_SUCCESS);

    // tempBuffer:
    err = kernelTestNeighbourBlockSearch.setArg(10, tempBuffer.buffer);
    assert(err == CL_SUCCESS);

    // myBlockNumber:
    err = kernelTestNeighbourBlockSearch.setArg(11, (cl_int) 28);
    assert(err == CL_SUCCESS);

    cout << "Used Work Items: " << fluidSimulation->usedWorkItems << endl;
    fluidSimulation->clCommandQueue.enqueueNDRangeKernel(
            kernelTestNeighbourBlockSearch, cl::NullRange, cl::NDRange(fluidSimulation->usedWorkItems),
            cl::NDRange((int) (MAXIMUM_PARTICLES_PER_BLOCK)), NULL, NULL);


    const unsigned int size = tempBuffer.getSize() / (sizeof (cl_uint));
    cl_uint* buffer = new cl_uint[size];

    bool succ = tempBuffer.read((void*) buffer);
    assert(succ);

    fluidSimulation->getClCommandQueue().finish();

    const int iterations = 27; // size / 2;
    cl_uint neighborBlocks = 0;
    cl_uint neighborParticles = 0;
    for (int i = 0; i < iterations; i++) {
        cl_uint first = buffer[i * 2];
        cl_uint second = buffer[i * 2 + 1];
        if (first != 0) {
            neighborBlocks++;
            neighborParticles += first;
        }
        //  cout << "#" << i << " first = " << first << "; second = " << second << endl;
    }
#define P(A) (#A) << " = " << (A) << "; "    

    cout << P(neighborBlocks) << P(neighborParticles) << endl;

    cl_uint first = buffer[27 * 2];
    cl_uint second = buffer[27 * 2 + 1];
    cout << "workGroupId = " << first << "; second = " << second << endl;

}

void FluidSimulationTest::testPrintBlockCoordsLookup() {
    const unsigned int size = fluidSimulation->blockCoordsLookupBuffer.getSize() / (sizeof (cl_uint));
    cl_uint* buffer = new cl_uint[size];

    fluidSimulation->blockCoordsLookupBuffer.read((void*) buffer);

    fluidSimulation->getClCommandQueue().finish();

    for (unsigned i = 0; i < size / BLOCK_LOOKUP_CELL_SIZE; i++) {

        cl_uint lookupX = buffer[i * BLOCK_LOOKUP_CELL_SIZE];
        cl_uint lookupY = buffer[i * BLOCK_LOOKUP_CELL_SIZE + 1];
        cl_uint lookupZ = buffer[i * BLOCK_LOOKUP_CELL_SIZE + 2];

        cout << "#" << i << ") BLOCK LOOKUP: x = "
                << lookupX << "; y = "
                << lookupY << "; z = "
                << lookupZ << endl;
    }

    delete[] buffer;
}

void FluidSimulationTest::testPrintDensity() {
    const unsigned int size = fluidSimulation->densityBuffer.getSize() / (sizeof (cl_float));
    cl_float* buffer = new cl_float[size];

    fluidSimulation->densityBuffer.read((void*) buffer);

    fluidSimulation->getClCommandQueue().finish();

    cl_uint counter = 0;
    for (unsigned i = 0; i < size / 2; i++) {

        cl_float density = buffer[i * 2];

        cl_float densityInv = buffer[i * 2 + 1];

        if (densityInv != 1.0f) {
            counter++;
        }

        cout << "#" << i << ") density = "
                << density << "; densityInv = "
                << densityInv << endl;
    }

    cout << "COUNTER = " << counter << endl;

    delete[] buffer;
}

bool FluidSimulationTest::checkZIndex(cl_uint val) {
    cl_uint x, y, z;
    fluidSimulation->restoreInterleave(val, &x, &y, &z);
    bool succ = true;

    //        if (x % BLOCK_SIZE != 0) {
    //            cout << "ERROR: x % BLOCK_SIZE != 0; " << x << endl;
    //            return false;
    //        }
    //
    //        if (y % BLOCK_SIZE != 0) {
    //            cout << "ERROR: y % BLOCK_SIZE != 0; " << y << endl;
    //            return false;
    //        }
    //
    //        if (z % BLOCK_SIZE != 0) {
    //            cout << "ERROR: z % BLOCK_SIZE != 0; " << z << endl;
    //            return false;
    //        }

    if (x >= GRID_DIMENSION_X) {
        cout << "ERROR: x >= GRID_DIMENSION_X; " << x << endl;
        return false;
    }

    if (y >= GRID_DIMENSION_X) {
        cout << "ERROR: y >= GRID_DIMENSION_X; " << y << endl;
        return false;
    }

    if (z >= GRID_DIMENSION_X) {
        cout << "ERROR: z >= GRID_DIMENSION_X; " << z << endl;
        return false;
    }

    return succ;
}

void FluidSimulationTest::testPrintTempBuffer() {
    const unsigned int size = tempBuffer.getSize() / (sizeof (cl_uint));
    cl_uint* buffer = new cl_uint[size];

    tempBuffer.read((void*) buffer);

    fluidSimulation->getClCommandQueue().finish();

    for (unsigned i = 0; i < size / 2; i++) {

        cl_uint first = buffer[i * 2];
        cl_uint second = buffer[i * 2 + 1];

        if (first == 0 && second == 0) {
            continue;
        }
        //            cout << "#" << i << ") particleCount = "
        //                    << first << "; firstParticleIndex = "
        //                    << second << endl;

        //    if(first == 3131313131) {
        //      break;
        //     }
        if (!checkZIndex(second)) {
            cout << "#" << i << ") particleCount = "
                    << first << "; firstParticleIndex = "
                    << second << endl;
            // break;
        }
    }

    cl_uint max = 0xFFFFFFFF;
    cout << "max = " << max << endl;

    delete[] buffer;
}

void FluidSimulationTest::testPrintHashParticles() {
    const unsigned int size = fluidSimulation->radixSortPPBuffer.getFirst().getSize() / (sizeof (cl_uint));
    cl_uint* buffer = new cl_uint[size];

    fluidSimulation->radixSortPPBuffer.readFirst((void*) buffer);

    fluidSimulation->getClCommandQueue().finish();

    for (unsigned i = 0; i < size / SORT_CELL_SIZE; i++) {

        cl_uint particleSerialIndex = buffer[i * SORT_CELL_SIZE];
        //            if (particleSerialIndex == 0) {
        //                continue;
        //            }
        cl_uint particleZIndex = buffer[i * SORT_CELL_SIZE + 1];
        cout << "#" << i << ") serialIndex = "
                << particleSerialIndex << "; zIndex = "
                << particleZIndex << endl;
    }

    delete[] buffer;
}

void FluidSimulationTest::testPrintSortedParticles() {
    cout << "testPrintSortedParticles()" << endl;
    const unsigned int size = fluidSimulation->radixSortPPBuffer.getFirst().getSize() / (sizeof (cl_uint));
    cl_uint* buffer = new cl_uint[size];

    fluidSimulation->radixSortPPBuffer.readFirst((void*) buffer);

    fluidSimulation->getClCommandQueue().finish();

    for (unsigned i = 0; i < size / SORT_CELL_SIZE; i++) {

        cl_uint particleSerialIndex = buffer[i * SORT_CELL_SIZE];

        cl_uint particleZIndex = buffer[i * SORT_CELL_SIZE + 1];
        cout << "#" << i << ") serialIndex = "
                << particleSerialIndex << "; zIndex = "
                << particleZIndex << endl;
    }

    delete[] buffer;
}

bool FluidSimulationTest::testAfterBlockAndInterleaveLookupLoaded() {
    fluidSimulation->getClCommandQueue().finish();
    cout << "starting testAfterBlockAndInterleaveLookupLoaded()" << endl;
    bool failed = false;

    const unsigned int interleaveSize = fluidSimulation->interleaveLookupBuffer.getSize() / (sizeof (cl_uint));
    cl_uint* interleaveBuffer = new cl_uint[interleaveSize];

    fluidSimulation->interleaveLookupBuffer.read((void*) interleaveBuffer);

    fluidSimulation->getClCommandQueue().finish();

    const unsigned int blockSize = fluidSimulation->blockCoordsLookupBuffer.getSize() / (sizeof (cl_uint));
    cl_uint* blockBuffer = new cl_uint[blockSize];

    fluidSimulation->blockCoordsLookupBuffer.read((void*) blockBuffer);

    fluidSimulation->getClCommandQueue().finish();
    
    FluidParameters fluidParameters = fluidSimulation->getFluidParameters();

    for (unsigned i = 0; i < blockSize / BLOCK_LOOKUP_CELL_SIZE; i++) {

        cl_uint lookupX = blockBuffer[i * BLOCK_LOOKUP_CELL_SIZE];
        cl_uint lookupY = blockBuffer[i * BLOCK_LOOKUP_CELL_SIZE + 1];
        cl_uint lookupZ = blockBuffer[i * BLOCK_LOOKUP_CELL_SIZE + 2];

        cl_uint expectedVal = i * fluidParameters.getBlockSize3D();

        cl_uint spreadedX = interleaveBuffer[lookupX * INTERLEAVE_CELL_SIZE];
        cl_uint spreadedY = interleaveBuffer[lookupY * INTERLEAVE_CELL_SIZE + 1];
        cl_uint spreadedZ = interleaveBuffer[lookupZ * INTERLEAVE_CELL_SIZE + 2];

        cl_uint val = spreadedX | spreadedY | spreadedZ;
        if (val != expectedVal) {
            failed = true;
            cout << "ERROR: TEST BLOCK AND INTERLEVAVE FAILED!" << endl;
            cout << "#" << i << ") BLOCK LOOKUP: x = "
                    << lookupX << "; y = "
                    << lookupY << "; z = "
                    << lookupZ << endl;
            cout << "expectedVal = " << expectedVal << "; val = " << val << endl;
            break;
        }

        //assert(val == expectedVal);


    }

    delete[] blockBuffer;
    delete[] interleaveBuffer;

    if (failed) {
        cout << "testAfterBlockAndInterleaveLookupLoaded() FAILED! :(" << endl;
    } else {
        cout << "testAfterBlockAndInterleaveLookupLoaded() SUCCEED! :)" << endl;
    }

    return !failed;
}

void FluidSimulationTest::printBinaryInt(unsigned int i) {
    for (int j = 31; j >= 0; j--) {

        if ((j + 1) % 4 == 0) {
            cout << " ";
        }

        unsigned int shift = 1 << j;

        if ((shift & i) != 0) {
            cout << "1";
        } else {
            cout << "0";
        }
    }
    cout << endl;
}

bool FluidSimulationTest::testAfterRunHashParticles() {

    fluidSimulation->getClCommandQueue().finish();
    cout << "statrting testAfterRunHashParticles()" << endl;
    const unsigned int size = fluidSimulation->radixSortPPBuffer.getFirst().getSize() / (sizeof (cl_uint));
    cl_uint* buffer = new cl_uint[size];
    
        FluidParameters fluidParameters = fluidSimulation->getFluidParameters();

    fluidSimulation->radixSortPPBuffer.readFirst((void*) buffer);
   
    cl_uint maxZIndexValue = fluidParameters.getGridPoints() - 1;

    fluidSimulation->getClCommandQueue().finish();

    bool failed = false;


    for (unsigned i = 0; i < size / SORT_CELL_SIZE; i++) {

        cl_uint particleSerialIndex = buffer[i * SORT_CELL_SIZE];
        //            if (particleSerialIndex == 0) {
        //                continue;
        //            }
        cl_uint particleZIndex = buffer[i * SORT_CELL_SIZE + 1];

        cl_uint blockNo = particleZIndex / fluidParameters.getBlockSize3D();
        if (blockNo >= fluidParameters.getBlockCount()) {
            cout << "ERROR: (blockNo(" << blockNo << ") >= fluidSimulation->getFluidParameters()->blockCount("
                    << fluidParameters.getBlockCount() << ") " << endl;
            cout << "\tfor particle P(" << i << ").serialIndex = "
                    << particleSerialIndex << "; P(" << i << ").zIndex = "
                    << particleZIndex << " > maxZIndexValue(" << maxZIndexValue << ")" << endl;
            failed = true;
            break;
        }

        if (particleZIndex > maxZIndexValue) {
            cout << "ERROR: P(" << i << ").serialIndex = "
                    << particleSerialIndex << "; P(" << i << ").zIndex = "
                    << particleZIndex << " > maxZIndexValue(" << maxZIndexValue << ")" << endl;

            float fx = fluidSimulation->positionArray[i * 4];
            float fy = fluidSimulation->positionArray[i * 4 + 1];
            float fz = fluidSimulation->positionArray[i * 4 + 2];

            unsigned int x = (int) fx;
            unsigned int y = (int) fy;
            unsigned int z = (int) fz;

            cout << "\t P(" << i << ").x = " << fx
                    << "; y = " << fy
                    << "; z = " << fz << endl;

            if (x >= fluidParameters.getGridDimensionX()) {
                cout << "\tCaused by x(" << x << ") >= fluidSimulation->getFluidParameters()->gridDimensionX(" 
                        << fluidParameters.getGridDimensionX() << ")" << endl;
            }

            if (y >= fluidParameters.getGridDimensionY()) {
                cout << "\tCaused by y(" << y << ") >= fluidSimulation->getFluidParameters()->gridDimensionY("
                        << fluidParameters.getGridDimensionY() << ")" << endl;
            }

            if (z >= fluidParameters.getGridDimensionZ()) {
                cout << "\tCaused by z(" << z << ") >= fluidSimulation->getFluidParameters()->gridDimensionZ("
                        << fluidParameters.getGridDimensionZ() << ")" << endl;
            }

            unsigned int val = ((fluidSimulation->spreadBits(x)) | (fluidSimulation->spreadBits(y) << 1) | (fluidSimulation->spreadBits(z) << 2));
            cout << "ERROR: val = " << val << endl;
            cout << "v:";
            printBinaryInt(val);

            unsigned int gridPoints = fluidParameters.getGridPoints();
            cout << "gridPoints(" << gridPoints << ")\ng:";
            printBinaryInt(gridPoints);

            failed = true;
            break;
        }
    }

    delete[] buffer;

    if (failed) {
        cout << "testAfterRunHashParticles() FAILED :(" << endl;
    } else {
        cout << "testAfterRunHashParticles() SUCCEED :)" << endl;
    }

    return !failed;
}

bool FluidSimulationTest::testAfterRunSort() {
    fluidSimulation->getClCommandQueue().finish();
    cout << "statrting testAfterRunSort()" << endl;
    const unsigned int size = fluidSimulation->radixSortPPBuffer.getFirst().getSize() / (sizeof (cl_uint));
    cl_uint* buffer = new cl_uint[size];

    fluidSimulation->radixSortPPBuffer.readFirst((void*) buffer);

    fluidSimulation->getClCommandQueue().finish();

    bool failed = false;

    unsigned int iterations = fluidSimulation->getParticleCount() / (sizeof (cl_uint)) / SORT_CELL_SIZE - 1;
    for (unsigned i = 0; i < iterations; i++) {

        //cl_uint particleASerialIndex = buffer[i * SORT_CELL_SIZE];
        cl_uint particleAZIndex = buffer[i * SORT_CELL_SIZE];
        //cl_uint particleBSerialIndex = buffer[(i + 1) * SORT_CELL_SIZE];
        cl_uint particleBZIndex = buffer[(i + 1) * SORT_CELL_SIZE];

        //           cout << "P(" << i << ").zIndex = " << particleAZIndex
        //                    << "  P(" << (i + 1) << ").zIndex = " << particleBZIndex << endl;
        if (!(particleAZIndex <= particleBZIndex)) {
            cout << "ERROR: P(" << i << ").zIndex = " << particleAZIndex
                    << " > P(" << (i + 1) << ").zIndex = " << particleBZIndex << endl;
            failed = true;
            break;
        }
    }

    delete[] buffer;

    if (failed) {
        cout << "testAfterRunSort() FAILED :(" << endl;
    } else {
        cout << "testAfterRunSort() SUCCEED :)" << endl;
    }

    return !failed;
}

bool FluidSimulationTest::testAfterRunCreateCompactBlockListDetailed() {
    cout << "starting testAfterRunCreateCompactBlockListDetailed()" << endl;
    fluidSimulation->getClCommandQueue().finish();

    if (!fluidSimulation->positionPPBuffer.readFirst(fluidSimulation->positionArray)) {
        cout << "ERROR: cannot read positionPPBuffer buffer!!" << endl;
        cout << "testAfterRunCreateBlockListDetailed() FAILED! :(" << endl;
        return false;
    }

    if (!fluidSimulation->gpu2cpuDataBuffer.read(fluidSimulation->gpu2cpuArray)) {
        cout << "ERROR: cannot read gpu2cpuArray buffer!!" << endl;
        cout << "testAfterRunCreateBlockListDetailed() FAILED! :(" << endl;
        return false;
    }

    const cl_uint compactBlockListSize = fluidSimulation->gpu2cpuArray[0];

    cout << "Compact block list size = " << compactBlockListSize << endl;
    fluidSimulation->getClCommandQueue().finish();

    const unsigned int size = fluidSimulation->radixSortPPBuffer.getSize() / (sizeof (cl_uint));
    cl_uint* buffer = new cl_uint[size];

    //radixSortPPBuffer.getFirst().buffer
    fluidSimulation->radixSortPPBuffer.readFirst((void*) buffer);

    cl_uint totalParticles = 0;

    bool failed = false;
    cl_uint xmax = 0;
    cl_uint ymax = 0;
    cl_uint zmax = 0;

    for (unsigned i = 0; i < compactBlockListSize; i++) {
        cl_uint particleCount = buffer[i * BLOCK_CELL_SIZE];
        cl_uint firstParticleIndex = buffer[i * BLOCK_CELL_SIZE + 1];

        float* firstParticle = &(fluidSimulation->positionArray[firstParticleIndex * 4]);
        cl_uint zIndex = *((cl_uint*) (&firstParticle[3]));
        cl_uint thisBlockIndex = zIndex / BLOCK_SIZE_3D;

        cl_uint x, y, z;
        fluidSimulation->restoreInterleave(thisBlockIndex, &x, &y, &z);
        //        cout << "#" << i << ") particleCount = " << particleCount
        //                << "; firstParticleIndex = " << firstParticleIndex << "; blockCoords [" << x
        //                << ", " << y << ", " << z << "]" << endl;

        xmax = xmax < x ? x : xmax;
        ymax = ymax < y ? y : ymax;
        zmax = zmax < z ? z : xmax;

        if (particleCount > MAXIMUM_PARTICLES_PER_BLOCK) {
            cout << "ERROR: #" << i << ") particleCount(" << particleCount
                    << ") > MAXIMUM_PARTICLES_PER_BLOCK("
                    << MAXIMUM_PARTICLES_PER_BLOCK << ") " << endl;

            totalParticles = fluidSimulation->getParticleCount();
            failed = true;
            break;

        }
        totalParticles += particleCount;
    }

    cout << "xmax = " << xmax << "; ymax = " << ymax << "; zmax = " << zmax << endl;

    if (totalParticles != fluidSimulation->getParticleCount()) {
        cout << "ERROR: totalParticles("
                << totalParticles << ") != fluidSimulation->getParticleCount()("
                << fluidSimulation->getParticleCount() << ")" << endl;
        failed = true;
    }

    delete[] buffer;

    if (failed) {
        cout << "testAfterRunCreateCompactBlockListDetailed() FAILED! :(" << endl;
    } else {
        cout << "testAfterRunCreateCompactBlockListDetailed() SUCCEED! :)" << endl;
    }

    return !failed;
}

bool FluidSimulationTest::testAfterRunCreateCompactBlockList() {
    cout << "starting testAfterRunCreateCompactBlockList()" << endl;
    fluidSimulation->getClCommandQueue().finish();

    if (!fluidSimulation->gpu2cpuDataBuffer.read(fluidSimulation->gpu2cpuArray)) {
        cout << "ERROR: cannot read gpu2cpuArray buffer!!" << endl;
        cout << "testAfterRunCreateBlockList() FAILED! :(" << endl;
        return false;
    }

    const cl_uint compactBlockListSize = fluidSimulation->gpu2cpuArray[0];

    cout << "Compact block list size = " << compactBlockListSize << endl;
    fluidSimulation->getClCommandQueue().finish();

    const unsigned int size = fluidSimulation->radixSortPPBuffer.getSize() / (sizeof (cl_uint));
    cl_uint* buffer = new cl_uint[size];

    //radixSortPPBuffer.getFirst().buffer
    fluidSimulation->radixSortPPBuffer.readFirst((void*) buffer);

    cl_uint totalParticles = 0;

    bool failed = false;

    for (unsigned i = 0; i < compactBlockListSize; i++) {
        cl_uint particleCount = buffer[i * BLOCK_CELL_SIZE];
        //  cl_uint firstParticle = buffer[i * BLOCK_CELL_SIZE + 1];

        if (particleCount > MAXIMUM_PARTICLES_PER_BLOCK) {
            cout << "ERROR: #" << i << ") particleCount(" << particleCount
                    << ") > MAXIMUM_PARTICLES_PER_BLOCK("
                    << MAXIMUM_PARTICLES_PER_BLOCK << ") " << endl;

            totalParticles = fluidSimulation->getParticleCount();
            failed = true;
            break;

        }
        totalParticles += particleCount;
    }

    if (totalParticles != fluidSimulation->getParticleCount()) {
        cout << "ERROR: totalParticles("
                << totalParticles << ") != fluidSimulation->getParticleCount()("
                << fluidSimulation->getParticleCount() << ")" << endl;
        failed = true;
    }

    delete[] buffer;

    if (failed) {
        cout << "testAfterRunCreateCompactBlockList() FAILED! :(" << endl;
    } else {
        cout << "testAfterRunCreateCompactBlockList() SUCCEED! :)" << endl;
    }

    return !failed;
}

bool FluidSimulationTest::testAfterRunCreateBlockList() {
    fluidSimulation->getClCommandQueue().finish();
    cout << "starting testAfterRunCreateBlockList()" << endl;
    const unsigned int size = fluidSimulation->radixSortPPBuffer.getSize() / (sizeof (cl_uint));
    cl_uint* buffer = new cl_uint[size];

    fluidSimulation->radixSortPPBuffer.readSecond((void*) buffer);

    cl_uint minimumParticles = 99999999;
    cl_uint maximumParticles = 0;
    cl_uint totalParticles = 0;
    cl_uint firstNonZeroParticle = 0;

    cl_uint realSize = fluidSimulation->getFluidParameters().getBlockCount();

    for (unsigned i = 0; i < realSize; i++) {
        cl_uint particleCount = buffer[i * BLOCK_CELL_SIZE];

        if (particleCount < minimumParticles) {
            minimumParticles = particleCount;
        }

        if (particleCount > maximumParticles) {
            maximumParticles = particleCount;
        }

        totalParticles += particleCount;

        cl_uint firstParticle = buffer[i * BLOCK_CELL_SIZE + 1];

        if (firstNonZeroParticle == 0 && firstParticle != 0) {
            firstNonZeroParticle = firstParticle;
        }
        //            if (particleCount != 0)
        //                cout << "#" << i << ") particleCount = " << particleCount << "; firstParticle = " << firstParticle << endl;
    }

    cout << "TOTAL PARTICLES: " << totalParticles
            << "; MINIMUM PARTICLES: " << minimumParticles
            << "; MAXIMUM PARTICLES: " << maximumParticles
            << "; firstNonZeroParticle: " << firstNonZeroParticle << endl;

    bool failed = false;

    if (totalParticles != fluidSimulation->getParticleCount()) {
        cout << "ERROR: totalParticles("
                << totalParticles << ") != fluidSimulation->getParticleCount()("
                << fluidSimulation->getParticleCount() << ")" << endl;
        failed = true;
    }

    if (firstNonZeroParticle == 0) {
        cout << "ERROR: firstNonZeroParticle == 0" << endl;
        failed = true;
    }


    delete[] buffer;

    if (failed) {
        cout << "testAfterRunCreateBlockList() FAILED! :(" << endl;
    } else {
        cout << "testAfterRunCreateBlockList() SUCCEED! :)" << endl;
    }

    return !failed;
}

void FluidSimulationTest::runReadPositionAndCompareOld() {

    float* newPosition = new float[fluidSimulation->getParticleCount() * 4];
    bool succ = fluidSimulation->positionPPBuffer.readFirst(newPosition);
    assert(succ);
    fluidSimulation->getClCommandQueue().finish();


    for (int i = 0; i < fluidSimulation->getParticleCount(); i++) {

        //             float * positionVector = positionArray + 4 * i;
        //            
        //            float x = positionVector[0];
        //            float y = positionVector[1];
        //            float z = positionVector[2];
        //            float w = positionVector[3];

        float x = fluidSimulation->positionArray[4 * i];
        float y = fluidSimulation->positionArray[4 * i + 1];
        float z = fluidSimulation->positionArray[4 * i + 2];
        float w = fluidSimulation->positionArray[4 * i + 3];

        float nx = newPosition[4 * i];
        float ny = newPosition[4 * i + 1];
        float nz = newPosition[4 * i + 2];
        float nw = newPosition[4 * i + 3];



        if ((x != nx) || (y != ny) || (z != nz)) {// || (w != nw)) {
            cout << "#" << i << ": x: " << x << "; y: " << y
                    << "; z: " << z << "; w: " << w << endl;

            cout << "#" << i << ": nx: " << nx << "; ny: " << ny
                    << "; nz: " << nz << "; nw: " << nw << endl;

            break;
        }

    }

    delete[] newPosition;
}

void FluidSimulationTest::checkRadixSort() {
    cl_uint radixSortResult[fluidSimulation->radixSortPPBuffer.getSize()];

    //cout << "Construct the random list (MAX_INT = " << _MAXINT << ")" << endl;
    // construction of a random list
    cl_uint maxint = RAND_MAX;

    //    assert(_MAXINT != 0);
    for (cl_uint i = 0; i < fluidSimulation->fluidParameters->getParticleCount(); i++) {
        radixSortResult[SORT_Z_INDEX(i)] = ((rand()) % (maxint + 1));
    }

    if (!fluidSimulation->radixSortPPBuffer.writeFirst(radixSortResult)) {
        cout << "ERROR: cannot write buffer" << endl;
        return;
    }

    fluidSimulation->clCommandQueue.finish();


    fluidSimulation->radixSort->sort();

    cout << "Get the data from the GPU" << endl;

    if (!fluidSimulation->radixSortPPBuffer.readFirst(radixSortResult)) {
        cout << "ERROR: Cannot read buffer1!" << endl;
        return;
    }

    //recieveDataFromGPU(radixSortResult);
    fluidSimulation->clCommandQueue.finish();
    cout << "Test order" << endl;

    // first see if the final list is ordered
    for (cl_uint i = 0; i < fluidSimulation->fluidParameters->getParticleCount() - 1; i++) {
        if (!(radixSortResult[SORT_Z_INDEX(i)] <= radixSortResult[SORT_Z_INDEX(i + 1)])) {
            cout << "Error (#" << i << ") = " << radixSortResult[SORT_Z_INDEX(i)] <<
                    " , (#" << i + 1 << ") =  " << radixSortResult[SORT_Z_INDEX(i + 1) ] << endl;
        }
        assert(radixSortResult[SORT_Z_INDEX(i)] <= radixSortResult[SORT_Z_INDEX(i + 1)]);
    }

    cout << "test OK !" << endl;

    //    fluidSimulation->radixSort.printProfilingInfo();
}

#define PP(A) (#A) << "=" << (A)

bool FluidSimulationTest::testImage2DBuffer() {
    cout << "Starting testImage2DBuffer()..." << endl;
    const size_t size = 4096;

    CLBufferImage2D buffer;
    if (!buffer.init(UINT4, fluidSimulation->clContext, fluidSimulation->clCommandQueue,
            size * sizeof (cl_uint), 1024, "test", CL_MEM_READ_WRITE)) {
        return false;
    }

    cl_uint* in = new cl_uint[size];
    for (cl_uint i = 0; i < size; i++) {
        in[i] = i;
    }

    if (!buffer.write(in)) {
        delete[] in;
        return false;
    }

    fluidSimulation->clCommandQueue.finish();

    cl_uint* out = (cl_uint*) calloc(size, sizeof (cl_uint));

    if (!buffer.read(out)) {
        delete[] in;
        free(out);
        assert(false);
        return false;
    }

    for (cl_uint i = 0; i < size; i++) {
        //        cout << "#" << i << ") " << PP(in[i]) << "; " << PP(out[i]) << endl;

        if (in[i] != out[i]) {
            cout << "#" << i << ") " << PP(in[i]) << " != " << PP(out[i]) << endl;
            return false;
        }
    }


    cl_int err;

    err = kernelTestImage2D.setArg(0, tempBuffer.buffer);
    assert(err == CL_SUCCESS);

    err = kernelTestImage2D.setArg(1, buffer.buffer);
    assert(err == CL_SUCCESS);

    err = fluidSimulation->clCommandQueue.enqueueNDRangeKernel(
            kernelTestImage2D, cl::NullRange, cl::NDRange(1024),
            cl::NDRange(256), NULL, NULL);
    assert(err == CL_SUCCESS);

    fluidSimulation->clCommandQueue.finish();

    cout << PP(tempBuffer.getSize()) << endl;
    if (!tempBuffer.read(out, 0, size * sizeof (cl_uint))) {

        delete[] in;
        free(out);
        assert(false);
        return false;
    }


    for (cl_uint i = 0; i < size; i++) {
        //   cout << "#" << i << ") " << PP(in[i]) << "; " << PP(out[i]) << endl;

        if (in[i] != out[i]) {
            cout << "#" << i << ") " << PP(in[i]) << " != " << PP(out[i]) << endl;
            return false;
        }
    }

    delete[] in;
    free(out);

    cout << "testImage2DBuffer() SUCCEED! :) " << endl;
    return true;
}

bool FluidSimulationTest::testNeighborLookup() {

    cl_int err;

    err = neighborLookupTest.setArg(0, fluidSimulation->neighborLookupBuffer.buffer);
    assert(err == CL_SUCCESS);

    err = neighborLookupTest.setArg(1, tempBuffer.buffer);
    assert(err == CL_SUCCESS);



    err = fluidSimulation->clCommandQueue.enqueueNDRangeKernel(
            neighborLookupTest, cl::NullRange, cl::NDRange(32),
            cl::NDRange(32), NULL, NULL);
    assert(err == CL_SUCCESS);

    fluidSimulation->clCommandQueue.finish();

    cout << PP(tempBuffer.getSize()) << endl;

    cl_int* buf = new cl_int[tempBuffer.getSize() / sizeof (char) ];

    if (!tempBuffer.read(buf)) {
        delete buf;
        assert(false);
        return false;
    }

    for (int i = 0; i < 32; i++) {
        int x = buf[i * 4];
        int y = buf[i * 4 + 1];
        int z = buf[i * 4 + 2];
        int w = buf[i * 4 + 3];
        cout << i << ") " << PP(x) << "; " << PP(y) << "; " << PP(z) << "; " << PP(w) << endl;
    }

    cout << "testNeighborLookup() SUCCEED! :) " << endl;

    return true;
}

bool FluidSimulationTest::testStep() {
    
    if(!testNeighborLookup()) {
        cout << "testNeighborLookup() failed!!" << endl;
        return false;
    } 

    if (!testImage2DBuffer()) {
        cout << "Test image2d failed!!" << endl;
        return false;
    }

    fluidSimulation->runReadPosition();

    if (!testAfterBlockAndInterleaveLookupLoaded()) {
        return false;
    }

    fluidSimulation->runHashParticles();

    if (!testAfterRunHashParticles()) {
        return false;
    }

    fluidSimulation->runSort();

    if (!testAfterRunSort()) {
        return false;
    }

    fluidSimulation->runReorderPositionAndVelocity();

    cout << "TWICE: " << endl;
    fluidSimulation->runReadPosition();

    fluidSimulation->runClearSecondBuffer();

    fluidSimulation->runCreateBlockList();

    if (!testAfterRunCreateBlockList()) {
        return false;
    }

    fluidSimulation->runClearGpu2CpuBuffer();

    fluidSimulation->runCreateCompactBlockList();

    if (!testAfterRunCreateCompactBlockList()) {
        return false;
    }


    if (!testAfterRunCreateCompactBlockListDetailed()) {
        return false;
    }

    fluidSimulation->runReadUsedWorkGroups();

    //   testNeighbourBlockSearch();

    //    fluidSimulation->runComputeDensityAndPressure();
    //
    //    //    testPrintDensity();
    //    //  testPrintTempBuffer();
    //
    //    fluidSimulation->runComputeForcesAndIntegrate();

    //    runReadPositionAndCompareOld();
}
