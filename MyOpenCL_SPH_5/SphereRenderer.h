/* 
 * File:   SphereRenderer.h
 * Author: Revers
 *
 * Created on 18 grudzień 2011, 00:19
 */

#ifndef SPHERERENDERER_H
#define	SPHERERENDERER_H

#include "Renderer.h"

#include "FluidSimulation.h"
#include "ControlPanel.h"
#include "TargetCamera.h"

class SphereRenderer : public Renderer {

    enum DisplayMode {
        PARTICLE_POINTS,
        PARTICLE_SPHERES
    };

    enum DisplayLists {
        BOUNDARY_LIST = 1,
        GRID_LIST = 2,
        CENTER_LIST = 3
    };

    const float m_fHalfViewRadianFactor;
    int m_numParticles;
    float m_pointSize;
    float m_particleRadius;
    float m_fov;

    GLuint m_program;
    GLuint m_vbo;

    int windowWidth;
    int windowHeight;
public:

    SphereRenderer(ControlPanel* controlPanel_, TargetCamera* camera_) :
    Renderer(controlPanel_, camera_), m_fHalfViewRadianFactor(0.5f * 3.1415926535f / 180.0f) {
        m_numParticles = 0;
        m_pointSize = 1.0f;
        m_particleRadius = 0.984215f; // 0.125f * 4.0f;
        m_fov = 0.0f;

        m_program = 0;
        m_vbo = 0;
        
        windowWidth = 0;
        windowHeight = 0;
    }

    virtual bool init();

    virtual void render();

    virtual void windowSizeChanged(int newWidth, int newHeight) {
        windowWidth = newWidth;
        windowHeight = newHeight;
    }

    virtual ~SphereRenderer() {
    }

    void setFOV(float fov) {
        m_fov = fov;
    }

    float getFOV() const {
        return m_fov;
    }

private:
    SphereRenderer(const SphereRenderer& orig);

    GLuint compileProgram(const char *vsource, const char *fsource);

    void drawPoints();

    void display(DisplayMode mode /* = PARTICLE_POINTS */);

    void compileCenterList();

    void compileGridList();

    void compileBoundaryList();

};

#endif	/* SPHERERENDERER_H */

