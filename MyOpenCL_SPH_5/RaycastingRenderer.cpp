/* 
 * File:   RaycastingRenderer.cpp
 * Author: Revers
 * 
 * Created on 18 grudzień 2011, 01:15
 */

// GLEW and GLUT includes
#include <GL/glew.h>
#include <CL/cl_gl.h>
#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/freeglut_ext.h>
#include <GL/glext.h>

#include "RaycastingRenderer.h"
#include "FluidSimulationTest.h"

#include "SOIL.h"

using namespace std;

#define USE_CUBEMAP_RENDERER

//#define CUBE_MAP_HOT_DESSERT
#define CUBE_MAP_BRIGHTDAY
//#define CUBE_MAP_TERRAIN
//#define CUBE_MAP_PANORAMA
//#define CUBE_MAP_COLOR_GRADIENT
//#define CUBE_MAP_HILLS
//#define CUBE_MAP_DEBUG

#if defined (__APPLE__) || defined(MACOSX)
#define GL_SHARING_EXTENSION "cl_APPLE_gl_sharing"
#else
#define GL_SHARING_EXTENSION "cl_khr_gl_sharing"
#endif

#define KERNEL_FILE "MySPH_5_Raycaster.cl"
#define CXX_DIRECTORY "D:\\Revers\\NetBeansProjects\\MyOpenCL_FILES\\"

#define CHECK_ERR(A) if(!(A)) { assert((A)); return false; }

#define LOCAL_SIZE_X 16
#define LOCAL_SIZE_Y 16

#define MATRIX_SIZE 16

bool RaycastingRenderer::init() {
    if (!initCL()) {
        cout << "initCL() FAILED!!" << endl;
        return false;
    }

    if (!createProgramAndKernels()) {
        cout << "createProgramAndKernels() FAILED!!" << endl;
        return false;
    }

    if (!initCLVolume()) {
        cout << "initCLVolume() FAILED!!" << endl;
        return false;
    }

    if (!initPboRaycastinglBuffer()) {
        cout << "initBuffers() FAILED!!" << endl;
        return false;
    }

    //    generateImage();
    if (!init3dTex()) {
        cout << "init3dTex() FAILED!!" << endl;
        return false;
    }

    if (!loadCubeMap()) {
        cout << "loadCubeMap() FAILED!!" << endl;
    }

    setupTextureParameters();

    if (!initCubeMapBuffers()) {
        cout << "initCubeMapBuffers() FAILED!!" << endl;
    }

    if (!setFixedKernelArguments()) {
        cout << "setFixedKernelArguments() FAILED!!" << endl;
        return false;
    }

    return true;
}

bool RaycastingRenderer::initCubeMapBuffers() {

    cl_int err;
    cubeMapPosXBuffer = cl::Image2DGL(clContext, CL_MEM_READ_ONLY,
            GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, tex_cube, &err);
    CHECK_ERR(err == CL_SUCCESS);

    cubeMapNegXBuffer = cl::Image2DGL(clContext, CL_MEM_READ_ONLY,
            GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, tex_cube, &err);
    CHECK_ERR(err == CL_SUCCESS);

    cubeMapPosYBuffer = cl::Image2DGL(clContext, CL_MEM_READ_ONLY,
            GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, tex_cube, &err);
    CHECK_ERR(err == CL_SUCCESS);

    cubeMapNegYBuffer = cl::Image2DGL(clContext, CL_MEM_READ_ONLY,
            GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, tex_cube, &err);
    CHECK_ERR(err == CL_SUCCESS);

    cubeMapPosZBuffer = cl::Image2DGL(clContext, CL_MEM_READ_ONLY,
            GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, tex_cube, &err);
    CHECK_ERR(err == CL_SUCCESS);

    cubeMapNegZBuffer = cl::Image2DGL(clContext, CL_MEM_READ_ONLY,
            GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, tex_cube, &err);
    CHECK_ERR(err == CL_SUCCESS);

    return true;
}

void RaycastingRenderer::renderBoxFrame() {

    CLBufferPboGL& buff = controlPanel->fluidSimulation->getPboTex3dBuffer();
    buff.acquireGLObject();

    cl_int err = kernelDrawFrame.setArg(0, buff.buffer);
    assert(err == CL_SUCCESS);

    err = clCommandQueue.enqueueNDRangeKernel(
            kernelDrawFrame,
            cl::NullRange,
            cl::NDRange(IMAGE3D_BASE_LENGTH),
            cl::NDRange(IMAGE3D_BASE_LENGTH),
            NULL,
            NULL);

    assert(err == CL_SUCCESS);
    clCommandQueue.finish();

    buff.releaseGLObject();
}

void RaycastingRenderer::renderTexture3D() {

    if (controlPanel->drawBoundaries) {
        renderBoxFrame();
    }

    // bind the texture and PBO
    glBindTexture(GL_TEXTURE_3D, tex3dId);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, controlPanel->fluidSimulation->getPboTex3dBuffer().getPixelBufferObject());

    // copy pixels from PBO to texture object
    // Use offset instead of ponter.
    glTexSubImage3D(GL_TEXTURE_3D, 0, 0, 0, 0,
            IMAGE3D_WIDTH, IMAGE3D_HEIGHT, IMAGE3D_DEPTH, PIXEL_FORMAT, GL_UNSIGNED_INT, NULL);

    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
    glBindTexture(GL_TEXTURE_3D, 0);
}

void RaycastingRenderer::renderRaycastingPBO() {
    setTransform(camera->getTotalRotation(), 0, 0, camera->getCameraDistance());

    cl_int err = CL_SUCCESS;

    glFlush();

    //    acquireOpenGLContext();
    //        
    //    err = kernelCubmapRender.setArg(5, cubeMapPosXBuffer);
    //    assert(err == CL_SUCCESS);
    //
    //    void* buffLoc = (void*)&(cubeMapNegXBuffer());
    //    clSetKernelArg(kernelCubmapRender(), 6, sizeof(cl_mem), buffLoc);
    //    //err = kernelCubmapRender.setArg(6, cubeMapNegXBuffer);
    //    assert(err == CL_SUCCESS);
    //
    //    err = kernelCubmapRender.setArg(7, cubeMapPosYBuffer);
    //    assert(err == CL_SUCCESS);
    //
    //    err = kernelCubmapRender.setArg(8, cubeMapNegYBuffer);
    //    assert(err == CL_SUCCESS);
    //
    //    err = kernelCubmapRender.setArg(9, cubeMapPosZBuffer);
    //    assert(err == CL_SUCCESS);
    //
    //    err = kernelCubmapRender.setArg(10, cubeMapNegZBuffer);
    //    assert(err == CL_SUCCESS);

    pboRaycastingBuffer.acquireGLObject();

    bool succ = matrixBuffer.write(transformMatrix.dataPointer(), 0,
            MATRIX_SIZE * sizeof (float), CL_FALSE);
    assert(succ);

    // execute OpenCL kernel, writing results to PBO
    const size_t localSize[] = {LOCAL_SIZE_X, LOCAL_SIZE_Y};

#ifdef USE_CUBEMAP_RENDERER

    kernelCubmapRender.setArg(11, controlPanel->fluidSimulation->getLightDirection());

    err = clCommandQueue.enqueueNDRangeKernel(
            kernelCubmapRender, cl::NullRange, cl::NDRange(gridSize[0], gridSize[1]),
            cl::NDRange(localSize[0], localSize[1]), NULL, NULL);

    assert(err == CL_SUCCESS);

#else
    kernelPhongRender.setArg(6, controlPanel->fluidSimulation->getLightDirection());

    err = clCommandQueue.enqueueNDRangeKernel(
            kernelPhongRender, cl::NullRange, cl::NDRange(gridSize[0], gridSize[1]),
            cl::NDRange(localSize[0], localSize[1]), NULL, NULL);

    assert(err == CL_SUCCESS);
#endif

    pboRaycastingBuffer.releaseGLObject();

    // releaseOpenGLCOntext();
    clCommandQueue.finish();
}

void RaycastingRenderer::drawSurroundings() {
    static const GLfloat vertex[4 * 6][3] = {
        /* Positive X face. */
        { 1, -1, -1},
        { 1, 1, -1},
        { 1, 1, 1},
        { 1, -1, 1},
        /* Negative X face. */
        { -1, -1, -1},
        { -1, 1, -1},
        { -1, 1, 1},
        { -1, -1, 1},
        /* Positive Y face. */
        { -1, 1, -1},
        { 1, 1, -1},
        { 1, 1, 1},
        { -1, 1, 1},
        /* Negative Y face. */
        { -1, -1, -1},
        { 1, -1, -1},
        { 1, -1, 1},
        { -1, -1, 1},
        /* Positive Z face. */
        { -1, -1, 1},
        { 1, -1, 1},
        { 1, 1, 1},
        { -1, 1, 1},
        /* Negatieve Z face. */
        { -1, -1, -1},
        { 1, -1, -1},
        { 1, 1, -1},
        { -1, 1, -1},
    };

    const float surroundingsDistance = 100;
    int i;

    glPushMatrix();

    //glMultMatrixf(camera->getTotalRotation().toMatrix44().dataPointer());
    camera->apply();
    //    glLoadIdentity();
    //    gluLookAt(eyePosition[0], eyePosition[1], eyePosition[2],
    //            0, 0, 0,
    //            0, 1, 0);
    /* Scale the cube to be drawn by the desired surrounding distance. */
    //  glPushMatrix();
    glScalef(surroundingsDistance,
            surroundingsDistance,
            surroundingsDistance);

    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_CUBE_MAP);
    glBindTexture(GL_TEXTURE_CUBE_MAP, tex_cube);
    
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    glBegin(GL_QUADS);
    /* For each vertex of each face of the cube... */
    for (i = 0; i < 4 * 6; i++) {
        glTexCoord3fv(vertex[i]);
        glVertex3fv(vertex[i]);
    }
    glEnd();

    glDisable(GL_TEXTURE_CUBE_MAP);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);


    glPopMatrix();
}

void RaycastingRenderer::render() {
    drawSurroundings();

    if (controlPanel->fluidSimulation == NULL) {
        return;
    }

    // tempFillBuffer();
    renderTexture3D();
    renderRaycastingPBO();

    glMatrixMode(GL_PROJECTION);

    glPushMatrix();
    glLoadIdentity();

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    // draw image from PBO
    // glClear(GL_COLOR_BUFFER_BIT);
    glDisable(GL_DEPTH_TEST);
    //glRasterPos2i(xOffset, yOffset);
    //glRasterPos2i(-1, -1);
    glRasterPos2f(xOffsetF, yOffsetF);
    //    float posX = -1.0f;
    //    float posY = 0.0f;
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    //glRasterPos2f(posX, posY);
    // glRasterPos2f(0.5, 0.5);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, pboRaycastingBuffer.getPixelBufferObject());
    glDrawPixels(width, height, GL_RGBA, GL_UNSIGNED_BYTE, 0);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);

    glEnable(GL_DEPTH_TEST);
    glPopMatrix();
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
}

void RaycastingRenderer::windowSizeChanged(int newWidth, int newHeight) {
    int minVal = min(newWidth, newHeight);

    width = minVal;
    height = minVal;

    xOffset = (newWidth - width) >> 1; // divided by 2
    yOffset = (newHeight - height) >> 1; // divided by 2

    xOffsetF = -1.0f + (float) xOffset / (float) width;
    yOffsetF = -1.0f + (float) yOffset / (float) height;

    //    width = newWidth;
    //    height = newHeight;
    initPboRaycastinglBuffer();
}

void RaycastingRenderer::setTransform(rev::graph::Quaternionf& rotation, float translationX, float translationY, float translationZ) {
    float* mat = translationMatrix.dataPointer();
    mat[12] = translationX;
    mat[13] = translationY;
    mat[14] = translationZ;

    transformMatrix = rotation.toMatrix44() * translationMatrix;
}

size_t RaycastingRenderer::roundUp(int group_size, int global_size) {
    if (group_size == 0) return 0;
    int r = global_size % group_size;
    if (r == 0) {
        return global_size;
    } else {
        return global_size + group_size - r;
    }
}

bool RaycastingRenderer::initPboRaycastinglBuffer() {
    if (!pboRaycastingBuffer.init(clContext, clCommandQueue, width * height * sizeof (GLubyte) * 4,
            "PixelBuffer", CL_MEM_WRITE_ONLY)) {
        return false;
    }

    // calculate new grid size
    gridSize[0] = roundUp(LOCAL_SIZE_X, width);
    gridSize[1] = roundUp(LOCAL_SIZE_Y, height);

    cl_int err;
    err = kernelPhongRender.setArg(0, pboRaycastingBuffer.buffer);
    CHECK_ERR(err == CL_SUCCESS);

    err = kernelPhongRender.setArg(1, width);
    CHECK_ERR(err == CL_SUCCESS);

    err = kernelPhongRender.setArg(2, height);
    CHECK_ERR(err == CL_SUCCESS);

    //=========================================================================
    // kernelCubmapRender:
    //=========================================================================
    err = kernelCubmapRender.setArg(0, pboRaycastingBuffer.buffer);
    CHECK_ERR(err == CL_SUCCESS);

    err = kernelCubmapRender.setArg(1, width);
    CHECK_ERR(err == CL_SUCCESS);

    err = kernelCubmapRender.setArg(2, height);
    CHECK_ERR(err == CL_SUCCESS);

    return true;
}

bool RaycastingRenderer::initCLVolume() {
    cl_int err;

    // create transfer function texture
    float transferFunc[] = {
        0.0, 0.0, 0.0, 0.0,
        1.0, 0.0, 0.0, 1.0,
        1.0, 0.5, 0.0, 1.0,
        1.0, 1.0, 0.0, 1.0,
        0.0, 1.0, 0.0, 1.0,
        0.0, 1.0, 1.0, 1.0,
        0.0, 0.0, 1.0, 1.0,
        1.0, 0.0, 1.0, 1.0,
        1.0, 0.0, 0.0, 0.0,
    };

    transferFuncBuffer = cl::Image2D(clContext, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
            cl::ImageFormat(CL_RGBA, CL_FLOAT), 9, 1, 0, transferFunc, &err);
    CHECK_ERR(err == CL_SUCCESS);

    cl_context context = clContext();
    // Create samplers for transfer function, linear interpolation and nearest interpolation 
    transferFuncSampler = clCreateSampler(context, true, CL_ADDRESS_CLAMP_TO_EDGE, CL_FILTER_LINEAR, &err);
    CHECK_ERR(err == CL_SUCCESS);

    volumeSamplerLinear = clCreateSampler(context, true, CL_ADDRESS_CLAMP_TO_EDGE, CL_FILTER_LINEAR, &err);
    CHECK_ERR(err == CL_SUCCESS);

    volumeSamplerNearest = clCreateSampler(context, true, CL_ADDRESS_CLAMP_TO_EDGE, CL_FILTER_NEAREST, &err);
    CHECK_ERR(err == CL_SUCCESS);

    if (!matrixBuffer.init(clContext, clCommandQueue,
            MATRIX_SIZE * sizeof (float),
            "invViewMatrix",
            CL_MEM_READ_ONLY)) {
        return false;
    }

    return true;
}

bool RaycastingRenderer::createProgramAndKernels() {
    cl_int err;
    string sourceCode = OpenCLUtil::readCLFile(KERNEL_FILE, true, "Parameters.h");

    cl::Program::Sources
    sources(1, std::make_pair(sourceCode.c_str(), sourceCode.length()));

    clProgram = cl::Program(clContext, sources, &err);
    CHECK_ERR(err == CL_SUCCESS);

    string flags = "-cl-fast-relaxed-math -DIMAGE_SUPPORT -DPIXELS_PER_WORK_ITEM=";
    flags += pixelsPerWorkItem;

    err = clProgram.build(clDevices, flags.c_str());

    if (err != CL_SUCCESS) {

        string str =
                clProgram.getBuildInfo<CL_PROGRAM_BUILD_LOG > (clDevices[0]);
        cout << "Program Error Info: " << str << endl;
        return false;
    }

    kernelPhongRender = cl::Kernel(clProgram, "phongRender", &err);
    CHECK_ERR(err == CL_SUCCESS);

    kernelCubmapRender = cl::Kernel(clProgram, "render", &err);
    CHECK_ERR(err == CL_SUCCESS);

    kernelDrawFrame = cl::Kernel(clProgram, "drawFrame", &err);
    CHECK_ERR(err == CL_SUCCESS);

    return true;
}

bool RaycastingRenderer::setFixedKernelArguments() {
    cl_int err;
    err = kernelPhongRender.setArg(3, matrixBuffer.buffer);
    CHECK_ERR(err == CL_SUCCESS);

    if (linearFiltering) {
        err = kernelPhongRender.setArg(5, volumeSamplerLinear);
        CHECK_ERR(err == CL_SUCCESS);
    } else {
        err = kernelPhongRender.setArg(5, volumeSamplerNearest);
        CHECK_ERR(err == CL_SUCCESS);
    }

    //=========================================================================
    // kernelCubmapRender:
    //=========================================================================

    err = kernelCubmapRender.setArg(3, matrixBuffer.buffer);
    CHECK_ERR(err == CL_SUCCESS);

    err = kernelCubmapRender.setArg(5, cubeMapPosXBuffer);
    CHECK_ERR(err == CL_SUCCESS);

    err = kernelCubmapRender.setArg(6, cubeMapNegXBuffer);
    CHECK_ERR(err == CL_SUCCESS);

    err = kernelCubmapRender.setArg(7, cubeMapPosYBuffer);
    CHECK_ERR(err == CL_SUCCESS);

    err = kernelCubmapRender.setArg(8, cubeMapNegYBuffer);
    CHECK_ERR(err == CL_SUCCESS);

    err = kernelCubmapRender.setArg(9, cubeMapPosZBuffer);
    CHECK_ERR(err == CL_SUCCESS);

    err = kernelCubmapRender.setArg(10, cubeMapNegZBuffer);
    CHECK_ERR(err == CL_SUCCESS);

    return true;
}

bool RaycastingRenderer::init3dTex() {
    glGenTextures(1, &tex3dId);
    glBindTexture(GL_TEXTURE_3D, tex3dId);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage3D(
            GL_TEXTURE_3D,
            0,
            GL_RGBA,
            IMAGE3D_WIDTH,
            IMAGE3D_HEIGHT,
            IMAGE3D_DEPTH,
            0,
            PIXEL_FORMAT,
            GL_FLOAT,
            NULL);

    glBindTexture(GL_TEXTURE_3D, 0);

    cl_int err;
    image3dBuffer = cl::Image3DGL(clContext, CL_MEM_READ_WRITE, GL_TEXTURE_3D, 0, tex3dId, &err);
    CHECK_ERR(err == CL_SUCCESS);

    err = kernelPhongRender.setArg(4, image3dBuffer);
    CHECK_ERR(err == CL_SUCCESS);

    err = kernelCubmapRender.setArg(4, image3dBuffer);
    CHECK_ERR(err == CL_SUCCESS);

    return true;
}


// ---------------------------------------------------------------------------------------------------------

bool RaycastingRenderer::loadCubeMap() {
#if defined(CUBE_MAP_PANORAMA)
    tex_cube = SOIL_load_OGL_cubemap
            (
            "img/panorama_positive_x.png",
            "img/panorama_negative_x.png",
            "img/panorama_positive_y.png",
            "img/panorama_negative_y.png",
            "img/panorama_positive_z.png",
            "img/panorama_negative_z.png",
            SOIL_LOAD_RGBA,
            SOIL_CREATE_NEW_ID,
            SOIL_FLAG_MIPMAPS
            );
#elif defined(CUBE_MAP_TERRAIN)

    tex_cube = SOIL_load_OGL_cubemap
            (
            "img/cubemap_terrain/terrain_positive_x.png",
            "img/cubemap_terrain/terrain_negative_x.png",
            "img/cubemap_terrain/terrain_positive_y.png",
            "img/cubemap_terrain/terrain_negative_y.png",
            "img/cubemap_terrain/terrain_positive_z.png",
            "img/cubemap_terrain/terrain_negative_z.png",
            SOIL_LOAD_RGBA,
            SOIL_CREATE_NEW_ID,
            SOIL_FLAG_MIPMAPS
            );
#elif defined(CUBE_MAP_BRIGHTDAY)
    tex_cube = SOIL_load_OGL_cubemap
            (
            "img/cubemap_brightday2/brightday2_positive_x.png",
            "img/cubemap_brightday2/brightday2_negative_x.png",
            "img/cubemap_brightday2/brightday2_positive_y.png",
            "img/cubemap_brightday2/brightday2_negative_y.png",
            "img/cubemap_brightday2/brightday2_positive_z.png",
            "img/cubemap_brightday2/brightday2_negative_z.png",
            SOIL_LOAD_RGBA,
            SOIL_CREATE_NEW_ID,
            SOIL_FLAG_MIPMAPS
            );
#elif defined(CUBE_MAP_COLOR_GRADIENT)

    tex_cube = SOIL_load_OGL_cubemap
            (
            "img/cubemap_gradient/gradient_positive_x.png",
            "img/cubemap_gradient/gradient_negative_x.png",
            "img/cubemap_gradient/gradient_positive_y.png",
            "img/cubemap_gradient/gradient_negative_y.png",
            "img/cubemap_gradient/gradient_positive_z.png",
            "img/cubemap_gradient/gradient_negative_z.png",
            SOIL_LOAD_RGBA,
            SOIL_CREATE_NEW_ID,
            SOIL_FLAG_MIPMAPS
            );
#elif defined(CUBE_MAP_HILLS)

    tex_cube = SOIL_load_OGL_cubemap
            (
            "img/cubemap_hills/hills_positive_x.png",
            "img/cubemap_hills/hills_negative_x.png",
            "img/cubemap_hills/hills_positive_y.png",
            "img/cubemap_hills/hills_negative_y.png",
            "img/cubemap_hills/hills_positive_z.png",
            "img/cubemap_hills/hills_negative_z.png",
            SOIL_LOAD_RGBA,
            SOIL_CREATE_NEW_ID,
            SOIL_FLAG_MIPMAPS
            );
#elif defined(CUBE_MAP_HOT_DESSERT)

    tex_cube = SOIL_load_OGL_cubemap
            (
            "img/cubemap_hotdesert/hotdesert_positive_x.png",
            "img/cubemap_hotdesert/hotdesert_negative_x.png",
            "img/cubemap_hotdesert/hotdesert_positive_y.png",
            "img/cubemap_hotdesert/hotdesert_negative_y.png",
            "img/cubemap_hotdesert/hotdesert_positive_z.png",
            "img/cubemap_hotdesert/hotdesert_negative_z.png",
            SOIL_LOAD_RGBA,
            SOIL_CREATE_NEW_ID,
            SOIL_FLAG_MIPMAPS
            );

#elif defined(CUBE_MAP_DEBUG)

    tex_cube = SOIL_load_OGL_cubemap
            (
            "img/cubemap_debug/debug_positive_x.png",
            "img/cubemap_debug/debug_negative_x.png",
            "img/cubemap_debug/debug_positive_y.png",
            "img/cubemap_debug/debug_negative_y.png",
            "img/cubemap_debug/debug_positive_z.png",
            "img/cubemap_debug/debug_negative_z.png",


            SOIL_LOAD_RGBA,
            SOIL_CREATE_NEW_ID,
            0
            );

#endif

    if (tex_cube == 0) {
        cout << "ERROR: Nie moge zaladowac tekstury!!" << endl;
        return false;
    }

    return true;
}

void RaycastingRenderer::setupTextureParameters() {
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    //glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
}

