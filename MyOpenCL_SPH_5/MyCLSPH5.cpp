/* 
 * File:   MyCLSPH4.cpp
 * Author: Revers
 *
 * Created on 30 październik 2011, 16:44
 */

#include <cmath>
#include <iostream>
#include <cstdlib>

// GLEW and GLUT includes
#include <GL/glew.h>
#include <CL/cl_gl.h>
#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/freeglut_ext.h>

#include "FPSCounter.h"
#include "ControlPanel.h"
#include "TargetCamera.h"

#include "FluidSimulation.h"
#include "FluidSimulationTest.h"

#include "Configuration.h"

#include "SphereRenderer.h"
#include "RaycastingRenderer.h"

using namespace std;

//------------------------------------------------
// Defines:
//------------------------------------------------
#define APP_TITLE "OpenCL Liquid Simulation"

#define FAILURE 1
#define SUCCESS 0

#define WINDOW_WIDTH 1280
#define WINDOW_HEIGHT 768
//================================================

//------------------------------------------------
// Globals:
//------------------------------------------------
int mouseButtons = 0;


ControlPanel controlPanel;

#ifdef USE_RAYCASTING
TargetCamera camera(0, 0, 8, 0, 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
#else
float center = 64;
//float translateX = center;
//float translateY = center;
//float translateZ = 328.0f;
float translateX = 180;
float translateY = center - 70;
float translateZ = 270;
TargetCamera camera(translateX, translateY, translateZ, center, center, center, WINDOW_WIDTH, WINDOW_HEIGHT);
#endif

SphereRenderer sphereRenderer(&controlPanel, &camera);
RaycastingRenderer raycastingRenderer(&controlPanel, &camera);

#ifdef USE_RAYCASTING
Renderer* currentRenderer = &raycastingRenderer;
#else
Renderer* currentRenderer = &sphereRenderer;
#endif

FPSCounter fpsCounter;
//================================================

#define PAUSE_FRAME 3000

int frameCounter = 0;

void keyboardFunc(unsigned char key, int x, int y) {
    controlPanel.keyboardEventHandlerGLUT(key, x, y);

    switch (key) {

        case 'A':
        case 'a':
        {
            cout << "camera position = " << camera.getPosition().toString() << endl;
        }
            break;
        case ' ':
        {
            cout << "frame counter = " << frameCounter << endl;
          //  frameCounter = 0;
        }
            break;
    }
}

void mouseFunc(int button, int state, int x, int y) {

    controlPanel.mouseEventHandlerGLUT(button, state, x, y);

    if (state == GLUT_DOWN) {
        mouseButtons |= 1 << button;

    } else if (state == GLUT_UP) {
        mouseButtons = 0;
    }

    if (state == GLUT_DOWN && controlPanel.isMouseOver() == false) {

        camera.click(x, y);
    }

    glutPostRedisplay();
}

void motionFunc(int x, int y) {
    if (controlPanel.isMouseOver()) {
        controlPanel.mouseMotionEventHandlerGLUT(x, y);
    } else if (mouseButtons & 1) {
        camera.drag(x, y);
    }
}

void mouseWheelFunc(int button, int dir, int x, int y) {
    float step = 2.0f;
    if (dir > 0) {
        // Zoom in
        camera.increaseDistance(-step);
    } else {
        // Zoom out
        camera.increaseDistance(step);
    }
}

void displayFunc() {
    fpsCounter.frameBegin();

    if (!controlPanel.paused) {
        controlPanel.fluidSimulation->step();
        frameCounter++;

        if (frameCounter == PAUSE_FRAME) {
            controlPanel.paused = true;
        }

    } else {

    }

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // set view matrix
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    currentRenderer->render();

    controlPanel.draw();

    glutSwapBuffers();
    glutPostRedisplay();

    if (fpsCounter.frameEnd()) {
        char title[256];
        sprintf(title, APP_TITLE " | %d fps ", fpsCounter.getFramesPerSec());

        glutSetWindowTitle(title);
    }
}

void reshapeFunc(int w, int h) {
    if (h == 0)
        h = 1;

    glViewport(0, 0, w, h);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    //#ifdef USE_RAYCASTING
    //    //glOrtho(0.0, 1.0, 0.0, 1.0, 0.0, 1.0);
    //    glOrtho(0.0, w, 0.0, h, 0.0, 1.0);
    //#else
    double viewAngle = 60.0;
    gluPerspective(
            viewAngle,
            (GLdouble) w / (GLdouble) h,
            0.1,
            10000.0);

    sphereRenderer.setFOV((float) viewAngle);
    //#endif

    currentRenderer->windowSizeChanged(w, h);

    controlPanel.windowResizeEvent(w, h);
    camera.setBounds(w, h);
}

int initializeGL(int argc, char * argv[]) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
    glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);

    glutCreateWindow(APP_TITLE);

    glewInit();
    if (!glewIsSupported("GL_VERSION_2_0 " "GL_ARB_pixel_buffer_object")) {
        std::cerr << "Error: Support for necessary OpenGL extensions missing." << std::endl;
        return FAILURE;
    }

    glPolygonMode(GL_FRONT, GL_FILL);
    glEnable(GL_DEPTH_TEST);
    glFrontFace(GL_CCW);
    glCullFace(GL_BACK);
    glEnable(GL_CULL_FACE);

    glutDisplayFunc(displayFunc);
    glutKeyboardFunc(keyboardFunc);
    glutMouseFunc(mouseFunc);
    glutMotionFunc(motionFunc);
    glutMouseWheelFunc(mouseWheelFunc);
    glutReshapeFunc(reshapeFunc);

    glPointSize(2.0f);

    //    glClearColor(0.5, 0.5, 0.5, 0);
    glClearColor(0, 0, 0, 0);
    //glClearColor(1, 1, 1, 0);

    return SUCCESS;
}

void Terminate() {
    // todo:
}

int oldMain() {
    FluidSimulation fluidSim(64 * 1024, WALL);
    fluidSim.setVerbose(true);
    //m_particleRadius = fluidSimulation->getParticleRadiusScaled();

    fluidSim.printParameters();

    bool success = fluidSim.init();

    cout << boolalpha << "init " << " success = " << success << endl;
    if (!success) {
        return -1;
    }

    //    FluidSimulationTest test(&fluidSim);
    //
    //    test.testStep();

    return 0;
}

int main(int argc, char** argv) {
    srand(time(0));

    float f = 1.0f / 64.0f;
    cout << "f = " << f << endl;

    if (Configuration::getInstance().init() == false) {
        cout << "ERROR: Configuration::getInstance().init() FAILED!" << endl;
        return -1;
    }

    if (initializeGL(argc, argv)) {
        cout << "ERROR: initializeGL FAILED!" << endl;
        return -1;
    }
#if 0
    oldMain();
#else
    /*=================================================================*/

    if (controlPanel.init() == false) {
        cout << "ERROR: ControlPanel init FAILED!" << endl;
        return -1;
    }

    if (!sphereRenderer.init()) {
        cout << "ERROR: SphereRenderer init FAILED!" << endl;
        return -1;
    }

    if (!raycastingRenderer.init()) {
        cout << "ERROR: RaycastingRenderer init FAILED!" << endl;
        return -1;
    }

    glutMainLoop();
    /*=================================================================*/
#endif
    return 0;
}


