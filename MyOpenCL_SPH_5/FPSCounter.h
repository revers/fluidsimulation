/* 
 * File:   FPSCounter.h
 * Author: Revers
 *
 * Created on 18 grudzień 2011, 01:05
 */

#ifndef FPSCOUNTER_H
#define	FPSCOUNTER_H

#include <ctime>

class FPSCounter {
    clock_t t1, t2;
    int frameCount;
    int frameRefCount;
    double totalElapsedTime;
    int framesPerSec;
public:

    FPSCounter() {
        frameCount = 0;
        frameRefCount = 90;
        totalElapsedTime = 0.0;
    }

    virtual ~FPSCounter() {
    }

    void frameBegin() {
        t1 = clock() * CLOCKS_PER_SEC;
        frameCount++;
    }

    bool frameEnd() {
        t2 = clock() * CLOCKS_PER_SEC;
        totalElapsedTime += (double) (t2 - t1);
        if (frameCount && frameCount > frameRefCount) {

            double fMs = (double) ((totalElapsedTime / (double) CLOCKS_PER_SEC) / (double) frameCount);
            framesPerSec = (int) (1.0 / (fMs / CLOCKS_PER_SEC));

            frameCount = 0;
            totalElapsedTime = 0.0;
        }
    }

    int getFramesPerSec() const {
        return framesPerSec;
    }

private:
    FPSCounter(const FPSCounter& orig);
};

#endif	/* FPSCOUNTER_H */

