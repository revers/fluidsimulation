/* 
 * File:   CLBufferImage2DImage2D.h
 * Author: Revers
 *
 * Created on 22 listopad 2011, 18:43
 */

#ifndef CLBUFFERIMAGE2D_H
#define	CLBUFFERIMAGE2D_H

#include <iostream>
#include <CL/cl.hpp>

enum CLBufferImage2DType {
    FLOAT4, UINT4, INT4
};

class CLBuffer;
class CLBufferVboGL;

class CLBufferImage2D {
private:
    cl::Context clContext;
    cl::CommandQueue clCommandQueue;
    size_t size;
    const char* debugName;
    size_t width;
    size_t height;
    size_t rowPitch;
public:
    cl::Image2D buffer;

    CLBufferImage2D() :
    size(0),
    debugName(NULL),
    width(0),
    height(0),
    rowPitch(0) {
    }

    CLBufferImage2D(const CLBufferImage2D& old);

    bool init(CLBufferImage2DType type,
            cl::Context clContext,
            cl::CommandQueue clCommandQueue,
            size_t size,
            size_t rowSize,
            const char* debugName = NULL,
            cl_mem_flags flags = CL_MEM_READ_WRITE);

    cl_int copyTo(CLBuffer& destBuffer);

    cl_int copyTo(CLBufferVboGL& destBuffer);

    cl_int copyTo(CLBufferImage2D destBuffer);

    bool write(void *sourcePtr, cl_bool blocking_write = CL_TRUE);

    bool read(void* destPtr, cl_bool blocking_read = CL_TRUE);

    size_t getSize() const {
        return size;
    }

    const char* getDebugName() const {
        return debugName;
    }

private:

    void printError(cl_int err, const char* beginMsg, const char* endMsg);
};

class CLPingPongBufferImage2D {
private:
    size_t size;
    size_t totalSize;
    const char* debugName;
    CLBufferImage2D first;
    CLBufferImage2D second;
public:

    CLPingPongBufferImage2D() :
    size(0),
    totalSize(0),
    debugName(NULL) {
    }

    CLPingPongBufferImage2D(const CLPingPongBufferImage2D& old);

    bool init(CLBufferImage2DType type,
            cl::Context clContext,
            cl::CommandQueue clCommandQueue,
            size_t size,
            size_t rowSize,
            const char* debugName = NULL,
            cl_mem_flags flags = CL_MEM_READ_WRITE);

    bool writeFirst(void *sourcePtr, cl_bool blocking_write = CL_TRUE);

    bool readFirst(void* destPtr, cl_bool blocking_read = CL_TRUE);

    bool writeSecond(void *sourcePtr, cl_bool blocking_write = CL_TRUE);

    bool readSecond(void* destPtr, cl_bool blocking_read = CL_TRUE);

    size_t getSize() const {
        return size;
    }

    size_t getTotalSize() const {
        return totalSize;
    }

    void swap() {
        cl::Image2D temp = first.buffer;
        first.buffer = second.buffer;
        second.buffer = temp;
    }

    CLBufferImage2D& getFirst() {
        return first;
    }

    CLBufferImage2D& getSecond() {
        return second;
    }

    const char* getDebugName() const {
        return debugName;
    }

private:

    void printError(const char* beginMsg, const char* endMsg);
};


#endif	/* CLBUFFERIMAGE2D_H */

