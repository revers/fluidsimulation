/* 
 * File:   Configuration.cpp
 * Author: Revers
 * 
 * Created on 20 listopad 2011, 23:06
 */
#include <fstream>
#include <iostream>
#include "Configuration.h"
#include "ATI_RadixSort.hpp"

using namespace std;

#define INIT_PLACEMENT_KEY "init_placement"
#define CXX_DIR_KEY "cxx_dir"
#define PARTICLE_COUNT_KEY "particle_count"

string trim(string& line) {
    size_t begin = 0;
    while (begin < line.length() &&
            (line[begin] == ' ' || line[begin] == '\t')) {
        begin++;
    }

    size_t len = line.length() - begin;
    size_t end = begin + len - 1;
    char c = line[end];

    while ((end > begin) && (c == ' ' || c == '\t')) {
        c = line[--end];
        len--;
    }

    return line.substr(begin, len);
}

inline bool containsStr(const char* str, string& line) {
    size_t pos = line.find(str);
    if (pos != string::npos) {
        return true;
    }

    return false;
}

bool Configuration::checkInitPlacement() {
    string s = getInitPlacement();

    bool found = false;
    if (s == "WALL") {
        found = true;
        initialParticlePlacement = WALL;
    } else if (s == "CUBE") {
        found = true;
        initialParticlePlacement = CUBE;
    } else if (s == "CENTER") {
        found = true;
        initialParticlePlacement = CENTER;
    }

    if (found == false) {
        cout << "ERROR: Wrong '" << INIT_PLACEMENT_KEY "' = '" << s << "' value!!" << endl;
        cout << "possible values are: WALL, CUBE, CENTER" << endl;
        return false;
    }

    return true;
}

bool Configuration::checkParticleCount() {
    const char* possibleValues[] = {"16", "32", "64", "128"};
    const size_t size = sizeof (possibleValues) / sizeof (const char*);

    //cout << "size = " << size << endl;
    int index = -1;

    const char* particleCountStr = getValue(PARTICLE_COUNT_KEY);

    for (int i = 0; i < size; i++) {
        if (strcmp(possibleValues[i], particleCountStr) == 0) {
            index = i;
            break;
        }
    }

    if (index == -1) {
        cout << "ERROR: Wrong '" << PARTICLE_COUNT_KEY << "' = " << particleCountStr << " value!!" << endl;
        cout << "Possible values are: ";
        for (int i = 0; i < size; i++) {
            cout << possibleValues[i] << ", ";
        }
        cout << endl;
        return false;
    }

    particleCount = (unsigned int) (atoi(possibleValues[index]));

    return true;
}

bool Configuration::checkAllKeys() {
    bool cxxDirFound = false;
    bool placementFound = false;
    bool particleCountFound = false;

    for (KeyValueMapIterator it = keyValueMap.begin();
            it != keyValueMap.end(); ++it) {
        if (it->first == CXX_DIR_KEY) {
            cxxDirFound = true;
        } else if (it->first == INIT_PLACEMENT_KEY) {
            placementFound = true;
        } else if (it->first == PARTICLE_COUNT_KEY) {
            particleCountFound = true;
        }
    }

    if (cxxDirFound == false) {
        cout << "ERROR: Cannot find '" << CXX_DIR_KEY
                << "' in the Configuration.txt file!!" << endl;
        return false;
    }

    if (placementFound == false) {
        cout << "ERROR: Cannot find '" << INIT_PLACEMENT_KEY
                << "' in the Configuration.txt file!!" << endl;
        return false;
    }

    if (particleCountFound == false) {
        cout << "ERROR: Cannot find '" << PARTICLE_COUNT_KEY
                << "' in the Configuration.txt file!!" << endl;
        return false;
    }

    return true;
}

bool Configuration::init() {
    cout << "Reading Configuration.txt..." << endl;

    std::string line;
    std::ifstream in("Configuration.txt");

    if (!in) {
        cout << "ERROR: FILE NOT FOUND: 'Configuration.txt'!!" << endl;
        return false;
    }

    while (std::getline(in, line)) {
        line = trim(line);

        if (line.length() == 0 || line[0] == '#') {
            continue;
        }

        if (containsStr("=", line) == false) {
            continue;
        }

        size_t equalSign = line.find("=");
        string key = line.substr(0, equalSign);
        key = trim(key);
        string value = line.substr(equalSign + 1);
        value = trim(value);

        keyValueMap[key] = value;
    }

    if (checkAllKeys() == false) {
        return false;
    }

    if (checkParticleCount() == false) {
        return false;
    }

    inited = true;

    if (checkInitPlacement() == false) {
        return false;
    }

    return true;
}

const char* Configuration::getValue(const char* key) {
    KeyValueMapIterator it = keyValueMap.find(key);
    if (it == keyValueMap.end()) {
        return NULL;
    } else {
        return it->second.c_str();
    }
}

const char* Configuration::getCxxDirectory() {
    assert(inited == true);
    return getValue(CXX_DIR_KEY);
}

std::string Configuration::getInitPlacement() {
    assert(inited == true);
    KeyValueMapIterator it = keyValueMap.find(INIT_PLACEMENT_KEY);
    if (it == keyValueMap.end()) {
        return string();
    } else {
        return it->second;
    }
}

unsigned int Configuration::getParticleCount() {
    assert(inited == true);
    return particleCount;
}
