/* 
 * File:   CLBufferGL.h
 * Author: Revers
 *
 * Created on 12 listopad 2011, 19:10
 */

#ifndef CLBUFFERVBOGL_H
#define	CLBUFFERVBOGL_H


#include <cassert>
#include <iostream>
#include <vector>

#include <CL/cl.hpp>
#include <GL/gl.h>

class CLBufferVboGL {
private:
    cl::Context clContext;
    cl::CommandQueue clCommandQueue;
    size_t size;
    const char* debugName;
    GLuint vertexBufferObject;
    std::vector<cl::Memory> bufferVector;
public:
    cl::BufferGL buffer;

    CLBufferVboGL() :
    size(0),
    debugName(NULL),
    vertexBufferObject(0),
    bufferVector(1) {
    }

    CLBufferVboGL(const CLBufferVboGL& old);

    virtual ~CLBufferVboGL();

    void acquireGLObject();

    void releaseGLObject();

    bool init(cl::Context clContext,
            cl::CommandQueue clCommandQueue,
            size_t size,
            const char* debugName = NULL,
            cl_mem_flags flags = CL_MEM_READ_WRITE,
            void* host_ptr = NULL);

    bool write(const void *sourcePtr, cl_bool blocking_write = CL_TRUE);

    bool write(const void *sourcePtr,
            size_t offset,
            size_t bytes,
            cl_bool blocking_write = CL_TRUE);

    bool read(void* destPtr, cl_bool blocking_read = CL_TRUE);

    bool read(void *destPtr,
            size_t offset,
            size_t bytes,
            cl_bool blocking_read = CL_TRUE);

    size_t getSize() const {
        return size;
    }

    GLuint getVertexBufferObject() const {
        return vertexBufferObject;
    }

    const char* getDebugName() const {
        return debugName;
    }

private:

    void printError(cl_int err, const char* beginMsg, const char* endMsg);
};

class CLPingPongBufferVboGL {
private:

    size_t size;
    size_t totalSize;
    const char* debugName;
    CLBufferVboGL first;
    CLBufferVboGL second;
public:

    CLPingPongBufferVboGL() :
    size(0),
    totalSize(0),
    debugName(NULL) {
    }

    CLPingPongBufferVboGL(const CLPingPongBufferVboGL& old);

    bool init(cl::Context clContext,
            cl::CommandQueue clCommandQueue,
            size_t size,
            const char* debugName = NULL,
            cl_mem_flags flags = CL_MEM_READ_WRITE,
            void* host_ptr = NULL);

    bool writeFirst(const void *sourcePtr, cl_bool blocking_write = CL_TRUE);

    bool writeFirst(const void *sourcePtr,
            size_t offset,
            size_t bytes,
            cl_bool blocking_write = CL_TRUE);

    bool readFirst(void* destPtr, cl_bool blocking_read = CL_TRUE);

    bool readFirst(void *destPtr,
            size_t offset,
            size_t bytes,
            cl_bool blocking_read = CL_TRUE);

    bool writeSecond(const void *sourcePtr, cl_bool blocking_write = CL_TRUE);

    bool writeSecond(const void *sourcePtr,
            size_t offset,
            size_t bytes,
            cl_bool blocking_write = CL_TRUE);

    bool readSecond(void* destPtr, cl_bool blocking_read = CL_TRUE);

    bool readSecond(void *destPtr,
            size_t offset,
            size_t bytes,
            cl_bool blocking_read = CL_TRUE);

    size_t getSize() const {
        return size;
    }

    size_t getTotalSize() const {
        return totalSize;
    }

    void swap() {
        cl::BufferGL temp = first.buffer;
        first.buffer = second.buffer;
        second.buffer = temp;
    }

    CLBufferVboGL& getFirst() {
        return first;
    }

    CLBufferVboGL& getSecond() {
        return second;
    }

    const char* getDebugName() const {
        return debugName;
    }

    void acquireSecondGLObject() {
        second.acquireGLObject();
    }

    void releaseSecondGLObject() {
        second.releaseGLObject();
    }

    void acquireFirstGLObject() {
        first.acquireGLObject();
    }

    void releaseFirstGLObject() {
        first.releaseGLObject();
    }

private:

    void printError(const char* beginMsg, const char* endMsg);

};

#endif	/* CLBUFFERVBOGL_H */

