/* 
 * File:   ControlPanel.h
 * Author: Revers
 *
 * Created on 6 grudzień 2011, 20:04
 */

#ifndef CONTROLPANEL_H
#define	CONTROLPANEL_H

#include <AntTweakBar.h>

#include "FluidSimulation.h"
#include "Configuration.h"

#include "RevVector.h"

class ControlPanel {

    typedef struct {
        float x;
        float y;
        float z;
    } Gravity;

    typedef enum {
        N16 = 16 * 1024,
        N32 = 32 * 1024,
        N64 = 64 * 1024,
        N128 = 128 * 1024
    } ParticleCount;

    struct TweakBarBounds {
        int x;
        int y;
        int width;
        int height;
    };

    TweakBarBounds settingsBarBounds;
    TweakBarBounds infoBarBounds;

    TwBar *infoBar;
    TwBar *settingsBar;

    ColoringSource coloringSource;
    ColoringGradient coloringGradient;
    InitialParticlePlacement particlePlacement;
    ParticleCount particleCount;
    float restDensity;
    float stiffness;
    float particleMass;
    float timeStep;
    float simulationScale;
    float accLimit;
    float damping;
    float viscosity;
    Gravity gravity;
    bool keepPreviousParams;
    bool mouseOver;

    const int blockLength;
    const int gridDimensionX;
    const int gridDimensionY;
    const int gridDimensionZ;
    const int totalBlocks;
    float avgParticles;
    float suggestedScale;
    float suggestedSupportRadius;

#ifdef USE_SURFACE_TENSION
    float surfaceTensionCoeff;
#endif

#if defined(USE_RAYCASTING)
    float colorFieldThreshold;
    float rMin;
    float distFieldFactor;
    rev::graph::Vector3f lightDirection;
#elif defined(EXTRACT_SURFACE_COLOR_FIELD)
    float colorFieldThreshold;
#elif defined(EXTRACT_SURFACE_CENTER_OF_MASS)
    float distanceThreshold;
    unsigned int minParticles;
#endif
public:
    FluidSimulation* fluidSimulation;
    bool drawBoundaries;
    bool drawGrid;
    bool drawCenter;
    bool started;
    bool paused;

    ControlPanel() :
    infoBar(NULL),
    settingsBar(NULL),
    keepPreviousParams(false),
    mouseOver(false),
    avgParticles(0),
    suggestedScale(0),
    suggestedSupportRadius(0),
    fluidSimulation(NULL),
    drawBoundaries(false),
    drawGrid(true),
    drawCenter(false),
    started(false),
    paused(true),
    blockLength(BLOCK_SIZE),
    gridDimensionX(GRID_DIMENSION_X),
    gridDimensionY(GRID_DIMENSION_Y),
    gridDimensionZ(GRID_DIMENSION_Z),
    totalBlocks((gridDimensionX*gridDimensionY*gridDimensionZ) / (blockLength*blockLength*blockLength)) {
#ifdef USE_RAYCASTING
        drawBoundaries = true;
#endif
    }

    virtual ~ControlPanel() {
        TwTerminate();

        if (fluidSimulation != NULL) {
            delete fluidSimulation;
        }
    }

    bool init(
            bool overrideGLUTPassiveMotionFunc = true,
            bool overrideGLUTSpecialFunc = true);

    bool isMouseOver() const {
        return mouseOver;
    }

    void draw() {
        TwDraw();
    }

    void windowResizeEvent(int w, int h);

    void keyboardEventHandlerGLUT(unsigned char key, int x, int y);

    void mouseMotionEventHandlerGLUT(int x, int y) {
        TwEventMouseMotionGLUT(x, y);
    }

    void mousePassiveEventHandlerGLUT(int x, int y) {
        TwEventMouseMotionGLUT(x, y);
    }

    void specialEventHandlerGLUT(int key, int x, int y) {
        TwEventSpecialGLUT(key, x, y);
    }

    void mouseEventHandlerGLUT(int button, int state, int x, int y);

    void startSimulation();

    void togglePause();

    void stopSimulation();

    void resetExternalForces();

private:
    ControlPanel(const ControlPanel& orig);

    void initSettingsBar();
    void initInfoBar();

    void initFluidParameters();

    FluidSimulation* createNewFluidSimulation();

    int restartSimulation();

    static void TW_CALL startAction(void* clientData);

    static void TW_CALL pauseAction(void* clientData);

    static void TW_CALL stopAction(void* clientData);

    static void TW_CALL setColoringSourceCallback(const void* value, void* clientData);

    static void TW_CALL getColoringSourceCallback(void* value, void* clientData);

    static void TW_CALL setColoringGradientCallback(const void* value, void* clientData);

    static void TW_CALL getColoringGradientCallback(void* value, void* clientData);

    static void TW_CALL setGravityXCallback(const void* value, void* clientData);

    static void TW_CALL getGravityXCallback(void* value, void* clientData);

    static void TW_CALL setGravityYCallback(const void* value, void* clientData);

    static void TW_CALL getGravityYCallback(void* value, void* clientData);

    static void TW_CALL setGravityZCallback(const void* value, void* clientData);

    static void TW_CALL getGravityZCallback(void* value, void* clientData);

    static void TW_CALL setExternalForcesCallback(const void* value, void* clientData);

    static void TW_CALL getExternalForcesCallback(void* value, void* clientData);

    static void TW_CALL resetGravityAction(void* clientData);

    static void TW_CALL setParticlePlacementCallback(const void* value, void* clientData);

    static void TW_CALL getParticlePlacementCallback(void* value, void* clientData);

    static void TW_CALL setParticleCountCallback(const void* value, void* clientData);

    static void TW_CALL getParticleCountCallback(void* value, void* clientData);

    static void TW_CALL setRestDensityCallback(const void* value, void* clientData);

    static void TW_CALL getRestDensityCallback(void* value, void* clientData);

    static void TW_CALL setStiffnessCallback(const void* value, void* clientData);

    static void TW_CALL getStiffnessCallback(void* value, void* clientData);

    static void TW_CALL setParticleMassCallback(const void* value, void* clientData);

    static void TW_CALL getParticleMassCallback(void* value, void* clientData);

    static void TW_CALL setTimestepCallback(const void* value, void* clientData);

    static void TW_CALL getTimestepCallback(void* value, void* clientData);

    static void TW_CALL setSimulationScaleCallback(const void* value, void* clientData);

    static void TW_CALL getSimulationScaleCallback(void* value, void* clientData);

    static void TW_CALL setAccelerationLimitCallback(const void* value, void* clientData);

    static void TW_CALL getAccelerationLimitCallback(void* value, void* clientData);

    static void TW_CALL setDampingCallback(const void* value, void* clientData);

    static void TW_CALL getDampingCallback(void* value, void* clientData);

    static void TW_CALL setViscosityCallback(const void* value, void* clientData);

    static void TW_CALL getViscosityCallback(void* value, void* clientData);

    static void TW_CALL getWorldSupportRadiusCallback(void* value, void* clientData);

    static void TW_CALL getWorldParticleRadiusCallback(void* value, void* clientData);

    static void TW_CALL getWorldParticleRestDistanceCallback(void* value, void* clientData);

    static void TW_CALL getWorldToSimulationScaleCallback(void* value, void* clientData);

    static void TW_CALL getTotalVolumeCallback(void* value, void* clientData);

    static void TW_CALL getTotalLengthCallback(void* value, void* clientData);

    static void TW_CALL getTotalParticlesBoxHeightCallback(void* value, void* clientData);

    static void TW_CALL getSimSupportRadiusCallback(void* value, void* clientData);

    static void TW_CALL getSimParticleRadiusCallback(void* value, void* clientData);

    static void TW_CALL getSimParticleRestDistanceCallback(void* value, void* clientData);

    static void TW_CALL getSimToWorldScaleCallback(void* value, void* clientData);

    static void TW_CALL getSimParticleMassCallback(void* value, void* clientData);

    static void TW_CALL getSimParticleVolumeCallback(void* value, void* clientData);

    static void TW_CALL getSimTotalMassCallback(void* value, void* clientData);

    static void TW_CALL getSimTotalVolumeCallback(void* value, void* clientData);

    static void TW_CALL getSimTotalLengthCallback(void* value, void* clientData);

    static void TW_CALL testAction(void* clientData);


#ifdef USE_SURFACE_TENSION

    static void TW_CALL setSurfaceTensionCoeffCallback(const void* value, void* clientData);

    static void TW_CALL getSurfaceTensionCoeffCallback(void* value, void* clientData);

#endif

#if defined(USE_RAYCASTING)
    static void TW_CALL setColorFieldThresholdCallback(const void* value, void* clientData);

    static void TW_CALL getColorFieldThresholdCallback(void* value, void* clientData);

    static void TW_CALL setRMinCallback(const void* value, void* clientData);

    static void TW_CALL getRMinCallback(void* value, void* clientData);

    static void TW_CALL setDistFieldFactorCallback(const void* value, void* clientData);

    static void TW_CALL getDistFieldFactorCallback(void* value, void* clientData);

    static void TW_CALL setLightDirectionCallback(const void* value, void* clientData);

    static void TW_CALL getLightDirectionCallback(void* value, void* clientData);

#elif defined(EXTRACT_SURFACE_COLOR_FIELD)
    static void TW_CALL setColorFieldThresholdCallback(const void* value, void* clientData);

    static void TW_CALL getColorFieldThresholdCallback(void* value, void* clientData);

#elif defined(EXTRACT_SURFACE_CENTER_OF_MASS)

    static void TW_CALL setDistanceThresholdCallback(const void* value, void* clientData);

    static void TW_CALL getDistanceThresholdCallback(void* value, void* clientData);

    static void TW_CALL setMinParticlesCallback(const void* value, void* clientData);

    static void TW_CALL getMinParticlesCallback(void* value, void* clientData);
#endif

};

#endif	/* CONTROLPANEL_H */

