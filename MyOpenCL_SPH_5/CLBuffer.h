/* 
 * File:   CLBuffer.h
 * Author: Revers
 *
 * Created on 12 listopad 2011, 19:08
 */

#ifndef CLBUFFER_H
#define	CLBUFFER_H

#include <iostream>
#include <CL/cl.hpp>

class CLBuffer {
private:
    cl::Context clContext;
    cl::CommandQueue clCommandQueue;
    size_t size;
    const char* debugName;
public:
    cl::Buffer buffer;

    CLBuffer() :
    size(0),
    debugName(NULL) {
    }

    CLBuffer(const CLBuffer& old);

    bool init(
            cl::Context clContext,
            cl::CommandQueue clCommandQueue,
            size_t size,
            const char* debugName = NULL,
            cl_mem_flags flags = CL_MEM_READ_WRITE,
            void* host_ptr = NULL);

    bool write(const void *sourcePtr, cl_bool blocking_write = CL_TRUE);

    bool write(
            const void *sourcePtr,
            size_t offset,
            size_t bytes,
            cl_bool blocking_write = CL_TRUE);

    bool read(void* destPtr, cl_bool blocking_read = CL_TRUE);

    bool read(
            void *destPtr,
            size_t offset,
            size_t bytes,
            cl_bool blocking_read = CL_TRUE);

    size_t getSize() const {
        return size;
    }

    const char* getDebugName() const {
        return debugName;
    }

private:

    void printError(cl_int err, const char* beginMsg, const char* endMsg);
};

class CLPingPongBuffer {
private:

    size_t size;
    size_t totalSize;
    const char* debugName;
    CLBuffer first;
    CLBuffer second;
public:

    CLPingPongBuffer() :
    size(0),
    totalSize(0),
    debugName(NULL) {
    }

    CLPingPongBuffer(const CLPingPongBuffer& old);

    bool init(
            cl::Context clContext,
            cl::CommandQueue clCommandQueue,
            size_t size,
            const char* debugName = NULL,
            cl_mem_flags flags = CL_MEM_READ_WRITE,
            void* host_ptr = NULL);

    bool writeFirst(const void *sourcePtr, cl_bool blocking_write = CL_TRUE);

    bool writeFirst(
            const void *sourcePtr,
            size_t offset,
            size_t bytes,
            cl_bool blocking_write = CL_TRUE);

    bool readFirst(void* destPtr, cl_bool blocking_read = CL_TRUE);

    bool readFirst(
            void *destPtr,
            size_t offset,
            size_t bytes,
            cl_bool blocking_read = CL_TRUE);

    bool writeSecond(const void *sourcePtr, cl_bool blocking_write = CL_TRUE);

    bool writeSecond(
            const void *sourcePtr,
            size_t offset,
            size_t bytes,
            cl_bool blocking_write = CL_TRUE);

    bool readSecond(void* destPtr, cl_bool blocking_read = CL_TRUE);

    bool readSecond(
            void *destPtr,
            size_t offset,
            size_t bytes,
            cl_bool blocking_read = CL_TRUE);

    size_t getSize() const {
        return size;
    }

    size_t getTotalSize() const {
        return totalSize;
    }

    void swap() {
        cl::Buffer temp = first.buffer;
        first.buffer = second.buffer;
        second.buffer = temp;
    }

    CLBuffer& getFirst() {
        return first;
    }

    CLBuffer& getSecond() {
        return second;
    }

    const char* getDebugName() const {
        return debugName;
    }

private:

    void printError(const char* beginMsg, const char* endMsg);
};


#endif	/* CLBUFFER_H */

