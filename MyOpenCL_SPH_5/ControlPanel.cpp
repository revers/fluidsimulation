/* 
 * File:   ControlPanel.cpp
 * Author: Revers
 * 
 * Created on 6 grudzień 2011, 20:03
 */

#include <cmath>
#include <GL/glut.h>
#include <iostream>

#include "ControlPanel.h"

using namespace std;

#define NUM_PARTICLE_COUNT 4
#define NUM_COLORING_TYPE 3
#define NUM_GRADIENT_TYPE 5
#define NUM_INIT_ARRANGEMENT 3

#define SETTINGS_TOOLBAR_WIDTH "290"
#define SETTINGS_TOOLBAR_HEIGHT "615"
#define INFO_TOOLBAR_WIDTH "290"
#define INFO_TOOLBAR_HEIGHT SETTINGS_TOOLBAR_HEIGHT

bool ControlPanel::init(
        bool overrideGLUTPassiveMotionFunc /*= true*/,
        bool overrideGLUTSpecialFunc /*= true*/) {

    if (Configuration::getInstance().isInitedCorrectly() == false) {
        if (Configuration::getInstance().init() == false) {
            return false;
        }
    }
    initFluidParameters();

    if (!TwInit(TW_OPENGL, NULL)) {
        cout << TwGetLastError() << endl;
        return false;
    }

    if (overrideGLUTPassiveMotionFunc) {
        glutPassiveMotionFunc((GLUTmousemotionfun) TwEventMouseMotionGLUT);
    }
    if (overrideGLUTSpecialFunc) {
        glutSpecialFunc((GLUTspecialfun) TwEventSpecialGLUT);
    }
    TwGLUTModifiersFunc(glutGetModifiers);

    initInfoBar();
    initSettingsBar();
}

void ControlPanel::initInfoBar() {
    infoBar = TwNewBar("Info");
    TwDefine(" Info size='" INFO_TOOLBAR_WIDTH " " INFO_TOOLBAR_HEIGHT "' color='80 80 80' ");

    TwAddVarRO(infoBar, "Block length", TW_TYPE_INT32, &(blockLength), " group='Grid'");
    TwAddVarRO(infoBar, "Block count", TW_TYPE_INT32, &(totalBlocks), " group='Grid'");
    TwAddVarRO(infoBar, "Grid dimension X", TW_TYPE_INT32, &(gridDimensionX), " group='Grid'");
    TwAddVarRO(infoBar, "Grid dimension Y", TW_TYPE_INT32, &(gridDimensionY), " group='Grid'");
    TwAddVarRO(infoBar, "Grid dimension Z", TW_TYPE_INT32, &(gridDimensionZ), " group='Grid'");

    TwAddVarCB(infoBar, "Support radius", TW_TYPE_INT32, NULL, getWorldSupportRadiusCallback, this,
            " group='World Scale' ");

    TwAddVarCB(infoBar, "Particle radius", TW_TYPE_FLOAT, NULL, getWorldParticleRadiusCallback, this,
            " group='World Scale' ");

    TwAddVarCB(infoBar, "Particle rest distance", TW_TYPE_FLOAT, NULL, getWorldParticleRestDistanceCallback, this,
            " group='World Scale' ");

    TwAddVarCB(infoBar, "World to Sim scale", TW_TYPE_FLOAT, NULL, getWorldToSimulationScaleCallback, this,
            " group='World Scale' ");

    TwAddVarCB(infoBar, "Total volume", TW_TYPE_FLOAT, NULL, getTotalVolumeCallback, this,
            " group='World Scale' ");

    TwAddVarCB(infoBar, "cuberoot(Total volume)", TW_TYPE_FLOAT, NULL, getTotalLengthCallback, this,
            " group='World Scale' ");

    TwAddVarCB(infoBar, "Particles box height", TW_TYPE_FLOAT, NULL, getTotalParticlesBoxHeightCallback, this,
            " group='World Scale' ");

    TwAddVarCB(infoBar, "Sim Support radius", TW_TYPE_FLOAT, NULL, getSimSupportRadiusCallback, this,
            " label='Support radius [m]' group='Simulation Scale' ");

    TwAddVarCB(infoBar, "Sim Particle radius", TW_TYPE_FLOAT, NULL, getSimParticleRadiusCallback, this,
            " label='Particle radius [m]' group='Simulation Scale' ");

    TwAddVarCB(infoBar, "Sim Particle rest distance", TW_TYPE_FLOAT, NULL, getSimParticleRestDistanceCallback, this,
            " label='Particle rest distance [m]' group='Simulation Scale' ");

    TwAddVarCB(infoBar, "Sim to World scale", TW_TYPE_FLOAT, NULL, getSimToWorldScaleCallback, this,
            " group='Simulation Scale' ");

    TwAddVarCB(infoBar, "Particle mass [kg]", TW_TYPE_FLOAT, NULL, getSimParticleMassCallback, this,
            " group='Simulation Scale' ");

    TwAddVarCB(infoBar, "Particle volume [m^3]", TW_TYPE_FLOAT, NULL, getSimParticleVolumeCallback, this,
            " group='Simulation Scale' ");

    TwAddVarCB(infoBar, "Total mass [kg]", TW_TYPE_FLOAT, NULL, getSimTotalMassCallback, this,
            " group='Simulation Scale' ");

    TwAddVarCB(infoBar, "Total volume [m^3]", TW_TYPE_FLOAT, NULL, getSimTotalVolumeCallback, this,
            " group='Simulation Scale' ");

    TwAddVarCB(infoBar, "cuberoot(Total volume) [m]", TW_TYPE_FLOAT, NULL, getSimTotalLengthCallback, this,
            " group='Simulation Scale' ");

//    TwAddVarRO(infoBar, "Sug Avarage particles", TW_TYPE_FLOAT, &(avgParticles), " label='Avarage particles' group='Suggested Scale'");
//    TwAddVarRO(infoBar, "Sug Support radius", TW_TYPE_FLOAT, &(suggestedSupportRadius), " label='Support radius' group='Suggested Scale'");
//    TwAddVarRO(infoBar, "Sug Simulation scale", TW_TYPE_FLOAT, &(suggestedScale), " label='Simulation scale' group='Suggested Scale'");
//    TwAddButton(infoBar, "Calculate", testAction, this, " group='Suggested Scale' ");

#if defined(USE_RAYCASTING)
    TwAddVarCB(infoBar, "LightDir", TW_TYPE_DIR3F, setLightDirectionCallback, getLightDirectionCallback, this,
            "label='Light Direction' opened=true group='Other' ");
#endif

}

void ControlPanel::initSettingsBar() {
    settingsBar = TwNewBar("Settings");
    TwDefine(" GLOBAL help='This example shows how to integrate AntTweakBar with GLUT and OpenGL.' "); // Message added to the help bar.
    TwDefine(" TW_HELP visible=false ");
    TwDefine(" Settings size='" SETTINGS_TOOLBAR_WIDTH " " SETTINGS_TOOLBAR_HEIGHT "' color='80 80 80' ");

    {
        // ShapeEV associates Shape enum values with labels that will be displayed instead of enum values
        TwEnumVal shapeEV[NUM_PARTICLE_COUNT] = {
            {N16, "16 384"},
            {N32, "32 768"},
            {N64, "65 536"},
            {N128, "131 072"}
        };
        TwType shapeType = TwDefineEnum("ShapeType", shapeEV, NUM_PARTICLE_COUNT);
        //TwAddVarRW(bar, "ParticlesCount", shapeType, &(particleCount), "label='Particle count' help='Number of particles.' group='Initial' ");
        TwAddVarCB(settingsBar, "ParticlesCount", shapeType, setParticleCountCallback,
                getParticleCountCallback, this, "label='Particle count' help='Number of particles.' group='Initial' ");
    }

    {
        TwEnumVal shapeEV[NUM_INIT_ARRANGEMENT] = {
            {CUBE, "Corner"},
            {WALL, "Wall"},
            {CENTER, "Center"},
        };
        TwType shapeType = TwDefineEnum("InitPosType", shapeEV, NUM_INIT_ARRANGEMENT);
        //TwAddVarRW(bar, "InitialArrangement", shapeType, &(particlePlacement), "label='Initial arrangement' group='Initial' ");
        TwAddVarCB(settingsBar, "InitialArrangement", shapeType, setParticlePlacementCallback,
                getParticlePlacementCallback, this, "label='Initial arrangement' group='Initial' ");
    }

    TwAddVarRW(settingsBar, "previousParams", TW_TYPE_BOOLCPP, &(keepPreviousParams), " label='Keep previous params' group='Initial'");

    //    TwAddVarRW(bar, "Rest density", TW_TYPE_FLOAT, &(restDensity),
    TwAddVarCB(settingsBar, "Rest density", TW_TYPE_FLOAT, setRestDensityCallback, getRestDensityCallback, this,
            " min=100.0 max=10000.0 step=10.0 group='Dynamic' ");

    //TwAddVarRW(bar, "Particle mass", TW_TYPE_FLOAT, &(particleMass),
    TwAddVarCB(settingsBar, "Particle mass", TW_TYPE_FLOAT, setParticleMassCallback, getParticleMassCallback, this,
            " min=0.000001 max=1.0 step=0.00005 group='Dynamic' ");

    //TwAddVarRW(bar, "Time step", TW_TYPE_FLOAT, &(timeStep),
    TwAddVarCB(settingsBar, "Time step", TW_TYPE_FLOAT, setTimestepCallback, getTimestepCallback, this,
            " min=0.0001 max=1.0 step=0.0001 group='Dynamic' ");

    //TwAddVarRW(bar, "Acceleration Limit", TW_TYPE_FLOAT, &(accLimit),
    TwAddVarCB(settingsBar, "Acceleration Limit", TW_TYPE_FLOAT, setAccelerationLimitCallback, getAccelerationLimitCallback, this,
            " min=10.0 max=10000.0 step=10.0 group='Dynamic' ");

    //TwAddVarRW(bar, "Damping", TW_TYPE_FLOAT, &(damping),
    TwAddVarCB(settingsBar, "Damping", TW_TYPE_FLOAT, setDampingCallback, getDampingCallback, this,
            " min=0.0 max=1.0 step=0.1 group='Dynamic' ");

    //TwAddVarRW(bar, "Viscosity", TW_TYPE_FLOAT, &(viscosity),
    TwAddVarCB(settingsBar, "Viscosity", TW_TYPE_FLOAT, setViscosityCallback, getViscosityCallback, this,
            " min=1.0 max=1000.0 step=0.5 group='Dynamic' ");

    //TwAddVarRW(bar, "Stiffness", TW_TYPE_FLOAT, &(stiffness),
    TwAddVarCB(settingsBar, "Stiffness", TW_TYPE_FLOAT, setStiffnessCallback, getStiffnessCallback, this,
            " min=10.0 max=10000.0 step=10.0 group='Dynamic' ");

    //TwAddVarRW(bar, "Simulation scale", TW_TYPE_FLOAT, &(simulationScale),
    TwAddVarCB(settingsBar, "Simulation scale", TW_TYPE_FLOAT, setSimulationScaleCallback, getSimulationScaleCallback, this,
            " min=0.0001 max=0.1 step=0.0001 group='Dynamic' ");

    TwAddButton(settingsBar, "Reset", resetGravityAction, this, "label='Reset [Backspace]' group='External Force' ");

    TwAddVarCB(settingsBar, "ExternalForces", TW_TYPE_DIR3F, setExternalForcesCallback, getExternalForcesCallback, this,
            "label='Force' opened=true group='External Force' ");

    TwAddVarRW(settingsBar, "Boundary Box", TW_TYPE_BOOLCPP, &(drawBoundaries), "group='Draw'");
#ifndef USE_RAYCASTING
    TwAddVarRW(settingsBar, "Grid", TW_TYPE_BOOLCPP, &(drawGrid), "group='Draw'");
    TwAddVarRW(settingsBar, "Center", TW_TYPE_BOOLCPP, &(drawCenter), "group='Draw'");
#endif
    
#ifdef USE_RAYCASTING
    TwAddVarCB(settingsBar, "R min", TW_TYPE_FLOAT, setRMinCallback, getRMinCallback, this,
            " min=0.01 max=5.0 step=0.05 group='Dist Field Generation' ");

    TwAddVarCB(settingsBar, "Dist Field Factor", TW_TYPE_FLOAT, setDistFieldFactorCallback, getDistFieldFactorCallback, this,
            " min=0.1 max=100.0 step=0.1 group='Dist Field Generation' ");
#else
    {
        TwEnumVal shapeEV[NUM_COLORING_TYPE] = {
            {VELOCITY, "Velocity"},
            {PRESSURE, "Pressure"},
            {FORCE, "Force"},
        };
        TwType shapeType = TwDefineEnum("ColoringType", shapeEV, NUM_COLORING_TYPE);
        //TwAddVarRW(bar, "Type", shapeType, &(coloringSource), " group='Coloring' ");
        TwAddVarCB(settingsBar, "Type", shapeType, setColoringSourceCallback, getColoringSourceCallback, this, " group='Coloring' ");
    }

    {
        TwEnumVal shapeEV[NUM_GRADIENT_TYPE] = {
            {White, "White"},
            {Blackish, "Black"},
            {BlackToCyan, "Black-Cyan"},
            {BlueToWhite, "Blue-White"},
            {HSVBlueToRed, "HSV Blue-Red"},
        };
        TwType shapeType = TwDefineEnum("GradientType", shapeEV, NUM_GRADIENT_TYPE);
        //TwAddVarRW(bar, "Gradient", shapeType, &(coloringGradient), " group='Coloring' ");
        TwAddVarCB(settingsBar, "Gradient", shapeType, setColoringGradientCallback, getColoringGradientCallback, this, " group='Coloring' ");
    }
#endif

#if defined(EXTRACT_SURFACE_COLOR_FIELD) || defined(USE_RAYCASTING)
    TwAddVarCB(settingsBar, "Color field threshold", TW_TYPE_FLOAT, setColorFieldThresholdCallback, getColorFieldThresholdCallback, this,
            " min=-1000.0 max=100000.0 step=10.0 group='Surface Extraction' ");
#elif defined(EXTRACT_SURFACE_CENTER_OF_MASS)
    TwAddVarCB(settingsBar, "Distance threshold", TW_TYPE_FLOAT, setDistanceThresholdCallback, getDistanceThresholdCallback, this,
            " min=0.0 max=100.0 step=0.01 group='Surface Extraction' ");

    TwAddVarCB(settingsBar, "Minimum particles", TW_TYPE_INT32, setMinParticlesCallback, getMinParticlesCallback, this,
            " min=1 max=1000 step=1 group='Surface Extraction' ");
#endif

#ifdef USE_SURFACE_TENSION
    TwAddVarCB(settingsBar, "Surface tension coeff", TW_TYPE_FLOAT, setSurfaceTensionCoeffCallback, getSurfaceTensionCoeffCallback, this,
            " min=0.0 max=1000000.0 step=0.00001 group='Surface Extraction' ");
#endif

    TwAddButton(settingsBar, "Start", startAction, this, "label='Start [Enter]' group='Control' ");
    TwAddButton(settingsBar, "Pause", pauseAction, this, "label='Pause [Space]' group='Control' readonly=true");
    TwAddButton(settingsBar, "Stop", stopAction, this, " label='Stop [Delete]' group='Control' readonly=true");
}

void ControlPanel::initFluidParameters() {
    Configuration& config = Configuration::getInstance();

    InitialParticlePlacement placement = config.getInitialParticlePlacement();

    FluidParameters params(config.getParticleCount() * 1024);

    coloringSource = VELOCITY;
    coloringGradient = HSVBlueToRed;
    particleCount = (ParticleCount) params.getParticleCount();
    restDensity = params.getRestDensity();
    stiffness = params.getStiffness();
    particleMass = params.getParticleMass();
    timeStep = params.getTimestep();
    simulationScale = params.getSimulationScale();
    accLimit = params.getAccelerationLimit();
    damping = params.getDamping();
    viscosity = params.getViscosity();

    gravity.x = params.getGravityX();
    gravity.y = params.getGravityY();
    gravity.z = params.getGravityZ();

    particlePlacement = placement;

#ifdef USE_SURFACE_TENSION
    surfaceTensionCoeff = TEMP_SURFACE_TENSION_COEFF;
#endif

#if defined(USE_RAYCASTING)
    colorFieldThreshold = TEMP_COLOR_FIELD_THRESHOLD;
    rMin = TEMP_R_MIN;
    distFieldFactor = TEMP_DIST_FIELD_FACTOR;
    lightDirection.set(0.74f, -0.57f, -0.36f);
    //(1, -1, -1);
    lightDirection.normalize();
    
#elif defined(EXTRACT_SURFACE_COLOR_FIELD) 
    colorFieldThreshold = TEMP_COLOR_FIELD_THRESHOLD;
#elif defined(EXTRACT_SURFACE_CENTER_OF_MASS)
    minParticles = 19;
    distanceThreshold = 1.35f;
#endif

    // drawBoundaries = true;
}

FluidSimulation* ControlPanel::createNewFluidSimulation() {
    if (keepPreviousParams == false) {
        Configuration& config = Configuration::getInstance();
        config.setParticleCount((unsigned int) particleCount / 1024);
        config.setInitialParticlePlacement(particlePlacement);
        initFluidParameters();
    }
    FluidSimulation* f = new FluidSimulation((unsigned int) particleCount, particlePlacement);
    f->setColoringSource(coloringSource);
    f->setColoringGradient(coloringGradient);
    f->setRestDensity(restDensity);
    f->setStiffness(stiffness);
    f->setParticleMass(particleMass);
    f->setTimestep(timeStep);
    f->setSimulationScale(simulationScale);
    f->setAccelerationLimit(accLimit);
    f->setDamping(damping);
    f->setViscosity(viscosity);
    f->setGravityX(gravity.x);
    f->setGravityY(gravity.y);
    f->setGravityZ(gravity.z);


#ifdef USE_SURFACE_TENSION
    f->setSurfaceTensionCoeff(surfaceTensionCoeff);
#endif

#if defined(USE_RAYCASTING)
    f->setColorFieldThreshold(colorFieldThreshold);
    f->setRMin(rMin);
    f->setDistFieldFactor(distFieldFactor);
    f->setLightDirection(lightDirection.x, lightDirection.y, lightDirection.z);
#elif defined(EXTRACT_SURFACE_COLOR_FIELD) 
    f->setColorFieldThreshold(colorFieldThreshold);
#elif defined(EXTRACT_SURFACE_CENTER_OF_MASS)
    f->setDistanceThreshold(distanceThreshold);
    f->setMinParticles(minParticles);
#endif

    return f;
}

int ControlPanel::restartSimulation() {
    if (fluidSimulation != NULL) {
        delete fluidSimulation;
    }

    fluidSimulation = createNewFluidSimulation();
    fluidSimulation->setVerbose(false);

    //    fluidSimulation->printParameters();

    bool success = fluidSimulation->init();

    cout << boolalpha << "init " << " success = " << success << endl;
    if (!success) {
        return -1;
    }

    return 0;
}
#define MARGIN 5

void ControlPanel::windowResizeEvent(int w, int h) {
    TwWindowSize(w, h);

    TwGetParam(infoBar, NULL, "size", TW_PARAM_INT32, 2, &infoBarBounds.width);
    infoBarBounds.x = w - infoBarBounds.width - MARGIN;
    infoBarBounds.y = MARGIN;
    TwSetParam(infoBar, NULL, "position", TW_PARAM_INT32, 2, &infoBarBounds.x);

    settingsBarBounds.x = MARGIN;
    settingsBarBounds.y = MARGIN;
    TwSetParam(settingsBar, NULL, "position", TW_PARAM_INT32, 2, &settingsBarBounds.x);
    TwDefine(" Settings valueswidth=fit ");
    TwDefine(" Info valueswidth=70 ");
}

void ControlPanel::keyboardEventHandlerGLUT(unsigned char key, int x, int y) {

    switch (key) {

            /* ESCAPE_KEY = 27 */
        case 27:
        case 'q':
        case 'Q':
        {
            exit(0);
        }
            break;
        case ' ':
            togglePause();
            break;
            // BACKSPACE
        case 8:
            resetExternalForces();
            break;
            //ENTER:
        case 13:
            startSimulation();
            break;
            //DELETE:
        case 127:
            stopSimulation();
            break;
        default:
            break;
    }

    TwEventKeyboardGLUT(key, x, y);
}

void ControlPanel::mouseEventHandlerGLUT(int button, int state, int x, int y) {
    TwGetParam(settingsBar, NULL, "size", TW_PARAM_INT32, 2, &settingsBarBounds.width);
    TwGetParam(settingsBar, NULL, "position", TW_PARAM_INT32, 2, &settingsBarBounds.x);

    TwGetParam(infoBar, NULL, "size", TW_PARAM_INT32, 2, &infoBarBounds.width);
    TwGetParam(infoBar, NULL, "position", TW_PARAM_INT32, 2, &infoBarBounds.x);

    if (state == GLUT_DOWN) {

        if ((x >= settingsBarBounds.x
                && x < settingsBarBounds.x + settingsBarBounds.width
                && y >= settingsBarBounds.y
                && y < settingsBarBounds.y + settingsBarBounds.height)
                ||
                (x >= infoBarBounds.x
                && x < infoBarBounds.x + infoBarBounds.width
                && y >= infoBarBounds.y
                && y < infoBarBounds.y + infoBarBounds.height)) {
            mouseOver = true;
        }

    } else if (state == GLUT_UP) {
        mouseOver = false;
    }

    TwEventMouseButtonGLUT(button, state, x, y);
}

// ---------------------------------------------------------------------------------------------------------

void ControlPanel::startSimulation() {
    if (started) {
        return;
    }
    paused = false;
    started = true;
    TwDefine("Settings/Start readonly=true");
    TwDefine("Settings/Pause readonly=false");
    TwDefine("Settings/Stop readonly=false");
    TwDefine("Settings/ParticlesCount readonly=true");
    TwDefine("Settings/InitialArrangement readonly=true");
    //TwDefine("Settings/previousParams readonly=true");

    if (restartSimulation() != 0) {
        cout << "ERROR: CANNOT START SIMULATION!!" << endl;
    }
}

void ControlPanel::togglePause() {
    if (started) {
        paused = !paused;
    }
}

void TW_CALL ControlPanel::startAction(void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);
    controlPanel->startSimulation();
}

void TW_CALL ControlPanel::pauseAction(void* clientData) {

    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);
    if (!controlPanel->started) {
        return;
    }

    controlPanel->paused = !controlPanel->paused;
}

void ControlPanel::stopSimulation() {
    if (!started) {
        return;
    }

    started = false;
    paused = true;

    TwDefine("Settings/Start readonly=false");
    TwDefine("Settings/Pause readonly=true");
    TwDefine("Settings/Stop readonly=true");
    TwDefine("Settings/ParticlesCount readonly=false");
    TwDefine("Settings/InitialArrangement readonly=false");
    // TwDefine("Settings/previousParams readonly=false");

    delete fluidSimulation;
    fluidSimulation = NULL;
}

void TW_CALL ControlPanel::stopAction(void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);
    controlPanel->stopSimulation();
}

void TW_CALL ControlPanel::setColoringSourceCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->coloringSource = *(const ColoringSource*) value;

    if (controlPanel->fluidSimulation != NULL) {
        controlPanel->fluidSimulation->setColoringSource(controlPanel->coloringSource);
    }
}

void TW_CALL ControlPanel::getColoringSourceCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(ColoringSource*) value = controlPanel->coloringSource;
}

void TW_CALL ControlPanel::setColoringGradientCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->coloringGradient = *(const ColoringGradient*) value;

    if (controlPanel->fluidSimulation != NULL) {
        controlPanel->fluidSimulation->setColoringGradient(controlPanel->coloringGradient);
    }
}

void TW_CALL ControlPanel::getColoringGradientCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(ColoringGradient*) value = controlPanel->coloringGradient;
}

void TW_CALL ControlPanel::setGravityXCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->gravity.x = *(const float*) value;

    if (controlPanel->fluidSimulation != NULL) {
        controlPanel->fluidSimulation->setGravityX(controlPanel->gravity.x);
    }
}

void TW_CALL ControlPanel::getGravityXCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float*) value = controlPanel->gravity.x;
}

void TW_CALL ControlPanel::setGravityYCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->gravity.y = *(const float*) value;

    if (controlPanel->fluidSimulation != NULL) {
        controlPanel->fluidSimulation->setGravityY(controlPanel->gravity.y);
    }
}

void TW_CALL ControlPanel::getGravityYCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float*) value = controlPanel->gravity.y;
}

void TW_CALL ControlPanel::setGravityZCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->gravity.z = *(const float*) value;

    if (controlPanel->fluidSimulation != NULL) {
        controlPanel->fluidSimulation->setGravityZ(controlPanel->gravity.z);
    }
}

void TW_CALL ControlPanel::getGravityZCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float*) value = controlPanel->gravity.z;
}

void TW_CALL ControlPanel::setExternalForcesCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->gravity = *(const Gravity*) value;

    if (controlPanel->fluidSimulation != NULL) {
        controlPanel->fluidSimulation->setGravityX(controlPanel->gravity.x);
        controlPanel->fluidSimulation->setGravityY(controlPanel->gravity.y);
        controlPanel->fluidSimulation->setGravityZ(controlPanel->gravity.z);
    }
}

void TW_CALL ControlPanel::getExternalForcesCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(Gravity*) value = controlPanel->gravity;
}

void ControlPanel::resetExternalForces() {
    gravity.x = 0.0f;
    gravity.y = GRAVITY;
    gravity.z = 0.0f;

    if (fluidSimulation != NULL) {
        fluidSimulation->setGravityX(gravity.x);
        fluidSimulation->setGravityY(gravity.y);
        fluidSimulation->setGravityZ(gravity.z);
    }
}

void TW_CALL ControlPanel::resetGravityAction(void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);
    controlPanel->resetExternalForces();
}

void TW_CALL ControlPanel::setParticlePlacementCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->particlePlacement = *(const InitialParticlePlacement*) value;
}

void TW_CALL ControlPanel::getParticlePlacementCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(InitialParticlePlacement*) value = controlPanel->particlePlacement;
}

void TW_CALL ControlPanel::setParticleCountCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->particleCount = *(const ParticleCount*) value;
}

void TW_CALL ControlPanel::getParticleCountCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(ParticleCount*) value = controlPanel->particleCount;
}

void TW_CALL ControlPanel::setRestDensityCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->restDensity = *(const float*) value;

    if (controlPanel->fluidSimulation != NULL) {
        controlPanel->fluidSimulation->setRestDensity(controlPanel->restDensity);
    }
}

void TW_CALL ControlPanel::getRestDensityCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float*) value = controlPanel->restDensity;
}

void TW_CALL ControlPanel::setStiffnessCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->stiffness = *(const float*) value;

    if (controlPanel->fluidSimulation != NULL) {
        controlPanel->fluidSimulation->setStiffness(controlPanel->stiffness);
    }
}

void TW_CALL ControlPanel::getStiffnessCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float*) value = controlPanel->stiffness;
}

void TW_CALL ControlPanel::setParticleMassCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->particleMass = *(const float*) value;

    if (controlPanel->fluidSimulation != NULL) {
        controlPanel->fluidSimulation->setParticleMass(controlPanel->particleMass);
    }
}

void TW_CALL ControlPanel::getParticleMassCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float*) value = controlPanel->particleMass;
}

void TW_CALL ControlPanel::setTimestepCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->timeStep = *(const float*) value;

    if (controlPanel->fluidSimulation != NULL) {
        controlPanel->fluidSimulation->setTimestep(controlPanel->timeStep);
    }
}

void TW_CALL ControlPanel::getTimestepCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float*) value = controlPanel->timeStep;
}

void TW_CALL ControlPanel::setSimulationScaleCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->simulationScale = *(const float*) value;

    if (controlPanel->fluidSimulation != NULL) {
        controlPanel->fluidSimulation->setSimulationScale(controlPanel->simulationScale);
    }
}

void TW_CALL ControlPanel::getSimulationScaleCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float*) value = controlPanel->simulationScale;
}

void TW_CALL ControlPanel::setAccelerationLimitCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->accLimit = *(const float*) value;

    if (controlPanel->fluidSimulation != NULL) {
        controlPanel->fluidSimulation->setAccelerationLimit(controlPanel->accLimit);
    }
}

void TW_CALL ControlPanel::getAccelerationLimitCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float*) value = controlPanel->accLimit;
}

void TW_CALL ControlPanel::setDampingCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->damping = *(const float*) value;

    if (controlPanel->fluidSimulation != NULL) {
        controlPanel->fluidSimulation->setDamping(controlPanel->damping);
    }
}

void TW_CALL ControlPanel::getDampingCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float*) value = controlPanel->damping;
}

void TW_CALL ControlPanel::setViscosityCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->viscosity = *(const float*) value;

    if (controlPanel->fluidSimulation != NULL) {
        controlPanel->fluidSimulation->setViscosity(controlPanel->viscosity);
    }
}

void TW_CALL ControlPanel::getViscosityCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float*) value = controlPanel->viscosity;
}

void TW_CALL ControlPanel::getWorldSupportRadiusCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);
    if (controlPanel->fluidSimulation) {
        FluidParameters& fluidParams = controlPanel->fluidSimulation->getFluidParameters();
        *(int*) value = fluidParams.getSupportRadiusScaled();
    } else {
        *(int*) value = 0;
    }
}

void TW_CALL ControlPanel::getWorldParticleRadiusCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);
    if (controlPanel->fluidSimulation) {
        FluidParameters& fluidParams = controlPanel->fluidSimulation->getFluidParameters();
        *(float*) value = fluidParams.getParticleRadiusScaled();
    } else {
        *(float*) value = 0;
    }
}

void TW_CALL ControlPanel::getWorldParticleRestDistanceCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);
    if (controlPanel->fluidSimulation) {
        FluidParameters& fluidParams = controlPanel->fluidSimulation->getFluidParameters();
        *(float*) value = fluidParams.getParticleRestDistanceScaled();
    } else {
        *(float*) value = 0;
    }
}

void TW_CALL ControlPanel::getWorldToSimulationScaleCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);
    if (controlPanel->fluidSimulation) {
        FluidParameters& fluidParams = controlPanel->fluidSimulation->getFluidParameters();
        *(float*) value = fluidParams.getSimulationScale();
    } else {
        *(float*) value = 0;
    }
}

void TW_CALL ControlPanel::getSimSupportRadiusCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);
    if (controlPanel->fluidSimulation) {
        FluidParameters& fluidParams = controlPanel->fluidSimulation->getFluidParameters();
        *(float*) value = fluidParams.getSupportRadius();
    } else {
        *(float*) value = 0;
    }
}

void TW_CALL ControlPanel::getSimParticleRadiusCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);
    if (controlPanel->fluidSimulation) {
        FluidParameters& fluidParams = controlPanel->fluidSimulation->getFluidParameters();
        *(float*) value = fluidParams.getParticleRadius();
    } else {
        *(float*) value = 0;
    }
}

void TW_CALL ControlPanel::getSimParticleRestDistanceCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);
    if (controlPanel->fluidSimulation) {
        FluidParameters& fluidParams = controlPanel->fluidSimulation->getFluidParameters();
        *(float*) value = fluidParams.getParticleRestDistance();
    } else {
        *(float*) value = 0;
    }
}

void TW_CALL ControlPanel::getSimToWorldScaleCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);
    if (controlPanel->fluidSimulation) {
        FluidParameters& fluidParams = controlPanel->fluidSimulation->getFluidParameters();
        *(float*) value = fluidParams.getSimulationScaleInv();
    } else {
        *(float*) value = 0;
    }
}

void TW_CALL ControlPanel::getSimParticleMassCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);
    if (controlPanel->fluidSimulation) {
        FluidParameters& fluidParams = controlPanel->fluidSimulation->getFluidParameters();
        *(float*) value = fluidParams.getParticleMass();
    } else {
        *(float*) value = 0;
    }
}

void TW_CALL ControlPanel::getSimParticleVolumeCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);
    if (controlPanel->fluidSimulation) {
        FluidParameters& fluidParams = controlPanel->fluidSimulation->getFluidParameters();
        *(float*) value = fluidParams.getParticleVolume();
    } else {
        *(float*) value = 0;
    }
}

void TW_CALL ControlPanel::getSimTotalMassCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);
    if (controlPanel->fluidSimulation) {
        FluidParameters& fluidParams = controlPanel->fluidSimulation->getFluidParameters();
        *(float*) value = fluidParams.getTotalMass();
    } else {
        *(float*) value = 0;
    }
}

void TW_CALL ControlPanel::getSimTotalVolumeCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);
    if (controlPanel->fluidSimulation) {
        FluidParameters& fluidParams = controlPanel->fluidSimulation->getFluidParameters();
        *(float*) value = fluidParams.getTotalVolume();
    } else {
        *(float*) value = 0;
    }
}

void TW_CALL ControlPanel::getSimTotalLengthCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);
    if (controlPanel->fluidSimulation) {
        FluidParameters& fluidParams = controlPanel->fluidSimulation->getFluidParameters();
        *(float*) value = fluidParams.getTotalLength();
    } else {
        *(float*) value = 0;
    }
}

void TW_CALL ControlPanel::getTotalVolumeCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);
    if (controlPanel->fluidSimulation) {
        FluidParameters& fluidParams = controlPanel->fluidSimulation->getFluidParameters();
        *(float*) value = fluidParams.getTotalVolumeScaled();
    } else {
        *(float*) value = 0;
    }
}

void TW_CALL ControlPanel::getTotalLengthCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);
    if (controlPanel->fluidSimulation) {
        FluidParameters& fluidParams = controlPanel->fluidSimulation->getFluidParameters();
        *(float*) value = fluidParams.getTotalLengthScaled();
    } else {
        *(float*) value = 0;
    }
}

void TW_CALL ControlPanel::getTotalParticlesBoxHeightCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);
    if (controlPanel->fluidSimulation) {
        FluidParameters& fluidParams = controlPanel->fluidSimulation->getFluidParameters();
        *(float*) value = fluidParams.getTotalParticlesBoxHeight();
    } else {
        *(float*) value = 0;
    }
}

#ifdef USE_SURFACE_TENSION

void TW_CALL ControlPanel::setSurfaceTensionCoeffCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->surfaceTensionCoeff = *(const float*) value;

    if (controlPanel->fluidSimulation != NULL) {
        controlPanel->fluidSimulation->setSurfaceTensionCoeff(controlPanel->surfaceTensionCoeff);
    }
}

void TW_CALL ControlPanel::getSurfaceTensionCoeffCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float*) value = controlPanel->surfaceTensionCoeff;
}

#endif

#if defined(USE_RAYCASTING)

void TW_CALL ControlPanel::setColorFieldThresholdCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->colorFieldThreshold = *(const float*) value;

    if (controlPanel->fluidSimulation != NULL) {
        controlPanel->fluidSimulation->setColorFieldThreshold(controlPanel->colorFieldThreshold);
    }
}

void TW_CALL ControlPanel::getColorFieldThresholdCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float*) value = controlPanel->colorFieldThreshold;
}

void TW_CALL ControlPanel::setRMinCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->rMin = *(const float*) value;

    if (controlPanel->fluidSimulation != NULL) {
        controlPanel->fluidSimulation->setRMin(controlPanel->rMin);
    }
}

void TW_CALL ControlPanel::getRMinCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float*) value = controlPanel->rMin;
}

void TW_CALL ControlPanel::setDistFieldFactorCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->distFieldFactor = *(const float*) value;

    if (controlPanel->fluidSimulation != NULL) {
        controlPanel->fluidSimulation->setDistFieldFactor(controlPanel->distFieldFactor);
    }
}

void TW_CALL ControlPanel::getDistFieldFactorCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float*) value = controlPanel->distFieldFactor;
}

void TW_CALL ControlPanel::setLightDirectionCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->lightDirection = *(const rev::graph::Vector3f*) value;

    if (controlPanel->fluidSimulation != NULL) {
        controlPanel->lightDirection.normalize();

        controlPanel->fluidSimulation->setLightDirection(
                controlPanel->lightDirection.x,
                controlPanel->lightDirection.y,
                controlPanel->lightDirection.z);
    }
}

void TW_CALL ControlPanel::getLightDirectionCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(rev::graph::Vector3f*) value = controlPanel->lightDirection;
}

#elif defined(EXTRACT_SURFACE_COLOR_FIELD) 

void TW_CALL ControlPanel::setColorFieldThresholdCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->colorFieldThreshold = *(const float*) value;

    if (controlPanel->fluidSimulation != NULL) {
        controlPanel->fluidSimulation->setColorFieldThreshold(controlPanel->colorFieldThreshold);
    }
}

void TW_CALL ControlPanel::getColorFieldThresholdCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float*) value = controlPanel->colorFieldThreshold;
}

#elif defined(EXTRACT_SURFACE_CENTER_OF_MASS)

void TW_CALL ControlPanel::setDistanceThresholdCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->distanceThreshold = *(const float*) value;

    if (controlPanel->fluidSimulation != NULL) {
        controlPanel->fluidSimulation->setDistanceThreshold(controlPanel->distanceThreshold);
    }
}

void TW_CALL ControlPanel::getDistanceThresholdCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float*) value = controlPanel->distanceThreshold;
}

void TW_CALL ControlPanel::setMinParticlesCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->minParticles = *(const unsigned int*) value;

    if (controlPanel->fluidSimulation != NULL) {
        controlPanel->fluidSimulation->setMinParticles(controlPanel->minParticles);
    }
}

void TW_CALL ControlPanel::getMinParticlesCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(unsigned int*) value = controlPanel->minParticles;
}
#endif

void TW_CALL ControlPanel::testAction(void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);
    if (controlPanel->fluidSimulation) {
        FluidParameters& fluidParams = controlPanel->fluidSimulation->getFluidParameters();
        //fluidParams.printParameters();
        float blockLen = (float) controlPanel->blockLength;
        float restDistance = fluidParams.getParticleRestDistanceScaled();
        float ratio = blockLen / restDistance;
        controlPanel->avgParticles = ratio * ratio * ratio * 4;

        const float h = pow(((3.0 * fluidParams.getTotalVolume()
                * controlPanel->avgParticles)
                / (4.0 * M_PI * fluidParams.getParticleCount())), 1.0 / 3.0);
        controlPanel->suggestedSupportRadius = h;
        controlPanel->suggestedScale = h / blockLen;
    } else {
        cout << "FLUID SIM = NULL" << endl;
    }
}

