/* 
 * File:   OpenCLBase.cpp
 * Author: Revers
 * 
 * Created on 27 październik 2011, 17:48
 */

#include "OpenCLBase.h"

cl_platform_id OpenCLBase::getAmdOrDefaultPlatform() {
    std::vector<cl::Platform> objPlatforms; /**< CL platform list */


    cl::Platform::get(&objPlatforms);
    std::vector<cl::Platform>::iterator iter;
    for (iter = objPlatforms.begin(); iter != objPlatforms.end(); ++iter) {
        if ((*iter).getInfo<CL_PLATFORM_VENDOR > () == "Advanced Micro Devices, Inc.")
            break;
    }

    if (iter != objPlatforms.end()) {
        return (*iter)();
    }

    if (objPlatforms.size() == 0) {
        return NULL;
    }

    return (objPlatforms[0])();
}

#define CHECK_ERR(A) if(!(A)) return false
#define CHECK_CL_ERR(A) if((A) != CL_SUCCESS) return false

bool OpenCLBase::initCL(bool withOpenGL /* = true */) {
    //try {
    cl_int err;
    cl_platform_id platformId = getAmdOrDefaultPlatform();
    if (platformId == NULL) {
        std::cerr << "ERROR: There is not any platform!!";
        return false;
    }

    if (withOpenGL) {
#ifdef _WIN32
        HGLRC glCtx = wglGetCurrentContext();
#else //!_WIN32
        GLXContext glCtx = glXGetCurrentContext();
#endif //!_WIN32
        /////////////////////////////////////////////////////////////////
        // Create an OpenCL context
        /////////////////////////////////////////////////////////////////
        cl_context_properties cps[7] = {CL_CONTEXT_PLATFORM,
            (cl_context_properties) platformId,
#ifdef _WIN32
            CL_WGL_HDC_KHR, (intptr_t) wglGetCurrentDC(),
#else //!_WIN32
            CL_GLX_DISPLAY_KHR, (intptr_t) glXGetCurrentDisplay(),
#endif //!_WIN32
            CL_GL_CONTEXT_KHR, (intptr_t) glCtx, 0};

        clContext = cl::Context(CL_DEVICE_TYPE_GPU, cps);
    } else {
        cl_context_properties cps[3] = {CL_CONTEXT_PLATFORM,
            (cl_context_properties) platformId, 0};

        clContext = cl::Context(clDevType, cps, NULL, NULL, &err);
        CHECK_CL_ERR(err);
    }

    clDevices = clContext.getInfo<CL_CONTEXT_DEVICES > (&err);
    CHECK_CL_ERR(err);

    cl::Device& clDevice = clDevices[clDeviceId];
    if (enableProfiling) {
        clCommandQueue = cl::CommandQueue(clContext, clDevice, CL_QUEUE_PROFILING_ENABLE, &err);
    } else {
        clCommandQueue = cl::CommandQueue(clContext, clDevice, 0, &err);
    }
    CHECK_CL_ERR(err);

    clMaxWorkGroupSize = clDevice.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE > (&err);
    CHECK_CL_ERR(err);
    clMaxDimensions = clDevice.getInfo<CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS > (&err);
    CHECK_CL_ERR(err);
    //  std::cout << "maxWorkGroupSize = " << maxWorkGroupSize << std::endl;
    //  std::cout << "maxDimensions = " << maxDimensions << std::endl;
//    if (verbose) {
//        printDeviceInfo();
//    }

    //    } catch (cl::Error err) {
    //        std::cerr << "ERROR: " << err.what()
    //                << "(" << getOpenCLErrorCodeStr(err.err())
    //                << ")" << std::endl;
    //
    //        return false;
    //    }

    return true;

}

void OpenCLBase::oclPrintDevInfo(cl_device_id device) {
    char device_string[1024];
    bool nv_device_attibute_query = false;

    // CL_DEVICE_NAME
    clGetDeviceInfo(device, CL_DEVICE_NAME, sizeof (device_string), &device_string, NULL);
    printLog("  CL_DEVICE_NAME: \t\t\t%s\n", device_string);

    // CL_DEVICE_VENDOR
    clGetDeviceInfo(device, CL_DEVICE_VENDOR, sizeof (device_string), &device_string, NULL);
    printLog("  CL_DEVICE_VENDOR: \t\t\t%s\n", device_string);

    // CL_DRIVER_VERSION
    clGetDeviceInfo(device, CL_DRIVER_VERSION, sizeof (device_string), &device_string, NULL);
    printLog("  CL_DRIVER_VERSION: \t\t\t%s\n", device_string);

    // CL_DEVICE_INFO
    cl_device_type type;
    clGetDeviceInfo(device, CL_DEVICE_TYPE, sizeof (type), &type, NULL);
    if (type & CL_DEVICE_TYPE_CPU)
        printLog("  CL_DEVICE_TYPE:\t\t\t%s\n", "CL_DEVICE_TYPE_CPU");
    if (type & CL_DEVICE_TYPE_GPU)
        printLog("  CL_DEVICE_TYPE:\t\t\t%s\n", "CL_DEVICE_TYPE_GPU");
    if (type & CL_DEVICE_TYPE_ACCELERATOR)
        printLog("  CL_DEVICE_TYPE:\t\t\t%s\n", "CL_DEVICE_TYPE_ACCELERATOR");
    if (type & CL_DEVICE_TYPE_DEFAULT)
        printLog("  CL_DEVICE_TYPE:\t\t\t%s\n", "CL_DEVICE_TYPE_DEFAULT");

    // CL_DEVICE_MAX_COMPUTE_UNITS
    cl_uint compute_units;
    clGetDeviceInfo(device, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof (compute_units), &compute_units, NULL);
    printLog("  CL_DEVICE_MAX_COMPUTE_UNITS:\t\t%u\n", compute_units);

    // CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS
    size_t workitem_dims;
    clGetDeviceInfo(device, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, sizeof (workitem_dims), &workitem_dims, NULL);
    printLog("  CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS:\t%u\n", workitem_dims);

    // CL_DEVICE_MAX_WORK_ITEM_SIZES
    size_t workitem_size[3];
    clGetDeviceInfo(device, CL_DEVICE_MAX_WORK_ITEM_SIZES, sizeof (workitem_size), &workitem_size, NULL);
    printLog("  CL_DEVICE_MAX_WORK_ITEM_SIZES:\t%u / %u / %u \n", workitem_size[0], workitem_size[1], workitem_size[2]);

    // CL_DEVICE_MAX_WORK_GROUP_SIZE
    size_t workgroup_size;
    clGetDeviceInfo(device, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof (workgroup_size), &workgroup_size, NULL);
    printLog("  CL_DEVICE_MAX_WORK_GROUP_SIZE:\t%u\n", workgroup_size);

    // CL_DEVICE_MAX_CLOCK_FREQUENCY
    cl_uint clock_frequency;
    clGetDeviceInfo(device, CL_DEVICE_MAX_CLOCK_FREQUENCY, sizeof (clock_frequency), &clock_frequency, NULL);
    printLog("  CL_DEVICE_MAX_CLOCK_FREQUENCY:\t%u MHz\n", clock_frequency);

    // CL_DEVICE_ADDRESS_BITS
    cl_uint addr_bits;
    clGetDeviceInfo(device, CL_DEVICE_ADDRESS_BITS, sizeof (addr_bits), &addr_bits, NULL);
    printLog("  CL_DEVICE_ADDRESS_BITS:\t\t%u\n", addr_bits);

    // CL_DEVICE_MAX_MEM_ALLOC_SIZE
    cl_ulong max_mem_alloc_size;
    clGetDeviceInfo(device, CL_DEVICE_MAX_MEM_ALLOC_SIZE, sizeof (max_mem_alloc_size), &max_mem_alloc_size, NULL);
    printLog("  CL_DEVICE_MAX_MEM_ALLOC_SIZE:\t\t%u MByte\n", (unsigned int) (max_mem_alloc_size / (1024 * 1024)));

    // CL_DEVICE_GLOBAL_MEM_SIZE
    cl_ulong mem_size;
    clGetDeviceInfo(device, CL_DEVICE_GLOBAL_MEM_SIZE, sizeof (mem_size), &mem_size, NULL);
    printLog("  CL_DEVICE_GLOBAL_MEM_SIZE:\t\t%u MByte\n", (unsigned int) (mem_size / (1024 * 1024)));

    // CL_DEVICE_ERROR_CORRECTION_SUPPORT
    cl_bool error_correction_support;
    clGetDeviceInfo(device, CL_DEVICE_ERROR_CORRECTION_SUPPORT, sizeof (error_correction_support), &error_correction_support, NULL);
    printLog("  CL_DEVICE_ERROR_CORRECTION_SUPPORT:\t%s\n", error_correction_support == CL_TRUE ? "yes" : "no");

    // CL_DEVICE_LOCAL_MEM_TYPE
    cl_device_local_mem_type local_mem_type;
    clGetDeviceInfo(device, CL_DEVICE_LOCAL_MEM_TYPE, sizeof (local_mem_type), &local_mem_type, NULL);
    printLog("  CL_DEVICE_LOCAL_MEM_TYPE:\t\t%s\n", local_mem_type == 1 ? "local" : "global");

    // CL_DEVICE_LOCAL_MEM_SIZE
    clGetDeviceInfo(device, CL_DEVICE_LOCAL_MEM_SIZE, sizeof (mem_size), &mem_size, NULL);
    printLog("  CL_DEVICE_LOCAL_MEM_SIZE:\t\t%u KByte\n", (unsigned int) (mem_size / 1024));

    // CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE
    clGetDeviceInfo(device, CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE, sizeof (mem_size), &mem_size, NULL);
    printLog("  CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE:\t%u KByte\n", (unsigned int) (mem_size / 1024));

    // CL_DEVICE_QUEUE_PROPERTIES
    cl_command_queue_properties queue_properties;
    clGetDeviceInfo(device, CL_DEVICE_QUEUE_PROPERTIES, sizeof (queue_properties), &queue_properties, NULL);
    if (queue_properties & CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE)
        printLog("  CL_DEVICE_QUEUE_PROPERTIES:\t\t%s\n", "CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE");
    if (queue_properties & CL_QUEUE_PROFILING_ENABLE)
        printLog("  CL_DEVICE_QUEUE_PROPERTIES:\t\t%s\n", "CL_QUEUE_PROFILING_ENABLE");

    // CL_DEVICE_IMAGE_SUPPORT
    cl_bool image_support;
    clGetDeviceInfo(device, CL_DEVICE_IMAGE_SUPPORT, sizeof (image_support), &image_support, NULL);
    printLog("  CL_DEVICE_IMAGE_SUPPORT:\t\t%u\n", image_support);

    // CL_DEVICE_MAX_READ_IMAGE_ARGS
    cl_uint max_read_image_args;
    clGetDeviceInfo(device, CL_DEVICE_MAX_READ_IMAGE_ARGS, sizeof (max_read_image_args), &max_read_image_args, NULL);
    printLog("  CL_DEVICE_MAX_READ_IMAGE_ARGS:\t%u\n", max_read_image_args);

    // CL_DEVICE_MAX_WRITE_IMAGE_ARGS
    cl_uint max_write_image_args;
    clGetDeviceInfo(device, CL_DEVICE_MAX_WRITE_IMAGE_ARGS, sizeof (max_write_image_args), &max_write_image_args, NULL);
    printLog("  CL_DEVICE_MAX_WRITE_IMAGE_ARGS:\t%u\n", max_write_image_args);

    // CL_DEVICE_IMAGE2D_MAX_WIDTH, CL_DEVICE_IMAGE2D_MAX_HEIGHT, CL_DEVICE_IMAGE3D_MAX_WIDTH, CL_DEVICE_IMAGE3D_MAX_HEIGHT, CL_DEVICE_IMAGE3D_MAX_DEPTH
    size_t szMaxDims[5];
    printLog("\n  CL_DEVICE_IMAGE <dim>");
    clGetDeviceInfo(device, CL_DEVICE_IMAGE2D_MAX_WIDTH, sizeof (size_t), &szMaxDims[0], NULL);
    printLog("\t\t\t2D_MAX_WIDTH\t %u\n", szMaxDims[0]);
    clGetDeviceInfo(device, CL_DEVICE_IMAGE2D_MAX_HEIGHT, sizeof (size_t), &szMaxDims[1], NULL);
    printLog("\t\t\t\t\t2D_MAX_HEIGHT\t %u\n", szMaxDims[1]);
    clGetDeviceInfo(device, CL_DEVICE_IMAGE3D_MAX_WIDTH, sizeof (size_t), &szMaxDims[2], NULL);
    printLog("\t\t\t\t\t3D_MAX_WIDTH\t %u\n", szMaxDims[2]);
    clGetDeviceInfo(device, CL_DEVICE_IMAGE3D_MAX_HEIGHT, sizeof (size_t), &szMaxDims[3], NULL);
    printLog("\t\t\t\t\t3D_MAX_HEIGHT\t %u\n", szMaxDims[3]);
    clGetDeviceInfo(device, CL_DEVICE_IMAGE3D_MAX_DEPTH, sizeof (size_t), &szMaxDims[4], NULL);
    printLog("\t\t\t\t\t3D_MAX_DEPTH\t %u\n", szMaxDims[4]);

    // CL_DEVICE_EXTENSIONS: get device extensions, and if any then parse & log the string onto separate lines
    clGetDeviceInfo(device, CL_DEVICE_EXTENSIONS, sizeof (device_string), &device_string, NULL);
    if (device_string != 0) {
        printLog("\n  CL_DEVICE_EXTENSIONS:");
        std::string stdDevString;
        stdDevString = std::string(device_string);
        size_t szOldPos = 0;
        size_t szSpacePos = stdDevString.find(' ', szOldPos); // extensions string is space delimited
        while (szSpacePos != stdDevString.npos && (szSpacePos - szOldPos) > 0) {
            if (strcmp("cl_nv_device_attribute_query", stdDevString.substr(szOldPos, szSpacePos - szOldPos).c_str()) == 0)
                nv_device_attibute_query = true;

            if (szOldPos > 0) {
                printLog("\t\t");
            }
            printLog("\t\t\t%s\n", stdDevString.substr(szOldPos, szSpacePos - szOldPos).c_str());

            szOldPos = szSpacePos + 1;
            szSpacePos = stdDevString.find(' ', szOldPos);
        }
    } else {
        printLog("  CL_DEVICE_EXTENSIONS: None\n");
    }

    // CL_DEVICE_PREFERRED_VECTOR_WIDTH_<type>
    printLog("  CL_DEVICE_PREFERRED_VECTOR_WIDTH_<t>\t");
    cl_uint vec_width [6];
    clGetDeviceInfo(device, CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR, sizeof (cl_uint), &vec_width[0], NULL);
    clGetDeviceInfo(device, CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT, sizeof (cl_uint), &vec_width[1], NULL);
    clGetDeviceInfo(device, CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT, sizeof (cl_uint), &vec_width[2], NULL);
    clGetDeviceInfo(device, CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG, sizeof (cl_uint), &vec_width[3], NULL);
    clGetDeviceInfo(device, CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT, sizeof (cl_uint), &vec_width[4], NULL);
    clGetDeviceInfo(device, CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE, sizeof (cl_uint), &vec_width[5], NULL);
    printLog("CHAR %u, SHORT %u, INT %u, FLOAT %u, DOUBLE %u\n\n\n",
            vec_width[0], vec_width[1], vec_width[2], vec_width[3], vec_width[4]);
}

