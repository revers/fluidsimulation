/* 
 * File:   CLBufferImage2DImage2D.cpp
 * Author: Revers
 * 
 * Created on 22 listopad 2011, 18:43
 */

#include <cassert>

#include "Parameters.h"

#include "CLBufferImage2D.h"

#include "OpenCLBase.h"

#include "CLBuffer.h"
#include "CLBufferVboGL.h"

using namespace std;

#define IMG_NUM_CHANNELS 4

CLBufferImage2D::CLBufferImage2D(const CLBufferImage2D& old) {
    this->clContext = old.clContext;
    this->clCommandQueue = old.clCommandQueue;
    this->size = old.size;
    this->debugName = old.debugName;
    this->buffer = old.buffer;
    this->width = old.width;
    this->height = old.height;
    this->rowPitch = old.rowPitch;
}

bool CLBufferImage2D::init(
        CLBufferImage2DType type,
        cl::Context clContext,
        cl::CommandQueue clCommandQueue,
        size_t size,
        size_t rowSize,
        const char* debugName/* = NULL */,
        cl_mem_flags flags/* = CL_MEM_READ_WRITE */) {

    this->clContext = clContext;
    this->clCommandQueue = clCommandQueue;
    this->debugName = debugName;

    size_t oneElementSize = IMG_NUM_CHANNELS * sizeof (float);

    if ((size % oneElementSize) != 0) {
        assert((size % oneElementSize) == 0);
        return false;
    }

    size_t elements = size / oneElementSize;

    if ((elements % rowSize) != 0) {
        assert((elements % rowSize) == 0);
        return false;
    }

    width = rowSize;
    rowPitch = width * oneElementSize;
    height = elements / width;

    cl_int err;

    switch (type) {
        case UINT4:
        {
            buffer = cl::Image2D(clContext, flags, cl::ImageFormat(CL_RGBA, CL_UNSIGNED_INT32),
                    width, height, 0, NULL, &err);
            break;
        }
        case INT4:
        {
            buffer = cl::Image2D(clContext, flags, cl::ImageFormat(CL_RGBA, CL_SIGNED_INT32),
                    width, height, 0, NULL, &err);
            break;
        }
        case FLOAT4:
        {
            buffer = cl::Image2D(clContext, flags, cl::ImageFormat(CL_RGBA, CL_FLOAT),
                    width, height, 0, NULL, &err);
            break;
        }
    }


    if (err != CL_SUCCESS) {
        printError(err, "INITIALIZATION", " BUFFER FAILED");
        return false;
    }

    this->size = size;

    return true;
}

cl_int CLBufferImage2D::copyTo(CLBuffer& destBuffer) {
    cl_int err;

    cl::size_t < 3 > origin;
    origin[0] = 0;
    origin[1] = 0;
    origin[2] = 0;

    cl::size_t < 3 > region;
    region[0] = width;
    region[1] = height;
    region[2] = 1;

    err = clCommandQueue.enqueueCopyImageToBuffer(buffer, destBuffer.buffer, origin,
            region, 0, NULL, NULL);

    return err;
}

 cl_int CLBufferImage2D::copyTo(CLBufferImage2D destBuffer) {
     cl_int err;

    cl::size_t < 3 > srcOrigin;
    srcOrigin[0] = 0;
    srcOrigin[1] = 0;
    srcOrigin[2] = 0;
    
    cl::size_t < 3 > dstOrigin;
    dstOrigin[0] = 0;
    dstOrigin[1] = 0;
    dstOrigin[2] = 0;

    cl::size_t < 3 > region;
    region[0] = width;
    region[1] = height;
    region[2] = 1;
    
    err = clCommandQueue.enqueueCopyImage(buffer, destBuffer.buffer, srcOrigin, dstOrigin,
            region, NULL, NULL);

    return err;
 }

cl_int CLBufferImage2D::copyTo(CLBufferVboGL& destBuffer) {
    cl_int err;

    cl::size_t < 3 > origin;
    origin[0] = 0;
    origin[1] = 0;
    origin[2] = 0;

    cl::size_t < 3 > region;
    region[0] = width;
    region[1] = height;
    region[2] = 1;

    err = clCommandQueue.enqueueCopyImageToBuffer(buffer, destBuffer.buffer, origin,
            region, 0, NULL, NULL);

    return err;
}

bool CLBufferImage2D::write(void *sourcePtr, cl_bool blocking_write/* = CL_TRUE */) {
    assert(buffer() != 0);
    cl_int err;

    cl::size_t < 3 > origin;
    origin[0] = 0;
    origin[1] = 0;
    origin[2] = 0;

    cl::size_t < 3 > region;
    region[0] = width;
    region[1] = height;
    region[2] = 1;

    err = clCommandQueue.enqueueWriteImage(buffer, blocking_write, origin,
            region, rowPitch, 0, sourcePtr, NULL, NULL);

    if (err != CL_SUCCESS) {
        printError(err, "WRITE", " BUFFER FAILED");
        return false;
    }

    return true;
}

bool CLBufferImage2D::read(void* destPtr, cl_bool blocking_read/* = CL_TRUE */) {
    assert(buffer() != 0);
    cl_int err;

    cl::size_t < 3 > origin;
    origin[0] = 0;
    origin[1] = 0;
    origin[2] = 0;

    cl::size_t < 3 > region;
    region[0] = width;
    region[1] = height;
    region[2] = 1;

    err = clCommandQueue.enqueueReadImage(buffer, blocking_read, origin,
            region, rowPitch, 0, destPtr, NULL, NULL);

    if (err != CL_SUCCESS) {
        printError(err, "READ", " BUFFER FAILED");
        return false;
    }

    return true;
}

void CLBufferImage2D::printError(cl_int err, const char* beginMsg, const char* endMsg) {
    if (debugName) {
        std::cout << "ERROR: " << beginMsg << " '" << debugName << "'" << endMsg
                << OpenCLBase::getOpenCLErrorCodeStr(err) << std::endl;
    } else {
        std::cout << "ERROR: " << beginMsg << endMsg
                << OpenCLBase::getOpenCLErrorCodeStr(err) << std::endl;
    }
}

//------------------------------------------------------------------------------

CLPingPongBufferImage2D::CLPingPongBufferImage2D(const CLPingPongBufferImage2D& old) {
    this->size = old.size;
    this->totalSize = old.totalSize;
    this->debugName = old.debugName;
    this->first = old.first;
    this->second = old.second;
}

bool CLPingPongBufferImage2D::init(CLBufferImage2DType type,
        cl::Context clContext,
        cl::CommandQueue clCommandQueue,
        size_t size,
        size_t rowSize,
        const char* debugName/* = */,
        cl_mem_flags flags/* = CL_MEM_READ_WRITE */) {

    this->debugName = debugName;

    bool succ = first.init(type, clContext, clCommandQueue, size, rowSize, "PingPong_1", flags);

    if (succ == false) {
        printError("INITIALIZATION PING PONG #1", " BUFFER FAILED");
        return false;
    }

    succ = second.init(type, clContext, clCommandQueue, size, rowSize, "PingPong_2", flags);

    if (succ == false) {
        printError("INITIALIZATION PING PONG #2", " BUFFER FAILED");
        return false;
    }

    this->size = size;
    this->totalSize = size * 2;

    return true;
}

bool CLPingPongBufferImage2D::writeFirst(void *sourcePtr, cl_bool blocking_write/* = CL_TRUE */) {
    if (first.write(sourcePtr, blocking_write)) {
        return true;
    }

    printError("WRITE FIRST PING PONG", " BUFFER FAILED");
    return false;
}

bool CLPingPongBufferImage2D::readFirst(void* destPtr, cl_bool blocking_read/* = CL_TRUE */) {
    if (first.read(destPtr, blocking_read)) {
        return true;
    }

    printError("READ FIRST PING PONG", " BUFFER FAILED");
    return false;
}

bool CLPingPongBufferImage2D::writeSecond(void *sourcePtr, cl_bool blocking_write/* = CL_TRUE */) {
    if (second.write(sourcePtr, blocking_write)) {
        return true;
    }

    printError("WRITE SECOND PING PONG", " BUFFER FAILED");
    return false;
}

bool CLPingPongBufferImage2D::readSecond(void* destPtr, cl_bool blocking_read/* = CL_TRUE */) {
    if (second.read(destPtr, blocking_read)) {
        return true;
    }

    printError("READ SECOND PING PONG", " BUFFER FAILED");
    return false;
}

void CLPingPongBufferImage2D::printError(const char* beginMsg, const char* endMsg) {
    if (debugName) {
        std::cout << "ERROR: " << beginMsg << " '" << debugName << "'" << endMsg
                << "!!" << std::endl;
    } else {
        std::cout << "ERROR: " << beginMsg << endMsg << "!!" << std::endl;
    }
}

