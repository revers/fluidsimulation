/* 
 * File:   TargetCamera.h
 * Author: Revers
 *
 * Created on 3 grudzień 2011, 16:00
 */

#ifndef TARGETCAMERA_H
#define	TARGETCAMERA_H

#include "RevPoint.h"
#include "RevMatrix.h"
#include "RevQuaternion.h"

class TargetCamera {
    rev::graph::Point2f mousePt;

    rev::graph::Quaternionf totalRotation;
    rev::graph::Quaternionf lastTotalRotation;

    rev::graph::Matrix44f lookAtMatrix;

    rev::graph::Vector3f stVec;
    rev::graph::Vector3f enVec;
    float adjustWidth;
    float adjustHeight;

    float distanceFromTarget;

    rev::graph::Vector3f position;
    rev::graph::Vector3f target;
    rev::graph::Vector3f up;

    const rev::graph::Vector3f UP;
    const rev::graph::Vector3f FORWARD;
public:
    
    TargetCamera(rev::graph::Vector3f position_,
            rev::graph::Vector3f target_, float width, float height) :
    position(position_),
    target(target_),
    UP(0.0f, 1.0f, 0.0f),
    FORWARD(0.0f, 0.0f, 1.0f) {
        init(width, height);
    }
    
    TargetCamera(
            float posX, float posY, float posZ,
            float targetX, float targetY, float targetZ,
            float width, float height) :
    position(posX, posY, posZ),
    target(targetX, targetY, targetZ),
    UP(0.0f, 1.0f, 0.0f),
    FORWARD(0.0f, 0.0f, 1.0f) {
        init(width, height);
    }
    
     rev::graph::Quaternionf& getTotalRotation() {
        return totalRotation;
    }

    void setCameraDistance(float distance);

    float getCameraDistance() const {
        return distanceFromTarget;
    }

    void increaseDistance(float val) {
        float temp = distanceFromTarget + val;
        setCameraDistance(temp > 0 ? temp : 4.0f);
    }

    rev::graph::Vector3f getPosition() const {
        return position;
    }

    void setPosition(float x, float y, float z) {
        this->position.set(x, y, z);
        update();
    }

    void setPosition(const rev::graph::Vector3f& position) {
        this->position = position;
        update();
    }

    rev::graph::Vector3f getTarget() const {
        return target;
    }

    void setTarget(float x, float y, float z) {
        this->target.set(x, y, z);
        update();
    }

    void setTarget(const rev::graph::Vector3f& target) {
        this->target = target;
        update();
    }

    void click(int x, int y);

    void drag(int x, int y);

    void apply();

    virtual ~TargetCamera() {
    }

    void setBounds(float newWidth, float newHeight);

private:
    TargetCamera(const TargetCamera& orig);

    void mapToSphere(const rev::graph::Point2f* newPt, rev::graph::Vector3f* newVec) const;

    void click(const rev::graph::Point2f* newPt);

    void drag(const rev::graph::Point2f* newPt, rev::graph::Quaternionf* newRot);

    void init(float newWidth, float newHeight);

    void update();
};

#endif	/* TARGETCAMERA_H */

