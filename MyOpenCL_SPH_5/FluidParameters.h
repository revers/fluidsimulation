/* 
 * File:   FluidParameters.h
 * Author: Revers
 *
 * Created on 13 listopad 2011, 19:59
 */

#ifndef FLUIDPARAMETERS_H
#define	FLUIDPARAMETERS_H

#include "CL/cl.h"

class FluidParameters {
private:
    unsigned int nParticles;
    unsigned int xmin;
    unsigned int xmax;
    unsigned int ymin;
    unsigned int ymax;
    unsigned int zmin;
    unsigned int zmax;
    unsigned int blockSize;
    unsigned int blockSize3D;
    unsigned int gridDimensionX;
    unsigned int gridDimensionY;
    unsigned int gridDimensionZ;
    unsigned int gridPoints;
    unsigned int blockCount;
    unsigned int interleaveLookupLength;
    unsigned int blockCoordsLookupLength;

    float gravityX;
    float gravityY;
    float gravityZ;

    float simulationScale;
    float simulationScaleInv;
    float stiffness;
    float accelerationLimit;
    float damping;

    float supportRadiusScaled;
    float supportRadius;
    float restDensity;
    float viscosity;
    float particleMass;
    float timestep;

    float wPoly6Coefficient;
    float gradWPoly6Coefficient;
    float del2WPoly6Coefficient;
    float gradWspikyCoefficient;
    float del2WviscosityCoefficient;

    float particleVolume;
    float particleRestDistance;
    float particleRestDistanceScaled;
    float particleRadius;
    float particleRadiusScaled;
    float totalMass;
    float totalVolume;
    float totalLength;

    float totalLengthScaled;
    float totalVolumeScaled;
    float totalParticlesBoxHeight;
    
    float surfaceTensionCoeff;
public:
    FluidParameters(unsigned int particleCount);

    virtual ~FluidParameters() {
    }

    void printParameters();

    unsigned int getBlockCoordsLookupLength() const {
        return blockCoordsLookupLength;
    }

    unsigned int getBlockCount() const {
        return blockCount;
    }

    unsigned int getBlockSize() const {
        return blockSize;
    }

    unsigned int getBlockSize3D() const {
        return blockSize3D;
    }

    float getDel2WviscosityCoefficient() const {
        return del2WviscosityCoefficient;
    }

    float getGradWspikyCoefficient() const {
        return gradWspikyCoefficient;
    }

    unsigned int getGridDimensionX() const {
        return gridDimensionX;
    }

    unsigned int getGridDimensionY() const {
        return gridDimensionY;
    }

    unsigned int getGridDimensionZ() const {
        return gridDimensionZ;
    }

    unsigned int getGridPoints() const {
        return gridPoints;
    }

    unsigned int getInterleaveLookupLength() const {
        return interleaveLookupLength;
    }

    float getParticleRadius() const {
        return particleRadius;
    }

    float getParticleRadiusScaled() const {
        return particleRadiusScaled;
    }

    float getParticleRestDistance() const {
        return particleRestDistance;
    }

    float getParticleVolume() const {
        return particleVolume;
    }

    float getSimulationScaleInv() const {
        return simulationScaleInv;
    }

    float getSupportRadius() const {
        return supportRadius;
    }

    float getSupportRadiusScaled() const {
        return supportRadiusScaled;
    }

    float getTotalLength() const {
        return totalLength;
    }

    float getTotalMass() const {
        return totalMass;
    }

    float getTotalVolume() const {
        return totalVolume;
    }

    float getWPoly6Coefficient() const {
        return wPoly6Coefficient;
    }

    unsigned int getXmax() const {
        return xmax;
    }

    unsigned int getXmin() const {
        return xmin;
    }

    unsigned int getYmax() const {
        return ymax;
    }

    unsigned int getYmin() const {
        return ymin;
    }

    unsigned int getZmax() const {
        return zmax;
    }

    unsigned int getZmin() const {
        return zmin;
    }

    float getAccelerationLimit() const {
        return accelerationLimit;
    }

    void setAccelerationLimit(float accelerationLimit) {
        this->accelerationLimit = accelerationLimit;
    }

    float getDamping() const {
        return damping;
    }

    void setDamping(float damping) {
        this->damping = damping;
    }

    float getGravityX() const {
        return gravityX;
    }

    void setGravityX(float gravityX) {
        this->gravityX = gravityX;
    }

    float getGravityY() const {
        return gravityY;
    }

    void setGravityY(float gravityY) {
        this->gravityY = gravityY;
    }

    float getGravityZ() const {
        return gravityZ;
    }

    void setGravityZ(float gravityZ) {
        this->gravityZ = gravityZ;
    }

    float getParticleMass() const {
        return particleMass;
    }

    void setParticleMass(float particleMass);

    void setRestDensity(float restDensity);

    float getRestDensity() const {
        return restDensity;
    }

    float getStiffness() const {
        return stiffness;
    }

    void setStiffness(float stiffness) {
        this->stiffness = stiffness;
    }

    float getTimestep() const {
        return timestep;
    }

    void setTimestep(float timestep) {
        this->timestep = timestep;
    }

    float getViscosity() const {
        return viscosity;
    }

    void setViscosity(float viscosity) {
        this->viscosity = viscosity;
    }

    unsigned int getParticleCount() const {
        return nParticles;
    }

    void setParticleCount(unsigned int particleCount);

    void setSimulationScale(float simulationScale);

    float getSimulationScale() const {
        return simulationScale;
    }

    float getParticleRestDistanceScaled() const {
        return particleRestDistanceScaled;
    }

    float getGradlWPoly6Coefficient() const {
        return gradWPoly6Coefficient;
    }

    float getTotalLengthScaled() const {
        return totalLengthScaled;
    }

    float getTotalVolumeScaled() const {
        return totalVolumeScaled;
    }

    float getTotalParticlesBoxHeight() const {
        return totalParticlesBoxHeight;
    }
    
    float getDel2WPoly6Coefficient() const {
        return del2WPoly6Coefficient;
    }
    
    float getSurfaceTensionCoeff() const {
        return surfaceTensionCoeff;
    }

    void setSurfaceTensionCoeff(float surfaceTensionCoeff) {
        this->surfaceTensionCoeff = surfaceTensionCoeff;
    }
};

#endif	/* FLUIDPARAMETERS_H */

