/* 
 * File:   OpenGLUtil.cpp
 * Author: Revers
 * 
 * Created on 31 grudzień 2011, 14:45
 */

#include "OpenGLUtil.h"
#include <cstdio>

GLuint OpenGLUtil::compileProgram(const char *vsource, const char *fsource) {
    GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
    GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

    glShaderSource(vertexShader, 1, &vsource, 0);
    glShaderSource(fragmentShader, 1, &fsource, 0);

    glCompileShader(vertexShader);
    glCompileShader(fragmentShader);

    GLuint program = glCreateProgram();

    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);

    glLinkProgram(program);

    // check if program linked
    GLint success = 0;
    glGetProgramiv(program, GL_LINK_STATUS, &success);

    if (!success) {
        char temp[256];
        glGetProgramInfoLog(program, 256, 0, temp);
        printf("Failed to link program:\n%s\n", temp);
        glDeleteProgram(program);
        program = 0;
    }

    return program;
}

void OpenGLUtil::compileGridList(GLuint listId,
        unsigned int tesselation,
        float xMin, float xMax,
        float yMin, float yMax,
        float zMin, float zMax,
        float r, float g, float b) {

    float xDist = xMax - xMin;
    float yDist = yMax - yMin;
    float zDist = zMax - zMin;

    float xStep = xDist / (float) tesselation;
    float yStep = yDist / (float) tesselation;
    float zStep = zDist / (float) tesselation;

    unsigned int xMaxSteps = tesselation;
    unsigned int yMaxSteps = tesselation;
    unsigned int zMaxSteps = tesselation;

    glNewList(listId, GL_COMPILE);

    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    glColor3f(r, g, b);

    glBegin(GL_QUADS);
    {
        // Front wall:       
        glNormal3f(0.0f, 0.0f, -1.0f);
        for (unsigned int i = 0; i < yMaxSteps; i++) {
            for (unsigned int j = 0; j < xMaxSteps; j++) {

                glVertex3f(xMin + xStep * j, yMin + yStep * i + yStep, zMax);
                glVertex3f(xMin + xStep * j + xStep, yMin + yStep * i + yStep, zMax);
                glVertex3f(xMin + xStep * j + xStep, yMin + yStep * i, zMax);
                glVertex3f(xMin + xStep * j, yMin + yStep * i, zMax);
            }
        }

        // Back wall:
        glNormal3f(0.0f, 0.0f, 1.0f);
        for (unsigned int i = 0; i < yMaxSteps; i++) {
            for (unsigned int j = 0; j < xMaxSteps; j++) {
                glVertex3f(xMin + xStep * j + xStep, yMin + yStep * i, zMin);
                glVertex3f(xMin + xStep * j + xStep, yMin + yStep * i + yStep, zMin);
                glVertex3f(xMin + xStep * j, yMin + yStep * i + yStep, zMin);
                glVertex3f(xMin + xStep * j, yMin + yStep * i, zMin);
            }
        }

        // Top wall:
        glNormal3f(0.0f, -1.0f, 0.0f);
        for (unsigned int i = 0; i < zMaxSteps; i++) {
            for (unsigned int j = 0; j < xMaxSteps; j++) {
                glVertex3f(xMin + xStep * j + xStep, yMax, zMin + zStep * i);
                glVertex3f(xMin + xStep * j + xStep, yMax, zMin + zStep * i + zStep);
                glVertex3f(xMin + xStep * j, yMax, zMin + zStep * i + zStep);
                glVertex3f(xMin + xStep * j, yMax, zMin + zStep * i);
            }
        }

        // Bottom wall:
        glNormal3f(0.0f, 1.0f, 0.0f);
        for (unsigned int i = 0; i < zMaxSteps; i++) {
            for (unsigned int j = 0; j < xMaxSteps; j++) {
                glVertex3f(xMin + xStep * j, yMin, zMin + zStep * i + zStep);
                glVertex3f(xMin + xStep * j + xStep, yMin, zMin + zStep * i + zStep);
                glVertex3f(xMin + xStep * j + xStep, yMin, zMin + zStep * i);
                glVertex3f(xMin + xStep * j, yMin, zMin + zStep * i);
            }
        }

        // Right wall:
        glNormal3f(-1.0f, 0.0f, 0.0f);
        for (unsigned int i = 0; i < zMaxSteps; i++) {
            for (unsigned int j = 0; j < yMaxSteps; j++) {
                glVertex3f(xMax, yMin + yStep * j, zMin + zStep * i + zStep);
                glVertex3f(xMax, yMin + yStep * j + yStep, zMin + zStep * i + zStep);
                glVertex3f(xMax, yMin + yStep * j + yStep, zMin + zStep * i);
                glVertex3f(xMax, yMin + yStep * j, zMin + zStep * i);
            }
        }

        // Left wall: 
        glNormal3f(1.0f, 0.0f, 0.0f);
        for (unsigned int i = 0; i < zMaxSteps; i++) {
            for (unsigned int j = 0; j < yMaxSteps; j++) {
                glVertex3f(xMin, yMin + yStep * j + yStep, zMin + zStep * i);
                glVertex3f(xMin, yMin + yStep * j + yStep, zMin + zStep * i + zStep);
                glVertex3f(xMin, yMin + yStep * j, zMin + zStep * i + zStep);
                glVertex3f(xMin, yMin + yStep * j, zMin + zStep * i);
            }
        }
    }
    glEnd();

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    glEndList();
}

