/* 
 * File:   OpenGLUtil.h
 * Author: Revers
 *
 * Created on 31 grudzień 2011, 14:45
 */

#ifndef OPENGLUTIL_H
#define	OPENGLUTIL_H

#include <GL/glew.h>
#include <GL/gl.h>

class OpenGLUtil {
public:

    static GLuint compileProgram(const char *vsource, const char *fsource);

    static void compileGridList(GLuint listId,
            unsigned int tesselation,
            float xMin, float xMax,
            float yMin, float yMax,
            float zMin, float zMax,
            float r, float g, float b);


private:

    OpenGLUtil() {
    }

    virtual ~OpenGLUtil() {
    }

    OpenGLUtil(const OpenGLUtil& orig);

};

#endif	/* OPENGLUTIL_H */

