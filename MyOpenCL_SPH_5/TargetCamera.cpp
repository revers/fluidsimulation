/* 
 * File:   TargetCamera.cpp
 * Author: Revers
 * 
 * Created on 3 grudzień 2011, 16:00
 */

#include <cassert>
#include <cmath>
#include <iostream>

#include <GL/glew.h>
#include <GL/glut.h>
#include <GL/gl.h>

#include "TargetCamera.h"

using namespace std;
using namespace rev::graph;

//#define DRAW_TARGET_AXES

#define FUNC_SQRT sqrtf

//utility macros
//assuming IEEE-754(GLfloat), which i believe has max precision of 7 bits
#define EPSILON 1.0e-5

void TargetCamera::mapToSphere(const Point2f* newPt, Vector3f* newVec) const {
    Point2f tempPt;
    GLfloat length;

    //Copy paramter into temp point
    tempPt = *newPt;

    //Adjust point coords and scale down to range of [-1 ... 1]
    tempPt.x = (tempPt.x * this->adjustWidth) - 1.0f;
    tempPt.y = 1.0f - (tempPt.y * this->adjustHeight);

    //Compute the square of the length of the vector to the point from the center
    length = (tempPt.x * tempPt.x) + (tempPt.y * tempPt.y);

    //If the point is mapped outside of the sphere... (length > radius squared)
    if (length > 1.0f) {
        GLfloat norm;

        //Compute a normalizing factor (radius / sqrt(length))
        norm = 1.0f / FUNC_SQRT(length);

        //Return the "normalized" vector, a point on the sphere
        newVec->x = tempPt.x * norm;
        newVec->y = tempPt.y * norm;
        newVec->z = 0.0f;
    } else //Else it's on the inside
    {
        //Return a vector to a point mapped inside the sphere sqrt(radius squared - length)
        newVec->x = tempPt.x;
        newVec->y = tempPt.y;
        newVec->z = FUNC_SQRT(1.0f - length);
    }
}

void TargetCamera::init(float newWidth, float newHeight) {
    up = UP;

    totalRotation = Quaternionf::createIdentityQuaternion();
    lastTotalRotation = Quaternionf::createIdentityQuaternion();

    this->setBounds(newWidth, newHeight);

    update();
}

void TargetCamera::setCameraDistance(float distance) {
    distanceFromTarget = distance;

    rev::graph::Vector3f direction = position - target;
    direction.normalize();

    direction = direction * distanceFromTarget;

    position = direction + target;
    lookAtMatrix = rev::graph::Matrix44f::createLookAtMatrix(position, target, up);
}

void TargetCamera::update() {
    Vector3f direction = position - target;

    distanceFromTarget = direction.length();

    Vector3f forward = FORWARD;

    totalRotation = Quaternionf::getAngleBetween(direction, forward);

    Matrix33f rot = totalRotation.toMatrix33();
    position = rot * FORWARD * distanceFromTarget + target;
    up = rot * UP;

    lookAtMatrix = rev::graph::Matrix44f::createLookAtMatrix(position, target, up);
}

void TargetCamera::click(int x, int y) {
    mousePt.set((float) x, (float) y);
    lastTotalRotation = totalRotation;

    this->mapToSphere(&mousePt, &this->stVec);
}

void TargetCamera::drag(int x, int y) {
    mousePt.set((float) x, (float) y);

    Quaternionf quat;
    drag(&mousePt, &quat);

    totalRotation = quat * lastTotalRotation;

    Matrix33f rot = totalRotation.toMatrix33();
    position = rot * FORWARD * distanceFromTarget + target;
    up = rot * UP;

    lookAtMatrix = rev::graph::Matrix44f::createLookAtMatrix(position, target, up);
}



//Mouse drag, calculate rotation

void TargetCamera::drag(const Point2f* newPt, Quaternionf* newRot) {
    //Map the point to the sphere
    this->mapToSphere(newPt, &this->enVec);

    *newRot = Quaternionf::getAngleBetween(enVec, stVec);
    //newRot->normalize();
    //        //Return the quaternion equivalent to the rotation
    //    
    //        //Compute the vector perpendicular to the begin and end vectors
    //        Vector3f perp = this->stVec.crossProduct(this->enVec);
    //    
    //        //Compute the length of the perpendicular vector
    //        //if its non-zero
    //        if (perp.length() > EPSILON) {
    //            //We're ok, so return the perpendicular vector as the transform after all
    //            newRot->x = perp.x;
    //            newRot->y = perp.y;
    //            newRot->z = perp.z;
    //            //In the quaternion values, w is cosine (theta / 2), where theta is rotation angle            
    //            newRot->w = -this->stVec.dotProduct(this->enVec);
    //        } else //if its zero
    //        {
    //            //The begin and end vectors coincide, so return an identity transform
    //            newRot->x =
    //                    newRot->y =
    //                    newRot->z =
    //                    newRot->w = 0.0f;
    //        }

}

void TargetCamera::setBounds(float newWidth, float newHeight) {
    assert((newWidth > 1.0f) && (newHeight > 1.0f));

    //Set adjustment factor for width/height
    this->adjustWidth = 1.0f / ((newWidth - 1.0f) * 0.5f);
    this->adjustHeight = 1.0f / ((newHeight - 1.0f) * 0.5f);
}

void TargetCamera::apply() {
    glMultMatrixf(lookAtMatrix.dataPointer());

#ifdef DRAW_TARGET_AXES
    float len = 10.0f;
    Vector3f xPoint(len, 0, 0);
    Vector3f yPoint(0, len, 0);
    Vector3f zPoint(0, 0, len);

    xPoint += target;
    yPoint += target;
    zPoint += target;

    glLineWidth(4);

    glBegin(GL_LINES);
    glColor3f(1, 0, 0);
    glVertex3fv(target.dataPointer());
    glVertex3fv(xPoint.dataPointer());

    glColor3f(0, 1, 0);
    glVertex3fv(target.dataPointer());
    glVertex3fv(yPoint.dataPointer());

    glColor3f(0, 0, 1);
    glVertex3fv(target.dataPointer());
    glVertex3fv(zPoint.dataPointer());

    glEnd();

    glLineWidth(1);
#endif
}

