/* 
 * File:   FluidSimulationTest.h
 * Author: Revers
 *
 * Created on 13 listopad 2011, 19:35
 */

#ifndef FLUIDSIMULATIONTEST_H
#define	FLUIDSIMULATIONTEST_H

#include "CLBuffer.h"

class FluidSimulation;

class FluidSimulationTest {
    FluidSimulation* fluidSimulation;
    CLBuffer tempBuffer;
    cl::Kernel kernelTestNeighbourBlockSearch;
    cl::Kernel kernelTestImage2D;
    cl::Kernel neighborLookupTest;
public:

    FluidSimulationTest(FluidSimulation* fluidSimulation_);

    virtual ~FluidSimulationTest() {
    }

    bool testStep();

    void checkRadixSort();
private:
    FluidSimulationTest(const FluidSimulationTest& orig);

    void runReadPositionAndCompareOld();

    void testPrintInterleveLookup();

    void testPrintBlockCoordsLookup();

    void testPrintDensity();

    bool checkZIndex(cl_uint val);

    void testPrintTempBuffer();

    void testPrintHashParticles();

    void testPrintSortedParticles();

    bool testAfterBlockAndInterleaveLookupLoaded();

    void printBinaryInt(unsigned int i);

    bool testAfterRunHashParticles();

    bool testAfterRunSort();

    bool testAfterRunCreateCompactBlockListDetailed();

    bool testAfterRunCreateCompactBlockList();

    bool testAfterRunCreateBlockList();
    
    bool testNeighbourBlockSearch();
    
    bool testImage2DBuffer();
    
    bool testNeighborLookup();
};

#endif	/* FLUIDSIMULATIONTEST_H */

