/* 
 * File:   Renderer.h
 * Author: Revers
 *
 * Created on 18 grudzień 2011, 00:30
 */

#ifndef RENDERER_H
#define	RENDERER_H

#include "FluidSimulation.h"
#include "ControlPanel.h"
#include "TargetCamera.h"

class Renderer {
protected:
    ControlPanel* controlPanel;
    TargetCamera* camera;
public:

    Renderer(ControlPanel* controlPanel_, TargetCamera* camera_) :
    controlPanel(controlPanel_),
    camera(camera_) {
    }

    virtual ~Renderer() {
    }
    
    virtual bool init() = 0;

    virtual void render() = 0;

    virtual void windowSizeChanged(int newWidth, int newHeight) {
    }

    ControlPanel* getControlPanel() const {
        return controlPanel;
    }

    void setControlPanel(ControlPanel* controlPanel) {
        this->controlPanel = controlPanel;
    }

    TargetCamera* getTargetCamera() const {
        return camera;
    }

    void setTargetCamera(TargetCamera* camera) {
        this->camera = camera;
    }

};

#endif	/* RENDERER_H */

