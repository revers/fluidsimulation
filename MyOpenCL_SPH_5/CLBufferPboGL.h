/* 
 * File:   CLBufferGL.h
 * Author: Revers
 *
 * Created on 12 listopad 2011, 19:10
 */

#ifndef CLBUFFERPBOGL_H
#define	CLBUFFERPBOGL_H


#include <cassert>
#include <iostream>
#include <vector>

#include <CL/cl.hpp>
#include <GL/gl.h>

class CLBufferPboGL {
private:
    cl::Context clContext;
    cl::CommandQueue clCommandQueue;
    size_t size;
    const char* debugName;
    GLuint pixelBufferObject;
    std::vector<cl::Memory> bufferVector;
public:
    cl::BufferGL buffer;

    CLBufferPboGL() :
    size(0),
    debugName(NULL),
    pixelBufferObject(0),
    bufferVector(1) {
    }

    CLBufferPboGL(const CLBufferPboGL& old);

    virtual ~CLBufferPboGL();

    void acquireGLObject();

    void releaseGLObject();

    bool init(cl::Context clContext,
            cl::CommandQueue clCommandQueue,
            size_t size,
            const char* debugName = NULL,
            cl_mem_flags flags = CL_MEM_READ_WRITE,
            void* host_ptr = NULL);

    bool write(const void *sourcePtr, cl_bool blocking_write = CL_TRUE);

    bool write(const void *sourcePtr,
            size_t offset,
            size_t bytes,
            cl_bool blocking_write = CL_TRUE);

    bool read(void* destPtr, cl_bool blocking_read = CL_TRUE);

    bool read(void *destPtr,
            size_t offset,
            size_t bytes,
            cl_bool blocking_read = CL_TRUE);

    size_t getSize() const {
        return size;
    }

    GLuint getPixelBufferObject() const {
        return pixelBufferObject;
    }

    const char* getDebugName() const {
        return debugName;
    }

private:

    void printError(cl_int err, const char* beginMsg, const char* endMsg);
};

class CLPingPongBufferPboGL {
private:

    size_t size;
    size_t totalSize;
    const char* debugName;
    CLBufferPboGL first;
    CLBufferPboGL second;
public:

    CLPingPongBufferPboGL() :
    size(0),
    totalSize(0),
    debugName(NULL) {
    }

    CLPingPongBufferPboGL(const CLPingPongBufferPboGL& old);

    bool init(cl::Context clContext,
            cl::CommandQueue clCommandQueue,
            size_t size,
            const char* debugName = NULL,
            cl_mem_flags flags = CL_MEM_READ_WRITE,
            void* host_ptr = NULL);

    bool writeFirst(const void *sourcePtr, cl_bool blocking_write = CL_TRUE);

    bool writeFirst(const void *sourcePtr,
            size_t offset,
            size_t bytes,
            cl_bool blocking_write = CL_TRUE);

    bool readFirst(void* destPtr, cl_bool blocking_read = CL_TRUE);

    bool readFirst(void *destPtr,
            size_t offset,
            size_t bytes,
            cl_bool blocking_read = CL_TRUE);

    bool writeSecond(const void *sourcePtr, cl_bool blocking_write = CL_TRUE);

    bool writeSecond(const void *sourcePtr,
            size_t offset,
            size_t bytes,
            cl_bool blocking_write = CL_TRUE);

    bool readSecond(void* destPtr, cl_bool blocking_read = CL_TRUE);

    bool readSecond(void *destPtr,
            size_t offset,
            size_t bytes,
            cl_bool blocking_read = CL_TRUE);

    size_t getSize() const {
        return size;
    }

    size_t getTotalSize() const {
        return totalSize;
    }

    void swap() {
        cl::BufferGL temp = first.buffer;
        first.buffer = second.buffer;
        second.buffer = temp;
    }

    CLBufferPboGL& getFirst() {
        return first;
    }

    CLBufferPboGL& getSecond() {
        return second;
    }

    const char* getDebugName() const {
        return debugName;
    }

    void acquireSecondGLObject() {
        second.acquireGLObject();
    }

    void releaseSecondGLObject() {
        second.releaseGLObject();
    }

    void acquireFirstGLObject() {
        first.acquireGLObject();
    }

    void releaseFirstGLObject() {
        first.releaseGLObject();
    }

private:

    void printError(const char* beginMsg, const char* endMsg);

};

#endif	/* CLBUFFERPBOGL_H */

