/* 
 * File:   FluidSimulation.h
 * Author: Revers
 *
 * Created on 13 listopad 2011, 18:43
 */

#ifndef FLUIDSIMULATION_H
#define	FLUIDSIMULATION_H

#include "Parameters.h"

#ifdef NV_RADIX_SORT
#include "NV_RadixSort.h"
#elif defined(ATI_RADIX_SORT)
#include "ATI_RadixSort.hpp"
#endif

#include "OpenCLBase.h"

#include "FluidParameters.h"

#include "CLBuffer.h"
#include "CLBufferPboGL.h"
#include "CLBufferVboGL.h"
#include "CLBufferImage2D.h"


#define USE_ZERO_INITIAL_VELOCITY
#define DRAW_GRID
#define USE_FAST_RELAXED_MATH_FLAG

#define PARTICLE_PLACEMENT CUBE

typedef enum {
    CUBE = 1, WALL = 2, CENTER = 3, RANDOM, FLOOR
} InitialParticlePlacement;

typedef enum {
    VELOCITY, PRESSURE, FORCE
} ColoringSource;

typedef enum {
    White,
    Blackish,
    BlackToCyan,
    BlueToWhite,
    HSVBlueToRed,
} ColoringGradient;

// TODO: under VC++:
// #pragma pack(16) 
// instead of __attribute__ ((aligned (STRUCT_ALIGNMENT)))

typedef struct {
    float wPoly6Coefficient;
    float particleMass;
    float simulationScale;
    float supportRadiusScaled;
    float supportRadiusSqr;
    float restDensity;
    float restDensityInvSqr;
    float stiffness;
} DensityComputationData __attribute__((aligned(STRUCT_ALIGNMENT)));

typedef struct {
    float gradWspikyCoefficient;
    float del2WviscosityCoefficient;
    float particleMass;
    float simulationScale;
    float simulationScaleInv;
    float supportRadiusScaled;
    float supportRadius;
    float viscosity;
    float accelerationLimit;
    float gravityX;
    float gravityY;
    float gravityZ;
    float timeStep;
    float damping;
    ColoringSource coloringSource;
    ColoringGradient coloringGradient;
} ForceComputationData __attribute__((aligned(STRUCT_ALIGNMENT)));

typedef struct {
    float gradWspikyCoefficient;
    float del2WviscosityCoefficient;
    float particleMass;
    float simulationScale;
    float simulationScaleInv;
    float supportRadiusScaled;
    float supportRadius;
    float viscosity;
    float accelerationLimit;
    float gravityX;
    float gravityY;
    float gravityZ;
    float timeStep;
    float damping;
    ColoringSource coloringSource;
    ColoringGradient coloringGradient;
    float distThreshold;
    unsigned int minParticles;
    float boundaryOffset;
} ForceComputationDataExtract __attribute__((aligned(STRUCT_ALIGNMENT)));

typedef struct {
    float gradWPoly6Coefficient;
    float gradWspikyCoefficient;
    float del2WviscosityCoefficient;
    float particleMass;
    float simulationScale;
    float simulationScaleInv;
    float supportRadiusScaled;
    float supportRadiusSqr;
    float supportRadius;
    float viscosity;
    float accelerationLimit;
    float gravityX;
    float gravityY;
    float gravityZ;
    float timeStep;
    float damping;
    ColoringSource coloringSource;
    ColoringGradient coloringGradient;
    float colorFieldThreshold;
    float del2WPoly6Cofficient;
    float surfaceTensionCoeff;
} ForceComputationDataColorField __attribute__((aligned(STRUCT_ALIGNMENT)));

typedef struct {
    float gradWPoly6Coefficient;
    float gradWspikyCoefficient;
    float del2WviscosityCoefficient;
    float particleMass;
    float simulationScale;
    float simulationScaleInv;
    float supportRadiusScaled;
    float supportRadiusSqr;
    float supportRadius;
    float viscosity;
    float accelerationLimit;
    float gravityX;
    float gravityY;
    float gravityZ;
    float timeStep;
    float damping;
    float restDensity;
    float restDensityInv;
    float colorFieldThreshold;
    float rMin;
    float distFieldFactor;
    float del2WPoly6Cofficient;
    float surfaceTensionCoeff;
} ForceComputationDataDistField __attribute__((aligned(STRUCT_ALIGNMENT)));

class FluidSimulation : public OpenCLBase {
    friend class FluidSimulationTest;

    DensityComputationData densityComputationData;
    ForceComputationData forceComputationData;
    ForceComputationDataExtract forceComputationDataExtract;
    ForceComputationDataColorField forceComputationDataColorField;
    ForceComputationDataDistField forceComputationDataDistField;
    InitialParticlePlacement initialParticlePlacement;
#ifdef USE_LEAP_FROG
    bool initLeapFrog;
#endif

public:
    cl::Program clProgram;
    cl::Kernel kernelHashParticles;
    cl::Kernel kernelReorderPositionAndVelocity;
    cl::Kernel kernelClearBuffer;
    cl::Kernel kernelCreateBlockList;
    cl::Kernel kernelCreateCompactBlockList;
    cl::Kernel kernelComputeDensityAndPressure;
    cl::Kernel kernelComputeForcesAndIntegrate;
    cl::Kernel kernelComputeForcesAndIntegrateExtract;
    cl::Kernel kernelComputeForcesAndIntegrateColorField;
    cl::Kernel kernelComputeForcesAndIntegrateDistField;
    cl::Kernel kernelClearPboTex3d;
    
#ifdef NV_RADIX_SORT
    NVRadixSort* radixSort;
#elif defined(ATI_RADIX_SORT)
    ATIRadixSortCL* radixSort;
#endif

    CLPingPongBuffer radixSortPPBuffer; //sizeof (cl_uint) * SORT_CELL_SIZE * Parameters::nParticles,
    // position (x, y, z) + Z_Index
    CLPingPongBufferImage2D positionPPBuffer; // sizeof (cl_float4) * Parameters::nParticles
    // velocity (vx, vy, vz) + pressure (w):
    CLPingPongBufferImage2D velocityPPBuffer; // sizeof (cl_float4) * Parameters::nParticles
    // density (x) with inverse density (y):
    CLBuffer densityBuffer; // sizeof (cl_float2) * Parameters::nParticles

    // buffers for visualization:
    CLBufferVboGL positionBufferGL;
    CLBufferVboGL colorBuffer; // sizeof (cl_float4) * Parameters::nParticles
    CLBufferVboGL surfacePositionBufferGL; // sizeof (cl_float4) * Parameters::nParticles

    // buffer for GPU feedback information:
    CLBuffer gpu2cpuDataBuffer; // 32 bits

    // Constant lookup buffers:
    CLBuffer neighborLookupBuffer; // sizeof (cl_int4) * 32
    CLBuffer interleaveLookupBuffer; // sizeof (cl_uint) * INTERLEAVE_CELL_SIZE * Parameters::interleaveLookupLength,
    // Too large for constant memory so I use it as read-only image2d:
    CLBufferImage2D blockCoordsLookupBuffer; //   sizeof (cl_uint) * BLOCK_LOOKUP_CELL_SIZE * Parameters::blockCoordsLookupLength,

    //==========================================    
    CLBufferPboGL pboTex3dBuffer;
    //==========================================

    cl_uint gpu2cpuArray[8]; // 32bit length

    float* positionArray;
    float* velocityArray;

    cl_uint usedWorkItems;
    cl_uint surfaceParticles;
    FluidParameters* fluidParameters;

    cl_uint clearPboTex3dGlobalSize;
    
    cl_float4 lightDirection;
public:
    FluidSimulation(unsigned int particleCount, InitialParticlePlacement initialParticlePlacement);

    virtual ~FluidSimulation();

    bool init();

    void step();

    CLBufferPboGL& getPboTex3dBuffer() {
        return pboTex3dBuffer;
    }
    
    cl_float4& getLightDirection() {
        return lightDirection;
    }
    
    void setLightDirection(float x, float y, float z) {
        lightDirection.x = x;
        lightDirection.y = y;
        lightDirection.z = z;
    }

    cl_uint getParticleCount() {
        return fluidParameters->getParticleCount();
    }

    void printParameters() const {
        fluidParameters->printParameters();
    }

    float getParticleRadius() const {
        return fluidParameters->getParticleRadius();
    }

    float getParticleRadiusScaled() const {
        return fluidParameters->getParticleRadiusScaled();
    }

    FluidParameters& getFluidParameters() {
        return (*fluidParameters);
    }

    InitialParticlePlacement getInitialParticlePlacement() const {
        return initialParticlePlacement;
    }

    ColoringGradient getColoringGradient() const {
        return forceComputationData.coloringGradient;
    }

    void setColoringGradient(ColoringGradient coloringGradient) {
        forceComputationData.coloringGradient = coloringGradient;
        forceComputationDataExtract.coloringGradient = coloringGradient;
        forceComputationDataColorField.coloringGradient = coloringGradient;
    }

    ColoringSource getColoringSource() const {
        return forceComputationData.coloringSource;
    }

    void setColoringSource(ColoringSource coloringSource) {
        forceComputationData.coloringSource = coloringSource;
        forceComputationDataExtract.coloringSource = coloringSource;
        forceComputationDataColorField.coloringSource = coloringSource;
    }

    float getParticleMass() const {
        return fluidParameters->getParticleMass();
    }

    void setParticleMass(float particleMass) {
        fluidParameters->setParticleMass(particleMass);

        densityComputationData.particleMass = fluidParameters->getParticleMass();

        forceComputationData.particleMass = fluidParameters->getParticleMass();
        forceComputationDataExtract.particleMass = fluidParameters->getParticleMass();
        forceComputationDataColorField.particleMass = fluidParameters->getParticleMass();
        forceComputationDataDistField.particleMass = fluidParameters->getParticleMass();
    }

    float getAccelerationLimit() const {
        return fluidParameters->getAccelerationLimit();
    }

    void setAccelerationLimit(float accLimit) {
        fluidParameters->setAccelerationLimit(accLimit);

        forceComputationData.accelerationLimit = fluidParameters->getAccelerationLimit();
        forceComputationDataExtract.accelerationLimit = fluidParameters->getAccelerationLimit();
        forceComputationDataColorField.accelerationLimit = fluidParameters->getAccelerationLimit();
        forceComputationDataDistField.accelerationLimit = fluidParameters->getAccelerationLimit();
    }

    float getTimestep() const {
        return fluidParameters->getTimestep();
    }

    void setTimestep(float timestep) {
        fluidParameters->setTimestep(timestep);

        forceComputationData.timeStep = fluidParameters->getTimestep();
        forceComputationDataExtract.timeStep = fluidParameters->getTimestep();
        forceComputationDataColorField.timeStep = fluidParameters->getTimestep();
        forceComputationDataDistField.timeStep = fluidParameters->getTimestep();
    }

    float getDamping() const {
        return fluidParameters->getDamping();
    }

    void setDamping(float damping) {
        fluidParameters->setDamping(damping);

        forceComputationData.damping = fluidParameters->getDamping();
        forceComputationDataExtract.damping = fluidParameters->getDamping();
        forceComputationDataColorField.damping = fluidParameters->getDamping();
        forceComputationDataDistField.damping = fluidParameters->getDamping();
    }

    float getGravityX() const {
        return fluidParameters->getGravityX();
    }

    void setGravityX(float gravityX) {
        fluidParameters->setGravityX(gravityX);

        forceComputationData.gravityX = fluidParameters->getGravityX();
        forceComputationDataExtract.gravityX = fluidParameters->getGravityX();
        forceComputationDataColorField.gravityX = fluidParameters->getGravityX();
        forceComputationDataDistField.gravityX = fluidParameters->getGravityX();
    }

    float getGravityY() const {
        return fluidParameters->getGravityY();
    }

    void setGravityY(float gravityY) {
        fluidParameters->setGravityY(gravityY);

        forceComputationData.gravityY = fluidParameters->getGravityY();
        forceComputationDataExtract.gravityY = fluidParameters->getGravityY();
        forceComputationDataColorField.gravityY = fluidParameters->getGravityY();
        forceComputationDataDistField.gravityY = fluidParameters->getGravityY();
    }

    float getGravityZ() const {
        return fluidParameters->getGravityZ();
    }

    void setGravityZ(float gravityZ) {
        fluidParameters->setGravityZ(gravityZ);

        forceComputationData.gravityZ = fluidParameters->getGravityZ();
        forceComputationDataExtract.gravityZ = fluidParameters->getGravityZ();
        forceComputationDataColorField.gravityZ = fluidParameters->getGravityZ();
        forceComputationDataDistField.gravityZ = fluidParameters->getGravityZ();
    }

    float getRestDensity() const {
        return fluidParameters->getRestDensity();
    }

    void setRestDensity(float restDensity);

    float getSimulationScale() const {
        return fluidParameters->getSimulationScale();
    }

    void setSimulationScale(float simulationScale);

    float getStiffness() const {
        return fluidParameters->getStiffness();
    }

    void setStiffness(float stiffness) {
        fluidParameters->setStiffness(stiffness);

        densityComputationData.stiffness = fluidParameters->getStiffness();
    }

    float getViscosity() const {
        return fluidParameters->getViscosity();
    }

    void setViscosity(float viscosity) {
        fluidParameters->setViscosity(viscosity);

        forceComputationData.viscosity = fluidParameters->getViscosity();
        forceComputationDataExtract.viscosity = fluidParameters->getViscosity();
        forceComputationDataColorField.viscosity = fluidParameters->getViscosity();
        forceComputationDataDistField.viscosity = fluidParameters->getViscosity();
    }
#ifdef USE_SURFACE_TENSION
    float getSurfaceTensionCoeff() const {
        return fluidParameters->getSurfaceTensionCoeff();
    }

    void setSurfaceTensionCoeff(float surfaceTensionCoeff) {
        fluidParameters->setSurfaceTensionCoeff(surfaceTensionCoeff);

        forceComputationDataColorField.surfaceTensionCoeff = fluidParameters->getSurfaceTensionCoeff();
        forceComputationDataDistField.surfaceTensionCoeff = fluidParameters->getSurfaceTensionCoeff();
    }
#endif

#ifdef USE_RAYCASTING

    float getColorFieldThreshold() const {
        return forceComputationDataDistField.colorFieldThreshold;
    }

    void setColorFieldThreshold(float threshold) {
        forceComputationDataDistField.colorFieldThreshold = threshold;
    }

    float getDistFieldFactor() const {
        return forceComputationDataDistField.distFieldFactor;
    }

    void setDistFieldFactor(float distFieldFactor) {
        forceComputationDataDistField.distFieldFactor = distFieldFactor;
    }

    float getRMin() const {
        return forceComputationDataDistField.rMin;
    }

    void setRMin(float rMin) {
        forceComputationDataDistField.rMin = rMin;
    }
#elif defined(EXTRACT_SURFACE_COLOR_FIELD)

    float getColorFieldThreshold() const {
        return forceComputationDataColorField.colorFieldThreshold;
    }

    void setColorFieldThreshold(float threshold) {
        forceComputationDataColorField.colorFieldThreshold = threshold;
    }
#elif defined(EXTRACT_SURFACE_CENTER_OF_MASS)

    float getDistanceThreshold() const {
        return forceComputationDataExtract.distThreshold;
    }

    void setDistanceThreshold(float threshold) {
        forceComputationDataExtract.distThreshold = threshold;
    }

    unsigned int getMinParticles() const {
        return forceComputationDataExtract.minParticles;
    }

    void setMinParticles(unsigned int minParticles) {
        forceComputationDataExtract.minParticles = minParticles;
    }

#endif

private:
    FluidSimulation(const FluidSimulation& orig);

    void updateAllComputationData();

    bool setFixedKernelArguments();

    bool createBuffers();

    bool createProgramAndKernels();

    void createWallPoints();

    void createFloorPositions();

    void createRandomPositions();

    void createCubePositions();

    void createCenterPositions();

    bool createInitialPositionAndVelocity(InitialParticlePlacement placement);

    bool createBlockCoordsLookupTable();

    bool createInterleaveLookupTable();

    bool createNeighborLookupTable();

    cl_uint spreadBits(cl_uint x);

    cl_uint compressBits(cl_uint x);

    void restoreInterleave(cl_uint value, cl_uint* x, cl_uint* y, cl_uint* z);

    void fillColorBuffer(float r, float g, float b, float alpha);

    void runHashParticles();

    void runSort();

    void runReorderPositionAndVelocity();

    void runClearGpu2CpuBuffer();

    void runClearSecondBuffer();

    void runCreateBlockList();

    void runCreateCompactBlockList();

    void runReadUsedWorkGroups();

    void runComputeDensityAndPressure();

    void runComputeForcesAndIntegrate();

    void runReadSurfaceParticlesCount();

    void runComputeForcesAndIntegrateExtract();

    void runComputeForcesAndIntegrateColorField();

    void runComputeForcesAndIntegrateDistField();

    void runReadPosition();

    void runCopyPositionBuffer();

    void runClearPboTex3d();

    void runClearPboTex3dWithFrame();
    
    void runDrawFramePboTex3d();
};

#endif	/* FLUIDSIMULATION_H */

