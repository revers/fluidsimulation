
#include <cassert>
#include "CLBuffer.h"

#include "OpenCLBase.h"

CLBuffer::CLBuffer(const CLBuffer& old) {
    this->clContext = old.clContext;
    this->clCommandQueue = old.clCommandQueue;
    this->size = old.size;
    this->debugName = old.debugName;
    this->buffer = old.buffer;
}

bool CLBuffer::init(cl::Context clContext,
        cl::CommandQueue clCommandQueue,
        size_t size,
        const char* debugName/* = NULL */,
        cl_mem_flags flags/* = CL_MEM_READ_WRITE */,
        void* host_ptr/* = NULL */) {

    this->clContext = clContext;
    this->clCommandQueue = clCommandQueue;
    this->debugName = debugName;

    cl_int err;

    buffer = cl::Buffer(clContext, flags,
            size, host_ptr, &err);

    if (err != CL_SUCCESS) {
        printError(err, "INITIALIZATION", " BUFFER FAILED");
        return false;
    }

    this->size = size;

    return true;
}

bool CLBuffer::write(const void *sourcePtr, cl_bool blocking_write/* = CL_TRUE */) {
    assert(buffer() != 0);
    cl_int err;

    err = clCommandQueue.enqueueWriteBuffer(
            buffer, blocking_write, 0, size,
            sourcePtr, NULL, NULL);

    if (err != CL_SUCCESS) {
        printError(err, "WRITE", " BUFFER FAILED");
        return false;
    }

    return true;
}

bool CLBuffer::write(const void *sourcePtr,
        size_t offset,
        size_t bytes,
        cl_bool blocking_write/* = CL_TRUE */) {
    assert(buffer() != 0);
    cl_int err;

    err = clCommandQueue.enqueueWriteBuffer(
            buffer, blocking_write, offset, bytes,
            sourcePtr, NULL, NULL);

    if (err != CL_SUCCESS) {
        printError(err, "PARTIAL WRITE", " BUFFER FAILED");
        return false;
    }

    return true;
}

bool CLBuffer::read(void* destPtr, cl_bool blocking_read/* = CL_TRUE */) {
    assert(buffer() != 0);
    cl_int err;

    err = clCommandQueue.enqueueReadBuffer(buffer, blocking_read, 0,
            size, destPtr, NULL, NULL);

    if (err != CL_SUCCESS) {
        printError(err, "READ", " BUFFER FAILED");
        return false;
    }

    return true;
}

bool CLBuffer::read(void *destPtr,
        size_t offset,
        size_t bytes,
        cl_bool blocking_read/* = CL_TRUE */) {

    assert(buffer() != 0);
    cl_int err;

    err = clCommandQueue.enqueueReadBuffer(buffer, blocking_read, offset,
            bytes, destPtr, NULL, NULL);

    if (err != CL_SUCCESS) {
        printError(err, "PARTIAL READ", " BUFFER FAILED");
        return false;
    }

    return true;
}

void CLBuffer::printError(cl_int err, const char* beginMsg, const char* endMsg) {
    if (debugName) {
        std::cout << "ERROR: " << beginMsg << " '" << debugName << "'" << endMsg
                << OpenCLBase::getOpenCLErrorCodeStr(err) << std::endl;
    } else {
        std::cout << "ERROR: " << beginMsg << endMsg
                << OpenCLBase::getOpenCLErrorCodeStr(err) << std::endl;
    }
}

CLPingPongBuffer::CLPingPongBuffer(const CLPingPongBuffer& old) {
    this->size = old.size;
    this->totalSize = old.totalSize;
    this->debugName = old.debugName;
    this->first = old.first;
    this->second = old.second;
}

bool CLPingPongBuffer::init(cl::Context clContext,
        cl::CommandQueue clCommandQueue,
        size_t size,
        const char* debugName/* = NULL */,
        cl_mem_flags flags/* = CL_MEM_READ_WRITE */,
        void* host_ptr/* = NULL */) {

    this->debugName = debugName;

    bool succ = first.init(clContext, clCommandQueue, size, "PingPong_1", flags, host_ptr);

    if (succ == false) {
        printError("INITIALIZATION PING PONG #1", " BUFFER FAILED");
        return false;
    }

    succ = second.init(clContext, clCommandQueue, size, "PingPong_2", flags, host_ptr);

    if (succ == false) {
        printError("INITIALIZATION PING PONG #2", " BUFFER FAILED");
        return false;
    }

    this->size = size;
    this->totalSize = size * 2;

    return true;
}

bool CLPingPongBuffer::writeFirst(const void *sourcePtr, cl_bool blocking_write/* = CL_TRUE */) {
    if (first.write(sourcePtr, blocking_write)) {
        return true;
    }

    printError("WRITE FIRST PING PONG", " BUFFER FAILED");
    return false;
}

bool CLPingPongBuffer::writeFirst(const void *sourcePtr,
        size_t offset,
        size_t bytes,
        cl_bool blocking_write/* = CL_TRUE */) {

    if (first.write(sourcePtr, offset, bytes, blocking_write)) {
        return true;
    }

    printError("PARTIAL WRITE FIRST PING PONG", " BUFFER FAILED");
    return false;
}

bool CLPingPongBuffer::readFirst(void* destPtr, cl_bool blocking_read/* = CL_TRUE */) {
    if (first.read(destPtr, blocking_read)) {
        return true;
    }

    printError("READ FIRST PING PONG", " BUFFER FAILED");
    return false;
}

bool CLPingPongBuffer::readFirst(void *destPtr,
        size_t offset,
        size_t bytes,
        cl_bool blocking_read/* = CL_TRUE*/) {
    if (first.read(destPtr, offset, bytes, blocking_read)) {
        return true;
    }

    printError("PARTIAL READ FIRST PING PONG", " BUFFER FAILED");
    return false;
}

bool CLPingPongBuffer::writeSecond(const void *sourcePtr, cl_bool blocking_write/* = CL_TRUE */) {
    if (second.write(sourcePtr, blocking_write)) {
        return true;
    }

    printError("WRITE SECOND PING PONG", " BUFFER FAILED");
    return false;
}

bool CLPingPongBuffer::writeSecond(const void *sourcePtr,
        size_t offset,
        size_t bytes,
        cl_bool blocking_write/* = CL_TRUE */) {

    if (second.write(sourcePtr, offset, bytes, blocking_write)) {
        return true;
    }

    printError("PARTIAL WRITE SECOND PING PONG", " BUFFER FAILED");
    return false;
}

bool CLPingPongBuffer::readSecond(void* destPtr, cl_bool blocking_read/* = CL_TRUE */) {
    if (second.read(destPtr, blocking_read)) {
        return true;
    }

    printError("READ SECOND PING PONG", " BUFFER FAILED");
    return false;
}

bool CLPingPongBuffer::readSecond(void *destPtr,
        size_t offset,
        size_t bytes,
        cl_bool blocking_read/* = CL_TRUE */) {
    if (second.read(destPtr, offset, bytes, blocking_read)) {
        return true;
    }

    printError("PARTIAL READ SECOND PING PONG", " BUFFER FAILED");
    return false;
}

void CLPingPongBuffer::printError(const char* beginMsg, const char* endMsg) {
    if (debugName) {
        std::cout << "ERROR: " << beginMsg << " '" << debugName << "'" << endMsg
                << "!!" << std::endl;
    } else {
        std::cout << "ERROR: " << beginMsg << endMsg << "!!" << std::endl;
    }
}

