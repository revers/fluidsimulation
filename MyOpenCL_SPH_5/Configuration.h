/* 
 * File:   Configuration.h
 * Author: Revers
 *
 * Created on 20 listopad 2011, 23:06
 */

#ifndef CONFIGURATION_H
#define	CONFIGURATION_H

#include <cassert>
#include <string>
#include <map>
#include "Parameters.h"
#include "FluidSimulation.h"

class Configuration {
    typedef std::map<std::string, std::string> KeyValueMap;
    typedef KeyValueMap::iterator KeyValueMapIterator;
    KeyValueMap keyValueMap;

    bool inited;
    unsigned int particleCount;

    InitialParticlePlacement initialParticlePlacement;
private:

    Configuration() {
        inited = false;
    }
    Configuration(const Configuration &);
    Configuration& operator=(const Configuration&);
public:

    static Configuration& getInstance() {

        static Configuration instance;
        return instance;
    }

    bool init();

    const char* getCxxDirectory();
    unsigned int getParticleCount();
    
    bool isInitedCorrectly() {
        return inited;
    }

    void setParticleCount(unsigned int particleCount) {
        this->particleCount = particleCount;
    }

    InitialParticlePlacement getInitialParticlePlacement() const {
        return initialParticlePlacement;
    }

    void setInitialParticlePlacement(InitialParticlePlacement initialParticlePlacement) {
        this->initialParticlePlacement = initialParticlePlacement;
    }

private:
    bool checkAllKeys();
    bool checkParticleCount();
    bool checkInitPlacement();
    inline const char* getValue(const char* key);

    std::string getInitPlacement();

};

#endif	/* CONFIGURATION_H */

