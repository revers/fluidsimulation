#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc.exe
CCC=g++.exe
CXX=g++.exe
FC=gfortran.exe
AS=as.exe

# Macros
CND_PLATFORM=MinGW-Windows
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/SphereRenderer.o \
	${OBJECTDIR}/Configuration.o \
	${OBJECTDIR}/FluidParameters.o \
	${OBJECTDIR}/CLBufferPboGL.o \
	${OBJECTDIR}/NV_Scan.o \
	${OBJECTDIR}/OpenCLBase.o \
	${OBJECTDIR}/ControlPanel.o \
	${OBJECTDIR}/CLBufferVboGL.o \
	${OBJECTDIR}/OpenGLUtil.o \
	${OBJECTDIR}/CLBuffer.o \
	${OBJECTDIR}/NV_RadixSort.o \
	${OBJECTDIR}/FluidSimulation.o \
	${OBJECTDIR}/CLBufferImage2D.o \
	${OBJECTDIR}/FluidSimulationTest.o \
	${OBJECTDIR}/MyCLSPH5.o \
	${OBJECTDIR}/TargetCamera.o \
	${OBJECTDIR}/RaycastingRenderer.o \
	${OBJECTDIR}/shaders.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=../RevLib/dist/Release/MinGW-Windows/librevlib.a /C/MinGW/OpenCL/lib/libOpenCL.a /C/MinGW/lib/opengl/libglew32.dll.a /C/MinGW/lib/opengl/libfreeglut.a lib/libmysoil.a /C/MinGW/lib/opengl/libglu32.a /C/MinGW/lib/opengl/libopengl32.a lib/AntTweakBar.lib

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/myopencl_sph_5.exe

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/myopencl_sph_5.exe: ../RevLib/dist/Release/MinGW-Windows/librevlib.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/myopencl_sph_5.exe: /C/MinGW/OpenCL/lib/libOpenCL.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/myopencl_sph_5.exe: /C/MinGW/lib/opengl/libglew32.dll.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/myopencl_sph_5.exe: /C/MinGW/lib/opengl/libfreeglut.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/myopencl_sph_5.exe: lib/libmysoil.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/myopencl_sph_5.exe: /C/MinGW/lib/opengl/libglu32.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/myopencl_sph_5.exe: /C/MinGW/lib/opengl/libopengl32.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/myopencl_sph_5.exe: lib/AntTweakBar.lib

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/myopencl_sph_5.exe: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/myopencl_sph_5 ${OBJECTFILES} ${LDLIBSOPTIONS} 

${OBJECTDIR}/SphereRenderer.o: SphereRenderer.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -I../RevLib -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/SphereRenderer.o SphereRenderer.cpp

${OBJECTDIR}/Configuration.o: Configuration.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -I../RevLib -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/Configuration.o Configuration.cpp

${OBJECTDIR}/FluidParameters.o: FluidParameters.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -I../RevLib -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/FluidParameters.o FluidParameters.cpp

${OBJECTDIR}/CLBufferPboGL.o: CLBufferPboGL.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -I../RevLib -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/CLBufferPboGL.o CLBufferPboGL.cpp

${OBJECTDIR}/NV_Scan.o: NV_Scan.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -I../RevLib -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/NV_Scan.o NV_Scan.cpp

${OBJECTDIR}/OpenCLBase.o: OpenCLBase.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -I../RevLib -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/OpenCLBase.o OpenCLBase.cpp

${OBJECTDIR}/ControlPanel.o: ControlPanel.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -I../RevLib -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/ControlPanel.o ControlPanel.cpp

${OBJECTDIR}/CLBufferVboGL.o: CLBufferVboGL.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -I../RevLib -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/CLBufferVboGL.o CLBufferVboGL.cpp

${OBJECTDIR}/OpenGLUtil.o: OpenGLUtil.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -I../RevLib -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/OpenGLUtil.o OpenGLUtil.cpp

${OBJECTDIR}/CLBuffer.o: CLBuffer.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -I../RevLib -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/CLBuffer.o CLBuffer.cpp

${OBJECTDIR}/NV_RadixSort.o: NV_RadixSort.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -I../RevLib -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/NV_RadixSort.o NV_RadixSort.cpp

${OBJECTDIR}/FluidSimulation.o: FluidSimulation.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -I../RevLib -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/FluidSimulation.o FluidSimulation.cpp

${OBJECTDIR}/CLBufferImage2D.o: CLBufferImage2D.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -I../RevLib -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/CLBufferImage2D.o CLBufferImage2D.cpp

${OBJECTDIR}/FluidSimulationTest.o: FluidSimulationTest.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -I../RevLib -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/FluidSimulationTest.o FluidSimulationTest.cpp

${OBJECTDIR}/MyCLSPH5.o: MyCLSPH5.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -I../RevLib -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/MyCLSPH5.o MyCLSPH5.cpp

${OBJECTDIR}/TargetCamera.o: TargetCamera.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -I../RevLib -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/TargetCamera.o TargetCamera.cpp

${OBJECTDIR}/RaycastingRenderer.o: RaycastingRenderer.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -I../RevLib -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/RaycastingRenderer.o RaycastingRenderer.cpp

${OBJECTDIR}/shaders.o: shaders.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -I../RevLib -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/shaders.o shaders.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/myopencl_sph_5.exe

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
