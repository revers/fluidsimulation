/* 
 * File:   RaycastingRenderer.h
 * Author: Revers
 *
 * Created on 18 grudzień 2011, 01:15
 */

#ifndef RAYCASTINGRENDERER_H
#define	RAYCASTINGRENDERER_H

#include <cstdlib>
#include <iostream>
#include <vector>

#include "Renderer.h"

#include "OpenCLUtil.h"
#include "OpenCLBase.h"
#include "CLBuffer.h"
#include "CLBufferPboGL.h"

#include "RevGraphics.h"

#include "FluidSimulation.h"
#include "ControlPanel.h"
#include "TargetCamera.h"

class RaycastingRenderer : public Renderer, public OpenCLBase {
    typedef unsigned int uint;
    typedef unsigned char uchar;

    static const GLenum PIXEL_FORMAT = GL_RGBA;

    cl::Image2D transferFuncBuffer;

    CLBufferPboGL pboRaycastingBuffer;
    CLBuffer matrixBuffer;

    cl::Image3DGL image3dBuffer;
    //CLBufferPboGL pboTex3dBuffer;
    GLuint tex3dId;

    //----------------------------------
    cl::Image2DGL cubeMapPosXBuffer;
    cl::Image2DGL cubeMapNegXBuffer;
    cl::Image2DGL cubeMapPosYBuffer;
    cl::Image2DGL cubeMapNegYBuffer;
    cl::Image2DGL cubeMapPosZBuffer;
    cl::Image2DGL cubeMapNegZBuffer;
    //----------------------------------

    cl::Program clProgram;
    cl::Kernel kernelPhongRender;
    cl::Kernel kernelCubmapRender;
    cl::Kernel kernelDrawFrame;

    uint width;
    uint height;
    // size_t volumeSize[3];
    size_t gridSize[2];

    float density;
    float brightness;
    float transferOffset;
    float transferScale;
    bool linearFiltering;

    cl_sampler volumeSamplerLinear;
    cl_sampler volumeSamplerNearest;
    cl_sampler transferFuncSampler;

    rev::graph::Matrix44f transformMatrix;
    rev::graph::Matrix44f translationMatrix;

    char pixelsPerWorkItem[5];

    cl_ulong offset;

    int xOffset;
    int yOffset;
    float xOffsetF;
    float yOffsetF;

    GLuint tex_cube;

public:

    RaycastingRenderer(ControlPanel* controlPanel_, TargetCamera* camera_) :
    Renderer(controlPanel_, camera_) {

        tex_cube = 0;

        xOffset = 0;
        yOffset = 0;

        itoa(PIXELS_PER_WORK_ITEM, pixelsPerWorkItem, 10);
        offset = 0;

        volumeSamplerLinear = 0;
        volumeSamplerNearest = 0;
        transferFuncSampler = 0;

        transformMatrix = rev::graph::Matrix44f::createIdentityMatrix();
        translationMatrix = rev::graph::Matrix44f::createIdentityMatrix();
        width = 512;
        height = 512;

        gridSize[0] = width;
        gridSize[1] = height;

        density = 0.05f;
        brightness = 1.0f;
        transferOffset = 0.0f;
        transferScale = 1.0f;
        linearFiltering = true;
    }

    virtual ~RaycastingRenderer() {
        if (volumeSamplerLinear)clReleaseSampler(volumeSamplerLinear);
        if (volumeSamplerNearest)clReleaseSampler(volumeSamplerNearest);
        if (transferFuncSampler)clReleaseSampler(transferFuncSampler);
    }

    virtual bool init();

    virtual void render();

    virtual void windowSizeChanged(int newWidth, int newHeight);

private:
    RaycastingRenderer(const RaycastingRenderer& orig);

    void renderTexture3D();
    void renderRaycastingPBO();

    void renderBoxFrame();

    void setTransform(rev::graph::Quaternionf& rotation, float translationX, float translationY, float translationZ);

    size_t roundUp(int group_size, int global_size);

    bool initCubeMapBuffers();

    bool initPboRaycastinglBuffer();

    bool initCLVolume();

    bool createProgramAndKernels();

    bool setFixedKernelArguments();

    bool init3dTex();

    bool loadCubeMap();

    void setupTextureParameters();

    void drawSurroundings();

};

#endif	/* RAYCASTINGRENDERER_H */

