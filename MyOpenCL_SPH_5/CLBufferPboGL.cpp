#include <GL/glew.h>
#include <CL/cl_gl.h>
#include <GL/glut.h>
#include <GL/gl.h>

#include "OpenCLBase.h"
#include "CLBufferPboGL.h"

CLBufferPboGL::CLBufferPboGL(const CLBufferPboGL& old) {
    this->clContext = old.clContext;
    this->clCommandQueue = old.clCommandQueue;
    this->size = old.size;
    this->debugName = old.debugName;
    this->buffer = old.buffer;
    this->pixelBufferObject = old.pixelBufferObject;
    this->bufferVector = old.bufferVector;
}

CLBufferPboGL::~CLBufferPboGL() {
    if (pixelBufferObject != 0) {
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glDeleteBuffers(1, &pixelBufferObject);
    }
}

void CLBufferPboGL::acquireGLObject() {
    clCommandQueue.enqueueAcquireGLObjects(&bufferVector, NULL, NULL);
    clCommandQueue.finish();
}

void CLBufferPboGL::releaseGLObject() {
    clCommandQueue.enqueueReleaseGLObjects(&bufferVector, NULL, NULL);
    clCommandQueue.finish();
}

bool CLBufferPboGL::init(cl::Context clContext,
        cl::CommandQueue clCommandQueue,
        size_t size,
        const char* debugName/* = NULL */,
        cl_mem_flags flags/* = CL_MEM_READ_WRITE */,
        void* host_ptr/* = NULL */) {

    this->clContext = clContext;
    this->clCommandQueue = clCommandQueue;
    this->debugName = debugName;

    cl_int err;


    char* mem = NULL;

    if (host_ptr == NULL) {
        mem = new char[size];
        memset(mem, 0, size);
    } else {
        mem = (char*) host_ptr;
    }

    /////////////////////////////////////////////////////////////////
    // OpenGL BEGIN
    /////////////////////////////////////////////////////////////////
    /*
     * Create Pixel buffer object
     */
    glGenBuffers(1, &pixelBufferObject);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, pixelBufferObject);
    glBufferData(GL_PIXEL_UNPACK_BUFFER, size, (GLvoid *) mem, GL_STREAM_DRAW);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);

    /////////////////////////////////////////////////////////////////
    // OpenGL END
    /////////////////////////////////////////////////////////////////
    if (host_ptr == NULL) {
        delete[] mem;
    }

    buffer = cl::BufferGL(clContext, flags, pixelBufferObject, &err);

    if (err != CL_SUCCESS) {
        printError(err, "INITIALIZATION", " BUFFER FAILED");
        return false;
    }

    bufferVector[0] = buffer;

    this->size = size;

    return true;
}

bool CLBufferPboGL::write(const void *sourcePtr, cl_bool blocking_write/* = CL_TRUE */) {
    assert(buffer() != 0);
    acquireGLObject();
    cl_int err;

    err = clCommandQueue.enqueueWriteBuffer(
            buffer, blocking_write, 0, size,
            sourcePtr, NULL, NULL);

    if (err != CL_SUCCESS) {
        releaseGLObject();
        printError(err, "WRITE", " BUFFER FAILED");
        return false;
    }

    releaseGLObject();

    return true;
}

bool CLBufferPboGL::write(const void *sourcePtr,
        size_t offset,
        size_t bytes,
        cl_bool blocking_write/* = CL_TRUE */) {
    assert(buffer() != 0);
    acquireGLObject();
    cl_int err;

    err = clCommandQueue.enqueueWriteBuffer(
            buffer, blocking_write, offset, bytes,
            sourcePtr, NULL, NULL);

    if (err != CL_SUCCESS) {
        releaseGLObject();
        printError(err, "PARTIAL WRITE", " BUFFER FAILED");
        return false;
    }

    releaseGLObject();

    return true;
}

bool CLBufferPboGL::read(void* destPtr, cl_bool blocking_read/* = CL_TRUE */) {
    assert(buffer() != 0);
    acquireGLObject();
    cl_int err;

    err = clCommandQueue.enqueueReadBuffer(buffer, blocking_read, 0,
            size, destPtr, NULL, NULL);

    if (err != CL_SUCCESS) {
        releaseGLObject();
        printError(err, "READ", " BUFFER FAILED");
        return false;
    }

    releaseGLObject();

    return true;
}

bool CLBufferPboGL::read(void *destPtr,
        size_t offset,
        size_t bytes,
        cl_bool blocking_read/* = CL_TRUE */) {

    assert(buffer() != 0);
    acquireGLObject();
    cl_int err;

    err = clCommandQueue.enqueueReadBuffer(buffer, blocking_read, offset,
            bytes, destPtr, NULL, NULL);

    if (err != CL_SUCCESS) {
        releaseGLObject();
        printError(err, "PARTIAL READ", " BUFFER FAILED");
        return false;
    }

    releaseGLObject();

    return true;
}

void CLBufferPboGL::printError(cl_int err, const char* beginMsg, const char* endMsg) {
    if (debugName) {
        std::cout << "ERROR: " << beginMsg << " '" << debugName << "'" << endMsg
                << OpenCLBase::getOpenCLErrorCodeStr(err) << std::endl;
    } else {
        std::cout << "ERROR: " << beginMsg << endMsg
                << OpenCLBase::getOpenCLErrorCodeStr(err) << std::endl;
    }
}

//===============================================================================

CLPingPongBufferPboGL::CLPingPongBufferPboGL(const CLPingPongBufferPboGL& old) {
    this->size = old.size;
    this->totalSize = old.totalSize;
    this->debugName = old.debugName;
    this->first = old.first;
    this->second = old.second;
}

bool CLPingPongBufferPboGL::init(cl::Context clContext,
        cl::CommandQueue clCommandQueue,
        size_t size,
        const char* debugName/* = NULL */,
        cl_mem_flags flags/* = CL_MEM_READ_WRITE */,
        void* host_ptr/* = NULL */) {

    this->debugName = debugName;

    bool succ = first.init(clContext, clCommandQueue, size, "PingPongGL_1", flags, host_ptr);

    if (succ == false) {
        printError("INITIALIZATION PING PONG #1", " BUFFER FAILED");
        return false;
    }

    succ = second.init(clContext, clCommandQueue, size, "PingPongGL_2", flags, host_ptr);

    if (succ == false) {
        printError("INITIALIZATION PING PONG #2", " BUFFER FAILED");
        return false;
    }

    this->size = size;
    this->totalSize = size * 2;

    return true;
}

bool CLPingPongBufferPboGL::writeFirst(const void *sourcePtr, cl_bool blocking_write/* = CL_TRUE */) {
    if (first.write(sourcePtr, blocking_write)) {
        return true;
    }

    printError("WRITE FIRST PING PONG", " BUFFER FAILED");
    return false;
}

bool CLPingPongBufferPboGL::writeFirst(const void *sourcePtr,
        size_t offset,
        size_t bytes,
        cl_bool blocking_write/* = CL_TRUE */) {

    if (first.write(sourcePtr, offset, bytes, blocking_write)) {
        return true;
    }

    printError("PARTIAL WRITE FIRST PING PONG", " BUFFER FAILED");
    return false;
}

bool CLPingPongBufferPboGL::readFirst(void* destPtr, cl_bool blocking_read/* = CL_TRUE */) {
    if (first.read(destPtr, blocking_read)) {
        return true;
    }

    printError("READ FIRST PING PONG", " BUFFER FAILED");
    return false;
}

bool CLPingPongBufferPboGL::readFirst(void *destPtr,
        size_t offset,
        size_t bytes,
        cl_bool blocking_read/* = CL_TRUE */) {
    if (first.read(destPtr, offset, bytes, blocking_read)) {
        return true;
    }

    printError("PARTIAL READ FIRST PING PONG", " BUFFER FAILED");
    return false;
}

bool CLPingPongBufferPboGL::writeSecond(const void *sourcePtr, cl_bool blocking_write/* = CL_TRUE */) {
    if (second.write(sourcePtr, blocking_write)) {
        return true;
    }

    printError("WRITE SECOND PING PONG", " BUFFER FAILED");
    return false;
}

bool CLPingPongBufferPboGL::writeSecond(const void *sourcePtr,
        size_t offset,
        size_t bytes,
        cl_bool blocking_write/* = CL_TRUE */) {

    if (second.write(sourcePtr, offset, bytes, blocking_write)) {
        return true;
    }

    printError("PARTIAL WRITE SECOND PING PONG", " BUFFER FAILED");
    return false;
}

bool CLPingPongBufferPboGL::readSecond(void* destPtr, cl_bool blocking_read/* = CL_TRUE */) {
    if (second.read(destPtr, blocking_read)) {
        return true;
    }

    printError("READ SECOND PING PONG", " BUFFER FAILED");
    return false;
}

bool CLPingPongBufferPboGL::readSecond(void *destPtr,
        size_t offset,
        size_t bytes,
        cl_bool blocking_read/* = CL_TRUE */) {
    if (second.read(destPtr, offset, bytes, blocking_read)) {
        return true;
    }

    printError("PARTIAL READ SECOND PING PONG", " BUFFER FAILED");
    return false;
}

void CLPingPongBufferPboGL::printError(const char* beginMsg, const char* endMsg) {
    if (debugName) {
        std::cout << "ERROR: " << beginMsg << " '" << debugName << "'" << endMsg
                << "!!" << std::endl;
    } else {
        std::cout << "ERROR: " << beginMsg << endMsg << "!!" << std::endl;
    }
}

