/* 
 * File:   FluidSimulation.cpp
 * Author: Revers
 * 
 * Created on 13 listopad 2011, 18:43
 */
#include <iostream>
#include <cassert>
#include <cmath>

#include "FluidSimulation.h"

#include "Parameters.h"
#include "OpenCLUtil.h"
#include "Configuration.h"

#define KERNEL_FILE "MySPH_5.cl"
#define MY_WORK_GROUP_SIZE 256    

#define INTERLEAVE_ZERO(A, I) (A[INTERLEAVE_CELL_SIZE * (I)])
#define INTERLEAVE_ONE(A, I) (A[INTERLEAVE_CELL_SIZE * (I) + 1])
#define INTERLEAVE_TWO(A, I) (A[INTERLEAVE_CELL_SIZE * (I) + 2])

#define CHECK_ERR(A) if(!(A)) return false
#define CHECK_CL_ERR(A) if((A) != CL_SUCCESS) return false

#define SCALE(MIN, MAX, X) ((MIN) + (X) * ((MAX) - (MIN)))
#define SQR(A) ((A)*(A))

#define RADIX_SORT_GROUP_SIZE 128
#define RADIX_SORT_KEY_BITS 32

using namespace std;

FluidSimulation::FluidSimulation(unsigned int particleCount, InitialParticlePlacement initialParticlePlacement_) :
OpenCLBase(false),
radixSort(NULL),
positionArray(NULL),
velocityArray(NULL) {

    lightDirection.x = -1;
    lightDirection.y = 1;
    lightDirection.z = -1;
    lightDirection.w = 0;

    clearPboTex3dGlobalSize = 0;

    fluidParameters = new FluidParameters(particleCount);

    //fluidParameters->printParameters();

    initialParticlePlacement = initialParticlePlacement_;


#ifdef USE_LEAP_FROG
    initLeapFrog = true;
#endif

    // for clearBuffers:
    assert(fluidParameters->getParticleCount() % 2 == 0);
    // for speed and for runCreateCompactBlockList:
    assert(fluidParameters->getParticleCount() % 256 == 0);

    densityComputationData.stiffness = fluidParameters->getStiffness();
    densityComputationData.supportRadiusScaled = fluidParameters->getSupportRadiusScaled();
    //  float hScaled = (fluidParameters->getsupportRadiusScaled) * (fluidParameters->simulationScale)();
    densityComputationData.supportRadiusSqr = SQR(fluidParameters->getSupportRadius());
    densityComputationData.particleMass = fluidParameters->getParticleMass();
    densityComputationData.restDensity = fluidParameters->getRestDensity();
    densityComputationData.restDensityInvSqr = SQR(1.0f / densityComputationData.restDensity);
    densityComputationData.simulationScale = fluidParameters->getSimulationScale();
    densityComputationData.wPoly6Coefficient = fluidParameters->getWPoly6Coefficient();

    forceComputationData.gradWspikyCoefficient = fluidParameters->getGradWspikyCoefficient();
    forceComputationData.del2WviscosityCoefficient = fluidParameters->getDel2WviscosityCoefficient();
    forceComputationData.particleMass = fluidParameters->getParticleMass();
    forceComputationData.simulationScale = fluidParameters->getSimulationScale();
    forceComputationData.simulationScaleInv = fluidParameters->getSimulationScaleInv();
    forceComputationData.supportRadius = fluidParameters->getSupportRadius();
    forceComputationData.supportRadiusScaled = fluidParameters->getSupportRadiusScaled();
    forceComputationData.viscosity = fluidParameters->getViscosity();
    forceComputationData.accelerationLimit = fluidParameters->getAccelerationLimit();
    forceComputationData.gravityX = fluidParameters->getGravityX();
    forceComputationData.gravityY = fluidParameters->getGravityY();
    forceComputationData.gravityZ = fluidParameters->getGravityZ();
    forceComputationData.timeStep = fluidParameters->getTimestep();
    forceComputationData.damping = fluidParameters->getDamping();
    forceComputationData.coloringSource = VELOCITY;
    forceComputationData.coloringGradient = HSVBlueToRed;

#ifdef EXTRACT_SURFACE_CENTER_OF_MASS
    forceComputationDataExtract.gradWspikyCoefficient = fluidParameters->getGradWspikyCoefficient();
    forceComputationDataExtract.del2WviscosityCoefficient = fluidParameters->getDel2WviscosityCoefficient();
    forceComputationDataExtract.particleMass = fluidParameters->getParticleMass();
    forceComputationDataExtract.simulationScale = fluidParameters->getSimulationScale();
    forceComputationDataExtract.simulationScaleInv = fluidParameters->getSimulationScaleInv();
    forceComputationDataExtract.supportRadius = fluidParameters->getSupportRadius();
    forceComputationDataExtract.supportRadiusScaled = fluidParameters->getSupportRadiusScaled();
    forceComputationDataExtract.viscosity = fluidParameters->getViscosity();
    forceComputationDataExtract.accelerationLimit = fluidParameters->getAccelerationLimit();
    forceComputationDataExtract.gravityX = fluidParameters->getGravityX();
    forceComputationDataExtract.gravityY = fluidParameters->getGravityY();
    forceComputationDataExtract.gravityZ = fluidParameters->getGravityZ();
    forceComputationDataExtract.timeStep = fluidParameters->getTimestep();
    forceComputationDataExtract.damping = fluidParameters->getDamping();
    forceComputationDataExtract.coloringSource = VELOCITY;
    forceComputationDataExtract.coloringGradient = HSVBlueToRed;
    forceComputationDataExtract.minParticles = 19; ////19;
    forceComputationDataExtract.distThreshold = 1.5f; //35f;
    forceComputationDataExtract.boundaryOffset = 2.0f;
#endif

#ifdef EXTRACT_SURFACE_COLOR_FIELD
    forceComputationDataColorField.gradWspikyCoefficient = fluidParameters->getGradWspikyCoefficient();
    forceComputationDataColorField.del2WviscosityCoefficient = fluidParameters->getDel2WviscosityCoefficient();
    forceComputationDataColorField.particleMass = fluidParameters->getParticleMass();
    forceComputationDataColorField.simulationScale = fluidParameters->getSimulationScale();
    forceComputationDataColorField.simulationScaleInv = fluidParameters->getSimulationScaleInv();
    forceComputationDataColorField.supportRadius = fluidParameters->getSupportRadius();
    forceComputationDataColorField.supportRadiusScaled = fluidParameters->getSupportRadiusScaled();
    forceComputationDataColorField.viscosity = fluidParameters->getViscosity();
    forceComputationDataColorField.accelerationLimit = fluidParameters->getAccelerationLimit();
    forceComputationDataColorField.gravityX = fluidParameters->getGravityX();
    forceComputationDataColorField.gravityY = fluidParameters->getGravityY();
    forceComputationDataColorField.gravityZ = fluidParameters->getGravityZ();
    forceComputationDataColorField.timeStep = fluidParameters->getTimestep();
    forceComputationDataColorField.damping = fluidParameters->getDamping();
    forceComputationDataColorField.coloringSource = VELOCITY;
    forceComputationDataColorField.coloringGradient = HSVBlueToRed;
    forceComputationDataColorField.colorFieldThreshold = TEMP_COLOR_FIELD_THRESHOLD;
    forceComputationDataColorField.gradWPoly6Coefficient = fluidParameters->getGradlWPoly6Coefficient();
    forceComputationDataColorField.supportRadiusSqr = SQR(fluidParameters->getSupportRadius());
    forceComputationDataColorField.del2WPoly6Cofficient = fluidParameters->getDel2WPoly6Coefficient();
    forceComputationDataColorField.surfaceTensionCoeff = TEMP_SURFACE_TENSION_COEFF;
#endif

#ifdef USE_RAYCASTING
    forceComputationDataDistField.gradWspikyCoefficient = fluidParameters->getGradWspikyCoefficient();
    forceComputationDataDistField.del2WviscosityCoefficient = fluidParameters->getDel2WviscosityCoefficient();
    forceComputationDataDistField.particleMass = fluidParameters->getParticleMass();
    forceComputationDataDistField.simulationScale = fluidParameters->getSimulationScale();
    forceComputationDataDistField.simulationScaleInv = fluidParameters->getSimulationScaleInv();
    forceComputationDataDistField.supportRadius = fluidParameters->getSupportRadius();
    forceComputationDataDistField.supportRadiusScaled = fluidParameters->getSupportRadiusScaled();
    forceComputationDataDistField.viscosity = fluidParameters->getViscosity();
    forceComputationDataDistField.accelerationLimit = fluidParameters->getAccelerationLimit();
    forceComputationDataDistField.gravityX = fluidParameters->getGravityX();
    forceComputationDataDistField.gravityY = fluidParameters->getGravityY();
    forceComputationDataDistField.gravityZ = fluidParameters->getGravityZ();
    forceComputationDataDistField.timeStep = fluidParameters->getTimestep();
    forceComputationDataDistField.damping = fluidParameters->getDamping();
    forceComputationDataDistField.colorFieldThreshold = TEMP_COLOR_FIELD_THRESHOLD;
    forceComputationDataDistField.gradWPoly6Coefficient = fluidParameters->getGradlWPoly6Coefficient();
    forceComputationDataDistField.supportRadiusSqr = SQR(fluidParameters->getSupportRadius());

    forceComputationDataDistField.restDensity = fluidParameters->getRestDensity();
    forceComputationDataDistField.restDensityInv = 1.0f / fluidParameters->getRestDensity();
    forceComputationDataDistField.rMin = 0.5f;
    forceComputationDataDistField.distFieldFactor = 1.7f;
    forceComputationDataDistField.del2WPoly6Cofficient = fluidParameters->getDel2WPoly6Coefficient();
    forceComputationDataDistField.surfaceTensionCoeff = TEMP_SURFACE_TENSION_COEFF;
#endif
}

#define RELEASE_ARRAY(X) if((X) != NULL) { delete [](X); } (X) = NULL; 

FluidSimulation::~FluidSimulation() {
    RELEASE_ARRAY(positionArray);
    RELEASE_ARRAY(velocityArray);

    if (radixSort) {
        delete radixSort;
        radixSort = NULL;
    }

    if (fluidParameters) {
        delete fluidParameters;
    }
}

void FluidSimulation::setRestDensity(float restDensity) {
    fluidParameters->setRestDensity(restDensity);

    densityComputationData.restDensity = fluidParameters->getRestDensity();
    densityComputationData.restDensityInvSqr = SQR(1.0f / densityComputationData.restDensity);

    forceComputationDataDistField.restDensity = fluidParameters->getRestDensity();
    forceComputationDataDistField.restDensityInv = 1.0f / fluidParameters->getRestDensity();
}

void FluidSimulation::setSimulationScale(float simulationScale) {
    fluidParameters->setSimulationScale(simulationScale);

    densityComputationData.simulationScale = fluidParameters->getSimulationScale();
    densityComputationData.supportRadiusScaled = fluidParameters->getSupportRadiusScaled();
    densityComputationData.supportRadiusSqr = SQR(fluidParameters->getSupportRadius());
    densityComputationData.wPoly6Coefficient = fluidParameters->getWPoly6Coefficient();

    forceComputationData.simulationScale = fluidParameters->getSimulationScale();
    forceComputationData.simulationScaleInv = fluidParameters->getSimulationScaleInv();
    forceComputationData.supportRadius = fluidParameters->getSupportRadius();
    forceComputationData.supportRadiusScaled = fluidParameters->getSupportRadiusScaled();
    forceComputationData.gradWspikyCoefficient = fluidParameters->getGradWspikyCoefficient();
    forceComputationData.del2WviscosityCoefficient = fluidParameters->getDel2WviscosityCoefficient();

    forceComputationDataExtract.simulationScale = fluidParameters->getSimulationScale();
    forceComputationDataExtract.simulationScaleInv = fluidParameters->getSimulationScaleInv();
    forceComputationDataExtract.supportRadius = fluidParameters->getSupportRadius();
    forceComputationDataExtract.supportRadiusScaled = fluidParameters->getSupportRadiusScaled();
    forceComputationDataExtract.gradWspikyCoefficient = fluidParameters->getGradWspikyCoefficient();
    forceComputationDataExtract.del2WviscosityCoefficient = fluidParameters->getDel2WviscosityCoefficient();

    forceComputationDataColorField.simulationScale = fluidParameters->getSimulationScale();
    forceComputationDataColorField.simulationScaleInv = fluidParameters->getSimulationScaleInv();
    forceComputationDataColorField.supportRadius = fluidParameters->getSupportRadius();
    forceComputationDataColorField.supportRadiusScaled = fluidParameters->getSupportRadiusScaled();
    forceComputationDataColorField.gradWspikyCoefficient = fluidParameters->getGradWspikyCoefficient();
    forceComputationDataColorField.del2WviscosityCoefficient = fluidParameters->getDel2WviscosityCoefficient();
    forceComputationDataColorField.gradWPoly6Coefficient = fluidParameters->getGradlWPoly6Coefficient();
    forceComputationDataColorField.supportRadiusSqr = SQR(fluidParameters->getSupportRadius());
    forceComputationDataColorField.del2WPoly6Cofficient = fluidParameters->getDel2WPoly6Coefficient();

    forceComputationDataDistField.simulationScale = fluidParameters->getSimulationScale();
    forceComputationDataDistField.simulationScaleInv = fluidParameters->getSimulationScaleInv();
    forceComputationDataDistField.supportRadius = fluidParameters->getSupportRadius();
    forceComputationDataDistField.supportRadiusScaled = fluidParameters->getSupportRadiusScaled();
    forceComputationDataDistField.gradWspikyCoefficient = fluidParameters->getGradWspikyCoefficient();
    forceComputationDataDistField.del2WviscosityCoefficient = fluidParameters->getDel2WviscosityCoefficient();
    forceComputationDataDistField.gradWPoly6Coefficient = fluidParameters->getGradlWPoly6Coefficient();
    forceComputationDataDistField.supportRadiusSqr = SQR(fluidParameters->getSupportRadius());
    forceComputationDataDistField.del2WPoly6Cofficient = fluidParameters->getDel2WPoly6Coefficient();
}

bool FluidSimulation::init() {
    if (!initCL()) {
        cout << "initCL() FAILED!!" << endl;
        return false;
    }

    //cl_uint val = clDevices[0].getInfo<CL_DEVICE_MAX_CONSTANT_ARGS > ();
    //cl_uint max = clDevices[0].getInfo<CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE > ();
    // cout << "CL_DEVICE_MAX_CONSTANT_ARGS = " << val << endl;
    //cout << "CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE = " << max << "; " << blockCoordsLookupBuffer.getSize() << endl;

    if (!createBuffers()) {
        cout << "createBuffers() FAILED!!" << endl;
        return false;
    }

    if (!createProgramAndKernels()) {
        cout << "createProgramAndKernels() FAILED!!" << endl;
        return false;
    }

    if (!createInterleaveLookupTable()) {
        cout << "createInterleaveLookupTable() FAILED!!" << endl;
        return false;
    }

    if (!createBlockCoordsLookupTable()) {
        cout << "createBlockCoordsLookupTable() FAILED!!" << endl;
        return false;
    }

    if (!createNeighborLookupTable()) {
        cout << "createNeighborLookupTable() FAILED!!" << endl;
        return false;
    }

    if (!createInitialPositionAndVelocity(initialParticlePlacement)) {
        cout << "createInitialPositionAndVelocity() FAILED!!" << endl;
        return false;
    }

#ifdef NV_RADIX_SORT
    radixSort = new NVRadixSort(&radixSortPPBuffer,
            clContext(),
            clCommandQueue(),
            fluidParameters->getParticleCount(),
            RADIX_SORT_GROUP_SIZE,
            RADIX_SORT_KEY_BITS);
#elif defined(ATI_RADIX_SORT)
    radixSort = new ATIRadixSortCL;
    radixSort->initializeSort(&radixSortPPBuffer, clContext, clCommandQueue, fluidParameters->getParticleCount(), 32, true);
#endif

    if (!setFixedKernelArguments()) {
        cout << "setFixedKernelArguments() FAILED!!" << endl;
        return false;
    }

    fillColorBuffer(0, 0, 1, 1);

    runCopyPositionBuffer();

    return true;
}

bool FluidSimulation::createBuffers() {
    if (!gpu2cpuDataBuffer.init(clContext,
            clCommandQueue,
            sizeof (gpu2cpuArray),
            "gpu2Cpu")) {
        return false;
    }

    if (fluidParameters->getParticleCount() > fluidParameters->getBlockCount()) {
        if (!radixSortPPBuffer.init(clContext,
                clCommandQueue,
                sizeof (cl_uint) * SORT_CELL_SIZE * fluidParameters->getParticleCount(),
                "RadixSort")) {
            return false;
        }
    } else {
        if (!radixSortPPBuffer.init(clContext,
                clCommandQueue,
                sizeof (cl_uint) * SORT_CELL_SIZE * fluidParameters->getBlockCount(),
                "RadixSort")) {
            return false;
        }
    }

    if (!positionPPBuffer.init(INT4, clContext,
            clCommandQueue,
            sizeof (cl_float4) * fluidParameters->getParticleCount(),
            1024,
            "Position")) {
        return false;
    }

    if (!velocityPPBuffer.init(INT4, clContext,
            clCommandQueue,
            sizeof (cl_float4) * fluidParameters->getParticleCount(),
            1024,
            "Velocity")) {
        return false;
    }

    if (!positionBufferGL.init(clContext,
            clCommandQueue,
            sizeof (cl_float4) * fluidParameters->getParticleCount(),
            "PositionGL")) {
        return false;
    }

    if (!surfacePositionBufferGL.init(clContext,
            clCommandQueue,
            sizeof (cl_float4) * fluidParameters->getParticleCount(),
            "SurfacePositionGL")) {
        return false;
    }

    if (!colorBuffer.init(clContext,
            clCommandQueue,
            sizeof (cl_float4) * fluidParameters->getParticleCount(),
            "Color")) {
        return false;
    }

    if (!densityBuffer.init(clContext,
            clCommandQueue,
            sizeof (cl_float2) * fluidParameters->getParticleCount(),
            "Density")) {
        return false;
    }

    if (!interleaveLookupBuffer.init(clContext,
            clCommandQueue,
            sizeof (cl_uint) * INTERLEAVE_CELL_SIZE * fluidParameters->getInterleaveLookupLength(),
            "Interleave Lookup",
            CL_MEM_READ_ONLY)) {
        return false;
    }

    if (!neighborLookupBuffer.init(clContext,
            clCommandQueue,
            4 * sizeof (cl_int) * 32,
            "Neighbor Lookup",
            CL_MEM_READ_ONLY)) {
        return false;
    }

    if (!blockCoordsLookupBuffer.init(UINT4,
            clContext,
            clCommandQueue,
            sizeof (cl_uint) * BLOCK_LOOKUP_CELL_SIZE * fluidParameters->getBlockCoordsLookupLength(),
            1024,
            "Block Coords Lookup",
            CL_MEM_READ_ONLY)) {
        return false;
    }

    //======================================================================
    if (!pboTex3dBuffer.init(clContext, clCommandQueue,
            IMAGE3D_WIDTH * IMAGE3D_HEIGHT * IMAGE3D_DEPTH * ONE_PIXEL_SIZE, "pboTex3D")) {
        return false;
    }

    clearPboTex3dGlobalSize = pboTex3dBuffer.getSize() / (PIXELS_PER_WORK_ITEM * ONE_PIXEL_SIZE);

    assert(pboTex3dBuffer.getSize() % (PIXELS_PER_WORK_ITEM * ONE_PIXEL_SIZE) == 0);
    assert(clearPboTex3dGlobalSize % MY_WORK_GROUP_SIZE == 0);
    //======================================================================

    return true;
}

void FluidSimulation::fillColorBuffer(float r, float g, float b, float alpha) {
    float* temp = new float[4 * fluidParameters->getParticleCount()];
    for (int i = 0; i < fluidParameters->getParticleCount(); i++) {
        temp[i * 4] = r;
        temp[i * 4 + 1] = g;
        temp[i * 4 + 2] = b;
        temp[i * 4 + 3] = alpha;
    }

    if (!colorBuffer.write(temp)) {
        cout << "ERROR: Cannot fill color buffer!!" << endl;
    }

    delete[] temp;
}

bool FluidSimulation::createProgramAndKernels() {
    cl_int err;

    Configuration& config = Configuration::getInstance();

    string sourceCode = OpenCLUtil::readCLFile(KERNEL_FILE, config.getCxxDirectory(),
            "Parameters.h");
    cl::Program::Sources
    sources(1, std::make_pair(sourceCode.c_str(), sourceCode.length()));

    clProgram = cl::Program(clContext, sources, &err);
    CHECK_ERR(err == CL_SUCCESS);
#ifdef USE_FAST_RELAXED_MATH_FLAG
    const char* flags = "-cl-fast-relaxed-math";
#else
    const char* flags = NULL;
#endif
    err = clProgram.build(clDevices, flags);

    if (err != CL_SUCCESS) {

        string str =
                clProgram.getBuildInfo<CL_PROGRAM_BUILD_LOG > (clDevices[0]);
        cout << "Program Error Info: " << str << endl;
        return false;
    }

    kernelHashParticles = cl::Kernel(clProgram, "hashParticles", &err);
    CHECK_ERR(err == CL_SUCCESS);

    kernelReorderPositionAndVelocity = cl::Kernel(clProgram, "reorderPositionAndVelocity", &err);
    CHECK_ERR(err == CL_SUCCESS);

    kernelClearBuffer = cl::Kernel(clProgram, "clearBuffer", &err);
    CHECK_ERR(err == CL_SUCCESS);

    kernelCreateBlockList = cl::Kernel(clProgram, "createBlockList", &err);
    CHECK_ERR(err == CL_SUCCESS);

    kernelCreateCompactBlockList = cl::Kernel(clProgram, "createCompactBlockList", &err);
    CHECK_ERR(err == CL_SUCCESS);

    kernelComputeDensityAndPressure = cl::Kernel(clProgram, "computeDensityAndPressure", &err);
    CHECK_ERR(err == CL_SUCCESS);

    kernelComputeForcesAndIntegrate = cl::Kernel(clProgram, "computeForcesAndIntegrate", &err);
    CHECK_ERR(err == CL_SUCCESS);

    kernelComputeForcesAndIntegrateExtract = cl::Kernel(clProgram, "computeForcesAndIntegrateExtract", &err);
    CHECK_ERR(err == CL_SUCCESS);

    kernelComputeForcesAndIntegrateColorField = cl::Kernel(clProgram, "computeForcesAndIntegrateColorField", &err);
    CHECK_ERR(err == CL_SUCCESS);

    kernelComputeForcesAndIntegrateDistField = cl::Kernel(clProgram, "computeForcesAndIntegrateDistField", &err);
    CHECK_ERR(err == CL_SUCCESS);

    kernelClearPboTex3d = cl::Kernel(clProgram, "clearPboTex3d", &err);
    CHECK_ERR(err == CL_SUCCESS);

    return true;
}

bool FluidSimulation::setFixedKernelArguments() {
    cl_int err;

    //---------------------------------------
    // KERNEL COMPUTE DENSITY AND PRESSURE
    //---------------------------------------
    // neighborLookupBuffer:
    err = kernelComputeDensityAndPressure.setArg(0, neighborLookupBuffer.buffer);
    CHECK_ERR(err == CL_SUCCESS);

    // interleaveLookupBuffer:
    err = kernelComputeDensityAndPressure.setArg(1, interleaveLookupBuffer.buffer);
    CHECK_ERR(err == CL_SUCCESS);

    // blockCoordsLookup:
    err = kernelComputeDensityAndPressure.setArg(2, blockCoordsLookupBuffer.buffer);
    CHECK_ERR(err == CL_SUCCESS);

    // localNeighborPositionBuffer:
    err = kernelComputeDensityAndPressure.setArg(9, sizeof (cl_float4) * 32, NULL);
    CHECK_ERR(err == CL_SUCCESS);

    //localNeighborBlockBuffer:
    err = kernelComputeDensityAndPressure.setArg(10, sizeof (cl_uint2) * 32, NULL);
    CHECK_ERR(err == CL_SUCCESS);

    //    // computationData
    //    err = kernelComputeDensityAndPressure.setArg(11, sizeof (DensityComputationData), (void*) (&densityComputationData));
    //    CHECK_ERR(err == CL_SUCCESS);

    //---------------------------------------
    // KERNEL COMPUTE FORCES AND INTEGRATE
    //---------------------------------------
    // neighborLookupBuffer:
    err = kernelComputeForcesAndIntegrate.setArg(0, neighborLookupBuffer.buffer);
    CHECK_ERR(err == CL_SUCCESS);

    // interleaveLookupBuffer:
    err = kernelComputeForcesAndIntegrate.setArg(1, interleaveLookupBuffer.buffer);
    CHECK_ERR(err == CL_SUCCESS);

    // blockCoordsLookup:
    err = kernelComputeForcesAndIntegrate.setArg(2, blockCoordsLookupBuffer.buffer);
    CHECK_ERR(err == CL_SUCCESS);

    // colorBuffer:
    err = kernelComputeForcesAndIntegrate.setArg(10, colorBuffer.buffer);
    assert(err == CL_SUCCESS);

    // localNeighborPositionBuffer:
    err = kernelComputeForcesAndIntegrate.setArg(11, sizeof (cl_float4) * 32, NULL);
    CHECK_ERR(err == CL_SUCCESS);

    // localNeighborVelocityBuffer:
    err = kernelComputeForcesAndIntegrate.setArg(12, sizeof (cl_float4) * 32, NULL);
    CHECK_ERR(err == CL_SUCCESS);

    // localNeighborDensityBuffer:
    err = kernelComputeForcesAndIntegrate.setArg(13, sizeof (cl_float2) * 32, NULL);
    CHECK_ERR(err == CL_SUCCESS);

    //localNeighborBlockBuffer:
    err = kernelComputeForcesAndIntegrate.setArg(14, sizeof (cl_uint2) * 32, NULL);
    CHECK_ERR(err == CL_SUCCESS);

    //    // computationData
    //    err = kernelComputeForcesAndIntegrate.setArg(15, sizeof (ForceComputationData), (void*) (&forceComputationData));
    //    CHECK_ERR(err == CL_SUCCESS);

    //---------------------------------------
    // KERNEL COMPUTE FORCES AND INTEGRATE EXTACT
    //---------------------------------------
    // neighborLookupBuffer:
    err = kernelComputeForcesAndIntegrateExtract.setArg(0, neighborLookupBuffer.buffer);
    CHECK_ERR(err == CL_SUCCESS);

    // interleaveLookupBuffer:
    err = kernelComputeForcesAndIntegrateExtract.setArg(1, interleaveLookupBuffer.buffer);
    CHECK_ERR(err == CL_SUCCESS);

    // blockCoordsLookup:
    err = kernelComputeForcesAndIntegrateExtract.setArg(2, blockCoordsLookupBuffer.buffer);
    CHECK_ERR(err == CL_SUCCESS);

    // colorBuffer:
    err = kernelComputeForcesAndIntegrateExtract.setArg(10, colorBuffer.buffer);
    assert(err == CL_SUCCESS);

    // gpu2cpuBuffer:
    err = kernelComputeForcesAndIntegrateExtract.setArg(11, gpu2cpuDataBuffer.buffer);
    assert(err == CL_SUCCESS);

    // gpu2cpuBuffer:
    err = kernelComputeForcesAndIntegrateExtract.setArg(12, surfacePositionBufferGL.buffer);
    assert(err == CL_SUCCESS);

    // localNeighborPositionBuffer:
    err = kernelComputeForcesAndIntegrateExtract.setArg(13, sizeof (cl_float4) * 32, NULL);
    CHECK_ERR(err == CL_SUCCESS);

    // localNeighborVelocityBuffer:
    err = kernelComputeForcesAndIntegrateExtract.setArg(14, sizeof (cl_float4) * 32, NULL);
    CHECK_ERR(err == CL_SUCCESS);

    // localNeighborDensityBuffer:
    err = kernelComputeForcesAndIntegrateExtract.setArg(15, sizeof (cl_float2) * 32, NULL);
    CHECK_ERR(err == CL_SUCCESS);

    //localNeighborBlockBuffer:
    err = kernelComputeForcesAndIntegrateExtract.setArg(16, sizeof (cl_uint2) * 32, NULL);
    CHECK_ERR(err == CL_SUCCESS);

    // computationData
    //    err = kernelComputeForcesAndIntegrateExtract.setArg(17, sizeof (ForceComputationDataExtract),
    //            (void*) (&forceComputationDataExtract));
    //    CHECK_ERR(err == CL_SUCCESS);

    //---------------------------------------
    // KERNEL COMPUTE FORCES AND INTEGRATE COLOR FIELD
    //---------------------------------------
    // neighborLookupBuffer:
    err = kernelComputeForcesAndIntegrateColorField.setArg(0, neighborLookupBuffer.buffer);
    CHECK_ERR(err == CL_SUCCESS);

    // interleaveLookupBuffer:
    err = kernelComputeForcesAndIntegrateColorField.setArg(1, interleaveLookupBuffer.buffer);
    CHECK_ERR(err == CL_SUCCESS);

    // blockCoordsLookup:
    err = kernelComputeForcesAndIntegrateColorField.setArg(2, blockCoordsLookupBuffer.buffer);
    CHECK_ERR(err == CL_SUCCESS);

    // colorBuffer:
    err = kernelComputeForcesAndIntegrateColorField.setArg(10, colorBuffer.buffer);
    assert(err == CL_SUCCESS);

    // gpu2cpuBuffer:
    err = kernelComputeForcesAndIntegrateColorField.setArg(11, gpu2cpuDataBuffer.buffer);
    assert(err == CL_SUCCESS);

    // gpu2cpuBuffer:
    err = kernelComputeForcesAndIntegrateColorField.setArg(12, surfacePositionBufferGL.buffer);
    assert(err == CL_SUCCESS);

    // localNeighborPositionBuffer:
    err = kernelComputeForcesAndIntegrateColorField.setArg(13, sizeof (cl_float4) * 32, NULL);
    CHECK_ERR(err == CL_SUCCESS);

    // localNeighborVelocityBuffer:
    err = kernelComputeForcesAndIntegrateColorField.setArg(14, sizeof (cl_float4) * 32, NULL);
    CHECK_ERR(err == CL_SUCCESS);

    // localNeighborDensityBuffer:
    err = kernelComputeForcesAndIntegrateColorField.setArg(15, sizeof (cl_float2) * 32, NULL);
    CHECK_ERR(err == CL_SUCCESS);

    //localNeighborBlockBuffer:
    err = kernelComputeForcesAndIntegrateColorField.setArg(16, sizeof (cl_uint2) * 32, NULL);
    CHECK_ERR(err == CL_SUCCESS);

    // computationData
    //    err = kernelComputeForcesAndIntegrateColorField.setArg(17, sizeof (ForceComputationDataColorField),
    //            (void*) (&forceComputationDataExtract));
    //    CHECK_ERR(err == CL_SUCCESS);

    //---------------------------------------
    // KERNEL COMPUTE FORCES AND INTEGRATE DIST FIELD
    //---------------------------------------
    // neighborLookupBuffer:
    err = kernelComputeForcesAndIntegrateDistField.setArg(0, neighborLookupBuffer.buffer);
    CHECK_ERR(err == CL_SUCCESS);

    // interleaveLookupBuffer:
    err = kernelComputeForcesAndIntegrateDistField.setArg(1, interleaveLookupBuffer.buffer);
    CHECK_ERR(err == CL_SUCCESS);

    // blockCoordsLookup:
    err = kernelComputeForcesAndIntegrateDistField.setArg(2, blockCoordsLookupBuffer.buffer);
    CHECK_ERR(err == CL_SUCCESS);

    // pboTex3dBuffer:
    err = kernelComputeForcesAndIntegrateDistField.setArg(10, pboTex3dBuffer.buffer);
    assert(err == CL_SUCCESS);

    // localNeighborPositionBuffer:
    err = kernelComputeForcesAndIntegrateDistField.setArg(11, sizeof (cl_float4) * 32, NULL);
    CHECK_ERR(err == CL_SUCCESS);

    // localNeighborVelocityBuffer:
    err = kernelComputeForcesAndIntegrateDistField.setArg(12, sizeof (cl_float4) * 32, NULL);
    CHECK_ERR(err == CL_SUCCESS);

    // localNeighborDensityBuffer:
    err = kernelComputeForcesAndIntegrateDistField.setArg(13, sizeof (cl_float2) * 32, NULL);
    CHECK_ERR(err == CL_SUCCESS);

    //localNeighborBlockBuffer:
    err = kernelComputeForcesAndIntegrateDistField.setArg(14, sizeof (cl_uint2) * 32, NULL);
    CHECK_ERR(err == CL_SUCCESS);

    // computationData
    //    err = kernelComputeForcesAndIntegrateDistField.setArg(15, sizeof (ForceComputationDataDistField),
    //            (void*) (&forceComputationDataExtract));
    //    CHECK_ERR(err == CL_SUCCESS);


    //---------------------------------------
    // KERNEL CLEAR PBO TEX 3D
    //---------------------------------------
    kernelClearPboTex3d.setArg(0, pboTex3dBuffer.buffer);
    CHECK_ERR(err == CL_SUCCESS);
    
    return true;
}

void FluidSimulation::createFloorPositions() {
    const float particleOffset = fluidParameters->getParticleRestDistance() * fluidParameters->getSimulationScaleInv() * 1.2f;

    cl_uint particleCounter = 0;

    for (float y = 0; y < fluidParameters->getYmax(); y += particleOffset) {
        for (float x = 0; x < fluidParameters->getXmax(); x += particleOffset) {
            for (float z = 0; z < fluidParameters->getZmax(); z += particleOffset) {
                positionArray[particleCounter * 4] = x;
                positionArray[particleCounter * 4 + 1] = y;
                positionArray[particleCounter * 4 + 2] = z;
                positionArray[particleCounter * 4 + 3] = 1;
                particleCounter++;
                if (particleCounter == fluidParameters->getParticleCount()) {
                    goto end;
                }
            }
        }
    }
end:
    return;
}

void FluidSimulation::createCenterPositions() {
    //    const float particleOffset = 3.42;
    const float factor =  1.2f;
    const float particleOffset = fluidParameters->getParticleRestDistance() * fluidParameters->getSimulationScaleInv() * factor;

    const cl_uint side = (cl_uint) ceil(pow(fluidParameters->getParticleCount(), 1.0 / 3.0));
    assert(side * particleOffset < GRID_DIMENSION_X);

    float particlesPerBlockLenght = (float) fluidParameters->getBlockSize() / particleOffset;
    float particlesPerBlock = particlesPerBlockLenght * particlesPerBlockLenght * particlesPerBlockLenght;
    cout << "PARTICLES PER BLOCK: " << particlesPerBlock << endl;

    // cl_uint level = 1;
    //    cl_uint levelY = 1;
    //    cl_uint levelZ = 1;
    cl_uint particleCounter = 0;

    float cubeSideLen = fluidParameters->getTotalLength() * fluidParameters->getSimulationScaleInv() * factor;
    float centerOffset = ((float) fluidParameters->getGridDimensionX() - cubeSideLen) / 2.0f;

    for (cl_uint i = 0; i < side && particleCounter < fluidParameters->getParticleCount(); i++) {
        for (cl_uint j = 0; j < side && particleCounter < fluidParameters->getParticleCount(); j++) {
            for (cl_uint k = 0; k < side && particleCounter < fluidParameters->getParticleCount(); k++) {
                positionArray[particleCounter * 4] = i * particleOffset + centerOffset;
                positionArray[particleCounter * 4 + 1] = j * particleOffset;
                positionArray[particleCounter * 4 + 2] = k * particleOffset + centerOffset;
                positionArray[particleCounter * 4 + 3] = 1;
                particleCounter++;
            }
        }
    }
}

void FluidSimulation::createCubePositions() {
    //    const float particleOffset = 3.42;
    const float particleOffset = fluidParameters->getParticleRestDistance() * fluidParameters->getSimulationScaleInv() * 1.2f;

    const cl_uint side = (cl_uint) ceil(pow(fluidParameters->getParticleCount(), 1.0 / 3.0));
    assert(side * particleOffset < GRID_DIMENSION_X);

    float particlesPerBlockLenght = (float) fluidParameters->getBlockSize() / particleOffset;
    float particlesPerBlock = particlesPerBlockLenght * particlesPerBlockLenght * particlesPerBlockLenght;
    cout << "PARTICLES PER BLOCK: " << particlesPerBlock << endl;

    // cl_uint level = 1;
    //    cl_uint levelY = 1;
    //    cl_uint levelZ = 1;
    cl_uint particleCounter = 0;

    for (cl_uint i = 0; i < side && particleCounter < fluidParameters->getParticleCount(); i++) {
        for (cl_uint j = 0; j < side && particleCounter < fluidParameters->getParticleCount(); j++) {
            for (cl_uint k = 0; k < side && particleCounter < fluidParameters->getParticleCount(); k++) {
                positionArray[particleCounter * 4] = i * particleOffset;
                positionArray[particleCounter * 4 + 1] = j * particleOffset;
                positionArray[particleCounter * 4 + 2] = k * particleOffset;
                positionArray[particleCounter * 4 + 3] = 1;
                particleCounter++;
            }
        }
    }

}

void FluidSimulation::createWallPoints() {
    const float particleOffset = fluidParameters->getParticleRestDistance() * fluidParameters->getSimulationScaleInv() * 1.2f;

    cl_uint particleCounter = 0;
    float offset = 1.5f;

    for (float x = offset; x < (fluidParameters->getXmax() - 2.0f * offset); x += particleOffset) {
        for (float y = offset; y < (fluidParameters->getYmax() - 2.0f * offset) * 0.7; y += particleOffset) {
            for (float z = offset; z < (fluidParameters->getZmax() - 2.0f * offset); z += particleOffset) {
                positionArray[particleCounter * 4] = x;
                positionArray[particleCounter * 4 + 1] = y;
                positionArray[particleCounter * 4 + 2] = z;
                positionArray[particleCounter * 4 + 3] = 1;
                particleCounter++;
                if (particleCounter == fluidParameters->getParticleCount()) {
                    goto end;
                }
            }
        }
    }
end:
    return;
}

void FluidSimulation::createRandomPositions() {
    for (int i = 0; i < fluidParameters->getParticleCount(); ++i) {
        float x, y, z;
        float r;
        r = ((float) rand() / (float) RAND_MAX);

        x = SCALE(XMIN, (XMAX / 10), r);
        r = ((float) rand() / (float) RAND_MAX);
        y = SCALE(YMIN, (YMAX), r);
        r = ((float) rand() / (float) RAND_MAX);
        z = SCALE(ZMIN, ZMAX, r);

        if (x == XMAX) {
            x -= 0.001f;
        }

        if (y == YMAX) {
            y -= 0.001f;
        }

        if (z == ZMAX) {
            z -= 0.001f;
        }

        float * positionVector = positionArray + 4 * i;
        positionVector[0] = x;
        positionVector[1] = y;
        positionVector[2] = z;
        positionVector[3] = 1;
    }
}

bool FluidSimulation::createInitialPositionAndVelocity(InitialParticlePlacement placement) {
    positionArray = new float[ 4 * fluidParameters->getParticleCount() ];
    velocityArray = new float[ 4 * fluidParameters->getParticleCount() ];

    switch (placement) {
        case FLOOR:
            createFloorPositions();
            break;
        case RANDOM:
            createRandomPositions();
            break;
        case CUBE:
            createCubePositions();
            break;
        case WALL:
            createWallPoints();
            break;
        case CENTER:
            createCenterPositions();
            break;
    }


    for (int i = 0; i < fluidParameters->getParticleCount(); ++i) {
        float r;
        r = ((float) rand() / (float) RAND_MAX);

        float * velocityVector = velocityArray + 4 * i;

#ifdef  USE_ZERO_INITIAL_VELOCITY
        velocityVector[0] = 0;
        velocityVector[1] = 0;
        velocityVector[2] = 0;
        velocityVector[3] = 1;
#else

        r = ((float) rand() / (float) RAND_MAX);
        velocityVector[ 0 ] = SCALE(-1.0f, 1.0f, r);
        r = ((float) rand() / (float) RAND_MAX);
        velocityVector[ 1 ] = SCALE(-1.0f, 1.0f, r);
        r = ((float) rand() / (float) RAND_MAX);
        velocityVector[ 2 ] = SCALE(-1.0f, 1.0f, r);
        velocityVector[ 3 ] = 1;
#endif
    }

    if (!positionPPBuffer.writeFirst((void*) positionArray)) {
        return false;
    }

    if (!velocityPPBuffer.writeFirst((void*) velocityArray)) {
        return false;
    }

    return true;
}

cl_uint FluidSimulation::spreadBits(cl_uint x) {
    x = (x | (x << 16)) & 0x030000FF;
    x = (x | (x << 8)) & 0x0300F00F;
    x = (x | (x << 4)) & 0x030C30C3;
    x = (x | (x << 2)) & 0x09249249;
    return x;
}

cl_uint FluidSimulation::compressBits(cl_uint x) {
    x = x & 0x09249249;
    x = (x | (x >> 2)) & 0x030C30C3;
    x = (x | (x >> 4)) & 0x0300F00F;
    x = (x | (x >> 8)) & 0x030000FF;
    x = (x | (x >> 16)) & 0x0000FFFF;

    return x;
}

void FluidSimulation::restoreInterleave(cl_uint value, cl_uint* x, cl_uint* y, cl_uint* z) {
    *x = compressBits(value);
    *y = compressBits(value >> 1);
    *z = compressBits(value >> 2);
}

bool FluidSimulation::createBlockCoordsLookupTable() {
    //cl_uint 
    cl_uint lookup[fluidParameters->getBlockCoordsLookupLength() * BLOCK_LOOKUP_CELL_SIZE];
    for (cl_uint i = 0; i < fluidParameters->getBlockCoordsLookupLength(); i++) {
        cl_uint val = i * fluidParameters->getBlockSize3D();
        cl_uint x, y, z;
        restoreInterleave(val, &x, &y, &z);
        lookup[(i * BLOCK_LOOKUP_CELL_SIZE)] = x;
        lookup[(i * BLOCK_LOOKUP_CELL_SIZE) + 1] = y;
        lookup[(i * BLOCK_LOOKUP_CELL_SIZE) + 2] = z;

        assert((spreadBits(x) | (spreadBits(y) << 1) | (spreadBits(z) << 2)) == val);
    }

    if (!blockCoordsLookupBuffer.write((void*) lookup)) {
        return false;
    }

    return true;
}

bool FluidSimulation::createInterleaveLookupTable() {
    cl_uint lookup[fluidParameters->getInterleaveLookupLength() * INTERLEAVE_CELL_SIZE];

    for (cl_uint i = 0; i < fluidParameters->getInterleaveLookupLength(); i++) {
        INTERLEAVE_ZERO(lookup, i) = spreadBits(i);
        INTERLEAVE_ONE(lookup, i) = spreadBits(i) << 1;
        INTERLEAVE_TWO(lookup, i) = spreadBits(i) << 2;
    }

    if (!interleaveLookupBuffer.write((void*) lookup)) {
        return false;
    }

    return true;
}

bool FluidSimulation::createNeighborLookupTable() {
    cl_int* lookup = (cl_int*) calloc(4 * 32, sizeof (cl_int));

    int index = 0;
    for (int x = 0; x < 3; x++) {
        for (int y = 0; y < 3; y++) {
            for (int z = 0; z < 3; z++) {
                lookup[index * 4] = (x - 1) * BLOCK_SIZE;
                lookup[index * 4 + 1] = (y - 1) * BLOCK_SIZE;
                lookup[index * 4 + 2] = (z - 1) * BLOCK_SIZE;

                index++;
            }
        }
    }

    if (!neighborLookupBuffer.write((void*) lookup)) {
        free(lookup);
        return false;
    }

    free(lookup);
    return true;
}

void FluidSimulation::runHashParticles() {
    kernelHashParticles.setArg(0, interleaveLookupBuffer.buffer);
    kernelHashParticles.setArg(1, positionPPBuffer.getFirst().buffer);
    kernelHashParticles.setArg(2, radixSortPPBuffer.getFirst().buffer);

    clCommandQueue.enqueueNDRangeKernel(
            kernelHashParticles, cl::NullRange, cl::NDRange(fluidParameters->getParticleCount()),
            cl::NDRange((int) (MY_WORK_GROUP_SIZE)), NULL, NULL);
}

void FluidSimulation::runSort() {
    radixSort->sort();
}

void FluidSimulation::runReorderPositionAndVelocity() {
    kernelReorderPositionAndVelocity.setArg(0, radixSortPPBuffer.getFirst().buffer);

    kernelReorderPositionAndVelocity.setArg(1, positionPPBuffer.getFirst().buffer);
    kernelReorderPositionAndVelocity.setArg(2, velocityPPBuffer.getFirst().buffer);

    kernelReorderPositionAndVelocity.setArg(3, positionPPBuffer.getSecond().buffer);
    kernelReorderPositionAndVelocity.setArg(4, velocityPPBuffer.getSecond().buffer);

    clCommandQueue.enqueueNDRangeKernel(
            kernelReorderPositionAndVelocity, cl::NullRange, cl::NDRange(fluidParameters->getParticleCount()),
            cl::NDRange((int) (MY_WORK_GROUP_SIZE)), NULL, NULL);
    clCommandQueue.finish();

    positionPPBuffer.swap();
    velocityPPBuffer.swap();
}

void FluidSimulation::runClearGpu2CpuBuffer() {
    kernelClearBuffer.setArg(0, gpu2cpuDataBuffer.buffer);


    clCommandQueue.enqueueNDRangeKernel(
            kernelClearBuffer, cl::NullRange, cl::NDRange(2),
            cl::NDRange(2), NULL, NULL);
}

void FluidSimulation::runClearSecondBuffer() {
    kernelClearBuffer.setArg(0, radixSortPPBuffer.getSecond().buffer);

    clCommandQueue.enqueueNDRangeKernel(
            kernelClearBuffer, cl::NullRange, cl::NDRange(radixSortPPBuffer.getSize() / (4 * sizeof (cl_uint))),
            cl::NDRange((int) (MY_WORK_GROUP_SIZE)), NULL, NULL);
}

void FluidSimulation::runCreateBlockList() {
    kernelCreateBlockList.setArg(0, radixSortPPBuffer.getFirst().buffer);
    kernelCreateBlockList.setArg(1, radixSortPPBuffer.getSecond().buffer);

    clCommandQueue.enqueueNDRangeKernel(
            kernelCreateBlockList, cl::NullRange, cl::NDRange(fluidParameters->getParticleCount()),
            cl::NDRange((int) (MY_WORK_GROUP_SIZE)), NULL, NULL);
}

void FluidSimulation::runCreateCompactBlockList() {
    kernelCreateCompactBlockList.setArg(0, radixSortPPBuffer.getSecond().buffer);
    kernelCreateCompactBlockList.setArg(1, radixSortPPBuffer.getFirst().buffer);
    kernelCreateCompactBlockList.setArg(2, gpu2cpuDataBuffer.buffer);

    clCommandQueue.enqueueNDRangeKernel(
            kernelCreateCompactBlockList, cl::NullRange, cl::NDRange(fluidParameters->getBlockCount()),
            cl::NDRange((int) (MY_WORK_GROUP_SIZE)), NULL, NULL);
}

void FluidSimulation::runReadUsedWorkGroups() {
    bool succ = gpu2cpuDataBuffer.read(gpu2cpuArray);
    assert(succ);

    cl_uint compactBlocks = gpu2cpuArray[0];
    cl_uint workGroups = compactBlocks;
    usedWorkItems = workGroups * MAXIMUM_PARTICLES_PER_BLOCK;
}

void FluidSimulation::runReadPosition() {
    bool succ = positionPPBuffer.readFirst(positionArray);
    assert(succ);

}

void FluidSimulation::runComputeDensityAndPressure() {
    cl_int err;

    // positionBuffer:
    err = kernelComputeDensityAndPressure.setArg(3, positionPPBuffer.getFirst().buffer);
    assert(err == CL_SUCCESS);

    // blockBuffer:
    err = kernelComputeDensityAndPressure.setArg(4, radixSortPPBuffer.getSecond().buffer);
    assert(err == CL_SUCCESS);

    // compactBlockBuffer:
    err = kernelComputeDensityAndPressure.setArg(5, radixSortPPBuffer.getFirst().buffer);
    assert(err == CL_SUCCESS);

    // densityBuffer:
    err = kernelComputeDensityAndPressure.setArg(6, densityBuffer.buffer);
    assert(err == CL_SUCCESS);

    // oldVelocityBuffer:
    err = kernelComputeDensityAndPressure.setArg(7, velocityPPBuffer.getFirst().buffer);
    assert(err == CL_SUCCESS);

    // newVelocityBuffer:
    err = kernelComputeDensityAndPressure.setArg(8, velocityPPBuffer.getSecond().buffer);
    assert(err == CL_SUCCESS);

    // computationData
    err = kernelComputeDensityAndPressure.setArg(11, sizeof (DensityComputationData), (void*) (&densityComputationData));
    assert(err == CL_SUCCESS);

    clCommandQueue.enqueueNDRangeKernel(
            kernelComputeDensityAndPressure, cl::NullRange, cl::NDRange(usedWorkItems),
            cl::NDRange((int) (MAXIMUM_PARTICLES_PER_BLOCK)), NULL, NULL);

    velocityPPBuffer.swap();
}

void FluidSimulation::runComputeForcesAndIntegrate() {
#ifdef USE_COLORING
    colorBuffer.acquireGLObject();
#endif

    cl_int err;

    // oldPositionBuffer:
    err = kernelComputeForcesAndIntegrate.setArg(3, positionPPBuffer.getFirst().buffer);
    assert(err == CL_SUCCESS);

    // oldVelocityBuffer:
    err = kernelComputeForcesAndIntegrate.setArg(4, velocityPPBuffer.getFirst().buffer);
    assert(err == CL_SUCCESS);

    // blockBuffer:
    err = kernelComputeForcesAndIntegrate.setArg(5, radixSortPPBuffer.getSecond().buffer);
    assert(err == CL_SUCCESS);

    // compactBlockBuffer:
    err = kernelComputeForcesAndIntegrate.setArg(6, radixSortPPBuffer.getFirst().buffer);
    assert(err == CL_SUCCESS);

    // densityBuffer:
    err = kernelComputeForcesAndIntegrate.setArg(7, densityBuffer.buffer);
    assert(err == CL_SUCCESS);

    // newPositionBuffer:
    err = kernelComputeForcesAndIntegrate.setArg(8, positionPPBuffer.getSecond().buffer);
    assert(err == CL_SUCCESS);

    // newVelocityBuffer:
    err = kernelComputeForcesAndIntegrate.setArg(9, velocityPPBuffer.getSecond().buffer);
    assert(err == CL_SUCCESS);

    // computationData
    err = kernelComputeForcesAndIntegrate.setArg(15, sizeof (ForceComputationData), (void*) (&forceComputationData));
    assert(err == CL_SUCCESS);

    clCommandQueue.enqueueNDRangeKernel(
            kernelComputeForcesAndIntegrate, cl::NullRange, cl::NDRange(usedWorkItems),
            cl::NDRange((int) (MAXIMUM_PARTICLES_PER_BLOCK)), NULL, NULL);

    velocityPPBuffer.swap();
    positionPPBuffer.swap();

#ifdef USE_LEAP_FROG
    if (initLeapFrog) {
        //        cl::Event evt;
        //        err = clCommandQueue.enqueueCopyBuffer(velocityPPBuffer.getFirst().buffer,
        //                velocityPPBuffer.getSecond().buffer, 0, 0, velocityPPBuffer.getSize(), NULL, &evt);
        //        assert(err == CL_SUCCESS);
        //        evt.wait();
        velocityPPBuffer.getFirst().copyTo(velocityPPBuffer.getSecond());
        initLeapFrog = false;
    }
#endif

#ifdef USE_COLORING
    colorBuffer.releaseGLObject();
#endif
}

void FluidSimulation::runCopyPositionBuffer() {
    cl_int err;
    positionBufferGL.acquireGLObject();

    //    err = clCommandQueue.enqueueCopyBuffer(positionPPBuffer.getFirst().buffer, positionBufferGL.buffer, 0, 0, positionBufferGL.getSize(), NULL, NULL);
    err = positionPPBuffer.getFirst().copyTo(positionBufferGL);
    assert(err == CL_SUCCESS);

    clCommandQueue.finish();
    positionBufferGL.releaseGLObject();
}

void FluidSimulation::runReadSurfaceParticlesCount() {
    bool succ = gpu2cpuDataBuffer.read(gpu2cpuArray);
    assert(succ);

    surfaceParticles = gpu2cpuArray[0];
}

void FluidSimulation::runComputeForcesAndIntegrateExtract() {
#ifdef USE_COLORING
    colorBuffer.acquireGLObject();
#endif

    cl_int err;

    // oldPositionBuffer:
    err = kernelComputeForcesAndIntegrateExtract.setArg(3, positionPPBuffer.getFirst().buffer);
    assert(err == CL_SUCCESS);

    // oldVelocityBuffer:
    err = kernelComputeForcesAndIntegrateExtract.setArg(4, velocityPPBuffer.getFirst().buffer);
    assert(err == CL_SUCCESS);

    // blockBuffer:
    err = kernelComputeForcesAndIntegrateExtract.setArg(5, radixSortPPBuffer.getSecond().buffer);
    assert(err == CL_SUCCESS);

    // compactBlockBuffer:
    err = kernelComputeForcesAndIntegrateExtract.setArg(6, radixSortPPBuffer.getFirst().buffer);
    assert(err == CL_SUCCESS);

    // densityBuffer:
    err = kernelComputeForcesAndIntegrateExtract.setArg(7, densityBuffer.buffer);
    assert(err == CL_SUCCESS);

    // newPositionBuffer:
    err = kernelComputeForcesAndIntegrateExtract.setArg(8, positionPPBuffer.getSecond().buffer);
    assert(err == CL_SUCCESS);

    // newVelocityBuffer:
    err = kernelComputeForcesAndIntegrateExtract.setArg(9, velocityPPBuffer.getSecond().buffer);
    assert(err == CL_SUCCESS);

    // computationData:
    err = kernelComputeForcesAndIntegrateExtract.setArg(17, sizeof (ForceComputationDataExtract),
            (void*) (&forceComputationDataExtract));
    assert(err == CL_SUCCESS);

    clCommandQueue.enqueueNDRangeKernel(
            kernelComputeForcesAndIntegrateExtract, cl::NullRange, cl::NDRange(usedWorkItems),
            cl::NDRange((int) (MAXIMUM_PARTICLES_PER_BLOCK)), NULL, NULL);

    velocityPPBuffer.swap();
    positionPPBuffer.swap();

#ifdef USE_LEAP_FROG
    if (initLeapFrog) {
        //        cl::Event evt;
        //        err = clCommandQueue.enqueueCopyBuffer(velocityPPBuffer.getFirst().buffer,
        //                velocityPPBuffer.getSecond().buffer, 0, 0, velocityPPBuffer.getSize(), NULL, &evt);
        //        assert(err == CL_SUCCESS);
        //        evt.wait();
        //        initLeapFrog = false;
        velocityPPBuffer.getFirst().copyTo(velocityPPBuffer.getSecond());
        initLeapFrog = false;
    }
#endif

#ifdef USE_COLORING
    colorBuffer.releaseGLObject();
#endif
}

void FluidSimulation::runComputeForcesAndIntegrateColorField() {
#ifdef USE_COLORING
    colorBuffer.acquireGLObject();
#endif

    cl_int err;

    // oldPositionBuffer:
    err = kernelComputeForcesAndIntegrateColorField.setArg(3, positionPPBuffer.getFirst().buffer);
    assert(err == CL_SUCCESS);

    // oldVelocityBuffer:
    err = kernelComputeForcesAndIntegrateColorField.setArg(4, velocityPPBuffer.getFirst().buffer);
    assert(err == CL_SUCCESS);

    // blockBuffer:
    err = kernelComputeForcesAndIntegrateColorField.setArg(5, radixSortPPBuffer.getSecond().buffer);
    assert(err == CL_SUCCESS);

    // compactBlockBuffer:
    err = kernelComputeForcesAndIntegrateColorField.setArg(6, radixSortPPBuffer.getFirst().buffer);
    assert(err == CL_SUCCESS);

    // densityBuffer:
    err = kernelComputeForcesAndIntegrateColorField.setArg(7, densityBuffer.buffer);
    assert(err == CL_SUCCESS);

    // newPositionBuffer:
    err = kernelComputeForcesAndIntegrateColorField.setArg(8, positionPPBuffer.getSecond().buffer);
    assert(err == CL_SUCCESS);

    // newVelocityBuffer:
    err = kernelComputeForcesAndIntegrateColorField.setArg(9, velocityPPBuffer.getSecond().buffer);
    assert(err == CL_SUCCESS);

    // computationData:
    err = kernelComputeForcesAndIntegrateColorField.setArg(17, sizeof (ForceComputationDataColorField),
            (void*) (&forceComputationDataColorField));
    assert(err == CL_SUCCESS);

    clCommandQueue.enqueueNDRangeKernel(
            kernelComputeForcesAndIntegrateColorField, cl::NullRange, cl::NDRange(usedWorkItems),
            cl::NDRange((int) (MAXIMUM_PARTICLES_PER_BLOCK)), NULL, NULL);

    velocityPPBuffer.swap();
    positionPPBuffer.swap();

#ifdef USE_LEAP_FROG
    if (initLeapFrog) {
        velocityPPBuffer.getFirst().copyTo(velocityPPBuffer.getSecond());
        initLeapFrog = false;
    }
#endif

#ifdef USE_COLORING
    colorBuffer.releaseGLObject();
#endif
}

void FluidSimulation::runClearPboTex3d() {
    //pboTex3dBuffer.acquireGLObject();

    clCommandQueue.enqueueNDRangeKernel(
            kernelClearPboTex3d,
            cl::NullRange,
            cl::NDRange(clearPboTex3dGlobalSize),
            cl::NDRange(MY_WORK_GROUP_SIZE),
            NULL,
            NULL);

    //  pboTex3dBuffer.releaseGLObject();
}

void FluidSimulation::runComputeForcesAndIntegrateDistField() {
    cl_int err;

    // oldPositionBuffer:
    err = kernelComputeForcesAndIntegrateDistField.setArg(3, positionPPBuffer.getFirst().buffer);
    assert(err == CL_SUCCESS);

    // oldVelocityBuffer:
    err = kernelComputeForcesAndIntegrateDistField.setArg(4, velocityPPBuffer.getFirst().buffer);
    assert(err == CL_SUCCESS);

    // blockBuffer:
    err = kernelComputeForcesAndIntegrateDistField.setArg(5, radixSortPPBuffer.getSecond().buffer);
    assert(err == CL_SUCCESS);

    // compactBlockBuffer:
    err = kernelComputeForcesAndIntegrateDistField.setArg(6, radixSortPPBuffer.getFirst().buffer);
    assert(err == CL_SUCCESS);

    // densityBuffer:
    err = kernelComputeForcesAndIntegrateDistField.setArg(7, densityBuffer.buffer);
    assert(err == CL_SUCCESS);

    // newPositionBuffer:
    err = kernelComputeForcesAndIntegrateDistField.setArg(8, positionPPBuffer.getSecond().buffer);
    assert(err == CL_SUCCESS);

    // newVelocityBuffer:
    err = kernelComputeForcesAndIntegrateDistField.setArg(9, velocityPPBuffer.getSecond().buffer);
    assert(err == CL_SUCCESS);

    // computationData:
    err = kernelComputeForcesAndIntegrateDistField.setArg(15, sizeof (ForceComputationDataDistField),
            (void*) (&forceComputationDataDistField));
    assert(err == CL_SUCCESS);

    clCommandQueue.enqueueNDRangeKernel(
            kernelComputeForcesAndIntegrateDistField, cl::NullRange, cl::NDRange(usedWorkItems),
            cl::NDRange((int) (MAXIMUM_PARTICLES_PER_BLOCK)), NULL, NULL);

    velocityPPBuffer.swap();
    positionPPBuffer.swap();

#ifdef USE_LEAP_FROG
    if (initLeapFrog) {
        velocityPPBuffer.getFirst().copyTo(velocityPPBuffer.getSecond());
        initLeapFrog = false;
    }
#endif
}

void FluidSimulation::step() {

    runHashParticles();

    runSort();

    runReorderPositionAndVelocity();

    runClearSecondBuffer();

    runCreateBlockList();

    runClearGpu2CpuBuffer();

    runCreateCompactBlockList();

    runReadUsedWorkGroups();

    runComputeDensityAndPressure();

#ifdef USE_RAYCASTING
    //====================================
    pboTex3dBuffer.acquireGLObject();

    runClearPboTex3d();

    runComputeForcesAndIntegrateDistField();

    pboTex3dBuffer.releaseGLObject();
    //====================================
#elif defined(EXTRACT_SURFACE_CENTER_OF_MASS)
    //====================================
    surfacePositionBufferGL.acquireGLObject();
    //runClearGpu2CpuBuffer();
    runComputeForcesAndIntegrateExtract();
    // runReadSurfaceParticlesCount();
    clCommandQueue.finish();
    surfacePositionBufferGL.releaseGLObject();
    runCopyPositionBuffer();
    // cout << "surfaceParticles = " << surfaceParticles << endl;
    //=====================================
#elif defined(EXTRACT_SURFACE_COLOR_FIELD) 
    //====================================
    surfacePositionBufferGL.acquireGLObject();
    //runClearGpu2CpuBuffer();
    runComputeForcesAndIntegrateColorField();
    // runReadSurfaceParticlesCount();
    clCommandQueue.finish();
    surfacePositionBufferGL.releaseGLObject();
    runCopyPositionBuffer();
    // cout << "surfaceParticles = " << surfaceParticles << endl;
    //=====================================
#else
    //====================================
    runComputeForcesAndIntegrate();

    runCopyPositionBuffer();
    //=====================================
#endif
}



