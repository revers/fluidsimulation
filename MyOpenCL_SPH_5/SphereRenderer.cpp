/* 
 * File:   SphereRenderer.cpp
 * Author: Revers
 * 
 * Created on 18 grudzień 2011, 00:19
 */
// GLEW and GLUT includes
#include <GL/glew.h>
#include <CL/cl_gl.h>
#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/freeglut_ext.h>

#include "SphereRenderer.h"

#include "shaders.h"

//#define RENDER_TYPE PARTICLE_POINTS
#define RENDER_TYPE PARTICLE_SPHERES

bool SphereRenderer::init() {

    //==============================
    m_program = compileProgram(vertexShader, spherePixelShader);

#if !defined(__APPLE__) && !defined(MACOSX)
    glClampColor(GL_CLAMP_VERTEX_COLOR, GL_FALSE);
    glClampColor(GL_CLAMP_FRAGMENT_COLOR, GL_FALSE);
#endif
    if (!m_program) {
        return false;
    }
    //==============================

    compileBoundaryList();
    compileGridList();
    compileCenterList();

    return true;
}

GLuint SphereRenderer::compileProgram(const char *vsource, const char *fsource) {
    GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
    GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

    glShaderSource(vertexShader, 1, &vsource, 0);
    glShaderSource(fragmentShader, 1, &fsource, 0);

    glCompileShader(vertexShader);
    glCompileShader(fragmentShader);

    GLuint program = glCreateProgram();

    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);

    glLinkProgram(program);

    // check if program linked
    GLint success = 0;
    glGetProgramiv(program, GL_LINK_STATUS, &success);

    if (!success) {
        char temp[256];
        glGetProgramInfoLog(program, 256, 0, temp);
        printf("Failed to link program:\n%s\n", temp);
        glDeleteProgram(program);
        program = 0;
    }

    return program;
}

void SphereRenderer::drawPoints() {
    FluidSimulation* fluidSimulation = controlPanel->fluidSimulation;

#ifdef USE_RAYCASTING
    //==========================================================================
    glBindBuffer(GL_ARRAY_BUFFER, fluidSimulation->positionBufferGL.getVertexBufferObject());
    glVertexPointer(4, GL_FLOAT, 0, 0);
    glEnableClientState(GL_VERTEX_ARRAY);

    glDrawArrays(GL_POINTS, 0, fluidSimulation->getParticleCount());

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_COLOR_ARRAY);
    //==========================================================================
#elif (defined(EXTRACT_SURFACE_CENTER_OF_MASS) || defined(EXTRACT_SURFACE_COLOR_FIELD)) && !defined(SHOW_ALL_PARTICLES)
    //==========================================================================
    glBindBuffer(GL_ARRAY_BUFFER, fluidSimulation->surfacePositionBufferGL.getVertexBufferObject());
    glVertexPointer(4, GL_FLOAT, 0, 0);
    glEnableClientState(GL_VERTEX_ARRAY);

#ifdef USE_COLORING
    glBindBuffer(GL_ARRAY_BUFFER, fluidSimulation->colorBuffer.getVertexBufferObject());
    glColorPointer(4, GL_FLOAT, 0, 0);
    glEnableClientState(GL_COLOR_ARRAY);
#endif


    glDrawArrays(GL_POINTS, 0, fluidSimulation->getParticleCount());

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_COLOR_ARRAY);
    //==========================================================================
#else
    //==========================================================================
    glBindBuffer(GL_ARRAY_BUFFER, fluidSimulation->positionBufferGL.getVertexBufferObject());
    glVertexPointer(4, GL_FLOAT, 0, 0);
    glEnableClientState(GL_VERTEX_ARRAY);


#ifdef USE_COLORING
    glBindBuffer(GL_ARRAY_BUFFER, fluidSimulation->colorBuffer.getVertexBufferObject());
    glColorPointer(4, GL_FLOAT, 0, 0);
    glEnableClientState(GL_COLOR_ARRAY);
#endif

    glDrawArrays(GL_POINTS, 0, fluidSimulation->getParticleCount());

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_COLOR_ARRAY);
    //==========================================================================
#endif
}

void SphereRenderer::render() {
    camera->apply();

    glUseProgram(0);

    if (controlPanel->drawGrid) {
        glCallList(GRID_LIST);
    }
    if (controlPanel->drawBoundaries) {
        glCallList(BOUNDARY_LIST);
    }

    if (controlPanel->drawCenter) {
        glCallList(CENTER_LIST);
    }


    if (controlPanel->started) {
        glColor3f(0, 0, 1);

        //---------------
        display(RENDER_TYPE);
        //---------------

        glFinish();
    }
}

void SphereRenderer::display(DisplayMode mode /* = PARTICLE_POINTS */) {
    switch (mode) {
        case PARTICLE_POINTS:
            // glColor3f(1, 1, 1);
            glPointSize(m_pointSize);
            drawPoints();
            break;

        default:
        case PARTICLE_SPHERES:
            glEnable(GL_POINT_SPRITE);
            glTexEnvi(GL_POINT_SPRITE, GL_COORD_REPLACE, GL_TRUE);
            glEnable(GL_VERTEX_PROGRAM_POINT_SIZE_NV);
            glDepthMask(GL_TRUE);
            glEnable(GL_DEPTH_TEST);

            glUseProgram(m_program);
            glUniform1f(glGetUniformLocation(m_program, "pointScale"),
                    (float) windowHeight / tanf(m_fov * m_fHalfViewRadianFactor));
            glUniform1f(glGetUniformLocation(m_program, "pointRadius"),
                    controlPanel->fluidSimulation->getParticleRadiusScaled()); //m_particleRadius);
            //            glUniform3f(glGetUniformLocation(m_program, "positionShift"), positionOffset,
            //                    0, positionOffset);

            drawPoints();

            glUseProgram(0);
            glDisable(GL_POINT_SPRITE);
            break;
    }
}

void SphereRenderer::compileCenterList() {
    glNewList(CENTER_LIST, GL_COMPILE);

    float len = 10.0f;
    float centerX = (float) (XMIN + XMAX) / 2.0f;
    float centerY = (float) (YMIN + YMAX) / 2.0f;
    float centerZ = (float) (ZMIN + ZMAX) / 2.0f;


    glLineWidth(4);

    glBegin(GL_LINES);
    glColor3f(1, 0, 0);
    glVertex3f(centerX, centerY, centerZ);
    glVertex3f(centerX + len, centerY, centerZ);

    glColor3f(0, 1, 0);
    glVertex3f(centerX, centerY, centerZ);
    glVertex3f(centerX, centerY + len, centerZ);

    glColor3f(0, 0, 1);
    glVertex3f(centerX, centerY, centerZ);
    glVertex3f(centerX, centerY, centerZ + len);

    glEnd();

    glLineWidth(1);


    glEndList();
}

void SphereRenderer::compileGridList() {
    glNewList(GRID_LIST, GL_COMPILE);

    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    glColor3f(0.7, 0.7, 0.7);

    glBegin(GL_QUADS);
    {
        // Front wall:       
        glNormal3f(0.0f, 0.0f, -1.0f);
        for (unsigned int i = YMIN; i < YMAX; i += BLOCK_SIZE) {
            for (unsigned int j = XMIN; j < XMAX; j += BLOCK_SIZE) {
                glVertex3f(j, i + BLOCK_SIZE, ZMAX);
                glVertex3f(j + BLOCK_SIZE, i + BLOCK_SIZE, ZMAX);
                glVertex3f(j + BLOCK_SIZE, i, ZMAX);
                glVertex3f(j, i, ZMAX);
            }
        }

        // Back wall:
        glNormal3f(0.0f, 0.0f, 1.0f);
        for (unsigned int i = YMIN; i < YMAX; i += BLOCK_SIZE) {
            for (unsigned int j = XMIN; j < XMAX; j += BLOCK_SIZE) {
                glVertex3f(j + BLOCK_SIZE, i, ZMIN);
                glVertex3f(j + BLOCK_SIZE, i + BLOCK_SIZE, ZMIN);
                glVertex3f(j, i + BLOCK_SIZE, ZMIN);
                glVertex3f(j, i, ZMIN);
            }
        }

        // Top wall:
        glNormal3f(0.0f, -1.0f, 0.0f);
        for (unsigned int i = ZMIN; i < ZMAX; i += BLOCK_SIZE) {
            for (unsigned int j = XMIN; j < XMAX; j += BLOCK_SIZE) {
                glVertex3f(j + BLOCK_SIZE, YMAX, i);
                glVertex3f(j + BLOCK_SIZE, YMAX, i + BLOCK_SIZE);
                glVertex3f(j, YMAX, i + BLOCK_SIZE);
                glVertex3f(j, YMAX, i);
            }
        }

        // Bottom wall:
        glNormal3f(0.0f, 1.0f, 0.0f);
        for (unsigned int i = ZMIN; i < ZMAX; i += BLOCK_SIZE) {
            for (unsigned int j = XMIN; j < XMAX; j += BLOCK_SIZE) {
                glVertex3f(j, YMIN, i + BLOCK_SIZE);
                glVertex3f(j + BLOCK_SIZE, YMIN, i + BLOCK_SIZE);
                glVertex3f(j + BLOCK_SIZE, YMIN, i);
                glVertex3f(j, YMIN, i);
            }
        }

        // Right wall:
        glNormal3f(-1.0f, 0.0f, 0.0f);
        for (unsigned int i = ZMIN; i < ZMAX; i += BLOCK_SIZE) {
            for (unsigned int j = YMIN; j < YMAX; j += BLOCK_SIZE) {
                glVertex3f(XMAX, j, i + BLOCK_SIZE);
                glVertex3f(XMAX, j + BLOCK_SIZE, i + BLOCK_SIZE);
                glVertex3f(XMAX, j + BLOCK_SIZE, i);
                glVertex3f(XMAX, j, i);
            }
        }

        // Left wall: 
        glNormal3f(1.0f, 0.0f, 0.0f);
        for (unsigned int i = ZMIN; i < ZMAX; i += BLOCK_SIZE) {
            for (unsigned int j = YMIN; j < YMAX; j += BLOCK_SIZE) {
                glVertex3f(XMIN, j + BLOCK_SIZE, i);
                glVertex3f(XMIN, j + BLOCK_SIZE, i + BLOCK_SIZE);
                glVertex3f(XMIN, j, i + BLOCK_SIZE);
                glVertex3f(XMIN, j, i);
            }
        }
    }
    glEnd();

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    glEndList();
}

void SphereRenderer::compileBoundaryList() {
    glNewList(BOUNDARY_LIST, GL_COMPILE);

    glLineWidth(2);
    glBegin(GL_LINES);
    {
        glColor3f(1, 0, 0);

        glVertex3f(XMIN, YMIN, ZMIN);
        glVertex3f(XMAX, YMIN, ZMIN);

        glColor3f(0, 1, 0);

        glVertex3f(XMIN, YMIN, ZMIN);
        glVertex3f(XMIN, YMAX, ZMIN);

        glColor3f(0, 0, 1);

        glVertex3f(XMIN, YMIN, ZMIN);
        glVertex3f(XMIN, YMIN, ZMAX);

        glColor3f(1, 1, 1);
        //glColor3f(0, 0, 0);

        glVertex3f(XMIN, YMAX, ZMIN);
        glVertex3f(XMIN, YMAX, ZMAX);

        glVertex3f(XMAX, YMIN, ZMIN);
        glVertex3f(XMAX, YMIN, ZMAX);

        glVertex3f(XMAX, YMAX, ZMIN);
        glVertex3f(XMAX, YMAX, ZMAX);

        glVertex3f(XMIN, YMIN, ZMAX);
        glVertex3f(XMAX, YMIN, ZMAX);

        glVertex3f(XMIN, YMAX, ZMIN);
        glVertex3f(XMAX, YMAX, ZMIN);

        glVertex3f(XMIN, YMAX, ZMAX);
        glVertex3f(XMAX, YMAX, ZMAX);

        glVertex3f(XMAX, YMIN, ZMIN);
        glVertex3f(XMAX, YMAX, ZMIN);

        glVertex3f(XMAX, YMIN, ZMAX);
        glVertex3f(XMAX, YMAX, ZMAX);

        glVertex3f(XMIN, YMIN, ZMAX);
        glVertex3f(XMIN, YMAX, ZMAX);
    }
    glEnd();
    glLineWidth(1);

    glEndList();
}

