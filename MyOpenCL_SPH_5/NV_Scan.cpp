/*
 * Copyright 1993-2010 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cassert>
#include <string>
#include <iostream>
#include <fstream>

#include "NV_Scan.h"

#include "OpenCLUtil.h"

#define KERNEL_FILE "NV_Scan.cl"

using namespace std;

cl_device_id oclGetFirstDev2(cl_context cxGPUContext) {
    size_t szParmDataBytes;
    cl_device_id* cdDevices;

    // get the list of GPU devices associated with context
    clGetContextInfo(cxGPUContext, CL_CONTEXT_DEVICES, 0, NULL, &szParmDataBytes);
    cdDevices = (cl_device_id*) malloc(szParmDataBytes);

    clGetContextInfo(cxGPUContext, CL_CONTEXT_DEVICES, szParmDataBytes, cdDevices, NULL);

    cl_device_id first = cdDevices[0];
    free(cdDevices);

    return first;
}

NVScan::NVScan(cl_context GPUContext,
        cl_command_queue CommandQue,
        unsigned int numElements) :
cxGPUContext(GPUContext),
cqCommandQueue(CommandQue),
mNumElements(numElements) {
    cl_int ciErrNum;
//#define PRT(A) cout << (#A) << " = " << (A) << endl

    //PRT(numElements);
    //PRT(MAX_WORKGROUP_INCLUSIVE_SCAN_SIZE);

    if (numElements > MAX_WORKGROUP_INCLUSIVE_SCAN_SIZE) {
        d_Buffer = clCreateBuffer(cxGPUContext, CL_MEM_READ_WRITE, numElements / MAX_WORKGROUP_INCLUSIVE_SCAN_SIZE * sizeof (cl_uint), NULL, &ciErrNum);
        assert((ciErrNum) == (CL_SUCCESS));
    } else {
        // REVERS: I want to use this sort with numElements == MAX_WORKGROUP_INCLUSIVE_SCAN_SIZE
        // so I need not empty d_Buffer
        d_Buffer = clCreateBuffer(cxGPUContext, CL_MEM_READ_WRITE, 32 * sizeof (cl_uint), NULL, &ciErrNum);
        assert((ciErrNum) == (CL_SUCCESS));
    }

    //shrLog("Create and build Scan program\n");
    //  size_t szKernelLength; // Byte size of kernel code
    //
    std::ifstream sourceFile(KERNEL_FILE);
    if (!sourceFile) {
        cout << "Failed to load kernel file : " << KERNEL_FILE << endl;
        assert(sourceFile);
        return;
    }

    std::string sourceCode(
            std::istreambuf_iterator<char>(sourceFile),
            (std::istreambuf_iterator<char>()));

    //  string sourceCode = OpenCLUtil::readCLFile(KERNEL_FILE, true, "Parameters.h");

    const char* csrc = sourceCode.c_str();
    const size_t csrcLen = sourceCode.length();

    cpProgram = clCreateProgramWithSource(cxGPUContext, 1, (const char **) &csrc, &csrcLen, &ciErrNum);

    //cpProgram = clCreateProgramWithSource(cxGPUContext, 1, (const char **) &sourceCode.c_str(), &sourceCode.length(), &ciErrNum);

    ciErrNum = clBuildProgram(cpProgram, 0, NULL, "-cl-fast-relaxed-math", NULL, NULL);

    if (ciErrNum != CL_SUCCESS) {
        if (ciErrNum == CL_BUILD_PROGRAM_FAILURE) {

            cl_device_id devId = oclGetFirstDev2(cxGPUContext);
            cl_int logStatus;
            char * buildLog = NULL;
            size_t buildLogSize = 0;
            logStatus = clGetProgramBuildInfo(cpProgram,
                    devId,
                    CL_PROGRAM_BUILD_LOG,
                    buildLogSize,
                    buildLog,
                    &buildLogSize);

            if (logStatus != CL_SUCCESS) {
                cout << "ERROR: " << OpenCLUtil::getOpenCLErrorCodeStr(logStatus) << endl;
            }
            assert(logStatus == CL_SUCCESS);

            buildLog = (char*) malloc(buildLogSize);

            if (buildLog == NULL) {
                cout << "Failed to allocate host memory. (buildLog)" << endl;
                assert(buildLog);
                return;
            }
            memset(buildLog, 0, buildLogSize);

            logStatus = clGetProgramBuildInfo(cpProgram,
                    devId,
                    CL_PROGRAM_BUILD_LOG,
                    buildLogSize,
                    buildLog,
                    NULL);
            if (logStatus != CL_SUCCESS) {
                cout << "clGetProgramBuildInfo failed." << endl;
                free(buildLog);
                assert(logStatus == CL_SUCCESS);
                return;
            }

            std::cout << " \n\t\t\tBUILD LOG\n";
            std::cout << " ************************************************\n";
            std::cout << buildLog << std::endl;
            std::cout << " ************************************************\n";
            free(buildLog);
        }
    }
    //    if (ciErrNum != CL_SUCCESS) {
    //        // write out standard error, Build Log and PTX, then cleanup and exit
    //        shrLogEx(LOGBOTH | ERRORMSG, ciErrNum, STDERROR);
    //        oclLogBuildInfo(cpProgram, oclGetFirstDev(cxGPUContext));
    //        oclLogPtx(cpProgram, oclGetFirstDev(cxGPUContext), "Scan.ptx");
    //        assert((ciErrNum) == (CL_SUCCESS));
    //    }

    ckScanExclusiveLocal1 = clCreateKernel(cpProgram, "scanExclusiveLocal1", &ciErrNum);
    assert((ciErrNum) == (CL_SUCCESS));
    ckScanExclusiveLocal2 = clCreateKernel(cpProgram, "scanExclusiveLocal2", &ciErrNum);
    assert((ciErrNum) == (CL_SUCCESS));
    ckUniformUpdate = clCreateKernel(cpProgram, "uniformUpdate", &ciErrNum);
    assert((ciErrNum) == (CL_SUCCESS));

    // free(cScan);
    //  free(cSourcePath);
}

NVScan::~NVScan() {
    cl_int ciErrNum;

    ciErrNum = clReleaseKernel(ckScanExclusiveLocal1);
    ciErrNum |= clReleaseKernel(ckScanExclusiveLocal2);
    ciErrNum |= clReleaseKernel(ckUniformUpdate);
    if (mNumElements > MAX_WORKGROUP_INCLUSIVE_SCAN_SIZE) {
        ciErrNum |= clReleaseMemObject(d_Buffer);
    }
    ciErrNum |= clReleaseProgram(cpProgram);
    assert((ciErrNum) == (CL_SUCCESS));
}

// main exclusive scan routine

void NVScan::scanExclusiveLarge(
        cl_mem d_Dst,
        cl_mem d_Src,
        unsigned int batchSize,
        unsigned int arrayLength
        ) {
    //Check power-of-two factorization
    unsigned int log2L;
    unsigned int factorizationRemainder = factorRadix2(log2L, arrayLength);
    assert((factorizationRemainder == 1) == (true));

    assert(((arrayLength >= MIN_LARGE_ARRAY_SIZE) && (arrayLength <= MAX_LARGE_ARRAY_SIZE)) == (true));

    //Check total batch size limit
    assert(((batchSize * arrayLength) <= MAX_BATCH_ELEMENTS) == (true));

    scanExclusiveLocal1(
            d_Dst,
            d_Src,
            //            (batchSize * arrayLength) / (4 * WORKGROUP_SIZE),
            (batchSize * arrayLength) / (4 * WORKGROUP_SIZE),
            4 * WORKGROUP_SIZE
            );

    scanExclusiveLocal2(
            d_Buffer,
            d_Dst,
            d_Src,
            batchSize,
            arrayLength / (4 * WORKGROUP_SIZE)
            );

    uniformUpdate(
            d_Dst,
            d_Buffer,
            (batchSize * arrayLength) / (4 * WORKGROUP_SIZE)
            );
}

void NVScan::scanExclusiveLocal1(
        cl_mem d_Dst,
        cl_mem d_Src,
        unsigned int n,
        unsigned int size
        ) {
    cl_int ciErrNum;
    size_t localWorkSize, globalWorkSize;

    ciErrNum = clSetKernelArg(ckScanExclusiveLocal1, 0, sizeof (cl_mem), (void *) &d_Dst);
    ciErrNum |= clSetKernelArg(ckScanExclusiveLocal1, 1, sizeof (cl_mem), (void *) &d_Src);
    //ciErrNum |= clSetKernelArg(ckScanExclusiveLocal1, 2, 2 * WORKGROUP_SIZE * sizeof (unsigned int), NULL);
    ciErrNum |= clSetKernelArg(ckScanExclusiveLocal1, 2, 2 * WORKGROUP_SIZE * sizeof (unsigned int), NULL);
    ciErrNum |= clSetKernelArg(ckScanExclusiveLocal1, 3, sizeof (unsigned int), (void *) &size);
    assert((ciErrNum) == (CL_SUCCESS));

    localWorkSize = WORKGROUP_SIZE;
    globalWorkSize = (n * size) / 4;

    ciErrNum = clEnqueueNDRangeKernel(cqCommandQueue, ckScanExclusiveLocal1, 1, NULL, &globalWorkSize, &localWorkSize, 0, NULL, NULL);
    assert((ciErrNum) == (CL_SUCCESS));
}

void NVScan::scanExclusiveLocal2(
        cl_mem d_Buffer,
        cl_mem d_Dst,
        cl_mem d_Src,
        unsigned int n,
        unsigned int size
        ) {
    cl_int ciErrNum;
    size_t localWorkSize, globalWorkSize;

    unsigned int elements = n * size;
    ciErrNum = clSetKernelArg(ckScanExclusiveLocal2, 0, sizeof (cl_mem), (void *) &d_Buffer);
    assert((ciErrNum) == (CL_SUCCESS));
    ciErrNum |= clSetKernelArg(ckScanExclusiveLocal2, 1, sizeof (cl_mem), (void *) &d_Dst);
    assert((ciErrNum) == (CL_SUCCESS));
    ciErrNum |= clSetKernelArg(ckScanExclusiveLocal2, 2, sizeof (cl_mem), (void *) &d_Src);
    assert((ciErrNum) == (CL_SUCCESS));
    //ciErrNum |= clSetKernelArg(ckScanExclusiveLocal2, 3, 2 * WORKGROUP_SIZE * sizeof (unsigned int), NULL);
    ciErrNum |= clSetKernelArg(ckScanExclusiveLocal2, 3, 2 * WORKGROUP_SIZE * sizeof (unsigned int), NULL);

    ciErrNum |= clSetKernelArg(ckScanExclusiveLocal2, 4, sizeof (unsigned int), (void *) &elements);

    ciErrNum |= clSetKernelArg(ckScanExclusiveLocal2, 5, sizeof (unsigned int), (void *) &size);
    //    if (ciErrNum != CL_SUCCESS) {
    //        cout << "ERROR: " << OpenCLUtil::getOpenCLErrorCodeStr(ciErrNum) << endl;
    //    }
    assert((ciErrNum) == (CL_SUCCESS));

    localWorkSize = WORKGROUP_SIZE;
    globalWorkSize = iSnapUp(elements, WORKGROUP_SIZE);

    ciErrNum = clEnqueueNDRangeKernel(cqCommandQueue, ckScanExclusiveLocal2, 1, NULL, &globalWorkSize, &localWorkSize, 0, NULL, NULL);
    assert((ciErrNum) == (CL_SUCCESS));
}

void NVScan::uniformUpdate(
        cl_mem d_Dst,
        cl_mem d_Buffer,
        unsigned int n
        ) {
    cl_int ciErrNum;
    size_t localWorkSize, globalWorkSize;

    ciErrNum = clSetKernelArg(ckUniformUpdate, 0, sizeof (cl_mem), (void *) &d_Dst);
    ciErrNum |= clSetKernelArg(ckUniformUpdate, 1, sizeof (cl_mem), (void *) &d_Buffer);
    assert((ciErrNum) == (CL_SUCCESS));

    localWorkSize = WORKGROUP_SIZE;
    globalWorkSize = n * WORKGROUP_SIZE;

    ciErrNum = clEnqueueNDRangeKernel(cqCommandQueue, ckUniformUpdate, 1, NULL, &globalWorkSize, &localWorkSize, 0, NULL, NULL);
    assert((ciErrNum) == (CL_SUCCESS));
}
