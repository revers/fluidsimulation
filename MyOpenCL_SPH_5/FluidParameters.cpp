/* 
 * File:   FluidParameters.cpp
 * Author: Revers
 * 
 * Created on 13 listopad 2011, 19:59
 */

#include <cmath>
#include <iostream>
#include "Parameters.h"
#include "FluidParameters.h"

using namespace std;

FluidParameters::FluidParameters(unsigned int particleCount) :

nParticles(particleCount),
xmin(XMIN),
xmax(XMAX),
ymin(YMIN),
ymax(YMAX),
zmin(ZMIN),
zmax(ZMAX),
blockSize(BLOCK_SIZE),
blockSize3D(BLOCK_SIZE_3D),
gridDimensionX(GRID_DIMENSION_X),
gridDimensionY(GRID_DIMENSION_Y),
gridDimensionZ(GRID_DIMENSION_Z),
gridPoints(GRID_POINTS),
blockCount(BLOCK_COUNT),
interleaveLookupLength(INTERLEAVE_LOOKUP_LENGTH),
blockCoordsLookupLength(BLOCK_COORDS_LOOKUP_LENGTH),

gravityX(0.0f),
gravityY(GRAVITY), //-0.98f*2),
gravityZ(0.0f),

simulationScale(0.0035f),
simulationScaleInv(1.0f / simulationScale),
stiffness(350.0f), //500.0f + 200.0f),
accelerationLimit(100.0f),
damping(0.5f), //75f),

supportRadiusScaled((float) blockSize),
supportRadius(supportRadiusScaled * simulationScale),
restDensity(1000.0f), //998.29f; 
viscosity(4.5f), //1.5f),
particleMass(16.384f / (float) nParticles), //0.02f; // [kg] 
timestep(0.0042f),

wPoly6Coefficient(315.0f / (64.0f * M_PI * pow(supportRadius, 9.0f))),
gradWPoly6Coefficient(-945.0f / (32.0f * M_PI * pow(supportRadius, 9.0f))),
del2WPoly6Coefficient(-945.0f / (32.0f * M_PI * pow(supportRadius, 9.0f))),
gradWspikyCoefficient(-45.0f / (M_PI * pow(supportRadius, 6.0f))),
del2WviscosityCoefficient(-gradWspikyCoefficient),

particleVolume(particleMass / restDensity), // [m^3]
particleRestDistance(pow(particleVolume, 1.0 / 3.0)), // [m]
particleRestDistanceScaled(particleRestDistance * simulationScaleInv),
particleRadius(pow(particleVolume * 3.0 / (4.0 * M_PI), 1.0 / 3.0)),
particleRadiusScaled(particleRadius * simulationScaleInv),
totalMass(particleMass * nParticles),
totalVolume(totalMass / restDensity), //particleVolume * nParticles;
totalLength(pow(totalVolume, 1.0 / 3.0)),
totalLengthScaled(totalLength * simulationScaleInv),
totalVolumeScaled(totalLengthScaled * totalLengthScaled * totalLengthScaled),
totalParticlesBoxHeight(totalVolumeScaled / (gridDimensionX * gridDimensionX)) {

    //
    //#define PRT(A) cout << (#A) << " = " << (A) << endl;
    //    PRT(totalVolume);
    //    PRT(totalVolumeScaled);
    //    PRT(totalLength);
    //    PRT(totalLengthScaled);

    //    - particle Spacing : 0.03
    //- supportRadius : 2 * 0.03
    //- viscosity coefficient : 250
    //- viscosity constant : viscosity coefficient * particle Spacing 
    //- density : 1000
    //- particle mass : particle volume * density
    //- stiffness value : 1000
    //- time step : 0.4 * supportRadius / Sqrt(stiffness Value)
    //    particleRestDistance = 0.03f;
    //    float supportRadiusScaled = 2.0f * particleRestDistance;
    //    supportRadius = (float)BLOCK_SIZE / 2.0f;
    //    particleVolume = particleRestDistance * particleRestDistance * particleRestDistance;
    //    restDensity = 1000.0f;
    //    particleMass = particleVolume * restDensity;
    //    gasStiffness = 1000.0f;
    //    viscosity = 250.0f*2.0f;// * particleRestDistance;
    //    timestep = 0.4f * supportRadiusScaled / sqrt(gasStiffness);
    //    simulationScale =  supportRadiusScaled / supportRadius;
    //    simulationScaleInv = 1.0f / simulationScale;
    //   cout << "SIMULATION SCALE = " << simulationScale << endl;
    //   cout << "SIMULATION SCALE INV = " << simulationScaleInv << endl;
}

void FluidParameters::setParticleCount(unsigned int particleCount) {
    nParticles = particleCount;
    particleMass = 16.384f / (float) nParticles;

    particleVolume = particleMass / restDensity; // [m^3]
    particleRestDistance = pow(particleVolume, 1.0 / 3.0); // [m]
    particleRestDistanceScaled = particleRestDistance * simulationScaleInv;
    particleRadius = pow(particleVolume * 3.0 / (4.0 * M_PI), 1.0 / 3.0);
    particleRadiusScaled = particleRadius * simulationScaleInv;
    totalMass = particleMass * nParticles;
    totalVolume = totalMass / restDensity; //particleVolume * nParticles;
    totalLength = pow(totalVolume, 1.0 / 3.0);

    totalLengthScaled = totalLength * simulationScaleInv;
    totalVolumeScaled = totalLengthScaled * totalLengthScaled * totalLengthScaled;
    totalParticlesBoxHeight = totalVolumeScaled / (gridDimensionX * gridDimensionX);

    //#define PRT(A) cout << (#A) << " = " << (A) << endl;
    //    PRT(totalVolume);
    //    PRT(totalVolumeScaled);
    //    PRT(totalLength);
    //    PRT(totalLengthScaled);
}

void FluidParameters::printParameters() {
    cout << "=====================================================" << endl;

    cout << "\nblockSize = " << blockSize << endl;
    cout << "blockSize3D = " << blockSize3D << endl;

    cout << "gridDimensionX = " << gridDimensionX << endl;
    cout << "gridDimensionY = " << gridDimensionY << endl;
    cout << "gridDimensionZ = " << gridDimensionZ << endl;
    cout << "gridPoints = " << gridPoints << endl;
    cout << "blockCount = " << blockCount << endl;
    cout << "interleaveLookupLength = " << interleaveLookupLength << endl;
    cout << "blockCoordsLookupLength = " << blockCoordsLookupLength << endl;

    cout << "\nxmin = " << xmin << "; xmax = " << xmax << endl;
    cout << "ymin = " << xmin << "; ymax = " << ymax << endl;
    cout << "zmin = " << xmin << "; zmax = " << zmax << endl;

    cout << "\nNumber of particles = " << nParticles << endl;
    cout << "Support radius = " << supportRadius << endl;
    cout << "Support radius scaled = " << supportRadiusScaled << " [m]" << endl;
    cout << "Rest density = " << restDensity << " [kg/m^3]" << endl;

    cout << "Particle mass = " << particleMass << " [kg]" << endl;

    cout << "Viscosity = " << viscosity << " [Pa*s = N*s/m^2 = kg/m*s]" << endl;
    cout << "Gas stiffness = " << stiffness << " [J = N*m = kg*m^2/s^2]" << endl;
    cout << "Timestep = " << timestep << "[s]" << endl;
    cout << "accelerationLimit = " << accelerationLimit << endl;

    //  float particlePerBlock = (float) nParticles / (float) blockCount;
    //  cout << "Particle per block = " << particlePerBlock << endl;

    cout << "simulationScale = " << simulationScale << endl;

    cout << "Particle volume = " << particleVolume << " [m^3]" << endl;
    cout << "Particle radius = " << particleRadius << " [m]" << endl;
    cout << "Particle radius scaled = " << particleRadiusScaled << endl;
    cout << "Particle rest distance = " << particleRestDistance << " [m]" << endl;
    const float particleRestDistanceScaled = particleRestDistance * simulationScaleInv;
    cout << "particleRestDistanceScaled = " << particleRestDistanceScaled << endl;
    const float oneParticleDensity = particleMass / particleVolume;
    cout << "One particle density = " << oneParticleDensity << endl;


    cout << "Total mass = " << totalMass << " [kg]" << endl;
    cout << "Total volume = " << totalVolume << " [m^3]" << " = " << totalLength << "^3" << endl;

    const float avgParticles = 64;
    cout << "avgParticles = " << avgParticles << endl;
    const float h = pow(((3.0 * totalVolume * avgParticles) / (4.0 * M_PI * nParticles)), 1.0 / 3.0);
    cout << "suggested h = " << h << endl;

    cout << "=====================================================" << endl;

    cout << "wPoly6Coeff = " << wPoly6Coefficient << endl;
    cout << "gradWPoly6Coeff = " << gradWPoly6Coefficient << endl;
    cout << "del2WPoly6Coeff = " << del2WPoly6Coefficient << endl;
    cout << "=====================================================" << endl;
}

void FluidParameters::setParticleMass(float particleMass) {
    this->particleMass = particleMass;

    particleVolume = particleMass / restDensity; // [m^3]
    particleRestDistance = pow(particleVolume, 1.0 / 3.0); // [m]
    particleRestDistanceScaled = particleRestDistance * simulationScaleInv;
    particleRadius = pow(particleVolume * 3.0 / (4.0 * M_PI), 1.0 / 3.0);
    particleRadiusScaled = particleRadius * simulationScaleInv;
    totalMass = particleMass * nParticles;
    totalVolume = totalMass / restDensity; //particleVolume * nParticles;
    totalLength = pow(totalVolume, 1.0 / 3.0);

    totalLengthScaled = totalLength * simulationScaleInv;
    totalVolumeScaled = totalLengthScaled * totalLengthScaled * totalLengthScaled;
    totalParticlesBoxHeight = totalVolumeScaled / (gridDimensionX * gridDimensionX);
}

void FluidParameters::setRestDensity(float restDensity) {
    this->restDensity = restDensity;

    particleVolume = particleMass / restDensity; // [m^3]
    particleRestDistance = pow(particleVolume, 1.0 / 3.0); // [m]
    particleRestDistanceScaled = particleRestDistance * simulationScaleInv;
    particleRadius = pow(particleVolume * 3.0 / (4.0 * M_PI), 1.0 / 3.0);
    particleRadiusScaled = particleRadius * simulationScaleInv;
    totalMass = particleMass * nParticles;
    totalVolume = totalMass / restDensity; //particleVolume * nParticles;
    totalLength = pow(totalVolume, 1.0 / 3.0);

    totalLengthScaled = totalLength * simulationScaleInv;
    totalVolumeScaled = totalLengthScaled * totalLengthScaled * totalLengthScaled;
    totalParticlesBoxHeight = totalVolumeScaled / (gridDimensionX * gridDimensionX);
}

void FluidParameters::setSimulationScale(float simulationScale) {
    this->simulationScale = simulationScale;
    this->simulationScaleInv = 1.0f / this->simulationScale;

    this->supportRadius = supportRadiusScaled * (simulationScale);

    particleRestDistanceScaled = particleRestDistance * simulationScaleInv;
    particleRadiusScaled = particleRadius * simulationScaleInv;
    totalLengthScaled = totalLength * simulationScaleInv;
    totalVolumeScaled = totalLengthScaled * totalLengthScaled * totalLengthScaled;
    totalParticlesBoxHeight = totalVolumeScaled / (gridDimensionX * gridDimensionX);

    wPoly6Coefficient = 315.0f / (64.0f * M_PI * pow(supportRadius, 9.0f));
    gradWPoly6Coefficient = -945.0f / (32.0f * M_PI * pow(supportRadius, 9.0f));
    del2WPoly6Coefficient = -945.0f / (32.0f * M_PI * pow(supportRadius, 9.0f));
    gradWspikyCoefficient = -45.0f / (M_PI * pow(supportRadius, 6.0f));
    del2WviscosityCoefficient = -gradWspikyCoefficient;
}

