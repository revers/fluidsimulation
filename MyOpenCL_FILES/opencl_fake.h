/* 
 * File:   opencl_fake.h
 * Author: Revers
 *
 * Created on 26 październik 2011, 21:50
 */

#ifndef OPENCL_FAKE_H
#define	OPENCL_FAKE_H

#include <CL/cl.h>

#ifdef	__cplusplus
extern "C" {
#endif

#define CLK_LOCAL_MEM_FENCE 0
#define CLK_GLOBAL_MEM_FENCE 1
#define __local
#define __kernel
#define __global
#define __constant
#define __write_only
#define __read_only
#define bool int
#define CLK_NORMALIZED_COORDS_FALSE 0
#define CLK_NORMALIZED_COORDS_TRUE 1
#define CLK_ADDRESS_CLAMP 1
#define CLK_FILTER_NEAREST 2
#define CLK_FILTER_LINEAR 5
#define CLK_ADDRESS_CLAMP_TO_EDGE 3
#define CLK_ADDRESS_NONE 4

    typedef int* image2d_t;
    typedef int* image3d_t;

    typedef int sampler_t;

    typedef cl_float4 float4;
    typedef cl_float3 float3;
    typedef cl_float2 float2;

    typedef cl_ulong2 ulong2;
    typedef cl_ulong3 ulong3;
    typedef cl_ulong4 ulong4;
    typedef cl_ulong8 ulong8;
    typedef cl_long4 long4;
    typedef cl_long3 long3;
    typedef cl_long2 long2;
    typedef unsigned long ulong;

    typedef cl_uint2 uint2;
    typedef cl_uint3 uint3;
    typedef cl_uint4 uint4;
    typedef cl_uint8 uint8;
    typedef cl_int4 int4;
    typedef cl_int3 int3;
    typedef cl_int2 int2;

    typedef cl_char16 char16;
    typedef cl_uchar16 uchar16;
    typedef unsigned int uint;

    uint4 read_imageui(image2d_t x, sampler_t y, uint2 z) {
        return 0;
    }

    int4 read_imagei(image2d_t x, sampler_t y, uint2 z) {
        return 0;
    }

    float4 read_imagef(image2d_t x, sampler_t y, uint2 z) {
        return 0;
    }

    void write_imagef(image2d_t x, uint2 y, float val) {

    }

    void write_imagei(image2d_t x, uint2 y, int val) {

    }

    void write_imageui(image2d_t x, uint2 y, uint val) {

    }

    int4 convert_int4(float4 a) {
        return 0;
    }

    int4 convert_int4_rtz(float4 a) {
        return 0;
    }

    float4 convert_float4(int4 a) {
        return 0;
    }

    int normalize(int a) {
        return 0;
    }

    int dot(int a, int b) {
        return 0;
    }

    int mix(int a, int b, int c) {
        return 0;
    }

    int4 as_int4(float4 f) {
        return 0;
    }

    uchar16 as_uchar16(uint4 i) {
        return 0;
    }

    uint4 as_uint4(uchar16 c) {
        return 0;
    }

    float4 as_float4(int4 i) {
        return 0;
    }

    float clamp(float a, float b, float c) {
        return 0;
    }

    int min(int a, int b) {
        return 0;
    }

    int max(int a, int b) {
        return 0;
    }

    int get_num_groups(int i) {
        return 0;
    }

    int get_global_id(int i) {
        return 0;
    }

    int get_group_id(int i) {
        return 0;
    }

    int get_local_id(int i) {
        return 0;
    }

    int get_local_size(int i) {
        return 0;
    }

    int get_global_size(int i) {
        return 0;
    }

    void barrier(int i) {
    }

    void mem_fence(int i) {

    }

    void atomic_xchg(int* ptr, int i) {

    }

    int atomic_inc(int* ptr) {

    }

    int atomic_min(int* ptr) {

    }

    int atomic_max(int* ptr, int val) {

    }

    float length(float4 f) {
        return 0;
    }


#ifdef	__cplusplus
}
#endif

#endif	/* OPENCL_FAKE_H */

