/* 
 * File:   Parameters.h
 * Author: Revers
 *
 * Created on 27 październik 2011, 02:29
 */

#ifndef PARAMETERS_H
#define	PARAMETERS_H

//$START$
//-----------------------------
//#define USE_BIGGER_BLOCK

#ifdef USE_BIGGER_BLOCK
#define BLOCK_SIZE 8
#define BLOCK_SIZE_3D (BLOCK_SIZE * BLOCK_SIZE * BLOCK_SIZE)
#define BLOCK_SIZE_3D_LOG_2 9 // 2^9 = 512. For fast division.
#else
#define BLOCK_SIZE 4
#define BLOCK_SIZE_3D (BLOCK_SIZE * BLOCK_SIZE * BLOCK_SIZE)
#define BLOCK_SIZE_3D_LOG_2 6 // 2^6 = 64. For fast division.
#endif

#define GRID_DIMENSION_X 128
#define GRID_DIMENSION_Y 128
#define GRID_DIMENSION_Z 128

#define GRID_POINTS (GRID_DIMENSION_X * GRID_DIMENSION_Y * GRID_DIMENSION_Z)

#define BLOCK_COUNT (GRID_POINTS / BLOCK_SIZE_3D)

#define BITS_PER_COMPONENT 10
#define INTERLEAVE_LOOKUP_LENGTH (1 << BITS_PER_COMPONENT) // in one dimension. lookup table has length: 3 * LOOKUP_SIZE.
#define BLOCK_COORDS_LOOKUP_LENGTH BLOCK_COUNT

#define INTERLEAVE_CELL_SIZE 4
#define BLOCK_LOOKUP_CELL_SIZE 4

#define BLOCK_CELL_SIZE 2
#define SORT_CELL_SIZE 2

#define MAXIMUM_PARTICLES_PER_BLOCK 32//64
//#define PARTICLE_COUNT (64 * 1024)

#define XMIN 0
#define XMAX GRID_DIMENSION_X
#define YMIN 0
#define YMAX GRID_DIMENSION_Y
#define ZMIN 0
#define ZMAX GRID_DIMENSION_Z

#define STRUCT_ALIGNMENT 16

#define USE_COLORING
#define USE_LEAP_FROG

#define NV_RADIX_SORT
//#define ATI_RADIX_SORT

#define USE_SURFACE_TENSION
#define SPH_ELEMENTS_PER_WORK_ITEM 64
//================================================
// Raycasting:
#define ONE_PIXEL_SIZE (sizeof(float) * 4)

#define IMAGE3D_BASE_LENGTH 64
#define IMAGE3D_WIDTH IMAGE3D_BASE_LENGTH
#define IMAGE3D_HEIGHT IMAGE3D_BASE_LENGTH
#define IMAGE3D_DEPTH IMAGE3D_BASE_LENGTH

#define WORLD_TO_IMAGE3D_SCALE 0.5f

#define MAX_PARTICLE_RANGE 3.0f
#define MAX_PARTICLE_RANGE_DIV_254 (MAX_PARTICLE_RANGE / 254.0f)
//================================================
//$END$

#endif	/* PARAMETERS_H */

