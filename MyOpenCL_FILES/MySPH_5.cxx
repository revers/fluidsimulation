#include "opencl_fake.h"
#include "MySPH_fake.h"

//$END_FAKE$

#define HASH_POSITION_SERIAL_INDEX(A) A.y
#define HASH_POSITION_Z_INDEX(A) A.x

#define DENSITY(A) A.x
#define DENSITY_INVERSE(A) A.y

#define BLOCK_PARTICLE_COUNT(A) A.x
#define BLOCK_FIRST_PARTICLE_INDEX(A) A.y

#define BLOCK_PARTICLE_COUNT_PTR(A) (A)
#define BLOCK_FIRST_PARTICLE_INDEX_PTR(A) (A + 1)

#define PRESSURE(A) A.w

#define DIVIDE(a, b) native_divide(a, b)
#define SQRT(x) native_sqrt(x)
#define DOT(a, b) dot(a, b)

#define SELECT4(A, B, C) select(A, B, (uint4) ((C) * 0xffffffff))
#define SELECT2(A, B, C) select(A, B, (uint2) ((C) * 0xffffffff))

#define BOUNDARY_1
//#define BOUNDARY_2

#define USE_OWN_DENSITY
#define CHECK_ACCELERATION_LIMIT
//#define GAS_STIFFNESS

#define USE_SELECT
#define USE_IF

#define EPSILON 0.00001f
#define SQRT_3  1.73205f
#define SQRT_2  1.41421f

typedef union {
    float4 flo4;
    uint4 uin4;
} PositionVect;

#define POSITION(posVect) ((posVect).flo4)
#define Z_INDEX(posVect) ((posVect).uin4.w)

typedef enum {
    VELOCITY, PRESSURE, FORCE
} ColoringSource;

typedef enum {
    White,
    Blackish,
    BlackToCyan,
    BlueToWhite,
    HSVBlueToRed,
} ColoringGradient;


// When I set image2d type to a FLOAT in the host code it doesn't work so instead 
// I must use CL_SIGNED_INT32 type, but luckly operator as_<type> works without 
// any overhead:

inline PositionVect getPosition(__read_only image2d_t img, int index) {
    const sampler_t smp = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_NONE | CLK_FILTER_NEAREST;
    //return (PositionVect) read_imagef(img, smp, (int2) ((index & 1023), (index >> 10)));
    return (PositionVect) as_float4(read_imagei(img, smp, (int2) ((index & 1023), (index >> 10))));
}

inline void setPosition(__write_only image2d_t img, int index, float4 value) {
    // write_imagef(img, (int2) ((index & 1023), (index >> 10)), value);
    write_imagei(img, (int2) ((index & 1023), (index >> 10)), as_int4(value));
}

inline float4 getVelocity(__read_only image2d_t img, int index) {
    const sampler_t smp = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_NONE | CLK_FILTER_NEAREST;
    //return read_imagef(img, smp, (int2) ((index & 1023), (index >> 10)));
    return as_float4(read_imagei(img, smp, (int2) ((index & 1023), (index >> 10))));
}

inline void setVelocity(__write_only image2d_t img, int index, float4 value) {
    // write_imagef(img, (int2) ((index & 1023), (index >> 10)), value);
    write_imagei(img, (int2) ((index & 1023), (index >> 10)), as_int4(value));
}

inline uint4 getBlockCoords(__read_only image2d_t img, int index) {
    const sampler_t smp = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_NONE | CLK_FILTER_NEAREST;
    return read_imageui(img, smp, (int2) ((index & 1023), (index >> 10)));
}

__kernel void hashParticles(
        __constant uint4* interleaveLookup,
        __read_only image2d_t position,
        __global uint2* radixSort) {
    int id = get_global_id(0);

    //float4 pos = position[id];
    float4 pos = getPosition(position, id).flo4;

    uint x = interleaveLookup[(uint) pos.x].x;
    uint y = interleaveLookup[(uint) pos.y].y;
    uint z = interleaveLookup[(uint) pos.z].z;

    uint zIndex = x | y | z;

    radixSort[id] = (uint2) (zIndex, id);
}

__kernel void reorderPositionAndVelocity(
        __global uint2* radixSort,
        __read_only image2d_t origPosition,
        __read_only image2d_t origVelocity,
        __write_only image2d_t newPosition,
        __write_only image2d_t newVelocity) {

    int id = get_global_id(0);
    uint2 hashCell = radixSort[id];

    uint zIndex = HASH_POSITION_Z_INDEX(hashCell);
    uint oldPos = HASH_POSITION_SERIAL_INDEX(hashCell);

    //PositionVect pos = origPosition[oldPos];
    PositionVect pos = getPosition(origPosition, oldPos);
    Z_INDEX(pos) = zIndex >> BLOCK_SIZE_3D_LOG_2;

    //    newPosition[id] = pos;
    //    newVelocity[id] = origVelocity[oldPos];
    float4 velocity = getVelocity(origVelocity, oldPos);
    setPosition(newPosition, id, pos.flo4);
    setVelocity(newVelocity, id, velocity);
}

__kernel void clearBuffer(__global uint4* buffer) {
    int id = get_global_id(0);

    buffer[id] = (uint4) (0, 0xFFFFFFFF, 0, 0xFFFFFFFF);
}

__kernel void createBlockList(
        __global uint2* radixSort,
        __global uint2* blockBuffer) {

    int id = get_global_id(0);
    uint2 hashCell = radixSort[id];

    uint zIndex = HASH_POSITION_Z_INDEX(hashCell);
    //uint nearestBlockIndex = zIndex / BLOCK_SIZE_3D;
    uint nearestBlockIndex = zIndex >> BLOCK_SIZE_3D_LOG_2;
    __global uint* cell = (__global uint*) (&(blockBuffer[nearestBlockIndex]));

    atomic_inc(BLOCK_PARTICLE_COUNT_PTR(cell));
    atomic_min(BLOCK_FIRST_PARTICLE_INDEX_PTR(cell), id);
}

void splitBlock(uint2 block,
        __global uint2* compatctBlockBuffer,
        __global volatile uint* gpu2cpuBuffer) {

    int origSize = (int) BLOCK_PARTICLE_COUNT(block);

    int size = MAXIMUM_PARTICLES_PER_BLOCK;
    uint firstParticleIndex = BLOCK_FIRST_PARTICLE_INDEX(block);

    int loop = 1;
    while (loop) {
        origSize -= MAXIMUM_PARTICLES_PER_BLOCK;
        loop = origSize > 0;

        size = select(MAXIMUM_PARTICLES_PER_BLOCK + origSize, MAXIMUM_PARTICLES_PER_BLOCK, loop);

        uint freeCell = atomic_inc(gpu2cpuBuffer);

        compatctBlockBuffer[freeCell] = (uint2) (size, firstParticleIndex);

        firstParticleIndex += MAXIMUM_PARTICLES_PER_BLOCK;
    }
}

__kernel void createCompactBlockList(
        __global uint2* blockBuffer,
        __global uint2* compatctBlockBuffer,
        __global volatile uint* gpu2cpuBuffer) {

    int id = get_global_id(0);
    uint2 block = blockBuffer[id];

    if (BLOCK_PARTICLE_COUNT(block) > 0) {
        if (BLOCK_PARTICLE_COUNT(block) <= MAXIMUM_PARTICLES_PER_BLOCK) {
            uint freeCell = atomic_inc(gpu2cpuBuffer);
            compatctBlockBuffer[freeCell] = block;
        } else {
            splitBlock(block, compatctBlockBuffer, gpu2cpuBuffer);
        }
    }
}

__kernel void testNeighborLookup(__constant int4* neighborLookup,
        __global int4* tempBuffer) {

    int id = get_global_id(0);
    tempBuffer[id] = neighborLookup[id];
}

__kernel void testImage2D(
        __global uint4* tempBuffer,
        __read_only image2d_t img) {

    const sampler_t imgSampler = CLK_NORMALIZED_COORDS_FALSE | //Natural coordinates
            CLK_ADDRESS_CLAMP | //Clamp to zeros
            CLK_FILTER_NEAREST; //Don't interpolate

    int id = get_global_id(0);

    int x = id & 1023;
    int y = id >> 10;

    int2 coord = (int2) (x, y);
    uint4 val = read_imageui(img, imgSampler, coord);

    tempBuffer[id] = val;
}

void loadNeighborBlockData(
        int workItemId,
        uint thisBlockIndex,
        __constant int4* neighborLookup,
        __constant uint4* interleaveLookup,
        __read_only image2d_t blockCoordsLookup,
        __global const uint2* blockBuffer,
        __local uint2* localNeighborBlockBuffer) {

    uint4 val = getBlockCoords(blockCoordsLookup, thisBlockIndex);
    //blockLookupVect coords = blockCoordsLookup[thisBlockIndex];

    int4 neighbor = neighborLookup[workItemId];
    int posX = ((int) val.x) + neighbor.x;
    int posY = ((int) val.y) + neighbor.y;
    int posZ = ((int) val.z) + neighbor.z;

    int max = GRID_DIMENSION_X;

    if ((posX < 0) || (posX >= max)
            || (posY < 0) || (posY >= max)
            || (posZ < 0) || (posZ >= max)) {

        localNeighborBlockBuffer[workItemId] = (uint2) (0, 0);
        return;
    }

    uint x = interleaveLookup[posX].x;
    uint y = interleaveLookup[posY].y;
    uint z = interleaveLookup[posZ].z;

    uint zIndex = x | y | z;
    // zIndex /= BLOCK_SIZE_3D;
    zIndex >>= BLOCK_SIZE_3D_LOG_2;

    localNeighborBlockBuffer[workItemId] = blockBuffer[zIndex];
}

typedef struct {
    float wPoly6Coefficient;
    float particleMass;
    float simulationScale;
    float supportRadiusScaled;
    float supportRadiusSqr;
    float restDensity;
    float restDensityInvSqr;
    float stiffness;
} DensityComputationData __attribute__((aligned(STRUCT_ALIGNMENT)));

__kernel void computeDensityAndPressure(
        __constant int4* neighborLookup,
        __constant uint4* interleaveLookup,
        __read_only image2d_t blockCoordsLookup,
        __read_only image2d_t positionBuffer,
        __global const uint2* blockBuffer,
        __global const uint2* compactBlockBuffer,
        __global float2* densityBuffer,
        __read_only image2d_t oldVelocityBuffer,
        __write_only image2d_t newVelocityBuffer,
        __local float4* localNeighborPositionBuffer, // sizeof(float4) * 32 (but we need 27)
        __local uint2* localNeighborBlockBuffer, // sizeof(uint2) * 32 (but we need 27)
        const DensityComputationData computationData) {

    int workGroupId = get_group_id(0);
    int workItemId = get_local_id(0);

    uint2 thisBlock = compactBlockBuffer[workGroupId];

    uint thisBlockParticleCount = BLOCK_PARTICLE_COUNT(thisBlock); // N
    uint thisBlockFirstParticleIndex = BLOCK_FIRST_PARTICLE_INDEX(thisBlock);
    PositionVect thisBlockFirstParticlePos = getPosition(positionBuffer, (thisBlockFirstParticleIndex));

    // uint thisBlockIndex = Z_INDEX(thisBlockFirstParticlePos) / BLOCK_SIZE_3D;
    // fast division:
    uint thisBlockIndex = Z_INDEX(thisBlockFirstParticlePos); //  >> BLOCK_SIZE_3D_LOG_2;
    float workItemIdDensity = 0.0f;

#ifdef USE_SELECT
    bool b = (workItemId < thisBlockParticleCount);
    float4 workItemIdPosition = SELECT4(((float4) (0, 0, 0, 0)),
            POSITION(getPosition(positionBuffer, ((thisBlockFirstParticleIndex + workItemId) * b))),
            b);
#else
    float4 workItemIdPosition = (float4) (0, 0, 0, 0);
    if (workItemId < thisBlockParticleCount) {
        workItemIdPosition = POSITION(getPosition(positionBuffer, (thisBlockFirstParticleIndex + workItemId)));
    }
#endif

    if (workItemId < 27) {
        loadNeighborBlockData(workItemId, thisBlockIndex, neighborLookup, interleaveLookup,
                blockCoordsLookup, blockBuffer, localNeighborBlockBuffer);
    }

    barrier(CLK_LOCAL_MEM_FENCE);


    uint maxParticlesInNeighbors = BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[0]);
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[1]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[2]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[3]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[4]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[5]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[6]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[7]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[8]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[9]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[10]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[11]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[12]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[13]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[14]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[15]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[16]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[17]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[18]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[19]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[20]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[21]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[22]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[23]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[24]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[25]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[26]));

    for (uint i = 0; i < maxParticlesInNeighbors; i++) {
        if (workItemId < 27) {
            uint2 block = localNeighborBlockBuffer[workItemId];

#ifdef USE_SELECT
            bool b = (BLOCK_PARTICLE_COUNT(block) > i);
            float4 position = SELECT4(workItemIdPosition,
                    POSITION(getPosition(positionBuffer, ((BLOCK_FIRST_PARTICLE_INDEX(block) + i) * b))),
                    b);
#else
            float4 position = workItemIdPosition;

            if (BLOCK_PARTICLE_COUNT(block) > i) {
                position = POSITION(getPosition(positionBuffer, (BLOCK_FIRST_PARTICLE_INDEX(block) + i)));
            }
#endif
            localNeighborPositionBuffer[workItemId] = position;
        }

        barrier(CLK_LOCAL_MEM_FENCE);
#ifdef USE_IF
        if (workItemId < thisBlockParticleCount) {
#endif
            for (uint j = 0; j < 27; j++) {

                float4 neighborPosition = localNeighborPositionBuffer[j];
                float4 d = workItemIdPosition - neighborPosition;
                d.w = 0.0f;

                float distanceSquared = DOT(d, d);
                float distance = SQRT(distanceSquared);
                bool tooFarAway = (distance > computationData.supportRadiusScaled);

                bool neighborIsMe = (distanceSquared == 0.0f);

                if (tooFarAway || neighborIsMe) {
                    continue;
                }

                float scaledDistance = distance * (computationData.simulationScale);

                //    float smoothingKernel = Wpoly6(scaledDistance, hSquared, wPoly6Coefficient);
                float x = computationData.supportRadiusSqr - scaledDistance * scaledDistance;
                float smoothingKernel = x * x * x;

                workItemIdDensity += smoothingKernel;
            }
#ifdef USE_IF
        }
#endif

        barrier(CLK_LOCAL_MEM_FENCE);
    }

    if (workItemId < thisBlockParticleCount) {
#ifdef USE_OWN_DENSITY
        workItemIdDensity += (computationData.supportRadiusSqr) * (computationData.supportRadiusSqr) * (computationData.supportRadiusSqr);
#endif
        uint originalIndex = thisBlockFirstParticleIndex + workItemId;
        float density = (computationData.particleMass) * (computationData.wPoly6Coefficient) * workItemIdDensity;
        float densityInv = DIVIDE(1.0f, density);

        densityBuffer[originalIndex] = (float2) (density, densityInv);

        float4 velocity = getVelocity(oldVelocityBuffer, originalIndex);
#ifdef GAS_STIFFNESS
        float drho = density - computationData.restDensity;
        PRESSURE(velocity) = (computationData.stiffness) * drho;
#else
        //   PRESSURE(velocity) = (computationData.gasStiffness) * (density * 0.001f * density * 0.001f - 1.0f);
        PRESSURE(velocity) = (computationData.stiffness) * (density * density * computationData.restDensityInvSqr - 1.0f);

#endif

        setVelocity(newVelocityBuffer, originalIndex, velocity);
        //oldVelocityBuffer[originalIndex] = velocity;
    }
}

//==============================================================================


#ifdef BOUNDARY_1

void handleBoundaryConditions(
        float4 * newVelocity,
        float timeStep,
        float4 * newPosition,
        float damping) {

    if ((*newPosition).x < ((float) XMIN)) {
        (*newPosition).x = ((float) XMIN);
        float4 normal = (float4) (1, 0, 0, 0);
        *newVelocity -= (1.0f + damping) * DOT(*newVelocity, normal) * normal;
    } else if ((*newPosition).x >= ((float) XMAX)) {
        (*newPosition).x = ((float) XMAX) - EPSILON;
        float4 normal = (float4) (-1, 0, 0, 0);
        *newVelocity -= (1.0f + damping) * DOT(*newVelocity, normal) * normal;
    }

    if ((*newPosition).y < ((float) YMIN)) {
        (*newPosition).y = ((float) YMIN);
        float4 normal = (float4) (0, 1, 0, 0);
        *newVelocity -= (1.0f + damping) * DOT(*newVelocity, normal) * normal;
    } else if ((*newPosition).y >= ((float) YMAX)) {
        (*newPosition).y = ((float) YMAX) - EPSILON;
        float4 normal = (float4) (0, -1, 0, 0);
        *newVelocity -= (1.0f + damping) * DOT(*newVelocity, normal) * normal;
    }

    if ((*newPosition).z < ((float) ZMIN)) {
        (*newPosition).z = ((float) ZMIN);
        float4 normal = (float4) (0, 0, 1, 0);
        *newVelocity -= (1.0f + damping) * DOT(*newVelocity, normal) * normal;
    } else if ((*newPosition).z >= ((float) ZMAX)) {
        (*newPosition).z = ((float) ZMAX) - EPSILON;
        float4 normal = (float4) (0, 0, -1, 0);
        *newVelocity -= (1.0f + damping) * DOT(*newVelocity, normal) * normal;
    }

}

#elif defined(BOUNDARY_2)

void handleBoundaryConditions(
        float4* newVelocity,
        float timeStep,
        float4* newPosition,
        float damping) {

    if ((*newPosition).x < ((float) XMIN)) {
        float penetration = -((*newPosition).x);
        (*newPosition).x = ((float) XMIN);

        float4 normal = (float4) (1, 0, 0, 0);

        *newVelocity -= (1.0f + damping * DIVIDE(penetration, timeStep * length(*newVelocity))) * DOT(*newVelocity, normal) * normal;
    } else if ((*newPosition).x >= ((float) XMAX)) {
        float penetration = ((*newPosition).x) - ((float) XMAX);
        (*newPosition).x = ((float) XMAX) - EPSILON;
        float4 normal = (float4) (-1, 0, 0, 0);
        *newVelocity -= (1.0f + damping * DIVIDE(penetration, timeStep * length(*newVelocity))) * DOT(*newVelocity, normal) * normal;
    }

    if ((*newPosition).y < ((float) YMIN)) {
        float penetration = -((*newPosition).y);
        (*newPosition).y = ((float) YMIN);
        float4 normal = (float4) (0, 1, 0, 0);
        *newVelocity -= (1.0f + damping * DIVIDE(penetration, timeStep * length(*newVelocity))) * DOT(*newVelocity, normal) * normal;
    } else if ((*newPosition).y >= ((float) YMAX)) {
        float penetration = ((*newPosition).y) - ((float) YMAX);
        (*newPosition).y = ((float) YMAX) - EPSILON;
        float4 normal = (float4) (0, -1, 0, 0);
        *newVelocity -= (1.0f + damping * DIVIDE(penetration, timeStep * length(*newVelocity))) * DOT(*newVelocity, normal) * normal;
    }

    if ((*newPosition).z < ((float) ZMIN)) {
        float penetration = -((*newPosition).z);
        (*newPosition).z = ((float) ZMIN);
        float4 normal = (float4) (0, 0, 1, 0);
        *newVelocity -= (1.0f + damping * DIVIDE(penetration, timeStep * length(*newVelocity))) * DOT(*newVelocity, normal) * normal;
    } else if ((*newPosition).z >= ((float) ZMAX)) {
        float penetration = ((*newPosition).z) - ((float) ZMAX);
        (*newPosition).z = ((float) ZMAX) - EPSILON;
        float4 normal = (float4) (0, 0, -1, 0);
        *newVelocity -= (1.0f + damping * DIVIDE(penetration, timeStep * length(*newVelocity))) * DOT(*newVelocity, normal) * normal;
    }

}

#endif


//==============================================================================

//from http://www.cs.rit.edu/~ncs/color/t_convert.html
//The hue value H runs from 0 to 360ş. 
//The saturation S is the degree of strength or purity and is from 0 to 1. 
//Purity is how much white is added to the color, so S=1 makes the purest color (no white). 
//Brightness V also ranges from 0 to 1, where 0 is the black.

float4 HSVtoRGB(float h, float s, float v) {
    float r = 0, g = 0, b = 0;
    int i;
    float f, p, q, t;
    if (s == 0) {
        // achromatic (grey)
        r = g = b = v;
        return (float4) (r, g, b, 1);
    }
    h /= 60; // sector 0 to 5
    i = floor(h);
    f = h - i; // factorial part of h
    p = v * (1.0f - s);
    q = v * (1.0f - s * f);
    t = v * (1.0f - s * (1.0f - f));
    switch (i) {
        case 0:
            r = v;
            g = t;
            b = p;
            break;
        case 1:
            r = q;
            g = v;
            b = p;
            break;
        case 2:
            r = p;
            g = v;
            b = t;
            break;
        case 3:
            r = p;
            g = q;
            b = v;
            break;
        case 4:
            r = t;
            g = p;
            b = v;
            break;
        default: // case 5:
            r = v;
            g = p;
            b = q;
            break;
    }

    return (float4) (r, g, b, 1);
}

float4 calculateColor(ColoringGradient coloringGradient, float colorScalar) {
    float4 color = (float4) (0, 0, 0, 1);
    switch (coloringGradient) {
        case White:
            // completely white
        {
            color = (float4) (1, 1, 1, 1);
        }
            break;
        case Blackish:
            // acromatic gradient with V from 0 to 0.5
        {
            float h = colorScalar * 0.5f;
            color = (float4) (h, h, h, 1);
        }
            break;
        case BlackToCyan:
        {
            color = (float4) (0, colorScalar, colorScalar, 1);
        }
            break;
        case BlueToWhite:
            // blue to white gradient
        {
            color = (float4) (1 - colorScalar, 1 - 0.5f * colorScalar, 1, 1);
        }
            break;
        case HSVBlueToRed:
            // hsv gradient from blue to red (0 to 245 degrees in hue)
        {
            float h = clamp((1 - colorScalar)*245.0f, 0.0f, 245.0f);
            color = HSVtoRGB(h, 0.8f, 1);
        }
            break;
    }
    return color;
}

typedef struct {
    float gradWspikyCoefficient;
    float del2WviscosityCoefficient;
    float particleMass;
    float simulationScale;
    float simulationScaleInv;
    float supportRadiusScaled;
    float supportRadius;
    float viscosity;
    float accelerationLimit;
    float gravityX;
    float gravityY;
    float gravityZ;
    float timeStep;
    float damping;
    ColoringSource coloringSource;
    ColoringGradient coloringGradient;
} ForceComputationData __attribute__((aligned(STRUCT_ALIGNMENT)));

__kernel void computeForcesAndIntegrate(
        __constant int4* neighborLookup,
        __constant uint4* interleaveLookup,
        __read_only image2d_t blockCoordsLookup,
        __read_only image2d_t oldPositionBuffer,
        __read_only image2d_t oldVelocityBuffer,
        __global const uint2* blockBuffer,
        __global const uint2* compactBlockBuffer,
        __global const float2* densityBuffer,
        __write_only image2d_t newPositionBuffer,
        __write_only image2d_t newVelocityBuffer,
        __global float4* colorBuffer,
        __local float4* localNeighborPositionBuffer, // sizeof(float4) * 32 (but we need only 27)
        __local float4* localNeighborVelocityBuffer, // sizeof(float4) * 32 (but we need only 27)
        __local float2* localNeighborDensityBuffer, // sizeof(float2) * 32 (but we need only 27)
        __local uint2* localNeighborBlockBuffer, // sizeof(uint2) * 32 (but we need only 27)
        const ForceComputationData computationData) {

    int workGroupId = get_group_id(0);
    int workItemId = get_local_id(0);

    uint2 thisBlock = compactBlockBuffer[workGroupId];

    uint thisBlockParticleCount = BLOCK_PARTICLE_COUNT(thisBlock); // N
    uint thisBlockFirstParticleIndex = BLOCK_FIRST_PARTICLE_INDEX(thisBlock);
    PositionVect thisBlockFirstParticlePos = getPosition(oldPositionBuffer, (thisBlockFirstParticleIndex));

    uint thisBlockIndex = Z_INDEX(thisBlockFirstParticlePos); //  >> BLOCK_SIZE_3D_LOG_2;

    float4 pressureForce = (float4) (0, 0, 0, 0);
    float4 viscosityForce = (float4) (0, 0, 0, 0);

#ifdef USE_SELECT
    bool b = (workItemId < thisBlockParticleCount);
    uint index = (thisBlockFirstParticleIndex + workItemId) * b;
    float4 zeroVect = (float4) (0, 0, 0, 0);

    float4 workItemIdPosition = SELECT4(zeroVect,
            POSITION(getPosition(oldPositionBuffer, (index))), b);

    float4 workItemIdVelocity = SELECT4(zeroVect,
            getVelocity(oldVelocityBuffer, (index)), b);

    float workItemIdPressure = PRESSURE(workItemIdVelocity);
#else
    float4 workItemIdPosition = (float4) (0, 0, 0, 0);
    float4 workItemIdVelocity = (float4) (0, 0, 0, 0);
    float workItemIdPressure = 0;

    if (workItemId < thisBlockParticleCount) {
        uint index = thisBlockFirstParticleIndex + workItemId;
        workItemIdPosition = POSITION(getPosition(oldPositionBuffer, (index)));
        workItemIdVelocity = getVelocity(oldVelocityBuffer, (index));
        workItemIdPressure = PRESSURE(workItemIdVelocity);
    }
#endif

    if (workItemId < 27) {
        loadNeighborBlockData(workItemId, thisBlockIndex, neighborLookup, interleaveLookup,
                blockCoordsLookup, blockBuffer, localNeighborBlockBuffer);
    }

    barrier(CLK_LOCAL_MEM_FENCE);

    uint maxParticlesInNeighbors = BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[0]);
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[1]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[2]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[3]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[4]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[5]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[6]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[7]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[8]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[9]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[10]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[11]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[12]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[13]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[14]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[15]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[16]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[17]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[18]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[19]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[20]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[21]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[22]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[23]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[24]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[25]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[26]));

    for (uint i = 0; i < maxParticlesInNeighbors; i++) {
        if (workItemId < 27) {
            uint2 block = localNeighborBlockBuffer[workItemId];

#ifdef USE_SELECT
            bool b = (BLOCK_PARTICLE_COUNT(block) > i);
            uint index = (BLOCK_FIRST_PARTICLE_INDEX(block) + i) * b;

            localNeighborPositionBuffer[workItemId] = SELECT4(workItemIdPosition,
                    POSITION(getPosition(oldPositionBuffer, (index))), b);

            localNeighborVelocityBuffer[workItemId] = SELECT4(zeroVect,
                    getVelocity(oldVelocityBuffer, (index)), b);

            localNeighborDensityBuffer[workItemId] = SELECT2(((float2) (0.0f, 0.0f)),
                    densityBuffer[index], b);
#else
            float4 position = workItemIdPosition;
            float4 velocity = (float4) (0, 0, 0, 0);
            float2 density = (float2) (0, 0);

            if (BLOCK_PARTICLE_COUNT(block) > i) {
                uint index = BLOCK_FIRST_PARTICLE_INDEX(block) + i;
                density = densityBuffer[index];
                position = POSITION(getPosition(oldPositionBuffer, (index)));
                velocity = getVelocity(oldVelocityBuffer, (index));
            }

            localNeighborPositionBuffer[workItemId] = position;
            localNeighborVelocityBuffer[workItemId] = velocity;
            localNeighborDensityBuffer[workItemId] = density;
#endif
        }

        barrier(CLK_LOCAL_MEM_FENCE);
#ifdef USE_IF
        if (workItemId < thisBlockParticleCount) {
#endif
            for (uint j = 0; j < 27; j++) {
                float4 neighborPosition = localNeighborPositionBuffer[j];
                float4 rVec = workItemIdPosition - neighborPosition;
                rVec.w = 0.0f;

                float distanceSquared = DOT(rVec, rVec);

                bool neighborIsMe = (distanceSquared == 0.0f);
                float distance = SQRT(distanceSquared);
                bool tooFarAway = (distance > computationData.supportRadiusScaled);

                if (tooFarAway || neighborIsMe) {
                    continue;
                }

                float scaledDistance = distance * (computationData.simulationScale);

                float4 neighborVelocity = localNeighborVelocityBuffer[j];
                float2 neighborDensity = localNeighborDensityBuffer[j];

                float neighborPressure = PRESSURE(neighborVelocity);
                float neighborDensityInv = DENSITY_INVERSE(neighborDensity);

                //--------------------------------------------------------------
                // float4 rVec = workItemIdPosition - neighborPosition;
                float4 scaledVec = rVec * (computationData.simulationScale);
                scaledVec /= scaledDistance;
                //rVec.w = 0.0f;
                float x = computationData.supportRadius - scaledDistance;
                float4 gradWspiky = x * x * scaledVec;

                pressureForce += (workItemIdPressure + neighborPressure) * neighborDensityInv * gradWspiky;

                //--------------------------------------------------------------              
                float del2Wviscosity = (computationData.supportRadius - scaledDistance);
                viscosityForce += (neighborVelocity - workItemIdVelocity) * neighborDensityInv * del2Wviscosity;
                //--------------------------------------------------------------
            }
#ifdef USE_IF
        }
#endif

        barrier(CLK_LOCAL_MEM_FENCE);
    }

    if (workItemId < thisBlockParticleCount) {
        uint originalIndex = thisBlockFirstParticleIndex + workItemId;

        float2 workItemIdDensity = densityBuffer[originalIndex];

        float densityInv = DENSITY_INVERSE(workItemIdDensity);
        pressureForce *= computationData.particleMass * computationData.gradWspikyCoefficient * 0.5f;
        viscosityForce *= computationData.viscosity * computationData.particleMass * computationData.del2WviscosityCoefficient;

        float4 acceleration = densityInv * (viscosityForce - pressureForce);

#ifdef CHECK_ACCELERATION_LIMIT
        float magnitude = acceleration.x * acceleration.x + acceleration.y * acceleration.y + acceleration.z * acceleration.z;
        float speed = SQRT(magnitude);
        bool tooBig = (speed > computationData.accelerationLimit); //magnitude > computationData.speedLimit * computationData.speedLimit);

        float scale = computationData.accelerationLimit / speed;
        acceleration = SELECT4(acceleration, acceleration * scale, tooBig);
#endif

        acceleration.w = 0.0f;


        /*----------------------------------*/
        /* INTEGRATE */
        /*----------------------------------*/

        // apply external forces
        float4 gravity = (float4) (computationData.gravityX, computationData.gravityY, computationData.gravityZ, 0.0f);
        acceleration += gravity;
        //  acceleration_ = gravity;
#ifdef USE_LEAP_FROG
        // Leapfrog integration		
        // v(t+1/2) = v(t-1/2) + a(t) dt	
        float4 newVelocity_ = workItemIdVelocity + computationData.timeStep * acceleration;
        float posTimeStep = computationData.timeStep * computationData.simulationScaleInv;
        // v(t+1) = [v(t-1/2) + v(t+1/2)] * 0.5
        float4 newPosition_ = workItemIdPosition + posTimeStep * (newVelocity_ + workItemIdVelocity) * 0.5f;
#else
        // Semi-implicit Euler integration 
        float4 newVelocity_ = workItemIdVelocity + computationData.timeStep * acceleration;
        float posTimeStep = computationData.timeStep * computationData.simulationScaleInv;
        float4 newPosition_ = workItemIdPosition + posTimeStep * newVelocity_;
#endif

        newVelocity_.w = 0;
        handleBoundaryConditions(&newVelocity_, posTimeStep, &newPosition_,
                computationData.damping);


        //__global float4* newPosition = &(newPositionBuffer[originalIndex]);
        // __global float4* newVelocity = &(newVelocityBuffer[originalIndex]);
        //  *newVelocity = newVelocity_;
        setVelocity(newVelocityBuffer, originalIndex, newVelocity_);
        newPosition_.w = 1.0f; // homogeneous coordinate for rendering
        //*newPosition = newPosition_;
        setPosition(newPositionBuffer, originalIndex, newPosition_);
        /*==================================*/

#ifdef USE_COLORING

        float4 color;
        switch (computationData.coloringSource) {
            case VELOCITY:
            {
                float colorScalar = (fabs(newVelocity_.x) + fabs(newVelocity_.y) + fabs(newVelocity_.z));
                colorScalar = clamp(colorScalar, 0.0f, 1.0f);
                //  color = (float4) (1.0f - colorScalar, 1.0f - 0.5f * colorScalar, 1.0f, 1.0f);
                color = calculateColor(computationData.coloringGradient, colorScalar);
                break;
            }
            case PRESSURE:
            {
                float colorScalar = clamp(((workItemIdPressure) / 1500.0f), 0.0f, 1.0f);
                color = calculateColor(computationData.coloringGradient, colorScalar);
                break;
            }
            case FORCE:
            {
                //acceleration /= 80.0f;
                float colorScalar = clamp((acceleration.x + acceleration.y + acceleration.z) * 0.33333f, 0.1f, 1.0f);
                color = calculateColor(computationData.coloringGradient, colorScalar);
                break;
            }
        }

        colorBuffer[originalIndex] = color;
#endif
    }

}

typedef struct {
    float gradWspikyCoefficient;
    float del2WviscosityCoefficient;
    float particleMass;
    float simulationScale;
    float simulationScaleInv;
    float supportRadiusScaled;
    float supportRadius;
    float viscosity;
    float accelerationLimit;
    float gravityX;
    float gravityY;
    float gravityZ;
    float timeStep;
    float damping;
    ColoringSource coloringSource;
    ColoringGradient coloringGradient;
    float distThreshold;
    uint minParticles;
    float boundaryOffset;
} ForceComputationDataExtract __attribute__((aligned(STRUCT_ALIGNMENT)));

__kernel void computeForcesAndIntegrateExtract(
        __constant int4* neighborLookup,
        __constant uint4* interleaveLookup,
        __read_only image2d_t blockCoordsLookup,
        __read_only image2d_t oldPositionBuffer,
        __read_only image2d_t oldVelocityBuffer,
        __global const uint2* blockBuffer,
        __global const uint2* compactBlockBuffer,
        __global const float2* densityBuffer,
        __write_only image2d_t newPositionBuffer,
        __write_only image2d_t newVelocityBuffer,
        __global float4* colorBuffer,
        __global volatile uint* gpu2cpuBuffer,
        __global float4* surfacePositionBuffer,
        __local float4* localNeighborPositionBuffer, // sizeof(float4) * 32 (but we need only 27)
        __local float4* localNeighborVelocityBuffer, // sizeof(float4) * 32 (but we need only 27)
        __local float2* localNeighborDensityBuffer, // sizeof(float2) * 32 (but we need only 27)
        __local uint2* localNeighborBlockBuffer, // sizeof(uint2) * 32 (but we need only 27)
        const ForceComputationDataExtract computationData) {

    int workGroupId = get_group_id(0);
    int workItemId = get_local_id(0);

    uint2 thisBlock = compactBlockBuffer[workGroupId];

    uint thisBlockParticleCount = BLOCK_PARTICLE_COUNT(thisBlock); // N
    uint thisBlockFirstParticleIndex = BLOCK_FIRST_PARTICLE_INDEX(thisBlock);
    PositionVect thisBlockFirstParticlePos = getPosition(oldPositionBuffer, (thisBlockFirstParticleIndex));

    uint thisBlockIndex = Z_INDEX(thisBlockFirstParticlePos); //  >> BLOCK_SIZE_3D_LOG_2;

    float4 pressureForce = (float4) (0, 0, 0, 0);
    float4 viscosityForce = (float4) (0, 0, 0, 0);

#ifdef USE_SELECT
    bool b = (workItemId < thisBlockParticleCount);
    uint index = (thisBlockFirstParticleIndex + workItemId) * b;
    float4 zeroVect = (float4) (0, 0, 0, 0);

    float4 workItemIdPosition = SELECT4(zeroVect,
            POSITION(getPosition(oldPositionBuffer, (index))), b);

    float4 workItemIdVelocity = SELECT4(zeroVect,
            getVelocity(oldVelocityBuffer, (index)), b);

    float workItemIdPressure = PRESSURE(workItemIdVelocity);
#else
    float4 workItemIdPosition = (float4) (0, 0, 0, 0);
    float4 workItemIdVelocity = (float4) (0, 0, 0, 0);
    float workItemIdPressure = 0;

    if (workItemId < thisBlockParticleCount) {
        uint index = thisBlockFirstParticleIndex + workItemId;
        workItemIdPosition = POSITION(getPosition(oldPositionBuffer, (index)));
        workItemIdVelocity = getVelocity(oldVelocityBuffer, (index));
        workItemIdPressure = PRESSURE(workItemIdVelocity);
    }
#endif

    if (workItemId < 27) {
        loadNeighborBlockData(workItemId, thisBlockIndex, neighborLookup, interleaveLookup,
                blockCoordsLookup, blockBuffer, localNeighborBlockBuffer);
    }

    barrier(CLK_LOCAL_MEM_FENCE);

    uint maxParticlesInNeighbors = BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[0]);
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[1]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[2]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[3]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[4]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[5]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[6]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[7]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[8]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[9]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[10]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[11]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[12]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[13]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[14]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[15]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[16]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[17]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[18]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[19]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[20]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[21]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[22]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[23]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[24]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[25]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[26]));

    float4 centerOfMass = workItemIdPosition;
    //(float4)(0, 0, 0, 0);
    uint totalNeighbors = 1;
    for (uint i = 0; i < maxParticlesInNeighbors; i++) {
        if (workItemId < 27) {
            uint2 block = localNeighborBlockBuffer[workItemId];

#ifdef USE_SELECT
            // uint particleCount = BLOCK_PARTICLE_COUNT(block);
            //totalNeighbors += particleCount;

            bool b = (BLOCK_PARTICLE_COUNT(block) > i);
            uint index = (BLOCK_FIRST_PARTICLE_INDEX(block) + i) * b;

            localNeighborPositionBuffer[workItemId] = SELECT4(workItemIdPosition,
                    POSITION(getPosition(oldPositionBuffer, (index))), b);

            localNeighborVelocityBuffer[workItemId] = SELECT4(zeroVect,
                    getVelocity(oldVelocityBuffer, (index)), b);

            localNeighborDensityBuffer[workItemId] = SELECT2(((float2) (0.0f, 0.0f)),
                    densityBuffer[index], b);
#else
            float4 position = workItemIdPosition;
            float4 velocity = (float4) (0, 0, 0, 0);
            float2 density = (float2) (0, 0);

            if (BLOCK_PARTICLE_COUNT(block) > i) {
                uint index = BLOCK_FIRST_PARTICLE_INDEX(block) + i;
                density = densityBuffer[index];
                position = POSITION(getPosition(oldPositionBuffer, (index)));
                velocity = getVelocity(oldVelocityBuffer, (index));
            }

            localNeighborPositionBuffer[workItemId] = position;
            localNeighborVelocityBuffer[workItemId] = velocity;
            localNeighborDensityBuffer[workItemId] = density;
#endif
        }

        barrier(CLK_LOCAL_MEM_FENCE);
#ifdef USE_IF
        if (workItemId < thisBlockParticleCount) {
#endif
            for (uint j = 0; j < 27; j++) {
                float4 neighborPosition = localNeighborPositionBuffer[j];
                float4 rVec = workItemIdPosition - neighborPosition;
                rVec.w = 0.0f;

                float distanceSquared = DOT(rVec, rVec);

                bool neighborIsMe = (distanceSquared == 0.0f);
                float distance = SQRT(distanceSquared);
                bool tooFarAway = (distance > computationData.supportRadiusScaled);

                if (tooFarAway || neighborIsMe) {
                    continue;
                }

                centerOfMass += neighborPosition;
                totalNeighbors += 1;

                float scaledDistance = distance * (computationData.simulationScale);

                float4 neighborVelocity = localNeighborVelocityBuffer[j];
                float2 neighborDensity = localNeighborDensityBuffer[j];

                float neighborPressure = PRESSURE(neighborVelocity);
                float neighborDensityInv = DENSITY_INVERSE(neighborDensity);

                //--------------------------------------------------------------
                //  float4 rVec = workItemIdPosition - neighborPosition;
                float4 scaledVec = rVec * (computationData.simulationScale);
                scaledVec /= scaledDistance;
                //   rVec.w = 0.0f;
                float x = computationData.supportRadius - scaledDistance;
                float4 gradWspiky = x * x * scaledVec;

                pressureForce += (workItemIdPressure + neighborPressure) * neighborDensityInv * gradWspiky;

                //--------------------------------------------------------------              
                float del2Wviscosity = (computationData.supportRadius - scaledDistance);
                viscosityForce += (neighborVelocity - workItemIdVelocity) * neighborDensityInv * del2Wviscosity;
                //--------------------------------------------------------------
            }
#ifdef USE_IF
        }
#endif

        barrier(CLK_LOCAL_MEM_FENCE);
    }

    if (workItemId < thisBlockParticleCount) {
        uint originalIndex = thisBlockFirstParticleIndex + workItemId;

        //#################################        
        float divisor = (float) (totalNeighbors);
        float4 rVec = DIVIDE(centerOfMass, (float4) (divisor, divisor, divisor, divisor)) - workItemIdPosition;
        rVec.w = 0.0f;
        float distanceSquared = DOT(rVec, rVec);
        float distance = SQRT(distanceSquared);

        bool isSurface = totalNeighbors < computationData.minParticles
                || distance > computationData.distThreshold;
        //        float maxRange = (float) GRID_DIMENSION_X - computationData.boundaryOffset;
        //        isSurface &= workItemIdPosition.x > computationData.boundaryOffset
        //                && workItemIdPosition.y > computationData.boundaryOffset
        //                && workItemIdPosition.z > computationData.boundaryOffset
        //                && workItemIdPosition.x < maxRange
        //                && workItemIdPosition.y < maxRange
        //                && workItemIdPosition.z < maxRange;

        float4 zero = (float4) (0.0f, 0.0f, 0.0f, 0.0f);
        workItemIdPosition.w = 1.0f;
        surfacePositionBuffer[originalIndex] = SELECT4(zero, workItemIdPosition, isSurface);
        //#################################

        float2 workItemIdDensity = densityBuffer[originalIndex];

        float densityInv = DENSITY_INVERSE(workItemIdDensity);
        pressureForce *= computationData.particleMass * computationData.gradWspikyCoefficient * 0.5f;
        viscosityForce *= computationData.viscosity * computationData.particleMass * computationData.del2WviscosityCoefficient;

        float4 acceleration = densityInv * (viscosityForce - pressureForce);

#ifdef CHECK_ACCELERATION_LIMIT
        float magnitude = acceleration.x * acceleration.x + acceleration.y * acceleration.y + acceleration.z * acceleration.z;
        float speed = SQRT(magnitude);
        bool tooBig = (speed > computationData.accelerationLimit); //magnitude > computationData.speedLimit * computationData.speedLimit);

        float scale = computationData.accelerationLimit / speed;
        acceleration = SELECT4(acceleration, acceleration * scale, tooBig);
#endif

        acceleration.w = 0.0f;


        /*----------------------------------*/
        /* INTEGRATE */
        /*----------------------------------*/

        // apply external forces
        float4 gravity = (float4) (computationData.gravityX, computationData.gravityY, computationData.gravityZ, 0.0f);
        acceleration += gravity;
        //  acceleration_ = gravity;
#ifdef USE_LEAP_FROG
        // Leapfrog integration		
        // v(t+1/2) = v(t-1/2) + a(t) dt	
        float4 newVelocity_ = workItemIdVelocity + computationData.timeStep * acceleration;
        float posTimeStep = computationData.timeStep * computationData.simulationScaleInv;
        // v(t+1) = [v(t-1/2) + v(t+1/2)] * 0.5
        float4 newPosition_ = workItemIdPosition + posTimeStep * (newVelocity_ + workItemIdVelocity) * 0.5f;
#else
        // Semi-implicit Euler integration 
        float4 newVelocity_ = workItemIdVelocity + computationData.timeStep * acceleration;
        float posTimeStep = computationData.timeStep * computationData.simulationScaleInv;
        float4 newPosition_ = workItemIdPosition + posTimeStep * newVelocity_;
#endif

        newVelocity_.w = 0;
        handleBoundaryConditions(&newVelocity_, posTimeStep, &newPosition_,
                computationData.damping);


        //__global float4* newPosition = &(newPositionBuffer[originalIndex]);
        // __global float4* newVelocity = &(newVelocityBuffer[originalIndex]);
        //  *newVelocity = newVelocity_;
        setVelocity(newVelocityBuffer, originalIndex, newVelocity_);
        newPosition_.w = 1.0f; // homogeneous coordinate for rendering
        //*newPosition = newPosition_;
        setPosition(newPositionBuffer, originalIndex, newPosition_);
        /*==================================*/

#ifdef USE_COLORING

        float4 color;
        switch (computationData.coloringSource) {
            case VELOCITY:
            {
                float colorScalar = (fabs(newVelocity_.x) + fabs(newVelocity_.y) + fabs(newVelocity_.z));
                colorScalar = clamp(colorScalar, 0.0f, 1.0f);
                //  color = (float4) (1.0f - colorScalar, 1.0f - 0.5f * colorScalar, 1.0f, 1.0f);
                color = calculateColor(computationData.coloringGradient, colorScalar);
                break;
            }
            case PRESSURE:
            {
                float colorScalar = clamp(((workItemIdPressure) / 1500.0f), 0.0f, 1.0f);
                color = calculateColor(computationData.coloringGradient, colorScalar);
                break;
            }
            case FORCE:
            {
                //acceleration /= 80.0f;
                float colorScalar = clamp((acceleration.x + acceleration.y + acceleration.z) / 3.0f, 0.1f, 1.0f);
                color = calculateColor(computationData.coloringGradient, colorScalar);
                break;
            }
        }

        colorBuffer[originalIndex] = color;
#endif
    }
}

//==============================================================================

typedef struct {
    float gradWPoly6Coefficient;
    float gradWspikyCoefficient;
    float del2WviscosityCoefficient;
    float particleMass;
    float simulationScale;
    float simulationScaleInv;
    float supportRadiusScaled;
    float supportRadiusSqr;
    float supportRadius;
    float viscosity;
    float accelerationLimit;
    float gravityX;
    float gravityY;
    float gravityZ;
    float timeStep;
    float damping;
    ColoringSource coloringSource;
    ColoringGradient coloringGradient;
    float colorFieldThreshold;
    float del2WPoly6Cofficient;
    float surfaceTensionCoeff;
} ForceComputationDataColorField __attribute__((aligned(STRUCT_ALIGNMENT)));

__kernel void computeForcesAndIntegrateColorField(
        __constant int4* neighborLookup,
        __constant uint4* interleaveLookup,
        __read_only image2d_t blockCoordsLookup,
        __read_only image2d_t oldPositionBuffer,
        __read_only image2d_t oldVelocityBuffer,
        __global const uint2* blockBuffer,
        __global const uint2* compactBlockBuffer,
        __global const float2* densityBuffer,
        __write_only image2d_t newPositionBuffer,
        __write_only image2d_t newVelocityBuffer,
        __global float4* colorBuffer,
        __global volatile uint* gpu2cpuBuffer,
        __global float4* surfacePositionBuffer,
        __local float4* localNeighborPositionBuffer, // sizeof(float4) * 32 (but we need only 27)
        __local float4* localNeighborVelocityBuffer, // sizeof(float4) * 32 (but we need only 27)
        __local float2* localNeighborDensityBuffer, // sizeof(float2) * 32 (but we need only 27)
        __local uint2* localNeighborBlockBuffer, // sizeof(uint2) * 32 (but we need only 27)
        const ForceComputationDataColorField computationData) {

    int workGroupId = get_group_id(0);
    int workItemId = get_local_id(0);

    uint2 thisBlock = compactBlockBuffer[workGroupId];

    uint thisBlockParticleCount = BLOCK_PARTICLE_COUNT(thisBlock); // N
    uint thisBlockFirstParticleIndex = BLOCK_FIRST_PARTICLE_INDEX(thisBlock);
    PositionVect thisBlockFirstParticlePos = getPosition(oldPositionBuffer, (thisBlockFirstParticleIndex));

    uint thisBlockIndex = Z_INDEX(thisBlockFirstParticlePos); //  >> BLOCK_SIZE_3D_LOG_2;

    float4 pressureForce = (float4) (0, 0, 0, 0);
    float4 viscosityForce = (float4) (0, 0, 0, 0);

#ifdef USE_SELECT
    bool b = (workItemId < thisBlockParticleCount);
    uint index = (thisBlockFirstParticleIndex + workItemId) * b;
    float4 zeroVect = (float4) (0, 0, 0, 0);

    float4 workItemIdPosition = SELECT4(zeroVect,
            POSITION(getPosition(oldPositionBuffer, (index))), b);

    float4 workItemIdVelocity = SELECT4(zeroVect,
            getVelocity(oldVelocityBuffer, (index)), b);

    float workItemIdPressure = PRESSURE(workItemIdVelocity);
#else
    float4 workItemIdPosition = (float4) (0, 0, 0, 0);
    float4 workItemIdVelocity = (float4) (0, 0, 0, 0);
    float workItemIdPressure = 0;

    if (workItemId < thisBlockParticleCount) {
        uint index = thisBlockFirstParticleIndex + workItemId;
        workItemIdPosition = POSITION(getPosition(oldPositionBuffer, (index)));
        workItemIdVelocity = getVelocity(oldVelocityBuffer, (index));
        workItemIdPressure = PRESSURE(workItemIdVelocity);
    }
#endif

    if (workItemId < 27) {
        loadNeighborBlockData(workItemId, thisBlockIndex, neighborLookup, interleaveLookup,
                blockCoordsLookup, blockBuffer, localNeighborBlockBuffer);
    }

    barrier(CLK_LOCAL_MEM_FENCE);

    uint maxParticlesInNeighbors = BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[0]);
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[1]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[2]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[3]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[4]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[5]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[6]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[7]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[8]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[9]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[10]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[11]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[12]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[13]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[14]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[15]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[16]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[17]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[18]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[19]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[20]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[21]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[22]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[23]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[24]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[25]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[26]));

    //float4 centerOfMass = workItemIdPosition;
    //(float4)(0, 0, 0, 0);
    //uint totalNeighbors = 1;
    float4 gradColorField = (float4) (0, 0, 0, 0);

#ifdef USE_SURFACE_TENSION
    float del2ColorField = 0.0f;
#endif

    for (uint i = 0; i < maxParticlesInNeighbors; i++) {
        if (workItemId < 27) {
            uint2 block = localNeighborBlockBuffer[workItemId];

#ifdef USE_SELECT
            // uint particleCount = BLOCK_PARTICLE_COUNT(block);
            //totalNeighbors += particleCount;

            bool b = (BLOCK_PARTICLE_COUNT(block) > i);
            uint index = (BLOCK_FIRST_PARTICLE_INDEX(block) + i) * b;

            localNeighborPositionBuffer[workItemId] = SELECT4(workItemIdPosition,
                    POSITION(getPosition(oldPositionBuffer, (index))), b);

            localNeighborVelocityBuffer[workItemId] = SELECT4(zeroVect,
                    getVelocity(oldVelocityBuffer, (index)), b);

            localNeighborDensityBuffer[workItemId] = SELECT2(((float2) (0.0f, 0.0f)),
                    densityBuffer[index], b);
#else
            float4 position = workItemIdPosition;
            float4 velocity = (float4) (0, 0, 0, 0);
            float2 density = (float2) (0, 0);

            if (BLOCK_PARTICLE_COUNT(block) > i) {
                uint index = BLOCK_FIRST_PARTICLE_INDEX(block) + i;
                density = densityBuffer[index];
                position = POSITION(getPosition(oldPositionBuffer, (index)));
                velocity = getVelocity(oldVelocityBuffer, (index));
            }

            localNeighborPositionBuffer[workItemId] = position;
            localNeighborVelocityBuffer[workItemId] = velocity;
            localNeighborDensityBuffer[workItemId] = density;
#endif
        }

        barrier(CLK_LOCAL_MEM_FENCE);
#ifdef USE_IF
        if (workItemId < thisBlockParticleCount) {
#endif
            for (uint j = 0; j < 27; j++) {
                float4 neighborPosition = localNeighborPositionBuffer[j];
                float4 rVec = workItemIdPosition - neighborPosition;
                rVec.w = 0.0f;

                float distanceSquared = DOT(rVec, rVec);

                bool neighborIsMe = (distanceSquared == 0.0f);
                float distance = SQRT(distanceSquared);
                bool tooFarAway = (distance > computationData.supportRadiusScaled);

                if (tooFarAway || neighborIsMe) {
                    continue;
                }

                //  centerOfMass += neighborPosition;
                //  totalNeighbors += 1;

                float scaledDistance = distance * (computationData.simulationScale);

                float4 neighborVelocity = localNeighborVelocityBuffer[j];
                float2 neighborDensity = localNeighborDensityBuffer[j];

                float neighborPressure = PRESSURE(neighborVelocity);
                float neighborDensityInv = DENSITY_INVERSE(neighborDensity);

                //--------------------------------------------------------------
                // PRESSURE:
                //  float4 rVec = workItemIdPosition - neighborPosition;
                float4 scaledVec = rVec * (computationData.simulationScale);
                scaledVec /= scaledDistance;
                //rVec.w = 0.0f;
                float x = computationData.supportRadius - scaledDistance;
                float4 gradWspiky = x * x * scaledVec;

                pressureForce += (workItemIdPressure + neighborPressure) * neighborDensityInv * gradWspiky;

                //--------------------------------------------------------------              
                // VISCOSITY:
                float del2Wviscosity = (computationData.supportRadius - scaledDistance);
                viscosityForce += (neighborVelocity - workItemIdVelocity) * neighborDensityInv * del2Wviscosity;
                //--------------------------------------------------------------
                // COLOR_FIELD:
                float rSquare = scaledDistance * scaledDistance;
                float alpha = computationData.supportRadiusSqr - rSquare;
                float gradWPoly6 = alpha * alpha;
                //gradWPoly6 = gradWPoly6 * gradWPoly6;
                gradColorField += scaledVec * neighborDensityInv * gradWPoly6;
                //--------------------------------------------------------------
#ifdef USE_SURFACE_TENSION
                // SURFACE TENSION:
                del2ColorField += neighborDensityInv * alpha * (3.0f * computationData.supportRadiusSqr - 7.0f * rSquare);
#endif
            }
#ifdef USE_IF
        }
#endif

        barrier(CLK_LOCAL_MEM_FENCE);
    }

    if (workItemId < thisBlockParticleCount) {
        uint originalIndex = thisBlockFirstParticleIndex + workItemId;

        //#################################
        // Color Field:
        gradColorField *= computationData.particleMass * computationData.gradWPoly6Coefficient;
        float gcfMagnitudeSquared = DOT(gradColorField, gradColorField);
        float gcfMagnitude = SQRT(gcfMagnitudeSquared);
        bool isSurface = (gcfMagnitude > computationData.colorFieldThreshold);

        float4 zero = (float4) (0.0f, 0.0f, 0.0f, 0.0f);
        workItemIdPosition.w = 1.0f;

        surfacePositionBuffer[originalIndex] = SELECT4(zero, workItemIdPosition, isSurface);
#ifdef USE_SURFACE_TENSION
        //#################################
        // Surface Tension:
        float4 surfaceTensionForce = (float4) (0, 0, 0, 0);
        if (isSurface) {
            //if (gcfMagnitude > 0.001f) {
            del2ColorField *= computationData.particleMass * computationData.del2WPoly6Cofficient;
            //del2ColorField = select(0.1f, del2ColorField, (del2ColorField == 0));
            surfaceTensionForce = -computationData.surfaceTensionCoeff * del2ColorField * gradColorField / gcfMagnitude;
        }
        //#################################
#endif

        float2 workItemIdDensity = densityBuffer[originalIndex];

        float densityInv = DENSITY_INVERSE(workItemIdDensity);
        pressureForce *= computationData.particleMass * computationData.gradWspikyCoefficient * 0.5f;
        viscosityForce *= computationData.viscosity * computationData.particleMass * computationData.del2WviscosityCoefficient;

#ifdef USE_SURFACE_TENSION
        float4 acceleration = densityInv * (viscosityForce - pressureForce) + surfaceTensionForce;
#else
        float4 acceleration = densityInv * (viscosityForce - pressureForce);
#endif

#ifdef CHECK_ACCELERATION_LIMIT
        float magnitude = acceleration.x * acceleration.x + acceleration.y * acceleration.y + acceleration.z * acceleration.z;
        float speed = SQRT(magnitude);
        bool tooBig = (speed > computationData.accelerationLimit); //magnitude > computationData.speedLimit * computationData.speedLimit);

        float scale = computationData.accelerationLimit / speed;
        acceleration = SELECT4(acceleration, acceleration * scale, tooBig);
#endif

        acceleration.w = 0.0f;


        /*----------------------------------*/
        /* INTEGRATE */
        /*----------------------------------*/

        // apply external forces
        float4 gravity = (float4) (computationData.gravityX, computationData.gravityY, computationData.gravityZ, 0.0f);
        acceleration += gravity;
        //  acceleration_ = gravity;
#ifdef USE_LEAP_FROG
        // Leapfrog integration		
        // v(t+1/2) = v(t-1/2) + a(t) dt	
        float4 newVelocity_ = workItemIdVelocity + computationData.timeStep * acceleration;
        float posTimeStep = computationData.timeStep * computationData.simulationScaleInv;
        // v(t+1) = [v(t-1/2) + v(t+1/2)] * 0.5
        float4 newPosition_ = workItemIdPosition + posTimeStep * (newVelocity_ + workItemIdVelocity) * 0.5f;
#else
        // Semi-implicit Euler integration 
        float4 newVelocity_ = workItemIdVelocity + computationData.timeStep * acceleration;
        float posTimeStep = computationData.timeStep * computationData.simulationScaleInv;
        float4 newPosition_ = workItemIdPosition + posTimeStep * newVelocity_;
#endif

        newVelocity_.w = 0;
        handleBoundaryConditions(&newVelocity_, posTimeStep, &newPosition_,
                computationData.damping);


        //__global float4* newPosition = &(newPositionBuffer[originalIndex]);
        // __global float4* newVelocity = &(newVelocityBuffer[originalIndex]);
        //  *newVelocity = newVelocity_;
        setVelocity(newVelocityBuffer, originalIndex, newVelocity_);
        newPosition_.w = 1.0f; // homogeneous coordinate for rendering
        //*newPosition = newPosition_;
        setPosition(newPositionBuffer, originalIndex, newPosition_);
        /*==================================*/

#ifdef USE_COLORING

        float4 color;
        switch (computationData.coloringSource) {
            case VELOCITY:
            {
                float colorScalar = (fabs(newVelocity_.x) + fabs(newVelocity_.y) + fabs(newVelocity_.z));
                colorScalar = clamp(colorScalar, 0.0f, 1.0f);
                //  color = (float4) (1.0f - colorScalar, 1.0f - 0.5f * colorScalar, 1.0f, 1.0f);
                color = calculateColor(computationData.coloringGradient, colorScalar);
                break;
            }
            case PRESSURE:
            {
                float colorScalar = clamp(((workItemIdPressure) / 1500.0f), 0.0f, 1.0f);
                color = calculateColor(computationData.coloringGradient, colorScalar);
                break;
            }
            case FORCE:
            {
                //acceleration /= 80.0f;
                float colorScalar = clamp((acceleration.x + acceleration.y + acceleration.z) / 3.0f, 0.1f, 1.0f);
                color = calculateColor(computationData.coloringGradient, colorScalar);
                break;
            }
        }

        colorBuffer[originalIndex] = color;
#endif
    }
}

//==============================================================================

__kernel void clearPboTex3d(__global uint4* output) {
    int baseIndex = get_global_id(0) * PIXELS_PER_WORK_ITEM;

    uint4 result = (uint4) (0xF2000000, 0, 0, 0xFF000000);
    //    uint4 result = (uint4) (0xFF000000, 0, 0, 0xFF000000);

    for (uint i = 0; i < PIXELS_PER_WORK_ITEM; i++) {
        output[baseIndex++] = result;
    }
}

// word grid cell lenght = 4.0
// texture 3d grid cell length = 2.0
//#define DIST_FIELD_FACTOR 1.7f
//#define R_MIN 0.1f
//0.5f

void mapToDistanceField(
        __global uint4* pboTex3dBuffer,
        float4 position,
        float2 density,
        float restDensity,
        float restDensityInv,
        float rMin,
        float distFieldFactor) {

    //==========================================================================
    //    // SIMPLE:
    //    position *= WORLD_TO_IMAGE3D_SCALE;
    //    int4 positionInt = convert_int4_rtz(position);
    //
    //    //For an array of [ A ][ B ][ C ], we can index it thus:
    //    //(a * B * C) + (b * C) + (c * 1)
    //    int index = positionInt.x * IMAGE3D_HEIGHT * IMAGE3D_DEPTH
    //            + positionInt.y * IMAGE3D_DEPTH + positionInt.z;
    //
    //    float4 result = (float4) (0.0f, 0.0f, 0.0f, 1.0f);
    //    result.x = clamp(DENSITY(density) * restDensityInv, 0.0f, 1.0f);
    //    pboTex3dBuffer[index] = result;
    //==========================================================================

    position.w = 0.0f;
    position *= WORLD_TO_IMAGE3D_SCALE;


    // TODO: remove clamp? rather not ;)
    //#define  2.0f
    float r = clamp(DENSITY(density) * restDensityInv * distFieldFactor, 0.0f, MAX_PARTICLE_RANGE);
    float rMaxSqr = r * r;
    //  float rMin = R_MIN;

    float4 rVec = (float4) (r, r, r, 0);

    int4 minCorner = convert_int4_rtz(position - rVec);
    minCorner = max(minCorner, (int4) (0, 0, 0, 0));

    int4 maxCorner = convert_int4_rtz(position + rVec);
    maxCorner = min(maxCorner,
            (int4) (IMAGE3D_WIDTH - 1, IMAGE3D_HEIGHT - 1, IMAGE3D_DEPTH - 1, 0));

    for (int x = minCorner.x; x <= maxCorner.x; x++) {
        for (int y = minCorner.y; y <= maxCorner.y; y++) {
            for (int z = minCorner.z; z <= maxCorner.z; z++) {

                float4 nodeDiff = (float4) ((float) x, (float) y, (float) z, 0) - position;

                float magnitude = nodeDiff.x * nodeDiff.x + nodeDiff.y * nodeDiff.y
                        + nodeDiff.z * nodeDiff.z;

                if (magnitude <= rMaxSqr) {

                    int index = x * IMAGE3D_HEIGHT * IMAGE3D_DEPTH
                            + y * IMAGE3D_DEPTH + z;

                    //     float4 currentVal = convert_float4( pboTex3dBuffer[index]);
                    uint4 currentVal = pboTex3dBuffer[index];
                    float currentValF = (float) currentVal.x * MAX_PARTICLE_RANGE_DIV_254;

                    float dist = SQRT(magnitude);
                    float val = min(currentValF, dist);
                    val = max(val, rMin);

                    uint newVal = (uint) (val * 255.0f) << 24;

                    //pboTex3dBuffer[index] = currentVal;

                    volatile __global uint* ptr = (volatile __global uint*)(&(pboTex3dBuffer[index]));

                    //                    bool isEdge = (x == 0) || (x == 63) || (y == 0) || (y == 63)
                    //                            || (z == 0) || (z == 63);
                    //                    if (isEdge) {
                    //                       *ptr = 250.0f;
                    //                    } else {
                    atomic_min(ptr, newVal);
                    //}


                    //(uint4) (0xAF000000, 0, 0, 0xFF000000);
                    //convert_uint4((currentVal * 10000000.0f));
                }

            }
        }
    }


    //    int4 positionInt = minCorner; //convert_int4(nearestNode);
    //
    //    //For an array of [ A ][ B ][ C ], we can index it thus:
    //    //(a * B * C) + (b * C) + (c * 1)
    //    int index = positionInt.x * IMAGE3D_HEIGHT * IMAGE3D_DEPTH
    //            + positionInt.y * IMAGE3D_DEPTH + positionInt.z;
    //
    //    float4 result = (float4) (0.0f, 0.0f, 0.0f, 1.0f);
    //    result.x = clamp(DENSITY(density) * restDensityInv, 0.0f, 1.0f);
    //    pboTex3dBuffer[index] = result;
}

typedef struct {
    float gradWPoly6Coefficient;
    float gradWspikyCoefficient;
    float del2WviscosityCoefficient;
    float particleMass;
    float simulationScale;
    float simulationScaleInv;
    float supportRadiusScaled;
    float supportRadiusSqr;
    float supportRadius;
    float viscosity;
    float accelerationLimit;
    float gravityX;
    float gravityY;
    float gravityZ;
    float timeStep;
    float damping;
    float restDensity;
    float restDensityInv;
    float colorFieldThreshold;
    float rMin;
    float distFieldFactor;
    float del2WPoly6Cofficient;
    float surfaceTensionCoeff;
} ForceComputationDataDistField __attribute__((aligned(STRUCT_ALIGNMENT)));

__kernel void computeForcesAndIntegrateDistField(
        __constant int4* neighborLookup,
        __constant uint4* interleaveLookup,
        __read_only image2d_t blockCoordsLookup,
        __read_only image2d_t oldPositionBuffer,
        __read_only image2d_t oldVelocityBuffer,
        __global const uint2* blockBuffer,
        __global const uint2* compactBlockBuffer,
        __global const float2* densityBuffer,
        __write_only image2d_t newPositionBuffer,
        __write_only image2d_t newVelocityBuffer,
        __global uint4* pboTex3dBuffer,
        __local float4* localNeighborPositionBuffer, // sizeof(float4) * 32 (but we need only 27)
        __local float4* localNeighborVelocityBuffer, // sizeof(float4) * 32 (but we need only 27)
        __local float2* localNeighborDensityBuffer, // sizeof(float2) * 32 (but we need only 27)
        __local uint2* localNeighborBlockBuffer, // sizeof(uint2) * 32 (but we need only 27)
        const ForceComputationDataDistField computationData) {

    int workGroupId = get_group_id(0);
    int workItemId = get_local_id(0);

    uint2 thisBlock = compactBlockBuffer[workGroupId];

    uint thisBlockParticleCount = BLOCK_PARTICLE_COUNT(thisBlock); // N
    uint thisBlockFirstParticleIndex = BLOCK_FIRST_PARTICLE_INDEX(thisBlock);
    PositionVect thisBlockFirstParticlePos = getPosition(oldPositionBuffer, (thisBlockFirstParticleIndex));

    uint thisBlockIndex = Z_INDEX(thisBlockFirstParticlePos); //  >> BLOCK_SIZE_3D_LOG_2;

    float4 pressureForce = (float4) (0, 0, 0, 0);
    float4 viscosityForce = (float4) (0, 0, 0, 0);

#ifdef USE_SELECT
    bool b = (workItemId < thisBlockParticleCount);
    uint index = (thisBlockFirstParticleIndex + workItemId) * b;
    float4 zeroVect = (float4) (0, 0, 0, 0);

    float4 workItemIdPosition = SELECT4(zeroVect,
            POSITION(getPosition(oldPositionBuffer, (index))), b);

    float4 workItemIdVelocity = SELECT4(zeroVect,
            getVelocity(oldVelocityBuffer, (index)), b);

    float workItemIdPressure = PRESSURE(workItemIdVelocity);
#else
    float4 workItemIdPosition = (float4) (0, 0, 0, 0);
    float4 workItemIdVelocity = (float4) (0, 0, 0, 0);
    float workItemIdPressure = 0;

    if (workItemId < thisBlockParticleCount) {
        uint index = thisBlockFirstParticleIndex + workItemId;
        workItemIdPosition = POSITION(getPosition(oldPositionBuffer, (index)));
        workItemIdVelocity = getVelocity(oldVelocityBuffer, (index));
        workItemIdPressure = PRESSURE(workItemIdVelocity);
    }
#endif

    if (workItemId < 27) {
        loadNeighborBlockData(workItemId, thisBlockIndex, neighborLookup, interleaveLookup,
                blockCoordsLookup, blockBuffer, localNeighborBlockBuffer);
    }

    barrier(CLK_LOCAL_MEM_FENCE);

    uint maxParticlesInNeighbors = BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[0]);
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[1]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[2]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[3]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[4]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[5]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[6]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[7]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[8]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[9]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[10]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[11]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[12]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[13]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[14]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[15]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[16]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[17]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[18]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[19]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[20]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[21]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[22]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[23]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[24]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[25]));
    maxParticlesInNeighbors = max(maxParticlesInNeighbors, BLOCK_PARTICLE_COUNT(localNeighborBlockBuffer[26]));

    //float4 centerOfMass = workItemIdPosition;
    //(float4)(0, 0, 0, 0);
    //uint totalNeighbors = 1;
    float4 gradColorField = (float4) (0, 0, 0, 0);

#ifdef USE_SURFACE_TENSION
    float del2ColorField = 0.0f;
#endif

    for (uint i = 0; i < maxParticlesInNeighbors; i++) {
        if (workItemId < 27) {
            uint2 block = localNeighborBlockBuffer[workItemId];

#ifdef USE_SELECT
            // uint particleCount = BLOCK_PARTICLE_COUNT(block);
            //totalNeighbors += particleCount;

            bool b = (BLOCK_PARTICLE_COUNT(block) > i);
            uint index = (BLOCK_FIRST_PARTICLE_INDEX(block) + i) * b;

            localNeighborPositionBuffer[workItemId] = SELECT4(workItemIdPosition,
                    POSITION(getPosition(oldPositionBuffer, (index))), b);

            localNeighborVelocityBuffer[workItemId] = SELECT4(zeroVect,
                    getVelocity(oldVelocityBuffer, (index)), b);

            localNeighborDensityBuffer[workItemId] = SELECT2(((float2) (0.0f, 0.0f)),
                    densityBuffer[index], b);
#else
            float4 position = workItemIdPosition;
            float4 velocity = (float4) (0, 0, 0, 0);
            float2 density = (float2) (0, 0);

            if (BLOCK_PARTICLE_COUNT(block) > i) {
                uint index = BLOCK_FIRST_PARTICLE_INDEX(block) + i;
                density = densityBuffer[index];
                position = POSITION(getPosition(oldPositionBuffer, (index)));
                velocity = getVelocity(oldVelocityBuffer, (index));
            }

            localNeighborPositionBuffer[workItemId] = position;
            localNeighborVelocityBuffer[workItemId] = velocity;
            localNeighborDensityBuffer[workItemId] = density;
#endif
        }

        barrier(CLK_LOCAL_MEM_FENCE);
#ifdef USE_IF
        if (workItemId < thisBlockParticleCount) {
#endif
            for (uint j = 0; j < 27; j++) {
                float4 neighborPosition = localNeighborPositionBuffer[j];
                float4 rVec = workItemIdPosition - neighborPosition;
                rVec.w = 0.0f;

                float distanceSquared = DOT(rVec, rVec);

                bool neighborIsMe = (distanceSquared == 0.0f);
                float distance = SQRT(distanceSquared);
                bool tooFarAway = (distance > computationData.supportRadiusScaled);

                if (tooFarAway || neighborIsMe) {
                    continue;
                }

                //  centerOfMass += neighborPosition;
                //  totalNeighbors += 1;

                float scaledDistance = distance * (computationData.simulationScale);

                float4 neighborVelocity = localNeighborVelocityBuffer[j];
                float2 neighborDensity = localNeighborDensityBuffer[j];

                float neighborPressure = PRESSURE(neighborVelocity);
                float neighborDensityInv = DENSITY_INVERSE(neighborDensity);

                //--------------------------------------------------------------
                // PRESSURE:
                //  float4 rVec = workItemIdPosition - neighborPosition;
                float4 scaledVec = rVec * (computationData.simulationScale);
                scaledVec /= scaledDistance;
                //rVec.w = 0.0f;
                float x = computationData.supportRadius - scaledDistance;
                float4 gradWspiky = x * x * scaledVec;

                pressureForce += (workItemIdPressure + neighborPressure) * neighborDensityInv * gradWspiky;

                //--------------------------------------------------------------              
                // VISCOSITY:
                float del2Wviscosity = (computationData.supportRadius - scaledDistance);
                viscosityForce += (neighborVelocity - workItemIdVelocity) * neighborDensityInv * del2Wviscosity;
                //--------------------------------------------------------------
                // COLOR_FIELD:
                float rSquare = scaledDistance * scaledDistance;
                float alpha = computationData.supportRadiusSqr - rSquare;
                float gradWPoly6 = alpha * alpha;
                //gradWPoly6 = gradWPoly6 * gradWPoly6;
                gradColorField += scaledVec * neighborDensityInv * gradWPoly6;
                //--------------------------------------------------------------
#ifdef USE_SURFACE_TENSION
                // SURFACE TENSION:
                del2ColorField += neighborDensityInv * alpha * (3.0f * computationData.supportRadiusSqr - 7.0f * rSquare);
#endif
            }
#ifdef USE_IF
        }
#endif

        barrier(CLK_LOCAL_MEM_FENCE);
    }

    if (workItemId < thisBlockParticleCount) {
        uint originalIndex = thisBlockFirstParticleIndex + workItemId;

        float2 workItemIdDensity = densityBuffer[originalIndex];

        //#################################
        gradColorField *= computationData.particleMass * computationData.gradWPoly6Coefficient;
        float gcfMagnitudeSquared = DOT(gradColorField, gradColorField);
        float gcfMagnitude = SQRT(gcfMagnitudeSquared);
        bool isSurface = (gcfMagnitude > computationData.colorFieldThreshold);

        if (isSurface) {
            mapToDistanceField(pboTex3dBuffer, workItemIdPosition, workItemIdDensity,
                    computationData.restDensity, computationData.restDensityInv,
                    computationData.rMin, computationData.distFieldFactor);
        }

        // float4 zero = (float4) (0.0f, 0.0f, 0.0f, 0.0f);
        // workItemIdPosition.w = 1.0f;
        // surfacePositionBuffer[originalIndex] = SELECT4(zero, workItemIdPosition, isSurface);
        //#################################
#ifdef USE_SURFACE_TENSION
        //#################################
        // Surface Tension:
        float4 surfaceTensionForce = (float4) (0, 0, 0, 0);
        //if (isSurface) {
        if (gcfMagnitude > 0.001f) {
            del2ColorField *= computationData.particleMass * computationData.del2WPoly6Cofficient;
            //del2ColorField = select(0.1f, del2ColorField, (del2ColorField == 0));
            surfaceTensionForce = -computationData.surfaceTensionCoeff * del2ColorField * gradColorField / gcfMagnitude;
        }
        //#################################
#endif



        float densityInv = DENSITY_INVERSE(workItemIdDensity);
        pressureForce *= computationData.particleMass * computationData.gradWspikyCoefficient * 0.5f;
        viscosityForce *= computationData.viscosity * computationData.particleMass * computationData.del2WviscosityCoefficient;

#ifdef USE_SURFACE_TENSION
        float4 acceleration = densityInv * (viscosityForce - pressureForce) + surfaceTensionForce;
#else
        float4 acceleration = densityInv * (viscosityForce - pressureForce);
#endif

#ifdef CHECK_ACCELERATION_LIMIT
        float magnitude = acceleration.x * acceleration.x + acceleration.y * acceleration.y + acceleration.z * acceleration.z;
        float speed = SQRT(magnitude);
        bool tooBig = (speed > computationData.accelerationLimit); //magnitude > computationData.speedLimit * computationData.speedLimit);

        float scale = computationData.accelerationLimit / speed;
        acceleration = SELECT4(acceleration, acceleration * scale, tooBig);
#endif

        acceleration.w = 0.0f;


        /*----------------------------------*/
        /* INTEGRATE */
        /*----------------------------------*/

        // apply external forces
        float4 gravity = (float4) (computationData.gravityX, computationData.gravityY, computationData.gravityZ, 0.0f);
        acceleration += gravity;
        //  acceleration_ = gravity;
#ifdef USE_LEAP_FROG
        // Leapfrog integration		
        // v(t+1/2) = v(t-1/2) + a(t) dt	
        float4 newVelocity_ = workItemIdVelocity + computationData.timeStep * acceleration;
        float posTimeStep = computationData.timeStep * computationData.simulationScaleInv;
        // v(t+1) = [v(t-1/2) + v(t+1/2)] * 0.5
        float4 newPosition_ = workItemIdPosition + posTimeStep * (newVelocity_ + workItemIdVelocity) * 0.5f;
#else
        // Semi-implicit Euler integration 
        float4 newVelocity_ = workItemIdVelocity + computationData.timeStep * acceleration;
        float posTimeStep = computationData.timeStep * computationData.simulationScaleInv;
        float4 newPosition_ = workItemIdPosition + posTimeStep * newVelocity_;
#endif

        newVelocity_.w = 0;
        handleBoundaryConditions(&newVelocity_, posTimeStep, &newPosition_,
                computationData.damping);

        setVelocity(newVelocityBuffer, originalIndex, newVelocity_);
        newPosition_.w = 1.0f; // homogeneous coordinate for rendering

        setPosition(newPositionBuffer, originalIndex, newPosition_);
        /*==================================*/

    }
}