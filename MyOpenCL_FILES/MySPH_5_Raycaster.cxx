#include "opencl_fake.h"
#include "MySPH_fake.h"

#define PIXELS_PER_WORK_ITEM 64
//$END_FAKE$

#define maxSteps 500
#define tstep 0.01f

//TODO: set as paremeters not constant
// 1.0f/64.0f 
// 64 - dimension of 3D texture
#define NEXT_NODE_DIST (1.0f/64.0f)

#define HIDE_WALL_PARTICLES

//#define REVERSE_DIRECTIONS


// intersect ray with a box
// http://www.siggraph.org/education/materials/HyperGraph/raytrace/rtinter3.htm

int intersectBox(float4 r_o, float4 r_d, float4 boxmin, float4 boxmax, float *tnear, float *tfar) {
    // compute intersection of ray with all six bbox planes
    float4 invR = (float4) (1.0f, 1.0f, 1.0f, 1.0f) / r_d;
    float4 tbot = invR * (boxmin - r_o);
    float4 ttop = invR * (boxmax - r_o);

    // re-order intersections to find smallest and largest on each axis
    float4 tmin = min(ttop, tbot);
    float4 tmax = max(ttop, tbot);

    // find the largest tmin and the smallest tmax
    float largest_tmin = max(max(tmin.x, tmin.y), max(tmin.x, tmin.z));
    float smallest_tmax = min(min(tmax.x, tmax.y), min(tmax.x, tmax.z));

    *tnear = largest_tmin;
    *tfar = smallest_tmax;

    return smallest_tmax > largest_tmin;
}

uint rgbaFloatToInt(float4 rgba) {
    rgba.x = clamp(rgba.x, 0.0f, 1.0f);
    rgba.y = clamp(rgba.y, 0.0f, 1.0f);
    rgba.z = clamp(rgba.z, 0.0f, 1.0f);
    rgba.w = clamp(rgba.w, 0.0f, 1.0f);
    return ((uint) (rgba.w * 255.0f) << 24) | ((uint) (rgba.z * 255.0f) << 16) | ((uint) (rgba.y * 255.0f) << 8) | (uint) (rgba.x * 255.0f);
}

__kernel void drawFrame(__global uint4* output) {
    int id = get_global_id(0);

    uint4 result = (uint4) (0xFF000000, 0, 0, 0xFF000000);

    const int maxVal = IMAGE3D_BASE_LENGTH - 1;
    const int heightTimesDepth = IMAGE3D_HEIGHT * IMAGE3D_DEPTH;

    //const int arr[2][3] = { { 1, 0, 0 }, { 0, 1, 0} };

    int x = id;
    int y = 0;
    int z = 0;

    int index = x * heightTimesDepth;
    output[index] = result;

    x = 0;
    y = id;
    // z = 0;

    index = y * IMAGE3D_DEPTH;
    output[index] = result;

    //    x = 0;
    y = 0;
    z = id;

    index = z;
    output[index] = result;
    //----------------------------------------
    x = id;
    y = maxVal;
    z = maxVal;

    index = x * heightTimesDepth
            + y * IMAGE3D_DEPTH + z;
    output[index] = result;

    x = maxVal;
    y = id;
    // z = maxVal;

    index = x * heightTimesDepth
            + y * IMAGE3D_DEPTH + z;
    output[index] = result;

    // x = maxVal;
    y = maxVal;
    z = id;

    index = x * heightTimesDepth
            + y * IMAGE3D_DEPTH + z;
    output[index] = result;
    //==========================================

    x = id;
    y = maxVal;
    z = 0;

    index = x * heightTimesDepth
            + y * IMAGE3D_DEPTH;
    output[index] = result;

    x = 0;
    y = id;
    z = maxVal;

    index = y * IMAGE3D_DEPTH + z;
    output[index] = result;

    x = id;
    y = 0;
    //z = maxVal;

    index = x * heightTimesDepth + z;
    output[index] = result;
    //----------------------------------------
    x = maxVal;
    y = id;
    z = 0;

    index = x * heightTimesDepth
            + y * IMAGE3D_DEPTH;
    output[index] = result;

    x = 0;
    y = maxVal;
    z = id;

    index = y * IMAGE3D_DEPTH + z;
    output[index] = result;

    x = maxVal;
    y = 0;
    //z = id;

    index = x * heightTimesDepth + z;
    output[index] = result;

}

__kernel void phongRender(
        __global uint* d_output, // 0
        uint imageW, // 1
        uint imageH, // 2
        __constant float* transformMatrix, // 3
        __read_only image3d_t volume, // 4
        sampler_t volumeSampler, // 5
        float4 lightDir) { //6

    uint x = get_global_id(0);
    uint y = get_global_id(1);

    float u = (x / (float) imageW)*2.0f - 1.0f;
    float v = (y / (float) imageH)*2.0f - 1.0f;

    //float tstep = 0.01f;
    float4 boxMin = (float4) (-1.0f, -1.0f, -1.0f, 1.0f);
    float4 boxMax = (float4) (1.0f, 1.0f, 1.0f, 1.0f);

    // calculate eye ray in world space
    float4 eyeRay_o; // eye origin 
    float4 eyeRay_d; // eye direction

    eyeRay_o = (float4) (transformMatrix[12], transformMatrix[13], transformMatrix[14], 1.0f);

    float4 color = normalize(((float4) (u, v, -3.0f, 0.0f)));
    eyeRay_d.x = dot(color, ((float4) (transformMatrix[0], transformMatrix[4], transformMatrix[8], 0)));
    eyeRay_d.y = dot(color, ((float4) (transformMatrix[1], transformMatrix[5], transformMatrix[9], 0)));
    eyeRay_d.z = dot(color, ((float4) (transformMatrix[2], transformMatrix[6], transformMatrix[10], 0)));

    eyeRay_d.w = 0.0f;

    // find intersection with box
    float tnear, tfar;
    int hit = intersectBox(eyeRay_o, eyeRay_d, boxMin, boxMax, &tnear, &tfar);
    if (!hit) {
        if ((x < imageW) && (y < imageH)) {
            // write output color
            uint i = (y * imageW) + x;
            d_output[i] = 0;
        }
        return;
    }
    //if (tnear < 0.0f) tnear = 0.0f; // clamp to near plane
    tnear = select(tnear, 0.0f, (tnear < 0.0f));

    // march along ray from back to front, accumulating color
    color = (float4) (0.0f, 0.0f, 0.0f, 0.0f);
    float t = tnear;

    const float4 ambientColor = (float4) (0.1f, 0.1f, 0.1f, 1.0f);
    const float4 diffuseColor = (float4) (0.0f, 0.0f, 1.0f, 1.0f);

    const float4 specularColor = (float4) (1.0f, 1.0f, 1.0f, 1.0f);

    for (uint i = 0; i < maxSteps; i++) {
        float4 pos = eyeRay_o + eyeRay_d*t;
        pos = pos * 0.5f + 0.5f; // map position to [0, 1] coordinates

        float4 sample = read_imagef(volume, volumeSampler, pos);


        if (sample.x < 0.9f) {

            float4 tempPos = pos;

            tempPos.x += NEXT_NODE_DIST;
            float x_plus = (read_imagef(volume, volumeSampler, tempPos)).x;

            tempPos.x = pos.x - NEXT_NODE_DIST;
            float x_minus = (read_imagef(volume, volumeSampler, tempPos)).x;

            tempPos.x = pos.x;
            tempPos.y += NEXT_NODE_DIST;
            float y_plus = (read_imagef(volume, volumeSampler, tempPos)).x;

            tempPos.y = pos.y - NEXT_NODE_DIST;
            float y_minus = (read_imagef(volume, volumeSampler, tempPos)).x;

            tempPos.y = pos.y;
            tempPos.z += NEXT_NODE_DIST;
            float z_plus = (read_imagef(volume, volumeSampler, tempPos)).x;

            tempPos.z = pos.z - NEXT_NODE_DIST;
            float z_minus = (read_imagef(volume, volumeSampler, tempPos)).x;

            //float4 normal = (float4) (x_plus - x_minus, y_plus - y_minus, z_plus - z_minus, 0.0f);
            float4 normal = (float4) (x_minus - x_plus, y_minus - y_plus, z_minus - z_plus, 0.0f);
            normal = normalize(normal);


            color = diffuseColor;
            float4 lighting = ambientColor;
            //------------------------------------
            // lambertian lighting:

            float NdotL = dot(lightDir, normal);
            lighting += clamp(NdotL, 0.0f, 1.0f);
            //lighting.w = 1.0f;
            lighting = clamp(lighting, 0.0f, 1.0f);

            //------------------------------------
            // specular lighting:
            //            float4 refl = 2.0f * normal * NdotL - lightDir;
            //            float4 view = normalize(eyeRay_d);
            //            
            //            color += specularColor * pow(clamp(dot(refl, view), 0.0f, 1.0f), 32.0f);          
            //            color = clamp(color, 0.0f, 1.0f);
            //------------------------------------

            color *= lighting;
            break;
        }

        t += tstep;
        if (t > tfar) break;
    }

    if ((x < imageW) && (y < imageH)) {
        // write output color
        uint i = (y * imageW) + x;

        //color.w = 1.0f;

        d_output[i] = rgbaFloatToInt(color);
    }
}

inline float4 getReflectionColor(float4 reflVec,
#ifdef REVERSE_DIRECTIONS
        __read_only image2d_t cubeMapNegX,
        __read_only image2d_t cubeMapPosX,
        __read_only image2d_t cubeMapNegY,
        __read_only image2d_t cubeMapPosY,
        __read_only image2d_t cubeMapNegZ,
        __read_only image2d_t cubeMapPosZ
#else
        __read_only image2d_t cubeMapPosX,
        __read_only image2d_t cubeMapNegX,
        __read_only image2d_t cubeMapPosY,
        __read_only image2d_t cubeMapNegY,
        __read_only image2d_t cubeMapPosZ,
        __read_only image2d_t cubeMapNegZ
#endif
        ) {

    const sampler_t textureSampler = CLK_NORMALIZED_COORDS_TRUE | CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_LINEAR;
    float4 result = (float4) (0, 0, 0, 0);

    float absX = fabs(reflVec.x);
    float absY = fabs(reflVec.y);
    float absZ = fabs(reflVec.z);

    if ((absX > absY) && (absX > absZ)) {
        if (reflVec.x < 0) {
            float u = reflVec.z / absX * 0.5f + 0.5f;
            float v = -reflVec.y / absX * 0.5f + 0.5f;

            result = read_imagef(cubeMapNegX, textureSampler, (float2) (u, v));
        } else {
            float u = -reflVec.z / absX * 0.5f + 0.5f;
            float v = -reflVec.y / absX * 0.5f + 0.5f;

            result = read_imagef(cubeMapPosX, textureSampler, (float2) (u, v));
        }
    } else if ((absY > absX) && (absY > absZ)) {
        if (reflVec.y < 0) {
            // negY
            float u = reflVec.x / absY * 0.5f + 0.5f;
            float v = -reflVec.z / absY * 0.5f + 0.5f;

            result = read_imagef(cubeMapNegY, textureSampler, (float2) (u, v));
        } else {
            // posY
            float u = reflVec.x / absY * 0.5f + 0.5f;
            float v = reflVec.z / absY * 0.5f + 0.5f;

            result = read_imagef(cubeMapPosY, textureSampler, (float2) (u, v));
        }
    } else if ((absZ > absX) && (absZ > absY)) {
        if (reflVec.z < 0) {
            //negZ
            float u = -reflVec.x / absZ * 0.5f + 0.5f;
            float v = -reflVec.y / absZ * 0.5f + 0.5f;

            result = read_imagef(cubeMapNegZ, textureSampler, (float2) (u, v));
        } else {
            // posZ
            float u = reflVec.x / absZ * 0.5f + 0.5f;
            float v = -reflVec.y / absZ * 0.5f + 0.5f;

            result = read_imagef(cubeMapPosZ, textureSampler, (float2) (u, v));
        }
    }

    return result;
}

__kernel void render(
        __global uint* d_output, // 0
        uint imageW, // 1
        uint imageH, // 2
        __constant float* transformMatrix, // 3
        __read_only image3d_t volume, // 4
        __read_only image2d_t cubeMapPosX, // 5
        __read_only image2d_t cubeMapNegX, // 6
        __read_only image2d_t cubeMapPosY, // 7
        __read_only image2d_t cubeMapNegY, // 8
        __read_only image2d_t cubeMapPosZ, // 9
        __read_only image2d_t cubeMapNegZ, // 10
        float4 lightDir) { // 11

    const sampler_t volumeSampler = CLK_NORMALIZED_COORDS_TRUE | CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_LINEAR;

    uint x = get_global_id(0);
    uint y = get_global_id(1);

    float u = (x / (float) imageW)*2.0f - 1.0f;
    float v = (y / (float) imageH)*2.0f - 1.0f;

    //float tstep = 0.01f;
    float4 boxMin = (float4) (-1.0f, -1.0f, -1.0f, 1.0f);
    float4 boxMax = (float4) (1.0f, 1.0f, 1.0f, 1.0f);

    // calculate eye ray in world space
    float4 eyeRay_o; // eye origin 
    float4 eyeRay_d; // eye direction

    eyeRay_o = (float4) (transformMatrix[12], transformMatrix[13], transformMatrix[14], 1.0f);

    float4 color = normalize(((float4) (u, v, -3.0f, 0.0f)));
    eyeRay_d.x = dot(color, ((float4) (transformMatrix[0], transformMatrix[4], transformMatrix[8], 0)));
    eyeRay_d.y = dot(color, ((float4) (transformMatrix[1], transformMatrix[5], transformMatrix[9], 0)));
    eyeRay_d.z = dot(color, ((float4) (transformMatrix[2], transformMatrix[6], transformMatrix[10], 0)));

    eyeRay_d.w = 0.0f;

    //float4 direction = 

    // find intersection with box
    float tnear, tfar;
    int hit = intersectBox(eyeRay_o, eyeRay_d, boxMin, boxMax, &tnear, &tfar);
    if (!hit) {
        if ((x < imageW) && (y < imageH)) {
            // write output color
            uint i = (y * imageW) + x;
            d_output[i] = 0;
        }
        return;
    }
    //if (tnear < 0.0f) tnear = 0.0f; // clamp to near plane
    tnear = select(tnear, 0.0f, (tnear < 0.0f));

    // march along ray from back to front, accumulating color

    //==========================================================================
    float2 coords = (float2) (0, 0);
    const sampler_t textureSampler = CLK_NORMALIZED_COORDS_TRUE | CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_LINEAR;
    color = read_imagef(cubeMapPosX, textureSampler, coords);
    color = read_imagef(cubeMapNegX, textureSampler, coords);
    color = read_imagef(cubeMapPosY, textureSampler, coords);
    color = read_imagef(cubeMapNegY, textureSampler, coords);
    color = read_imagef(cubeMapPosZ, textureSampler, coords);
    color = read_imagef(cubeMapNegZ, textureSampler, coords);
    //==========================================================================


    color = (float4) (0.0f, 0.0f, 0.0f, 0.0f);
    float t = tnear;

    const float4 ambientColor = (float4) (0.1f, 0.1f, 0.1f, 1.0f);
    const float4 diffuseColor = //(float4) (0.257f, 0.441f, 0.696f, 1.0f);
            (float4) (0.357f, 0.541f, 0.796f, 1.0f);
    //    const float4 diffuseColor = (float4)(0.457f, 0.641f, 0.896f, 1.0f);
    //(float4) (0.0f, 0.0f, 1.0f, 1.0f);
    //const float4 lightDir = normalize((float4) (1.0f, 1.0f, -1.0f, 0.0f));
    const float4 specularColor = (float4) (1.0f, 1.0f, 1.0f, 1.0f);

    for (uint i = 0; i < maxSteps; i++) {
        float4 pos = eyeRay_o + eyeRay_d*t;
        pos = pos * 0.5f + 0.5f; // map position to [0, 1] coordinates

        float4 sample = read_imagef(volume, volumeSampler, pos);


         if (sample.x <= 0.8f) {
        //if (sample.x <= 1.0f) {
            float4 normal;
            color = diffuseColor;
            float4 lighting = ambientColor;

#ifdef HIDE_WALL_PARTICLES
            float minEdge = 0.01f;
            float maxEdge = 0.99f;
            bool isEdge = (pos.x < minEdge) || (pos.x > maxEdge)
                    || (pos.y < minEdge) || (pos.y > maxEdge)
                    || (pos.z < minEdge) || (pos.z > maxEdge);
            if(isEdge) {
            //if (true) {
                // color = (float4) (1, 1, 1, 0);
                //  color = (float4)(0.357f, 0.541f, 0.796f, 1.0f);
                // break;
                normal = pos * 2.0f - 1.0f;
                float xAbs = fabs(normal.x);
                float yAbs = fabs(normal.y);
                float zAbs = fabs(normal.z);
                if (xAbs > yAbs && xAbs > zAbs) {
                    normal.y = 0;
                    normal.z = 0;
                } else if (yAbs > xAbs && yAbs > zAbs) {
                    normal.x = 0;
                    normal.z = 0;
                } else if (zAbs > xAbs && zAbs > yAbs) {
                    normal.x = 0;
                    normal.y = 0;
                }

                normal = normalize(normal);
//                                //------------------------------------
//                                // lambertian lighting:
//                                color = (float4) (0.349f, 0.435f, 0.522f, 1.0f);
//                                float NdotL = dot(lightDir, normal);
//                                lighting += clamp(NdotL, 0.0f, 1.0f);
                
                 //------------------------------------
            // lambertian lighting:
            float NdotL = dot(lightDir, normal);
            lighting += clamp(NdotL, 0.0f, 1.0f); // * LightColor;
            //------------------------------------
            //specular lighting:
            float4 refl = 2.0f * normal * NdotL - lightDir;
            refl = normalize(refl);

            float4 view = normalize(eyeRay_o - eyeRay_d * t);

            lighting += specularColor * pow(clamp(dot(refl, view), 0.0f, 1.0f), 16.0f);
            //------------------------------------
            // Refelction lighting:
            float NdotV = dot(view, normal);
            float4 reflView = 2.0f * NdotV * normal - view;
            //reflView = normalize(reflView);

            
            float4 reflColor = getReflectionColor(reflView,
                    cubeMapPosX, cubeMapNegX,
                    cubeMapPosY, cubeMapNegY,
                    cubeMapPosZ, cubeMapNegZ);
            //  lighting += reflColor * 0.5f;
            //------------------------------------

            color *= clamp(lighting, 0.0f, 1.0f);


            //color = reflColor;
               color = mix(color, reflColor, 0.1);
            } else {
#endif
                float4 tempPos = pos;

                tempPos.x += NEXT_NODE_DIST;
                float x_plus = (read_imagef(volume, volumeSampler, tempPos)).x;

                tempPos.x = pos.x - NEXT_NODE_DIST;
                float x_minus = (read_imagef(volume, volumeSampler, tempPos)).x;

                tempPos.x = pos.x;
                tempPos.y += NEXT_NODE_DIST;
                float y_plus = (read_imagef(volume, volumeSampler, tempPos)).x;

                tempPos.y = pos.y - NEXT_NODE_DIST;
                float y_minus = (read_imagef(volume, volumeSampler, tempPos)).x;

                tempPos.y = pos.y;
                tempPos.z += NEXT_NODE_DIST;
                float z_plus = (read_imagef(volume, volumeSampler, tempPos)).x;

                tempPos.z = pos.z - NEXT_NODE_DIST;
                float z_minus = (read_imagef(volume, volumeSampler, tempPos)).x;

                normal = (float4) (x_plus - x_minus, y_plus - y_minus, z_plus - z_minus, 0.0f);
                //normal = (float4) (x_minus - x_plus, y_minus - y_plus, z_minus - z_plus, 0.0f);

                normal = normalize(normal);
           // }
            //------------------------------------
            // lambertian lighting:
            float NdotL = dot(lightDir, normal);
            lighting += clamp(NdotL, 0.0f, 1.0f); // * LightColor;
            //------------------------------------
            //specular lighting:
            float4 refl = 2.0f * normal * NdotL - lightDir;
            refl = normalize(refl);

            float4 view = normalize(eyeRay_o - eyeRay_d * t);

            lighting += specularColor * pow(clamp(dot(refl, view), 0.0f, 1.0f), 16.0f);
            //------------------------------------
            // Refelction lighting:
            float NdotV = dot(view, normal);
            float4 reflView = 2.0f * NdotV * normal - view;
            //reflView = normalize(reflView);

            
            float4 reflColor = getReflectionColor(reflView,
                    cubeMapPosX, cubeMapNegX,
                    cubeMapPosY, cubeMapNegY,
                    cubeMapPosZ, cubeMapNegZ);
            //  lighting += reflColor * 0.5f;
            //------------------------------------

            color *= clamp(lighting, 0.0f, 1.0f);


            //color = reflColor;
               color = mix(color, reflColor, 0.5);
            }
#ifdef HIDE_WALL_PARTICLES
            //  }
#endif
            break;
        } else if (sample.x > 0.985f) {
            color = (float4) (0, 0, 0, 1);
            break;
        }

        t += tstep;
        if (t > tfar) break;
    }

    //color *= brightness;

    if ((x < imageW) && (y < imageH)) {
        // write output color
        uint i = (y * imageW) + x;

        //color.w = 1.0f;

        d_output[i] = rgbaFloatToInt(color);
    }
}

