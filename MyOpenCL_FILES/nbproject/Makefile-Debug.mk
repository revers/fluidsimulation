#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc.exe
CCC=g++.exe
CXX=g++.exe
FC=gfortran.exe
AS=as.exe

# Macros
CND_PLATFORM=MinGW-Windows
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/MySPH_5_Raycaster.o \
	${OBJECTDIR}/Scan_b2.o \
	${OBJECTDIR}/radixsort_cl_kernels.o \
	${OBJECTDIR}/MySPH_NEW_RadixSort.o \
	${OBJECTDIR}/MySPH_6_Raycaster.o \
	${OBJECTDIR}/CLRadixSort.o \
	${OBJECTDIR}/MyRaycasting5.o \
	${OBJECTDIR}/MyRaycasting2.o \
	${OBJECTDIR}/MyRadixSort5B.o \
	${OBJECTDIR}/MySPH_6.o \
	${OBJECTDIR}/MyRaycasting3.o \
	${OBJECTDIR}/MySPH.o \
	${OBJECTDIR}/CLRadixSort2.o \
	${OBJECTDIR}/RadixSort.o \
	${OBJECTDIR}/RadixSort2.o \
	${OBJECTDIR}/sphFluidDemo.o \
	${OBJECTDIR}/GLSLTest.o \
	${OBJECTDIR}/MySPH_2.o \
	${OBJECTDIR}/MyRadixSort.o \
	${OBJECTDIR}/RadixSort3.o \
	${OBJECTDIR}/Scan_b.o \
	${OBJECTDIR}/volumeRender.o \
	${OBJECTDIR}/MySPH_5.o \
	${OBJECTDIR}/temp.o \
	${OBJECTDIR}/MyRaycasting4.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/myopencl_files.exe

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/myopencl_files.exe: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/myopencl_files ${OBJECTFILES} ${LDLIBSOPTIONS} 

${OBJECTDIR}/MySPH_5_Raycaster.o: MySPH_5_Raycaster.cxx 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -I/C/MinGW/OpenCL/include -Imwc64x -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/MySPH_5_Raycaster.o MySPH_5_Raycaster.cxx

${OBJECTDIR}/Scan_b2.o: Scan_b2.cxx 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -I/C/MinGW/OpenCL/include -Imwc64x -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/Scan_b2.o Scan_b2.cxx

${OBJECTDIR}/radixsort_cl_kernels.o: radixsort_cl_kernels.cxx 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -I/C/MinGW/OpenCL/include -Imwc64x -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/radixsort_cl_kernels.o radixsort_cl_kernels.cxx

${OBJECTDIR}/MySPH_NEW_RadixSort.o: MySPH_NEW_RadixSort.cxx 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -I/C/MinGW/OpenCL/include -Imwc64x -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/MySPH_NEW_RadixSort.o MySPH_NEW_RadixSort.cxx

${OBJECTDIR}/MySPH_6_Raycaster.o: MySPH_6_Raycaster.cxx 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -I/C/MinGW/OpenCL/include -Imwc64x -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/MySPH_6_Raycaster.o MySPH_6_Raycaster.cxx

${OBJECTDIR}/CLRadixSort.o: CLRadixSort.cxx 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -I/C/MinGW/OpenCL/include -Imwc64x -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/CLRadixSort.o CLRadixSort.cxx

${OBJECTDIR}/MyRaycasting5.o: MyRaycasting5.cxx 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -I/C/MinGW/OpenCL/include -Imwc64x -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/MyRaycasting5.o MyRaycasting5.cxx

${OBJECTDIR}/MyRaycasting2.o: MyRaycasting2.cxx 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -I/C/MinGW/OpenCL/include -Imwc64x -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/MyRaycasting2.o MyRaycasting2.cxx

${OBJECTDIR}/MyRadixSort5B.o: MyRadixSort5B.cxx 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -I/C/MinGW/OpenCL/include -Imwc64x -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/MyRadixSort5B.o MyRadixSort5B.cxx

${OBJECTDIR}/MySPH_6.o: MySPH_6.cxx 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -I/C/MinGW/OpenCL/include -Imwc64x -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/MySPH_6.o MySPH_6.cxx

${OBJECTDIR}/MyRaycasting3.o: MyRaycasting3.cxx 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -I/C/MinGW/OpenCL/include -Imwc64x -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/MyRaycasting3.o MyRaycasting3.cxx

${OBJECTDIR}/MySPH.o: MySPH.cxx 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -I/C/MinGW/OpenCL/include -Imwc64x -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/MySPH.o MySPH.cxx

${OBJECTDIR}/CLRadixSort2.o: CLRadixSort2.cxx 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -I/C/MinGW/OpenCL/include -Imwc64x -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/CLRadixSort2.o CLRadixSort2.cxx

${OBJECTDIR}/RadixSort.o: RadixSort.cxx 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -I/C/MinGW/OpenCL/include -Imwc64x -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/RadixSort.o RadixSort.cxx

${OBJECTDIR}/RadixSort2.o: RadixSort2.cxx 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -I/C/MinGW/OpenCL/include -Imwc64x -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/RadixSort2.o RadixSort2.cxx

${OBJECTDIR}/sphFluidDemo.o: sphFluidDemo.cxx 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -I/C/MinGW/OpenCL/include -Imwc64x -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/sphFluidDemo.o sphFluidDemo.cxx

${OBJECTDIR}/GLSLTest.o: GLSLTest.cc 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -I/C/MinGW/OpenCL/include -Imwc64x -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/GLSLTest.o GLSLTest.cc

${OBJECTDIR}/MySPH_2.o: MySPH_2.cxx 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -I/C/MinGW/OpenCL/include -Imwc64x -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/MySPH_2.o MySPH_2.cxx

${OBJECTDIR}/MyRadixSort.o: MyRadixSort.cxx 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -I/C/MinGW/OpenCL/include -Imwc64x -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/MyRadixSort.o MyRadixSort.cxx

${OBJECTDIR}/RadixSort3.o: RadixSort3.cxx 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -I/C/MinGW/OpenCL/include -Imwc64x -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/RadixSort3.o RadixSort3.cxx

${OBJECTDIR}/Scan_b.o: Scan_b.cxx 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -I/C/MinGW/OpenCL/include -Imwc64x -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/Scan_b.o Scan_b.cxx

${OBJECTDIR}/volumeRender.o: volumeRender.cxx 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -I/C/MinGW/OpenCL/include -Imwc64x -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/volumeRender.o volumeRender.cxx

${OBJECTDIR}/MySPH_5.o: MySPH_5.cxx 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -I/C/MinGW/OpenCL/include -Imwc64x -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/MySPH_5.o MySPH_5.cxx

${OBJECTDIR}/temp.o: temp.cxx 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -I/C/MinGW/OpenCL/include -Imwc64x -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/temp.o temp.cxx

${OBJECTDIR}/MyRaycasting4.o: MyRaycasting4.cxx 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -I/C/MinGW/OpenCL/include -Imwc64x -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/MyRaycasting4.o MyRaycasting4.cxx

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/myopencl_files.exe

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
